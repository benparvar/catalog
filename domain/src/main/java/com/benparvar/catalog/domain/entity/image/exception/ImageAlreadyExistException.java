package com.benparvar.catalog.domain.entity.image.exception;

import com.benparvar.catalog.domain.entity.exception.AlreadyExistException;

public class ImageAlreadyExistException extends AlreadyExistException {
}
