package com.benparvar.catalog.domain.entity.unit;

import java.io.Serializable;
import java.util.Objects;

/**
 * The type Unit.
 */
public class Unit implements Serializable {
    private String code;
    private String name;

    private Unit(Builder builder) {
        code = builder.code;
        name = builder.name;
    }

    /**
     * New builder builder.
     *
     * @return the builder
     */
    public static Builder newBuilder() {
        return new Builder();
    }

    /**
     * Gets code.
     *
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * The type Builder.
     */
    public static final class Builder {
        private String code;
        private String name;

        private Builder() {
        }

        /**
         * Code builder.
         *
         * @param val the val
         * @return the builder
         */
        public Builder code(String val) {
            code = val;
            return this;
        }

        /**
         * Name builder.
         *
         * @param val the val
         * @return the builder
         */
        public Builder name(String val) {
            name = val;
            return this;
        }

        /**
         * Build unit.
         *
         * @return the unit
         */
        public Unit build() {
            return new Unit(this);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Unit unit = (Unit) o;
        return code.equals(unit.code) &&
                name.equals(unit.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(code, name);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Unit{");
        sb.append("code='").append(code).append('\'');
        sb.append(", name='").append(name).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
