package com.benparvar.catalog.domain.entity.image.exception;

import com.benparvar.catalog.domain.entity.exception.NotFoundException;

public class ImageNotFoundException extends NotFoundException {
}
