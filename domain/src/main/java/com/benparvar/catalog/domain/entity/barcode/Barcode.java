package com.benparvar.catalog.domain.entity.barcode;

import java.io.Serializable;
import java.util.Objects;

/**
 * The type Barcode.
 */
public class Barcode implements Serializable {
    private String code;
    private BarcodeType barcodeType;

    private Barcode(Builder builder) {
        code = builder.code;
        barcodeType = builder.barcodeType;
    }

    /**
     * Gets code.
     *
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * Gets barcode type.
     *
     * @return the barcode type
     */
    public BarcodeType getBarcodeType() {
        return barcodeType;
    }

    /**
     * New builder builder.
     *
     * @return the builder
     */
    public static Builder newBuilder() {
        return new Builder();
    }

    /**
     * The type Builder.
     */
    public static final class Builder {
        private String code;
        private BarcodeType barcodeType;

        private Builder() {
        }

        /**
         * Code builder.
         *
         * @param val the val
         * @return the builder
         */
        public Builder code(String val) {
            code = val;
            return this;
        }

        /**
         * Barcode type builder.
         *
         * @param val the val
         * @return the builder
         */
        public Builder barcodeType(BarcodeType val) {
            barcodeType = val;
            return this;
        }

        /**
         * Build barcode.
         *
         * @return the barcode
         */
        public Barcode build() {
            return new Barcode(this);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Barcode barcode = (Barcode) o;
        return code.equals(barcode.code) &&
                barcodeType == barcode.barcodeType;
    }

    @Override
    public int hashCode() {
        return Objects.hash(code, barcodeType);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Barcode{");
        sb.append("code='").append(code).append('\'');
        sb.append(", barcodeType=").append(barcodeType);
        sb.append('}');
        return sb.toString();
    }
}
