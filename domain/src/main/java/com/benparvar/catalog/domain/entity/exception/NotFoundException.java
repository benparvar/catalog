package com.benparvar.catalog.domain.entity.exception;

/**
 * The type Product not found exception.
 */
public class NotFoundException extends RuntimeException {
}
