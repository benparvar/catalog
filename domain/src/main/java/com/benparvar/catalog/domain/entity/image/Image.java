package com.benparvar.catalog.domain.entity.image;

import java.io.Serializable;
import java.util.Objects;

/**
 * The type Image.
 */
public class Image implements Serializable {
    private String description;
    private String url;

    private Image(Builder builder) {
        description = builder.description;
        url = builder.url;
    }

    /**
     * New builder builder.
     *
     * @return the builder
     */
    public static Builder newBuilder() {
        return new Builder();
    }

    /**
     * Gets description.
     *
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Gets url.
     *
     * @return the url
     */
    public String getUrl() {
        return url;
    }

    /**
     * The type Builder.
     */
    public static final class Builder {
        private String description;
        private String url;

        private Builder() {
        }

        /**
         * Description builder.
         *
         * @param val the val
         * @return the builder
         */
        public Builder description(String val) {
            description = val;
            return this;
        }

        /**
         * Url builder.
         *
         * @param val the val
         * @return the builder
         */
        public Builder url(String val) {
            url = val;
            return this;
        }

        /**
         * Build image.
         *
         * @return the image
         */
        public Image build() {
            return new Image(this);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Image image = (Image) o;
        return description.equals(image.description) &&
                url.equals(image.url);
    }

    @Override
    public int hashCode() {
        return Objects.hash(description, url);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Image{");
        sb.append("description='").append(description).append('\'');
        sb.append(", url=").append(url);
        sb.append('}');
        return sb.toString();
    }
}
