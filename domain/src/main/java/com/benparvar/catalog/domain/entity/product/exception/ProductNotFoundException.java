package com.benparvar.catalog.domain.entity.product.exception;

import com.benparvar.catalog.domain.entity.exception.NotFoundException;

public class ProductNotFoundException extends NotFoundException {
}
