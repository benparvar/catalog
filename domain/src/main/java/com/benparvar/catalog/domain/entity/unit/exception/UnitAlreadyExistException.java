package com.benparvar.catalog.domain.entity.unit.exception;

import com.benparvar.catalog.domain.entity.exception.AlreadyExistException;

public class UnitAlreadyExistException extends AlreadyExistException {
}
