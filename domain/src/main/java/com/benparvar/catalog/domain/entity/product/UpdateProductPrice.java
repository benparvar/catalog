package com.benparvar.catalog.domain.entity.product;

import java.math.BigDecimal;
import java.util.Objects;

/**
 * The type Update product price.
 */
public class UpdateProductPrice {
    private String barcode;
    private String storeCode;
    private BigDecimal price;

    private UpdateProductPrice(Builder builder) {
        barcode = builder.barcode;
        setStoreCode(builder.storeCode);
        price = builder.price;
    }

    /**
     * New builder builder.
     *
     * @return the builder
     */
    public static Builder newBuilder() {
        return new Builder();
    }

    /**
     * Gets price.
     *
     * @return the price
     */
    public BigDecimal getPrice() {
        return price;
    }

    /**
     * Gets barcode.
     *
     * @return the barcode
     */
    public String getBarcode() {
        return barcode;
    }

    /**
     * Gets store code.
     *
     * @return the store code
     */
    public String getStoreCode() {
        return storeCode;
    }


    /**
     * Sets store code.
     *
     * @param storeCode the store code
     */
    public void setStoreCode(String storeCode) {
        this.storeCode = storeCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UpdateProductPrice that = (UpdateProductPrice) o;
        return barcode.equals(that.barcode) &&
                storeCode.equals(that.storeCode) &&
                price.equals(that.price);
    }

    @Override
    public int hashCode() {
        return Objects.hash(barcode, storeCode, price);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("UpdateProductPrice{");
        sb.append("barcode='").append(barcode).append('\'');
        sb.append(", storeCode='").append(storeCode).append('\'');
        sb.append(", price=").append(price);
        sb.append('}');
        return sb.toString();
    }

    /**
     * The type Builder.
     */
    public static final class Builder {
        private String barcode;
        private String storeCode;
        private BigDecimal price;

        private Builder() {
        }

        /**
         * Barcode builder.
         *
         * @param val the val
         * @return the builder
         */
        public Builder barcode(String val) {
            barcode = val;
            return this;
        }

        /**
         * Store code builder.
         *
         * @param val the val
         * @return the builder
         */
        public Builder storeCode(String val) {
            storeCode = val;
            return this;
        }

        /**
         * Price builder.
         *
         * @param val the val
         * @return the builder
         */
        public Builder price(BigDecimal val) {
            price = val;
            return this;
        }

        /**
         * Build update price product.
         *
         * @return the update price product
         */
        public UpdateProductPrice build() {
            return new UpdateProductPrice(this);
        }
    }
}
