package com.benparvar.catalog.domain.entity.barcode.exception;

import com.benparvar.catalog.domain.entity.exception.AlreadyExistException;

public class BarcodeAlreadyExistException extends AlreadyExistException {
}
