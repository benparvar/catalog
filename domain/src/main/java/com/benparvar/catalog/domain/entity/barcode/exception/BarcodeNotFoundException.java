package com.benparvar.catalog.domain.entity.barcode.exception;

import com.benparvar.catalog.domain.entity.exception.NotFoundException;

public class BarcodeNotFoundException extends NotFoundException {
}
