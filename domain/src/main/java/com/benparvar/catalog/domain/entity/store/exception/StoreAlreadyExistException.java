package com.benparvar.catalog.domain.entity.store.exception;

import com.benparvar.catalog.domain.entity.exception.AlreadyExistException;

public class StoreAlreadyExistException extends AlreadyExistException {
}
