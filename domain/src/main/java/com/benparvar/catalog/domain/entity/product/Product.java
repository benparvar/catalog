package com.benparvar.catalog.domain.entity.product;

import com.benparvar.catalog.domain.entity.barcode.Barcode;
import com.benparvar.catalog.domain.entity.image.Image;
import com.benparvar.catalog.domain.entity.store.Store;
import com.benparvar.catalog.domain.entity.unit.Unit;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;

/**
 * The type Product.
 */
public class Product implements Serializable {
    private String code;
    private String name;
    private String description;
    private Barcode barcode;
    private Unit unit;
    private BigDecimal price;
    private Store store;
    private List<Image> images;

    private Product(Builder builder) {
        code = builder.code;
        name = builder.name;
        description = builder.description;
        barcode = builder.barcode;
        unit = builder.unit;
        price = builder.price;
        store = builder.store;
        images = builder.images;
    }

    /**
     * New builder builder.
     *
     * @return the builder
     */
    public static Builder newBuilder() {
        return new Builder();
    }

    /**
     * Gets code.
     *
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Gets description.
     *
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Gets barcode.
     *
     * @return the barcode
     */
    public Barcode getBarcode() {
        return barcode;
    }

    /**
     * Gets unit.
     *
     * @return the unit
     */
    public Unit getUnit() {
        return unit;
    }

    /**
     * Gets price.
     *
     * @return the price
     */
    public BigDecimal getPrice() {
        return price;
    }

    /**
     * Gets store.
     *
     * @return the store
     */
    public Store getStore() {
        return store;
    }

    /**
     * Gets images.
     *
     * @return the images
     */
    public List<Image> getImages() {
        return images;
    }

    /**
     * The type Builder.
     */
    public static final class Builder {
        private String code;
        private String name;
        private String description;
        private Barcode barcode;
        private Unit unit;
        private BigDecimal price;
        private Store store;
        private List<Image> images;

        private Builder() {
        }

        /**
         * Code builder.
         *
         * @param val the val
         * @return the builder
         */
        public Builder code(String val) {
            code = val;
            return this;
        }

        /**
         * Name builder.
         *
         * @param val the val
         * @return the builder
         */
        public Builder name(String val) {
            name = val;
            return this;
        }

        /**
         * Description builder.
         *
         * @param val the val
         * @return the builder
         */
        public Builder description(String val) {
            description = val;
            return this;
        }

        /**
         * Barcode builder.
         *
         * @param val the val
         * @return the builder
         */
        public Builder barcode(Barcode val) {
            barcode = val;
            return this;
        }

        /**
         * Unit builder.
         *
         * @param val the val
         * @return the builder
         */
        public Builder unit(Unit val) {
            unit = val;
            return this;
        }

        /**
         * Price builder.
         *
         * @param val the val
         * @return the builder
         */
        public Builder price(BigDecimal val) {
            price = val;
            return this;
        }

        /**
         * Store builder.
         *
         * @param val the val
         * @return the builder
         */
        public Builder store(Store val) {
            store = val;
            return this;
        }

        /**
         * Images builder.
         *
         * @param val the val
         * @return the builder
         */
        public Builder images(List<Image> val) {
            images = val;
            return this;
        }

        /**
         * Build product.
         *
         * @return the product
         */
        public Product build() {
            return new Product(this);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return code.equals(product.code) &&
                name.equals(product.name) &&
                description.equals(product.description) &&
                barcode.equals(product.barcode) &&
                unit.equals(product.unit) &&
                price.equals(product.price) &&
                store.equals(product.store) &&
                images.equals(product.images);
    }

    @Override
    public int hashCode() {
        return Objects.hash(code, name, description, barcode, unit, price, store, images);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Product{");
        sb.append("code='").append(code).append('\'');
        sb.append(", name='").append(name).append('\'');
        sb.append(", description='").append(description).append('\'');
        sb.append(", barcode=").append(barcode);
        sb.append(", unit=").append(unit);
        sb.append(", price=").append(price);
        sb.append(", store=").append(store);
        sb.append(", images=").append(images);
        sb.append('}');
        return sb.toString();
    }
}
