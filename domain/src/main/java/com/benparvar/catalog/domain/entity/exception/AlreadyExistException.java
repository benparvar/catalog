package com.benparvar.catalog.domain.entity.exception;

/**
 * The type Product already exist exception.
 */
public class AlreadyExistException extends RuntimeException {
}
