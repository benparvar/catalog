package com.benparvar.catalog.domain.entity.store.exception;

import com.benparvar.catalog.domain.entity.exception.NotFoundException;

public class StoreNotFoundException extends NotFoundException {
}
