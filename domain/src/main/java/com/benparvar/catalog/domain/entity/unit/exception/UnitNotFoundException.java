package com.benparvar.catalog.domain.entity.unit.exception;

import com.benparvar.catalog.domain.entity.exception.NotFoundException;

/**
 * The type Unit not found exception.
 */
public class UnitNotFoundException extends NotFoundException {
}
