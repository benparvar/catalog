package com.benparvar.catalog.domain.entity.product.exception;

import com.benparvar.catalog.domain.entity.exception.AlreadyExistException;

public class ProductAlreadyExistException extends AlreadyExistException {
}
