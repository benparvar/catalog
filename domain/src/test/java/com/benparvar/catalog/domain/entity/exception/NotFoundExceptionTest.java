package com.benparvar.catalog.domain.entity.exception;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * The type Not found exception test.
 */
class NotFoundExceptionTest {

    /**
     * Check exception integrity.
     */
    @Test
    public void checkExceptionIntegrity() {
        assertThrows(NotFoundException.class, () -> {
            try {
                throw new NotFoundException();
            } catch (NotFoundException e) {
                throw e;
            }
        });
    }

}