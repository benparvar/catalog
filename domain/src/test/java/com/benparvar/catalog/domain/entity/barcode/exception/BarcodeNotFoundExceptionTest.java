package com.benparvar.catalog.domain.entity.barcode.exception;

import com.benparvar.catalog.domain.entity.exception.NotFoundException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * The type Barcode not found exception test.
 */
class BarcodeNotFoundExceptionTest {

    /**
     * Check exception main integrity.
     */
    @Test
    public void checkExceptionMainIntegrity() {
        assertThrows(NotFoundException.class, () -> {
            try {
                throw new BarcodeNotFoundException();
            } catch (NotFoundException e) {
                throw e;
            }
        });
    }

    /**
     * Check exception integrity.
     */
    @Test
    public void checkExceptionIntegrity() {
        assertThrows(BarcodeNotFoundException.class, () -> {
            try {
                throw new BarcodeNotFoundException();
            } catch (BarcodeNotFoundException e) {
                throw e;
            }
        });
    }

}