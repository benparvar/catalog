package com.benparvar.catalog.domain.entity.image.exception;

import com.benparvar.catalog.domain.entity.barcode.exception.BarcodeAlreadyExistException;
import com.benparvar.catalog.domain.entity.exception.AlreadyExistException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * The type Image already exist exception test.
 */
class ImageAlreadyExistExceptionTest {
    /**
     * Check exception main integrity.
     */
    @Test
    public void checkExceptionMainIntegrity() {
        assertThrows(AlreadyExistException.class, () -> {
            try {
                throw new ImageAlreadyExistException();
            } catch (AlreadyExistException e) {
                throw e;
            }
        });
    }

    /**
     * Check exception integrity.
     */
    @Test
    public void checkExceptionIntegrity() {
        assertThrows(ImageAlreadyExistException.class, () -> {
            try {
                throw new ImageAlreadyExistException();
            } catch (ImageAlreadyExistException e) {
                throw e;
            }
        });
    }
}