package com.benparvar.catalog.domain.entity.store;

import com.benparvar.catalog.domain.entity.unit.Unit;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * The type Store test.
 */
class StoreTest {

    /**
     * Check hash code integrity.
     */
    @Test
    public void checkHashCodeIntegrity() {
        Store store1 = Store.newBuilder().code("001").name("get out of me").build();
        Store store2 = Store.newBuilder().code("001").name("get out of me").build();

        assertEquals(store1.hashCode(), store2.hashCode());
    }

    /**
     * Check equals integrity.
     */
    @Test
    public void checkEqualsIntegrity() {
        Store store1 = Store.newBuilder().code("001").name("get out of me").build();
        Store store2 = Store.newBuilder().code("001").name("get out of me").build();

        assertEquals(store1, store2);
    }

    /**
     * Check builder integrity.
     */
    @Test
    public void checkBuilderIntegrity() {
        Store store = Store.newBuilder().code("001").name("get out of me").build();

        assertNotNull(store);
        assertEquals("001", store.getCode());
        assertEquals("get out of me", store.getName());
    }

    /**
     * Check to string integrity.
     */
    @Test
    public void checkToStringIntegrity() {
        Store store = Store.newBuilder().code("001").name("get out of me").build();

        assertNotNull(store);
        assertEquals("Store{code='001', name='get out of me'}", store.toString());
    }
}