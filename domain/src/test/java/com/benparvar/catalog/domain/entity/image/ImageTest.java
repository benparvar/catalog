package com.benparvar.catalog.domain.entity.image;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * The type Image test.
 */
class ImageTest {
    /**
     * Check hash code integrity.
     */
    @Test
    public void checkHashCodeIntegrity() {
        Image image1 = Image.newBuilder().url("http://www.benparvar.com/image1.png").description("thumb image").build();
        Image image2 = Image.newBuilder().url("http://www.benparvar.com/image1.png").description("thumb image").build();

        assertEquals(image1.hashCode(), image2.hashCode());
    }

    /**
     * Check equals integrity.
     */
    @Test
    public void checkEqualsIntegrity() {
        Image image1 = Image.newBuilder().url("http://www.benparvar.com/image1.png").description("thumb image").build();
        Image image2 = Image.newBuilder().url("http://www.benparvar.com/image1.png").description("thumb image").build();

        assertEquals(image1, image2);
    }

    /**
     * Check builder integrity.
     */
    @Test
    public void checkBuilderIntegrity() {
        Image image = Image.newBuilder().url("http://www.benparvar.com/image1.png").description("thumb image").build();

        assertNotNull(image);
        assertEquals("thumb image", image.getDescription());
        assertEquals("http://www.benparvar.com/image1.png", image.getUrl());
    }

    /**
     * Check to string integrity.
     */
    @Test
    public void checkToStringIntegrity() {
        Image image = Image.newBuilder().url("http://www.benparvar.com/image1.png").description("thumb image").build();

        assertNotNull(image);
        assertEquals("Image{description='thumb image', url=http://www.benparvar.com/image1.png}", image.toString());
    }
}