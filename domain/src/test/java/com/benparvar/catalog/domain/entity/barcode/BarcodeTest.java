package com.benparvar.catalog.domain.entity.barcode;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * The type Barcode test.
 */
class BarcodeTest {
    /**
     * Check hash code integrity.
     */
    @Test
    public void checkHashCodeIntegrity() {
        Barcode barcode1 = Barcode.newBuilder().code("978020137962").barcodeType(BarcodeType.EAN_13).build();
        Barcode barcode2 = Barcode.newBuilder().code("978020137962").barcodeType(BarcodeType.EAN_13).build();

        assertEquals(barcode1.hashCode(), barcode2.hashCode());
    }

    /**
     * Check equals integrity.
     */
    @Test
    public void checkEqualsIntegrity() {
        Barcode barcode1 = Barcode.newBuilder().code("978020137962").barcodeType(BarcodeType.EAN_13).build();
        Barcode barcode2 = Barcode.newBuilder().code("978020137962").barcodeType(BarcodeType.EAN_13).build();

        assertEquals(barcode1, barcode2);
    }

    /**
     * Check builder integrity.
     */
    @Test
    public void checkBuilderIntegrity() {
        Barcode barcode = Barcode.newBuilder().code("978020137962").barcodeType(BarcodeType.EAN_13).build();

        assertNotNull(barcode);
        assertEquals("978020137962", barcode.getCode());
        assertEquals(BarcodeType.EAN_13, barcode.getBarcodeType());
    }

    /**
     * Check to string integrity.
     */
    @Test
    public void checkToStringIntegrity() {
        Barcode barcode = Barcode.newBuilder().code("978020137962").barcodeType(BarcodeType.EAN_13).build();

        assertNotNull(barcode);
        assertEquals("Barcode{code='978020137962', barcodeType=EAN_13}", barcode.toString());
    }
}