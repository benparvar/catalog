package com.benparvar.catalog.domain.entity.barcode.exception;

import com.benparvar.catalog.domain.entity.exception.AlreadyExistException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * The type Barcode already exist exception test.
 */
class BarcodeAlreadyExistExceptionTest {

    /**
     * Check exception main integrity.
     */
    @Test
    public void checkExceptionMainIntegrity() {
        assertThrows(AlreadyExistException.class, () -> {
            try {
                throw new BarcodeAlreadyExistException();
            } catch (AlreadyExistException e) {
                throw e;
            }
        });
    }

    /**
     * Check exception integrity.
     */
    @Test
    public void checkExceptionIntegrity() {
        assertThrows(BarcodeAlreadyExistException.class, () -> {
            try {
                throw new BarcodeAlreadyExistException();
            } catch (BarcodeAlreadyExistException e) {
                throw e;
            }
        });
    }

}