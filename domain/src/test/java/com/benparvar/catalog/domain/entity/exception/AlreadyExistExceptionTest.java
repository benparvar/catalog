package com.benparvar.catalog.domain.entity.exception;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * The type Already exist exception test.
 */
class AlreadyExistExceptionTest {

    /**
     * Check exception integrity.
     */
    @Test
    public void checkExceptionIntegrity() {
        assertThrows(AlreadyExistException.class, () -> {
            try {
                throw new AlreadyExistException();
            } catch (AlreadyExistException e) {
                throw e;
            }
        });
    }

}