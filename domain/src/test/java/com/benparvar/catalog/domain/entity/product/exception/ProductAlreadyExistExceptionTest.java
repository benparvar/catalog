package com.benparvar.catalog.domain.entity.product.exception;

import com.benparvar.catalog.domain.entity.exception.AlreadyExistException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * The type Product already exist exception test.
 */
class ProductAlreadyExistExceptionTest {
    /**
     * Check exception main integrity.
     */
    @Test
    public void checkExceptionMainIntegrity() {
        assertThrows(AlreadyExistException.class, () -> {
            try {
                throw new ProductAlreadyExistException();
            } catch (AlreadyExistException e) {
                throw e;
            }
        });
    }

    /**
     * Check exception integrity.
     */
    @Test
    public void checkExceptionIntegrity() {
        assertThrows(ProductAlreadyExistException.class, () -> {
            try {
                throw new ProductAlreadyExistException();
            } catch (ProductAlreadyExistException e) {
                throw e;
            }
        });
    }

}