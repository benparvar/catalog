package com.benparvar.catalog.domain.entity.image.exception;

import com.benparvar.catalog.domain.entity.exception.NotFoundException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * The type Image not found exception test.
 */
class ImageNotFoundExceptionTest {
    /**
     * Check exception main integrity.
     */
    @Test
    public void checkExceptionMainIntegrity() {
        assertThrows(NotFoundException.class, () -> {
            try {
                throw new ImageNotFoundException();
            } catch (NotFoundException e) {
                throw e;
            }
        });
    }

    /**
     * Check exception integrity.
     */
    @Test
    public void checkExceptionIntegrity() {
        assertThrows(ImageNotFoundException.class, () -> {
            try {
                throw new ImageNotFoundException();
            } catch (ImageNotFoundException e) {
                throw e;
            }
        });
    }
}