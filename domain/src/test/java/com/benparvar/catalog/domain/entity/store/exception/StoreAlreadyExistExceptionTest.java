package com.benparvar.catalog.domain.entity.store.exception;

import com.benparvar.catalog.domain.entity.exception.AlreadyExistException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * The type Store already exist exception test.
 */
class StoreAlreadyExistExceptionTest {
    /**
     * Check exception main integrity.
     */
    @Test
    public void checkExceptionMainIntegrity() {
        assertThrows(AlreadyExistException.class, () -> {
            try {
                throw new StoreAlreadyExistException();
            } catch (AlreadyExistException e) {
                throw e;
            }
        });
    }

    /**
     * Check exception integrity.
     */
    @Test
    public void checkExceptionIntegrity() {
        assertThrows(StoreAlreadyExistException.class, () -> {
            try {
                throw new StoreAlreadyExistException();
            } catch (StoreAlreadyExistException e) {
                throw e;
            }
        });
    }
}