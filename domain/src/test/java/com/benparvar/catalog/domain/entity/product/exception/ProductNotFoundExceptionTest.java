package com.benparvar.catalog.domain.entity.product.exception;

import com.benparvar.catalog.domain.entity.exception.NotFoundException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * The type Product not found exception test.
 */
class ProductNotFoundExceptionTest {
    /**
     * Check exception main integrity.
     */
    @Test
    public void checkExceptionMainIntegrity() {
        assertThrows(NotFoundException.class, () -> {
            try {
                throw new ProductNotFoundException();
            } catch (NotFoundException e) {
                throw e;
            }
        });
    }

    /**
     * Check exception integrity.
     */
    @Test
    public void checkExceptionIntegrity() {
        assertThrows(ProductNotFoundException.class, () -> {
            try {
                throw new ProductNotFoundException();
            } catch (ProductNotFoundException e) {
                throw e;
            }
        });
    }

}