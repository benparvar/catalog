package com.benparvar.catalog.domain.entity.product;

import com.benparvar.catalog.domain.entity.barcode.Barcode;
import com.benparvar.catalog.domain.entity.barcode.BarcodeType;
import com.benparvar.catalog.domain.entity.image.Image;
import com.benparvar.catalog.domain.entity.store.Store;
import com.benparvar.catalog.domain.entity.unit.Unit;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * The type Product test.
 */
class ProductTest {
    /**
     * Check hash code integrity.
     */
    @Test
    public void checkHashCodeIntegrity() {
        Product product1 = Product.newBuilder()
                .code("123")
                .barcode(Barcode.newBuilder()
                        .code("7897015222007")
                        .barcodeType(BarcodeType.EAN_13).build())
                .description("Merlot Dedicato tinto 750ml")
                .images(Arrays.asList(Image.newBuilder()
                        .description("thumb")
                        .url("http://www.montepaschoal.com.br/produtos/vinhos-dedicato/merlot-dedicato-750ml").build()))
                .name("Merlot 750ml")
                .price(BigDecimal.ONE)
                .store(Store.newBuilder()
                        .code("001")
                        .name("Matrix").build())
                .unit(Unit.newBuilder()
                        .code("PC")
                        .name("PIECE").build())
                .build();
        Product product2 = Product.newBuilder()
                .code("123")
                .barcode(Barcode.newBuilder()
                        .code("7897015222007")
                        .barcodeType(BarcodeType.EAN_13).build())
                .description("Merlot Dedicato tinto 750ml")
                .images(Arrays.asList(Image.newBuilder()
                        .description("thumb")
                        .url("http://www.montepaschoal.com.br/produtos/vinhos-dedicato/merlot-dedicato-750ml").build()))
                .name("Merlot 750ml")
                .price(BigDecimal.ONE)
                .store(Store.newBuilder()
                        .code("001")
                        .name("Matrix").build())
                .unit(Unit.newBuilder()
                        .code("PC")
                        .name("PIECE").build())
                .build();

        assertEquals(product1.hashCode(), product2.hashCode());
    }

    /**
     * Check equals integrity.
     */
    @Test
    public void checkEqualsIntegrity() {
        Product product1 = Product.newBuilder()
                .code("123")
                .barcode(Barcode.newBuilder()
                        .code("7897015222007")
                        .barcodeType(BarcodeType.EAN_13).build())
                .description("Merlot Dedicato tinto 750ml")
                .images(Arrays.asList(Image.newBuilder()
                        .description("thumb")
                        .url("http://www.montepaschoal.com.br/produtos/vinhos-dedicato/merlot-dedicato-750ml").build()))
                .name("Merlot 750ml")
                .price(BigDecimal.ONE)
                .store(Store.newBuilder()
                        .code("001")
                        .name("Matrix").build())
                .unit(Unit.newBuilder()
                        .code("PC")
                        .name("PIECE").build())
                .build();
        Product product2 = Product.newBuilder()
                .code("123")
                .barcode(Barcode.newBuilder()
                        .code("7897015222007")
                        .barcodeType(BarcodeType.EAN_13).build())
                .description("Merlot Dedicato tinto 750ml")
                .images(Arrays.asList(Image.newBuilder()
                        .description("thumb")
                        .url("http://www.montepaschoal.com.br/produtos/vinhos-dedicato/merlot-dedicato-750ml").build()))
                .name("Merlot 750ml")
                .price(BigDecimal.ONE)
                .store(Store.newBuilder()
                        .code("001")
                        .name("Matrix").build())
                .unit(Unit.newBuilder()
                        .code("PC")
                        .name("PIECE").build())
                .build();

        assertEquals(product1, product2);
    }

    /**
     * Check builder integrity.
     */
    @Test
    public void checkBuilderIntegrity() {
        Product product = Product.newBuilder()
                .code("123")
                .barcode(Barcode.newBuilder()
                        .code("7897015222007")
                        .barcodeType(BarcodeType.EAN_13).build())
                .description("Merlot Dedicato tinto 750ml")
                .images(Arrays.asList(Image.newBuilder()
                        .description("thumb")
                        .url("http://www.montepaschoal.com.br/produtos/vinhos-dedicato/merlot-dedicato-750ml").build()))
                .name("Merlot 750ml")
                .price(BigDecimal.ONE)
                .store(Store.newBuilder()
                        .code("001")
                        .name("Matrix").build())
                .unit(Unit.newBuilder()
                        .code("PC")
                        .name("PIECE").build())
                .build();

        assertNotNull(product);
        assertEquals("123", product.getCode());
        assertNotNull(product.getBarcode());
        assertEquals("7897015222007", product.getBarcode().getCode());
        assertEquals(BarcodeType.EAN_13, product.getBarcode().getBarcodeType());
        assertEquals("Merlot Dedicato tinto 750ml", product.getDescription());
        assertNotNull(product.getImages());
        assertFalse(product.getImages().isEmpty());
        assertEquals(1, product.getImages().size());
        assertEquals("thumb", product.getImages().get(0).getDescription());
        assertEquals("http://www.montepaschoal.com.br/produtos/vinhos-dedicato/merlot-dedicato-750ml",
                product.getImages().get(0).getUrl());
        assertEquals("Merlot 750ml", product.getName());
        assertEquals(BigDecimal.ONE, product.getPrice());
        assertNotNull(product.getStore());
        assertEquals("001", product.getStore().getCode());
        assertEquals("Matrix", product.getStore().getName());
        assertNotNull(product.getUnit());
        assertEquals("PC", product.getUnit().getCode());
        assertEquals("PIECE", product.getUnit().getName());
    }

    /**
     * Check to string integrity.
     */
    @Test
    public void checkToStringIntegrity() {
        Product product = Product.newBuilder()
                .code("123")
                .barcode(Barcode.newBuilder()
                        .code("7897015222007")
                        .barcodeType(BarcodeType.EAN_13).build())
                .description("Merlot Dedicato tinto 750ml")
                .images(Arrays.asList(Image.newBuilder()
                        .description("thumb")
                        .url("http://www.montepaschoal.com.br/produtos/vinhos-dedicato/merlot-dedicato-750ml").build()))
                .name("Merlot 750ml")
                .price(BigDecimal.ONE)
                .store(Store.newBuilder()
                        .code("001")
                        .name("Matrix").build())
                .unit(Unit.newBuilder()
                        .code("PC")
                        .name("PIECE").build())
                .build();

        assertNotNull(product);
        assertEquals("Product{code='123', name='Merlot 750ml', description='Merlot Dedicato tinto 750ml', barcode=Barcode{code='7897015222007', barcodeType=EAN_13}, unit=Unit{code='PC', name='PIECE'}, price=1, store=Store{code='001', name='Matrix'}, images=[Image{description='thumb', url=http://www.montepaschoal.com.br/produtos/vinhos-dedicato/merlot-dedicato-750ml}]}", product.toString());
    }
}