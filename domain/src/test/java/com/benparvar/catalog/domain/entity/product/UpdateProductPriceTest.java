package com.benparvar.catalog.domain.entity.product;

import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.*;

/**
 * The type Update product price test.
 */
class UpdateProductPriceTest {
    /**
     * Check hash code integrity.
     */
    @Test
    public void checkHashCodeIntegrity() {
        UpdateProductPrice update1 = UpdateProductPrice.newBuilder()
                .barcode("049000006841")
                .storeCode("12345")
                .price(BigDecimal.TEN)
                .build();

        UpdateProductPrice update2 = UpdateProductPrice.newBuilder()
                .barcode("049000006841")
                .storeCode("12345")
                .price(BigDecimal.TEN)
                .build();

        assertEquals(update1.hashCode(), update2.hashCode());
    }

    /**
     * Check equals integrity.
     */
    @Test
    public void checkEqualsIntegrity() {
        UpdateProductPrice update1 = UpdateProductPrice.newBuilder()
                .barcode("049000006841")
                .storeCode("12345")
                .price(BigDecimal.TEN)
                .build();

        UpdateProductPrice update2 = UpdateProductPrice.newBuilder()
                .barcode("049000006841")
                .storeCode("12345")
                .price(BigDecimal.TEN)
                .build();

        assertEquals(update1, update2);
    }

    /**
     * Check builder integrity.
     */
    @Test
    public void checkBuilderIntegrity() {
        UpdateProductPrice update = UpdateProductPrice.newBuilder()
                .barcode("049000006841")
                .storeCode("12345")
                .price(BigDecimal.TEN)
                .build();

        assertNotNull(update);
        assertEquals("049000006841", update.getBarcode());
        assertEquals("12345", update.getStoreCode());
        assertEquals(BigDecimal.TEN, update.getPrice());
    }

    /**
     * Check to string integrity.
     */
    @Test
    public void checkToStringIntegrity() {
        UpdateProductPrice update = UpdateProductPrice.newBuilder()
                .barcode("049000006841")
                .storeCode("12345")
                .price(BigDecimal.TEN)
                .build();

        assertNotNull(update);
        assertEquals("UpdateProductPrice{barcode='049000006841', storeCode='12345', price=10}", update.toString());
    }
}