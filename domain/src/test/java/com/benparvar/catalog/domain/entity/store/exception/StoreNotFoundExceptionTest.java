package com.benparvar.catalog.domain.entity.store.exception;

import com.benparvar.catalog.domain.entity.exception.NotFoundException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * The type Store not found exception test.
 */
class StoreNotFoundExceptionTest {
    /**
     * Check exception main integrity.
     */
    @Test
    public void checkExceptionMainIntegrity() {
        assertThrows(NotFoundException.class, () -> {
            try {
                throw new StoreNotFoundException();
            } catch (NotFoundException e) {
                throw e;
            }
        });
    }

    /**
     * Check exception integrity.
     */
    @Test
    public void checkExceptionIntegrity() {
        assertThrows(StoreNotFoundException.class, () -> {
            try {
                throw new StoreNotFoundException();
            } catch (StoreNotFoundException e) {
                throw e;
            }
        });
    }
}