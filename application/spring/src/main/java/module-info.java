module com.benparvar.catalog.spring {
    requires com.benparvar.catalog.usecase;
    requires com.benparvar.catalog.controller;
    requires com.benparvar.catalog.gateway;
    requires com.benparvar.catalog.repository;
    requires com.benparvar.catalog.domain;
    requires spring.web;
    requires spring.beans;

    requires spring.boot;
    requires spring.boot.autoconfigure;
    requires spring.context;
    requires com.fasterxml.jackson.databind;
    requires jackson.annotations;
    requires org.slf4j;
    requires spring.data.jpa;

    exports com.benparvar.catalog.spring.controller.store;
    exports com.benparvar.catalog.spring.config.store;

    exports com.benparvar.catalog.spring.controller.unit;
    exports com.benparvar.catalog.spring.config.unit;

    opens com.benparvar.catalog.spring.config.store to spring.core;
    opens com.benparvar.catalog.spring.config.unit to spring.core;
}