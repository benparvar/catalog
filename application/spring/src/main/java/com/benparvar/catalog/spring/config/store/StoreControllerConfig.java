package com.benparvar.catalog.spring.config.store;

import com.benparvar.catalog.controller.store.StoreController;
import com.benparvar.catalog.controller.store.model.create.CreateStoreMapper;
import com.benparvar.catalog.controller.store.model.delete.DeleteStoreMapper;
import com.benparvar.catalog.controller.store.model.find.FindStoreMapper;
import com.benparvar.catalog.controller.store.model.update.UpdateStoreMapper;
import com.benparvar.catalog.usecase.store.create.CreateStoreUseCase;
import com.benparvar.catalog.usecase.store.delete.DeleteStoreUseCase;
import com.benparvar.catalog.usecase.store.find.all.FindAllStoresUseCase;
import com.benparvar.catalog.usecase.store.find.code.FindStoreByCodeUseCase;
import com.benparvar.catalog.usecase.store.update.UpdateStoreUseCase;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * The type Store controller config.
 */
@Configuration
public class StoreControllerConfig {

    /**
     * Gets store controller.
     *
     * @param createStoreUseCase     the create store use case
     * @param createStoreMapper      the create store mapper
     * @param findAllStoresUseCase   the find all stores use case
     * @param findStoreMapper        the find store mapper
     * @param findStoreByCodeUseCase the find store by code use case
     * @param updateStoreUseCase     the update store use case
     * @param updateStoreMapper      the update store mapper
     * @param deleteStoreUseCase     the delete store use case
     * @param deleteStoreMapper      the delete store mapper
     * @return the store controller
     */
    @Bean
    public StoreController getStoreController(CreateStoreUseCase createStoreUseCase, CreateStoreMapper createStoreMapper,
                                              FindAllStoresUseCase findAllStoresUseCase, FindStoreMapper findStoreMapper,
                                              FindStoreByCodeUseCase findStoreByCodeUseCase, UpdateStoreUseCase updateStoreUseCase,
                                              UpdateStoreMapper updateStoreMapper, DeleteStoreUseCase deleteStoreUseCase,
                                              DeleteStoreMapper deleteStoreMapper) {
        return new StoreController(createStoreUseCase, createStoreMapper, findAllStoresUseCase, findStoreMapper,
                findStoreByCodeUseCase, updateStoreUseCase, updateStoreMapper, deleteStoreUseCase, deleteStoreMapper);
    }
}
