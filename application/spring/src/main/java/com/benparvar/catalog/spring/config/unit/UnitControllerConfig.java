package com.benparvar.catalog.spring.config.unit;


import com.benparvar.catalog.controller.unit.UnitController;
import com.benparvar.catalog.controller.unit.model.create.CreateUnitMapper;
import com.benparvar.catalog.controller.unit.model.delete.DeleteUnitMapper;
import com.benparvar.catalog.controller.unit.model.find.FindUnitMapper;
import com.benparvar.catalog.controller.unit.model.update.UpdateUnitMapper;
import com.benparvar.catalog.usecase.unit.create.CreateUnitUseCase;
import com.benparvar.catalog.usecase.unit.delete.DeleteUnitUseCase;
import com.benparvar.catalog.usecase.unit.find.all.FindAllUnitsUseCase;
import com.benparvar.catalog.usecase.unit.find.code.FindUnitByCodeUseCase;
import com.benparvar.catalog.usecase.unit.update.UpdateUnitUseCase;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * The type Unit controller config.
 */
@Configuration
public class UnitControllerConfig {


    /**
     * Gets unit controller.
     *
     * @param createUnitUseCase     the create unit use case
     * @param createUnitMapper      the create unit mapper
     * @param findAllUnitsUseCase   the find all units use case
     * @param findUnitMapper        the find unit mapper
     * @param findUnitByCodeUseCase the find unit by code use case
     * @param updateUnitUseCase     the update unit use case
     * @param updateUnitMapper      the update unit mapper
     * @param deleteUnitUseCase     the delete unit use case
     * @param deleteUnitMapper      the delete unit mapper
     * @return the unit controller
     */
    @Bean
    public UnitController getUnitController(CreateUnitUseCase createUnitUseCase, CreateUnitMapper createUnitMapper,
                                            FindAllUnitsUseCase findAllUnitsUseCase, FindUnitMapper findUnitMapper,
                                            FindUnitByCodeUseCase findUnitByCodeUseCase, UpdateUnitUseCase updateUnitUseCase,
                                            UpdateUnitMapper updateUnitMapper, DeleteUnitUseCase deleteUnitUseCase,
                                            DeleteUnitMapper deleteUnitMapper) {
        return new UnitController(createUnitUseCase, createUnitMapper, findAllUnitsUseCase, findUnitMapper,
                findUnitByCodeUseCase, updateUnitUseCase, updateUnitMapper, deleteUnitUseCase, deleteUnitMapper);
    }
}
