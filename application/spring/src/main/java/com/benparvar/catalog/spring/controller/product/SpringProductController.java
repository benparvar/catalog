package com.benparvar.catalog.spring.controller.product;

import com.benparvar.catalog.controller.product.ProductController;
import com.benparvar.catalog.controller.product.model.create.request.CreateProductRequest;
import com.benparvar.catalog.controller.product.model.create.request.UpdateProductPriceRequest;
import com.benparvar.catalog.controller.product.model.create.response.CreateProductResponse;
import com.benparvar.catalog.controller.product.model.find.response.FindProductResponse;
import com.benparvar.catalog.controller.product.model.update.response.UpdateProductResponse;
import com.benparvar.catalog.domain.entity.exception.AlreadyExistException;
import com.benparvar.catalog.domain.entity.exception.NotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;

/**
 * The type Spring product controller.
 */
@CrossOrigin
@RestController
@RequestMapping(value = "/api/v1/products", produces = MediaType.APPLICATION_JSON_VALUE)
public class SpringProductController {
    private final Logger log = LoggerFactory.getLogger(SpringProductController.class);
    @Autowired
    private final ProductController controller;

    /**
     * Instantiates a new Spring product controller.
     *
     * @param controller the controller
     */
    public SpringProductController(ProductController controller) {
        this.controller = controller;
    }

    /**
     * Create create product response.
     *
     * @param request the request
     * @return the create product response
     */
    @PostMapping(value = "", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public CreateProductResponse create(@RequestBody CreateProductRequest request) {
        log.info("create: {}", request);

        return controller.create(request);
    }

    /**
     * Find product by barcode and store code find product response.
     *
     * @param barcodeCode the barcode code
     * @param storeCode   the store code
     * @return the find product response
     */
    @GetMapping(value = "/{barcodeCode}/barcode/{storeCode}/store", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public FindProductResponse findProductByBarcodeAndStoreCode(@PathVariable String barcodeCode, @PathVariable String storeCode) {
        log.info("findProductByBarcodeAndStoreCode barcodeCode: {} storeCode: {}", barcodeCode, storeCode);

        return controller.findProductByBarcodeAndStoreCode(barcodeCode, storeCode);
    }

    /**
     * Update product price by barcode and store code update product response.
     *
     * @param request the request
     * @return the update product response
     */
    @PutMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public UpdateProductResponse updateProductPriceByBarcodeAndStoreCode(@RequestBody UpdateProductPriceRequest request) {
        log.info("updateProductPriceByBarcodeAndStoreCode request: {}", request);

        return controller.updateProductPriceByBarcodeAndStoreCode(request);
    }

    /**
     * Delete.
     *
     * @param barcodeCode the barcode code
     * @param storeCode   the store code
     */
    @DeleteMapping(value = "/{barcodeCode}/barcode/{storeCode}/store")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable String barcodeCode, @PathVariable String storeCode) {
        log.info("delete barcodeCode: {} storeCode: {}", barcodeCode, storeCode);

        controller.delete(barcodeCode, storeCode);
    }

    /**
     * Handle not found exception response entity.
     *
     * @param ex      the ex
     * @param request the request
     * @return the response entity
     */
    @ExceptionHandler({NotFoundException.class})
    public ResponseEntity<Object> handleNotFoundException(NotFoundException ex, WebRequest request) {
        log.error(ex.getMessage());

        return new ResponseEntity<>(
                "error: Sorry...", new HttpHeaders(), HttpStatus.NOT_FOUND);
    }

    /**
     * Handle already exist exception response entity.
     *
     * @param ex      the ex
     * @param request the request
     * @return the response entity
     */
    @ExceptionHandler({AlreadyExistException.class})
    public ResponseEntity<Object> handleAlreadyExistException(AlreadyExistException ex, WebRequest request) {
        log.error(ex.getMessage());

        return new ResponseEntity<>(
                "error: Already exist", new HttpHeaders(), HttpStatus.CONFLICT);
    }

    /**
     * Handle illegal argument exception response entity.
     *
     * @param ex      the ex
     * @param request the request
     * @return the response entity
     */
    @ExceptionHandler({IllegalArgumentException.class})
    public ResponseEntity<Object> handleIllegalArgumentException(IllegalArgumentException ex, WebRequest request) {
        log.error(ex.getMessage());
        return new ResponseEntity<>(
                "error: " + ex.getMessage(), new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }

    /**
     * Handle http message not readable exception response entity.
     *
     * @param ex      the ex
     * @param request the request
     * @return the response entity
     */
    @ExceptionHandler({HttpMessageNotReadableException.class})
    public ResponseEntity<Object> handleHttpMessageNotReadableException(HttpMessageNotReadableException ex,
                                                                        WebRequest request) {
        log.error(ex.getMessage());
        return new ResponseEntity<>(
                "error: " + ex.getMessage(), new HttpHeaders(), HttpStatus.UNPROCESSABLE_ENTITY);
    }
}
