package com.benparvar.catalog.spring.config.product;

import com.benparvar.catalog.controller.product.ProductController;
import com.benparvar.catalog.controller.product.model.create.mapper.CreateProductMapper;
import com.benparvar.catalog.controller.product.model.delete.DeleteProductMapper;
import com.benparvar.catalog.controller.product.model.find.mapper.FindProductMapper;
import com.benparvar.catalog.controller.product.model.update.mapper.UpdateProductMapper;
import com.benparvar.catalog.usecase.product.create.CreateProductUseCase;
import com.benparvar.catalog.usecase.product.delete.DeleteProductUseCase;
import com.benparvar.catalog.usecase.product.find.FindProductByBarcodeAndStoreCodeUseCase;
import com.benparvar.catalog.usecase.product.update.UpdateProductPriceByBarcodeAndStoreCodeCodeUseCase;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * The type Product controller config.
 */
@Configuration
public class ProductControllerConfig {

    /**
     * Gets product controller.
     *
     * @param createProductUseCase                               the create product use case
     * @param createProductMapper                                the create product mapper
     * @param findProductByBarcodeAndStoreCodeUseCase            the find product by barcode and store code use case
     * @param findProductMapper                                  the find product mapper
     * @param updateProductPriceByBarcodeAndStoreCodeCodeUseCase the update product price by barcode and store code code use case
     * @param updateProductMapper                                the update product mapper
     * @param deleteProductUseCase                               the delete product use case
     * @param deleteProductMapper                                the delete product mapper
     * @return the product controller
     */
    @Bean
    public ProductController getProductController(CreateProductUseCase createProductUseCase,
                                                  CreateProductMapper createProductMapper,
                                                  FindProductByBarcodeAndStoreCodeUseCase findProductByBarcodeAndStoreCodeUseCase,
                                                  FindProductMapper findProductMapper,
                                                  UpdateProductPriceByBarcodeAndStoreCodeCodeUseCase updateProductPriceByBarcodeAndStoreCodeCodeUseCase,
                                                  UpdateProductMapper updateProductMapper,
                                                  DeleteProductUseCase deleteProductUseCase,
                                                  DeleteProductMapper deleteProductMapper) {
        return new ProductController(createProductUseCase, createProductMapper, findProductByBarcodeAndStoreCodeUseCase,
                findProductMapper, updateProductPriceByBarcodeAndStoreCodeCodeUseCase, updateProductMapper,
                deleteProductUseCase, deleteProductMapper);
    }
}
