package com.benparvar.catalog.spring.config.image;

import com.benparvar.catalog.controller.product.model.create.mapper.CreateProductImageMapper;
import com.benparvar.catalog.controller.product.model.find.mapper.FindProductImageMapper;
import com.benparvar.catalog.controller.product.model.update.mapper.UpdateProductImageMapper;
import com.benparvar.catalog.repository.image.model.ImageModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * The type Image mapper config.
 */
@Configuration
public class ImageMapperConfig {

    /**
     * Gets image mapper.
     *
     * @return the image mapper
     */
    @Bean
    public CreateProductImageMapper getImageMapper() {
        return new CreateProductImageMapper();
    }

    /**
     * Gets image model mapper.
     *
     * @return the image model mapper
     */
    @Bean
    public ImageModelMapper getImageModelMapper() {
        return new ImageModelMapper();
    }

    /**
     * Gets find product image mapper.
     *
     * @return the find product image mapper
     */
    @Bean
    public FindProductImageMapper getFindProductImageMapper() {
        return new FindProductImageMapper();
    }

    /**
     * Gets update product image mapper.
     *
     * @return the update product image mapper
     */
    @Bean
    public UpdateProductImageMapper getUpdateProductImageMapper() {
        return new UpdateProductImageMapper();
    }
}
