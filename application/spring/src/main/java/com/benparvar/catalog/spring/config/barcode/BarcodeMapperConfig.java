package com.benparvar.catalog.spring.config.barcode;

import com.benparvar.catalog.controller.product.model.create.mapper.CreateProductBarcodeMapper;
import com.benparvar.catalog.controller.product.model.find.mapper.FindProductBarcodeMapper;
import com.benparvar.catalog.controller.product.model.update.mapper.UpdateProductBarcodeMapper;
import com.benparvar.catalog.repository.barcode.model.BarcodeModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * The type Barcode mapper config.
 */
@Configuration
public class BarcodeMapperConfig {

    /**
     * Gets barcode mapper.
     *
     * @return the barcode mapper
     */
    @Bean
    public CreateProductBarcodeMapper getBarcodeMapper() {
        return new CreateProductBarcodeMapper();
    }

    /**
     * Gets barcode model mapper barcode model mapper.
     *
     * @return the barcode model mapper
     */
    @Bean
    public BarcodeModelMapper getBarcodeModelMapper() {
        return new BarcodeModelMapper();
    }

    /**
     * Gets find product barcode mapper.
     *
     * @return the find product barcode mapper
     */
    @Bean
    public FindProductBarcodeMapper getFindProductBarcodeMapper() {
        return new FindProductBarcodeMapper();
    }

    /**
     * Gets update product barcode mapper.
     *
     * @return the update product barcode mapper
     */
    @Bean
    public UpdateProductBarcodeMapper getUpdateProductBarcodeMapper() {
        return new UpdateProductBarcodeMapper();
    }
}
