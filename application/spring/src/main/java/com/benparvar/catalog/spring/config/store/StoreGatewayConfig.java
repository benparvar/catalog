package com.benparvar.catalog.spring.config.store;

import com.benparvar.catalog.repository.store.StoreRepository;
import com.benparvar.catalog.repository.store.model.StoreModelMapper;
import com.benparvar.catalog.gateway.store.StoreGateway;
import com.benparvar.catalog.usecase.store.create.SaveStore;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * The type Store gateway config.
 */
@Configuration
public class StoreGatewayConfig {

    /**
     * Gets store unit.
     *
     * @param storeRepository    the store repository
     * @param storeModelMapper the store model mapper
     * @return the store unit
     */
    @Bean
    public SaveStore getSaveStore(StoreRepository storeRepository, StoreModelMapper storeModelMapper) {
        return new StoreGateway(storeRepository, storeModelMapper);
    }
}
