package com.benparvar.catalog.spring.config.product;

import com.benparvar.catalog.domain.entity.validator.checkdigit.BarcodeCheckDigit;
import com.benparvar.catalog.usecase.barcode.CheckBarcodeDigitUseCase;
import com.benparvar.catalog.usecase.product.create.CreateProductUseCase;
import com.benparvar.catalog.usecase.product.create.SaveProduct;
import com.benparvar.catalog.usecase.product.create.ValidateProductBeforeCreateUseCase;
import com.benparvar.catalog.usecase.product.delete.DeleteProduct;
import com.benparvar.catalog.usecase.product.delete.DeleteProductUseCase;
import com.benparvar.catalog.usecase.product.delete.ValidateProductBeforeDeleteUseCase;
import com.benparvar.catalog.usecase.product.find.FindProductByBarcodeAndStoreCode;
import com.benparvar.catalog.usecase.product.find.FindProductByBarcodeAndStoreCodeUseCase;
import com.benparvar.catalog.usecase.product.update.UpdateProductPriceByBarcodeAndStoreCodeCode;
import com.benparvar.catalog.usecase.product.update.UpdateProductPriceByBarcodeAndStoreCodeCodeUseCase;
import com.benparvar.catalog.usecase.product.update.ValidateBeforeUpdatePriceUseCase;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * The type Product use case config.
 */
@Configuration
public class ProductUseCaseConfig {

    /**
     * Gets barcode check digit.
     *
     * @return the barcode check digit
     */
    @Bean
    public BarcodeCheckDigit getBarcodeCheckDigit() {
        return new BarcodeCheckDigit();
    }

    /**
     * Gets check barcode digit use case.
     *
     * @param ean13CheckDigit the ean 13 check digit
     * @return the check barcode digit use case
     */
    @Bean
    public CheckBarcodeDigitUseCase getCheckBarcodeDigitUseCase(BarcodeCheckDigit ean13CheckDigit) {
        return new CheckBarcodeDigitUseCase(ean13CheckDigit);
    }

    /**
     * Gets validate product before create use case.
     *
     * @param checkBarcodeDigitUseCase the check barcode digit use case
     * @return the validate product before create use case
     */
    @Bean
    public ValidateProductBeforeCreateUseCase getValidateProductBeforeCreateUseCase(CheckBarcodeDigitUseCase checkBarcodeDigitUseCase) {
        return new ValidateProductBeforeCreateUseCase(checkBarcodeDigitUseCase);
    }

    /**
     * Gets create product use case.
     *
     * @param validateProductBeforeCreateUseCase the validate product before create use case
     * @param saveProductGateway                 the save product gateway
     * @return the create product use case
     */
    @Bean
    public CreateProductUseCase getCreateProductUseCase(ValidateProductBeforeCreateUseCase validateProductBeforeCreateUseCase,
                                                        SaveProduct saveProductGateway) {
        return new CreateProductUseCase(validateProductBeforeCreateUseCase, saveProductGateway);
    }

    /**
     * Get find product by barcode and store code use case find product by barcode and store code use case.
     *
     * @param findProductByBarcodeAndStoreCodeGateway the find product by barcode and store code gateway
     * @return the find product by barcode and store code use case
     */
    @Bean
    public FindProductByBarcodeAndStoreCodeUseCase getFindProductByBarcodeAndStoreCodeUseCase(
            FindProductByBarcodeAndStoreCode findProductByBarcodeAndStoreCodeGateway) {
        return new FindProductByBarcodeAndStoreCodeUseCase(findProductByBarcodeAndStoreCodeGateway);
    }

    /**
     * Gets update product price by barcode and store code code use case.
     *
     * @param validatePriceBeforeUpdateUseCase                   the validate price before update use case
     * @param updateProductPriceByBarcodeAndStoreCodeCodeGateway the update product price by barcode and store code code gateway
     * @return the update product price by barcode and store code code use case
     */
    @Bean
    public UpdateProductPriceByBarcodeAndStoreCodeCodeUseCase getUpdateProductPriceByBarcodeAndStoreCodeCodeUseCase(
            ValidateBeforeUpdatePriceUseCase validatePriceBeforeUpdateUseCase,
            UpdateProductPriceByBarcodeAndStoreCodeCode updateProductPriceByBarcodeAndStoreCodeCodeGateway) {
        return new UpdateProductPriceByBarcodeAndStoreCodeCodeUseCase(validatePriceBeforeUpdateUseCase,
                updateProductPriceByBarcodeAndStoreCodeCodeGateway);
    }

    /**
     * Gets validate price before update use case.
     *
     * @return the validate price before update use case
     */
    @Bean
    public ValidateBeforeUpdatePriceUseCase getValidatePriceBeforeUpdateUseCase() {
        return new ValidateBeforeUpdatePriceUseCase();
    }

    /**
     * Gets validate product before delete use case.
     *
     * @return the validate product before delete use case
     */
    @Bean
    public ValidateProductBeforeDeleteUseCase getValidateProductBeforeDeleteUseCase() {
        return new ValidateProductBeforeDeleteUseCase();
    }

    /**
     * Gets delete product use case.
     *
     * @param validateProductBeforeDeleteUseCase the validate product before delete use case
     * @param deleteProductGateway               the delete product gateway
     * @return the delete product use case
     */
    @Bean
    public DeleteProductUseCase getDeleteProductUseCase(ValidateProductBeforeDeleteUseCase validateProductBeforeDeleteUseCase,
                                                        DeleteProduct deleteProductGateway) {
        return new DeleteProductUseCase(validateProductBeforeDeleteUseCase, deleteProductGateway);
    }

}
