package com.benparvar.catalog.spring.config.store;

import com.benparvar.catalog.usecase.store.create.CreateStoreUseCase;
import com.benparvar.catalog.usecase.store.create.SaveStore;
import com.benparvar.catalog.usecase.store.create.ValidateStoreBeforeCreateUseCase;
import com.benparvar.catalog.usecase.store.delete.DeleteStore;
import com.benparvar.catalog.usecase.store.delete.DeleteStoreUseCase;
import com.benparvar.catalog.usecase.store.delete.ValidateStoreBeforeDeleteUseCase;
import com.benparvar.catalog.usecase.store.find.all.FindAllStores;
import com.benparvar.catalog.usecase.store.find.all.FindAllStoresUseCase;
import com.benparvar.catalog.usecase.store.find.code.FindStoreByCode;
import com.benparvar.catalog.usecase.store.find.code.FindStoreByCodeUseCase;
import com.benparvar.catalog.usecase.store.update.UpdateStore;
import com.benparvar.catalog.usecase.store.update.UpdateStoreUseCase;
import com.benparvar.catalog.usecase.store.update.ValidateStoreBeforeUpdateUseCase;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * The type Store use case config.
 */
@Configuration
public class StoreUseCaseConfig {

    /**
     * Gets validate store before create use case.
     *
     * @return the validate store before create use case
     */
    @Bean
    public ValidateStoreBeforeCreateUseCase getValidateStoreBeforeCreateUseCase() {
        return new ValidateStoreBeforeCreateUseCase();
    }

    /**
     * Gets create store use case.
     *
     * @param validateStoreBeforeCreateUseCase the validate store before create use case
     * @param saveStoreGateway                 the save store gateway
     * @return the create store use case
     */
    @Bean
    public CreateStoreUseCase getCreateStoreUseCase(ValidateStoreBeforeCreateUseCase validateStoreBeforeCreateUseCase,
                                                  SaveStore saveStoreGateway) {
        return new CreateStoreUseCase(validateStoreBeforeCreateUseCase, saveStoreGateway);
    }

    /**
     * Gets find all stores use case.
     *
     * @param findAllStoresGateway the find all stores gateway
     * @return the find all stores use case
     */
    @Bean
    public FindAllStoresUseCase getFindAllStoresUseCase(FindAllStores findAllStoresGateway) {
        return new FindAllStoresUseCase(findAllStoresGateway);
    }

    /**
     * Gets find store by code use case.
     *
     * @param findStoreByCodeGateway the find store by code gateway
     * @return the find store by code use case
     */
    @Bean
    public FindStoreByCodeUseCase getFindStoreByCodeUseCase(FindStoreByCode findStoreByCodeGateway) {
        return new FindStoreByCodeUseCase(findStoreByCodeGateway);
    }

    /**
     * Gets validate store before update use case.
     *
     * @return the validate store before update use case
     */
    @Bean
    public ValidateStoreBeforeUpdateUseCase getValidateStoreBeforeUpdateUseCase() {
        return new ValidateStoreBeforeUpdateUseCase();
    }

    /**
     * Gets update store use case.
     *
     * @param validateStoreBeforeUpdateUseCase the validate store before update use case
     * @param updateStoreGateway               the update store gateway
     * @return the update store use case
     */
    @Bean
    public UpdateStoreUseCase getUpdateStoreUseCase(ValidateStoreBeforeUpdateUseCase validateStoreBeforeUpdateUseCase,
                                                  UpdateStore updateStoreGateway) {
        return new UpdateStoreUseCase(validateStoreBeforeUpdateUseCase, updateStoreGateway);
    }

    /**
     * Gets validate store before delete use case.
     *
     * @return the validate store before delete use case
     */
    @Bean
    public ValidateStoreBeforeDeleteUseCase getValidateStoreBeforeDeleteUseCase() {
        return new ValidateStoreBeforeDeleteUseCase();
    }

    /**
     * Gets delete store use case.
     *
     * @param validateStoreBeforeDeleteUseCase the validate store before delete use case
     * @param deleteStoreGateway               the delete store gateway
     * @return the delete store use case
     */
    @Bean
    public DeleteStoreUseCase getDeleteStoreUseCase(ValidateStoreBeforeDeleteUseCase validateStoreBeforeDeleteUseCase,
                                                  DeleteStore deleteStoreGateway) {
        return new DeleteStoreUseCase(validateStoreBeforeDeleteUseCase, deleteStoreGateway);
    }
}
