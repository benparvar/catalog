package com.benparvar.catalog.spring.config.unit;

import com.benparvar.catalog.controller.product.model.create.mapper.CreateProductUnitMapper;
import com.benparvar.catalog.controller.product.model.find.mapper.FindProductUnitMapper;
import com.benparvar.catalog.controller.product.model.update.mapper.UpdateProductUnitMapper;
import com.benparvar.catalog.controller.unit.model.create.CreateUnitMapper;
import com.benparvar.catalog.controller.unit.model.delete.DeleteUnitMapper;
import com.benparvar.catalog.controller.unit.model.find.FindUnitMapper;
import com.benparvar.catalog.controller.unit.model.update.UpdateUnitMapper;
import com.benparvar.catalog.repository.unit.model.UnitModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * The type Unit mapper config.
 */
@Configuration
public class UnitMapperConfig {

    /**
     * Ge unit mapper unit mapper.
     *
     * @return the unit mapper
     */
    @Bean
    public CreateProductUnitMapper geUnitMapper() {
        return new CreateProductUnitMapper();
    }

    /**
     * Gets unit model mapper.
     *
     * @return the unit model mapper
     */
    @Bean
    public UnitModelMapper getUnitModelMapper() {
        return new UnitModelMapper();
    }

    /**
     * Gets create unit mapper.
     *
     * @return the create unit mapper
     */
    @Bean
    public CreateUnitMapper getCreateUnitMapper() {
        return new CreateUnitMapper();
    }

    /**
     * Gets find unit mapper.
     *
     * @return the find unit mapper
     */
    @Bean
    public FindUnitMapper getFindUnitMapper() {
        return new FindUnitMapper();
    }

    /**
     * Gets update unit mapper.
     *
     * @return the update unit mapper
     */
    @Bean
    public UpdateUnitMapper getUpdateUnitMapper() {
        return new UpdateUnitMapper();
    }

    /**
     * Gets delete unit mapper.
     *
     * @return the delete unit mapper
     */
    @Bean
    public DeleteUnitMapper getDeleteUnitMapper() {
        return new DeleteUnitMapper();
    }

    /**
     * Gets find product unit mapper.
     *
     * @return the find product unit mapper
     */
    @Bean
    public FindProductUnitMapper getFindProductUnitMapper() {
        return new FindProductUnitMapper();
    }

    /**
     * Gets update product unit mapper.
     *
     * @return the update product unit mapper
     */
    @Bean
    public UpdateProductUnitMapper getUpdateProductUnitMapper() {
        return new UpdateProductUnitMapper();
    }
}
