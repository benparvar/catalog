package com.benparvar.catalog.spring.config.store;

import com.benparvar.catalog.controller.product.model.create.mapper.CreateProductStoreMapper;
import com.benparvar.catalog.controller.product.model.find.mapper.FindProductStoreMapper;
import com.benparvar.catalog.controller.product.model.update.mapper.UpdateProductStoreMapper;
import com.benparvar.catalog.controller.store.model.create.CreateStoreMapper;
import com.benparvar.catalog.controller.store.model.delete.DeleteStoreMapper;
import com.benparvar.catalog.controller.store.model.find.FindStoreMapper;
import com.benparvar.catalog.controller.store.model.update.UpdateStoreMapper;
import com.benparvar.catalog.repository.store.model.StoreModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * The type Store mapper config.
 */
@Configuration
public class StoreMapperConfig {

    /**
     * Gets store mapper.
     *
     * @return the store mapper
     */
    @Bean
    public CreateProductStoreMapper getStoreMapper() {
        return new CreateProductStoreMapper();
    }

    /**
     * Gets store model mapper.
     *
     * @return the store model mapper
     */
    @Bean
    public StoreModelMapper getStoreModelMapper() {
        return new StoreModelMapper();
    }

    /**
     * Gets create store mapper.
     *
     * @return the create store mapper
     */
    @Bean
    public CreateStoreMapper getCreateStoreMapper() {
        return new CreateStoreMapper();
    }

    /**
     * Gets find store mapper.
     *
     * @return the find store mapper
     */
    @Bean
    public FindStoreMapper getFindStoreMapper() {
        return new FindStoreMapper();
    }

    /**
     * Gets update store mapper.
     *
     * @return the update store mapper
     */
    @Bean
    public UpdateStoreMapper getUpdateStoreMapper() {
        return new UpdateStoreMapper();
    }

    /**
     * Gets delete store mapper.
     *
     * @return the delete store mapper
     */
    @Bean
    public DeleteStoreMapper getDeleteStoreMapper() {
        return new DeleteStoreMapper();
    }

    /**
     * Gets find product store mapper.
     *
     * @return the find product store mapper
     */
    @Bean
    public FindProductStoreMapper getFindProductStoreMapper() {
        return new FindProductStoreMapper();
    }

    /**
     * Gets update product store mapper.
     *
     * @return the update product store mapper
     */
    @Bean
    public UpdateProductStoreMapper getUpdateProductStoreMapper() {
        return new UpdateProductStoreMapper();
    }
}
