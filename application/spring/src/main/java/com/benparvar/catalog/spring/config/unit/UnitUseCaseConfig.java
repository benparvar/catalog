package com.benparvar.catalog.spring.config.unit;

import com.benparvar.catalog.usecase.unit.create.CreateUnitUseCase;
import com.benparvar.catalog.usecase.unit.create.SaveUnit;
import com.benparvar.catalog.usecase.unit.create.ValidateUnitBeforeCreateUseCase;
import com.benparvar.catalog.usecase.unit.delete.DeleteUnit;
import com.benparvar.catalog.usecase.unit.delete.DeleteUnitUseCase;
import com.benparvar.catalog.usecase.unit.delete.ValidateUnitBeforeDeleteUseCase;
import com.benparvar.catalog.usecase.unit.find.all.FindAllUnits;
import com.benparvar.catalog.usecase.unit.find.all.FindAllUnitsUseCase;
import com.benparvar.catalog.usecase.unit.find.code.FindUnitByCode;
import com.benparvar.catalog.usecase.unit.find.code.FindUnitByCodeUseCase;
import com.benparvar.catalog.usecase.unit.update.UpdateUnit;
import com.benparvar.catalog.usecase.unit.update.UpdateUnitUseCase;
import com.benparvar.catalog.usecase.unit.update.ValidateUnitBeforeUpdateUseCase;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * The type Unit use case config.
 */
@Configuration
public class UnitUseCaseConfig {

    /**
     * Gets validate unit before create use case.
     *
     * @return the validate unit before create use case
     */
    @Bean
    public ValidateUnitBeforeCreateUseCase getValidateUnitBeforeCreateUseCase() {
        return new ValidateUnitBeforeCreateUseCase();
    }

    /**
     * Gets create unit use case.
     *
     * @param validateUnitBeforeCreateUseCase the validate unit before create use case
     * @param saveUnitGateway                 the save unit gateway
     * @return the create unit use case
     */
    @Bean
    public CreateUnitUseCase getCreateUnitUseCase(ValidateUnitBeforeCreateUseCase validateUnitBeforeCreateUseCase,
                                                  SaveUnit saveUnitGateway) {
        return new CreateUnitUseCase(validateUnitBeforeCreateUseCase, saveUnitGateway);
    }

    /**
     * Gets find all units use case.
     *
     * @param findAllUnitsGateway the find all units gateway
     * @return the find all units use case
     */
    @Bean
    public FindAllUnitsUseCase getFindAllUnitsUseCase(FindAllUnits findAllUnitsGateway) {
        return new FindAllUnitsUseCase(findAllUnitsGateway);
    }

    /**
     * Gets find unit by code use case.
     *
     * @param findUnitByCodeGateway the find unit by code gateway
     * @return the find unit by code use case
     */
    @Bean
    public FindUnitByCodeUseCase getFindUnitByCodeUseCase(FindUnitByCode findUnitByCodeGateway) {
        return new FindUnitByCodeUseCase(findUnitByCodeGateway);
    }

    /**
     * Gets validate unit before update use case.
     *
     * @return the validate unit before update use case
     */
    @Bean
    public ValidateUnitBeforeUpdateUseCase getValidateUnitBeforeUpdateUseCase() {
        return new ValidateUnitBeforeUpdateUseCase();
    }

    /**
     * Gets update unit use case.
     *
     * @param validateUnitBeforeUpdateUseCase the validate unit before update use case
     * @param updateUnitGateway               the update unit gateway
     * @return the update unit use case
     */
    @Bean
    public UpdateUnitUseCase getUpdateUnitUseCase(ValidateUnitBeforeUpdateUseCase validateUnitBeforeUpdateUseCase,
                                                  UpdateUnit updateUnitGateway) {
        return new UpdateUnitUseCase(validateUnitBeforeUpdateUseCase, updateUnitGateway);
    }

    /**
     * Gets validate unit before delete use case.
     *
     * @return the validate unit before delete use case
     */
    @Bean
    public ValidateUnitBeforeDeleteUseCase getValidateUnitBeforeDeleteUseCase() {
        return new ValidateUnitBeforeDeleteUseCase();
    }

    /**
     * Gets delete unit use case.
     *
     * @param validateUnitBeforeDeleteUseCase the validate unit before delete use case
     * @param deleteUnitGateway               the delete unit gateway
     * @return the delete unit use case
     */
    @Bean
    public DeleteUnitUseCase getDeleteUnitUseCase(ValidateUnitBeforeDeleteUseCase validateUnitBeforeDeleteUseCase,
                                                  DeleteUnit deleteUnitGateway) {
        return new DeleteUnitUseCase(validateUnitBeforeDeleteUseCase, deleteUnitGateway);
    }
}
