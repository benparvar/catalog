package com.benparvar.catalog.spring.config.unit;

import com.benparvar.catalog.repository.unit.UnitRepository;
import com.benparvar.catalog.repository.unit.model.UnitModelMapper;
import com.benparvar.catalog.gateway.unit.UnitGateway;
import com.benparvar.catalog.usecase.unit.create.SaveUnit;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * The type Unit gateway config.
 */
@Configuration
public class UnitGatewayConfig {

    /**
     * Gets save unit.
     *
     * @param unitRepository    the unit repository
     * @param unitModelMapper the unit model mapper
     * @return the save unit
     */
    @Bean
    public SaveUnit getSaveUnit(UnitRepository unitRepository, UnitModelMapper unitModelMapper) {
        return new UnitGateway(unitRepository, unitModelMapper);
    }
}
