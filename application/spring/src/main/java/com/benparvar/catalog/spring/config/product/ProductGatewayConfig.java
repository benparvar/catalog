package com.benparvar.catalog.spring.config.product;

import com.benparvar.catalog.repository.product.ProductRepository;
import com.benparvar.catalog.repository.product.model.ProductModelMapper;
import com.benparvar.catalog.repository.store.StoreRepository;
import com.benparvar.catalog.repository.unit.UnitRepository;
import com.benparvar.catalog.gateway.product.ProductGateway;
import com.benparvar.catalog.usecase.product.create.SaveProduct;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * The type Product gateway config.
 */
@Configuration
public class ProductGatewayConfig {

    /**
     * Gets save product.
     *
     * @param repository           the repository
     * @param productModelMapper the product model mapper
     * @param storeRepository      the store repository
     * @param unitRepository       the unit repository
     * @return the save product
     */
    @Bean
    public SaveProduct getSaveProduct(ProductRepository repository, ProductModelMapper productModelMapper,
                                      StoreRepository storeRepository, UnitRepository unitRepository) {
        return new ProductGateway(repository, productModelMapper, storeRepository, unitRepository);
    }
}
