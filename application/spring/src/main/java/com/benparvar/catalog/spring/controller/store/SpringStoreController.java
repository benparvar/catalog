package com.benparvar.catalog.spring.controller.store;

import com.benparvar.catalog.controller.store.StoreController;
import com.benparvar.catalog.controller.store.model.create.CreateStoreRequest;
import com.benparvar.catalog.controller.store.model.create.CreateStoreResponse;
import com.benparvar.catalog.controller.store.model.find.FindStoreResponse;
import com.benparvar.catalog.controller.store.model.update.UpdateStoreRequest;
import com.benparvar.catalog.controller.store.model.update.UpdateStoreResponse;
import com.benparvar.catalog.domain.entity.exception.AlreadyExistException;
import com.benparvar.catalog.domain.entity.exception.NotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;

import java.util.List;

/**
 * The type Spring store controller.
 */
@CrossOrigin
@RestController
@RequestMapping(value = "/api/v1/stores", produces = MediaType.APPLICATION_JSON_VALUE)
public class SpringStoreController {
    private final Logger log = LoggerFactory.getLogger(SpringStoreController.class);
    @Autowired
    private final StoreController controller;

    /**
     * Instantiates a new Spring store controller.
     *
     * @param controller the controller
     */
    public SpringStoreController(StoreController controller) {
        this.controller = controller;
    }

    /**
     * Create create store response.
     *
     * @param request the request
     * @return the create store response
     */
    @PostMapping(value = "", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public CreateStoreResponse create(@RequestBody CreateStoreRequest request) {
        log.info("create: {}", request);

        return controller.create(request);
    }

    /**
     * Find all list.
     *
     * @return the list
     */
    @GetMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public List<FindStoreResponse> findAll() {
        log.info("findAll:");

        return controller.findAll();
    }

    /**
     * Find by code find store response.
     *
     * @param code the code
     * @return the find store response
     */
    @GetMapping(value = "/{code}/code", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public FindStoreResponse findByCode(@PathVariable String code) {
        log.info("findByCode: {}", code);

        return controller.findByCode(code);
    }

    @PutMapping(value = "", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public UpdateStoreResponse update(@RequestBody UpdateStoreRequest request) {
        log.info("update: {}", request);

        return controller.update(request);
    }

    @DeleteMapping(value = "/{code}/code")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable String code) {
        log.info("delete: {}", code);

        controller.delete(code);
    }

    /**
     * Handle not found exception response entity.
     *
     * @param ex      the ex
     * @param request the request
     * @return the response entity
     */
    @ExceptionHandler({NotFoundException.class})
    public ResponseEntity<Object> handleNotFoundException(NotFoundException ex, WebRequest request) {
        log.error(ex.getMessage());

        return new ResponseEntity<>(
                "error: Sorry...", new HttpHeaders(), HttpStatus.NOT_FOUND);
    }


    /**
     * Handle already exist exception response entity.
     *
     * @param ex      the ex
     * @param request the request
     * @return the response entity
     */
    @ExceptionHandler({AlreadyExistException.class})
    public ResponseEntity<Object> handleAlreadyExistException(AlreadyExistException ex, WebRequest request) {
        log.error(ex.getMessage());

        return new ResponseEntity<>(
                "error: Already exist", new HttpHeaders(), HttpStatus.CONFLICT);
    }

    /**
     * Handle illegal argument exception response entity.
     *
     * @param ex      the ex
     * @param request the request
     * @return the response entity
     */
    @ExceptionHandler({IllegalArgumentException.class})
    public ResponseEntity<Object> handleIllegalArgumentException(IllegalArgumentException ex, WebRequest request) {
        log.error(ex.getMessage());
        return new ResponseEntity<>(
                "error: " + ex.getMessage(), new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }

    /**
     * Handle http message not readable exception response entity.
     *
     * @param ex      the ex
     * @param request the request
     * @return the response entity
     */
    @ExceptionHandler({HttpMessageNotReadableException.class})
    public ResponseEntity<Object> handleHttpMessageNotReadableException(HttpMessageNotReadableException ex,
                                                                        WebRequest request) {
        log.error(ex.getMessage());
        return new ResponseEntity<>(
                "error: " + ex.getMessage(), new HttpHeaders(), HttpStatus.UNPROCESSABLE_ENTITY);
    }
}
