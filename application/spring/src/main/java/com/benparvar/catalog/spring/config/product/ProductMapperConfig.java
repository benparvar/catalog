package com.benparvar.catalog.spring.config.product;

import com.benparvar.catalog.controller.product.model.create.mapper.*;
import com.benparvar.catalog.controller.product.model.delete.DeleteProductMapper;
import com.benparvar.catalog.controller.product.model.find.mapper.*;
import com.benparvar.catalog.controller.product.model.update.mapper.*;
import com.benparvar.catalog.repository.barcode.model.BarcodeModelMapper;
import com.benparvar.catalog.repository.image.model.ImageModelMapper;
import com.benparvar.catalog.repository.product.model.ProductModelMapper;
import com.benparvar.catalog.repository.store.model.StoreModelMapper;
import com.benparvar.catalog.repository.unit.model.UnitModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * The type Product mapper config.
 */
@Configuration
public class ProductMapperConfig {

    /**
     * Gets create product mapper.
     *
     * @param createProductUnitMapper    the unit mapper
     * @param createProductStoreMapper   the store mapper
     * @param createProductImageMapper   the image mapper
     * @param createProductBarcodeMapper the barcode mapper
     * @return the create product mapper
     */
    @Bean
    public CreateProductMapper getCreateProductMapper(CreateProductUnitMapper createProductUnitMapper, CreateProductStoreMapper createProductStoreMapper,
                                                      CreateProductImageMapper createProductImageMapper, CreateProductBarcodeMapper createProductBarcodeMapper) {
        return new CreateProductMapper(createProductUnitMapper, createProductStoreMapper, createProductImageMapper, createProductBarcodeMapper);
    }

    /**
     * Gets product model mapper.
     *
     * @param barcodeModelMapper the barcode model mapper
     * @param unitModelMapper    the unit model mapper
     * @param storeModelMapper   the store model mapper
     * @param imageModelMapper   the image model mapper
     * @return the product model mapper
     */
    @Bean
    public ProductModelMapper getProductModelMapper(BarcodeModelMapper barcodeModelMapper, UnitModelMapper unitModelMapper,
                                                    StoreModelMapper storeModelMapper, ImageModelMapper imageModelMapper) {
        return new ProductModelMapper(barcodeModelMapper, unitModelMapper, storeModelMapper, imageModelMapper);
    }

    /**
     * Gets find product mapper.
     *
     * @param unitMapper    the unit mapper
     * @param storeMapper   the store mapper
     * @param imageMapper   the image mapper
     * @param barcodeMapper the barcode mapper
     * @return the find product mapper
     */
    @Bean
    public FindProductMapper getFindProductMapper(FindProductUnitMapper unitMapper, FindProductStoreMapper storeMapper,
                                                  FindProductImageMapper imageMapper, FindProductBarcodeMapper barcodeMapper) {
        return new FindProductMapper(unitMapper, storeMapper, imageMapper, barcodeMapper);
    }

    /**
     * Gets update product mapper.
     *
     * @param unitMapper    the unit mapper
     * @param storeMapper   the store mapper
     * @param imageMapper   the image mapper
     * @param barcodeMapper the barcode mapper
     * @return the update product mapper
     */
    @Bean
    public UpdateProductMapper getUpdateProductMapper(UpdateProductUnitMapper unitMapper,
                                                      UpdateProductStoreMapper storeMapper,
                                                      UpdateProductImageMapper imageMapper,
                                                      UpdateProductBarcodeMapper barcodeMapper) {
        return new UpdateProductMapper(unitMapper, storeMapper, imageMapper, barcodeMapper);
    }

    /**
     * Gets delete product mapper.
     *
     * @return the delete product mapper
     */
    @Bean
    public DeleteProductMapper getDeleteProductMapper() {
        return new DeleteProductMapper();
    }
}
