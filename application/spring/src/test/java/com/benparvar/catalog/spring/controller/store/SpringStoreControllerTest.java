package com.benparvar.catalog.spring.controller.store;

import com.benparvar.catalog.spring.fixture.StoreJson;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * The type Spring store controller test.
 */
@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class SpringStoreControllerTest {

    @Autowired
    private MockMvc mockMvc;

    /**
     * Save stores will no payload will return unprocessable entity.
     *
     * @throws Exception the exception
     */
    @Test
    public void saveStoresWillNoPayloadWillReturnUnprocessableEntity() throws Exception {
        mockMvc.perform(post("/api/v1/stores")
                .accept(MediaType.APPLICATION_JSON)
                .content(StoreJson.NO_PAYLOAD)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isUnprocessableEntity())
                .andReturn();
    }

    /**
     * Save stores will empty payload will return bad request.
     *
     * @throws Exception the exception
     */
    @Test
    public void saveStoresWillEmptyPayloadWillReturnBadRequest() throws Exception {
        mockMvc.perform(post("/api/v1/stores")
                .accept(MediaType.APPLICATION_JSON)
                .content(StoreJson.EMPTY_PAYLOAD)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn();
    }

    /**
     * Save stores with valid payload will return created.
     *
     * @throws Exception the exception
     */
    @Test
    public void saveStoresWithValidPayloadWillReturnCreated() throws Exception {
        mockMvc.perform(post("/api/v1/stores")
                .accept(MediaType.APPLICATION_JSON)
                .content(StoreJson.VALID_PAYLOAD)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(content().json(StoreJson.VALID_RESPONSE))
                .andReturn();
    }

    /**
     * Save stores already saved with valid payload will return conflict.
     *
     * @throws Exception the exception
     */
    @Test
    public void saveStoresAlreadySavedWithValidPayloadWillReturnConflict() throws Exception {
        // Creating
        mockMvc.perform(post("/api/v1/stores")
                .accept(MediaType.APPLICATION_JSON)
                .content(StoreJson.VALID_PAYLOAD)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(content().json(StoreJson.VALID_RESPONSE))
                .andReturn();

        // Trying to create the same store again
        mockMvc.perform(post("/api/v1/stores")
                .accept(MediaType.APPLICATION_JSON)
                .content(StoreJson.VALID_PAYLOAD)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isConflict())
                .andReturn();
    }

    /**
     * Gets all stores with no entities in repository will return not found.
     *
     * @throws Exception the exception
     */
    @Test
    public void getAllStoresWithNoEntitiesInRepositoryWillReturnNotFound() throws Exception {
        mockMvc.perform(get("/api/v1/stores")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound())
                .andReturn();
    }

    /**
     * Gets all stores with entities in repository will return ok.
     *
     * @throws Exception the exception
     */
    @Test
    public void getAllStoresWithEntitiesInRepositoryWillReturnOk() throws Exception {
        // Creating
        mockMvc.perform(post("/api/v1/stores")
                .accept(MediaType.APPLICATION_JSON)
                .content(StoreJson.VALID_PAYLOAD)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(content().json(StoreJson.VALID_RESPONSE))
                .andReturn();

        // Reading
        mockMvc.perform(get("/api/v1/stores")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(StoreJson.VALID_ARRAY_RESPONSE))
                .andReturn();
    }

    /**
     * Delete stores with entities in repository will return ok.
     *
     * @throws Exception the exception
     */
    @Test
    public void deleteStoresWithEntitiesInRepositoryWillReturnNoContent() throws Exception {
        // Creating
        mockMvc.perform(post("/api/v1/stores")
                .accept(MediaType.APPLICATION_JSON)
                .content(StoreJson.VALID_PAYLOAD)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(content().json(StoreJson.VALID_RESPONSE))
                .andReturn();

        // Reading
        mockMvc.perform(delete("/api/v1/stores/666/code")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent())
                .andReturn();
    }

    /**
     * Gets by code stores with entities in repository will return ok.
     *
     * @throws Exception the exception
     */
    @Test
    public void getByCodeStoresWithEntitiesInRepositoryWillReturnOk() throws Exception {
        // Creating
        mockMvc.perform(post("/api/v1/stores")
                .accept(MediaType.APPLICATION_JSON)
                .content(StoreJson.VALID_PAYLOAD)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(content().json(StoreJson.VALID_RESPONSE))
                .andReturn();

        // Reading
        mockMvc.perform(get("/api/v1/stores/666/code")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(StoreJson.VALID_RESPONSE))
                .andReturn();
    }

    /**
     * Update stores with entities in repository will return ok.
     *
     * @throws Exception the exception
     */
    @Test
    public void updateStoresWithEntitiesInRepositoryWillReturnOk() throws Exception {
        // Creating
        mockMvc.perform(post("/api/v1/stores")
                .accept(MediaType.APPLICATION_JSON)
                .content(StoreJson.VALID_PAYLOAD)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(content().json(StoreJson.VALID_RESPONSE))
                .andReturn();

        // Updating
        mockMvc.perform(put("/api/v1/stores")
                .accept(MediaType.APPLICATION_JSON)
                .content(StoreJson.VALID_UPDATE_PAYLOAD)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(StoreJson.VALID_UPDATE_RESPONSE))
                .andReturn();
    }
}