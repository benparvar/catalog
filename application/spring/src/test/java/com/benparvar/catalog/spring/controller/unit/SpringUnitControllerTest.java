package com.benparvar.catalog.spring.controller.unit;

import com.benparvar.catalog.spring.fixture.UnitJson;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * The type Spring unit controller test.
 */
@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class SpringUnitControllerTest {

    @Autowired
    private MockMvc mockMvc;

    /**
     * Save units will no payload will return unprocessable entity.
     *
     * @throws Exception the exception
     */
    @Test
    public void saveUnitsWillNoPayloadWillReturnUnprocessableEntity() throws Exception {
        mockMvc.perform(post("/api/v1/units")
                .accept(MediaType.APPLICATION_JSON)
                .content(UnitJson.NO_PAYLOAD)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isUnprocessableEntity())
                .andReturn();
    }

    /**
     * Save units will empty payload will return bad request.
     *
     * @throws Exception the exception
     */
    @Test
    public void saveUnitsWillEmptyPayloadWillReturnBadRequest() throws Exception {
        mockMvc.perform(post("/api/v1/units")
                .accept(MediaType.APPLICATION_JSON)
                .content(UnitJson.EMPTY_PAYLOAD)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn();
    }

    /**
     * Save units with valid payload will return created.
     *
     * @throws Exception the exception
     */
    @Test
    public void saveUnitsWithValidPayloadWillReturnCreated() throws Exception {
        mockMvc.perform(post("/api/v1/units")
                .accept(MediaType.APPLICATION_JSON)
                .content(UnitJson.VALID_PAYLOAD)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(content().json(UnitJson.VALID_RESPONSE))
                .andReturn();
    }

    /**
     * Save units already saved with valid payload will return conflict.
     *
     * @throws Exception the exception
     */
    @Test
    public void saveUnitsAlreadySavedWithValidPayloadWillReturnConflict() throws Exception {
        // Creating
        mockMvc.perform(post("/api/v1/units")
                .accept(MediaType.APPLICATION_JSON)
                .content(UnitJson.VALID_PAYLOAD)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(content().json(UnitJson.VALID_RESPONSE))
                .andReturn();

        // Trying to create the same unit again
        mockMvc.perform(post("/api/v1/units")
                .accept(MediaType.APPLICATION_JSON)
                .content(UnitJson.VALID_PAYLOAD)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isConflict())
                .andReturn();
    }

    /**
     * Gets all units with no entities in repository will return not found.
     *
     * @throws Exception the exception
     */
    @Test
    public void getAllUnitsWithNoEntitiesInRepositoryWillReturnNotFound() throws Exception {
        mockMvc.perform(get("/api/v1/units")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound())
                .andReturn();
    }

    /**
     * Gets all units with entities in repository will return ok.
     *
     * @throws Exception the exception
     */
    @Test
    public void getAllUnitsWithEntitiesInRepositoryWillReturnOk() throws Exception {
        // Creating
        mockMvc.perform(post("/api/v1/units")
                .accept(MediaType.APPLICATION_JSON)
                .content(UnitJson.VALID_PAYLOAD)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(content().json(UnitJson.VALID_RESPONSE))
                .andReturn();

        // Reading
        mockMvc.perform(get("/api/v1/units")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(UnitJson.VALID_ARRAY_RESPONSE))
                .andReturn();
    }

    /**
     * Delete units with entities in repository will return ok.
     *
     * @throws Exception the exception
     */
    @Test
    public void deleteUnitsWithEntitiesInRepositoryWillReturnNoContent() throws Exception {
        // Creating
        mockMvc.perform(post("/api/v1/units")
                .accept(MediaType.APPLICATION_JSON)
                .content(UnitJson.VALID_PAYLOAD)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(content().json(UnitJson.VALID_RESPONSE))
                .andReturn();

        // Reading
        mockMvc.perform(delete("/api/v1/units/MP/code")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent())
                .andReturn();
    }

    /**
     * Gets by code units with entities in repository will return ok.
     *
     * @throws Exception the exception
     */
    @Test
    public void getByCodeUnitsWithEntitiesInRepositoryWillReturnOk() throws Exception {
        // Creating
        mockMvc.perform(post("/api/v1/units")
                .accept(MediaType.APPLICATION_JSON)
                .content(UnitJson.VALID_PAYLOAD)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(content().json(UnitJson.VALID_RESPONSE))
                .andReturn();

        // Reading
        mockMvc.perform(get("/api/v1/units/MP/code")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(UnitJson.VALID_RESPONSE))
                .andReturn();
    }

    /**
     * Update units with entities in repository will return ok.
     *
     * @throws Exception the exception
     */
    @Test
    public void updateUnitsWithEntitiesInRepositoryWillReturnOk() throws Exception {
        // Creating
        mockMvc.perform(post("/api/v1/units")
                .accept(MediaType.APPLICATION_JSON)
                .content(UnitJson.VALID_PAYLOAD)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(content().json(UnitJson.VALID_RESPONSE))
                .andReturn();

        // Reading
        mockMvc.perform(put("/api/v1/units")
                .accept(MediaType.APPLICATION_JSON)
                .content(UnitJson.VALID_UPDATE_PAYLOAD)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(UnitJson.VALID_UPDATE_RESPONSE))
                .andReturn();
    }
}