package com.benparvar.catalog.spring.fixture;

/**
 * The type Product json.
 */
public class ProductJson {

    /**
     * The constant NO_PAYLOAD.
     */
    public static String NO_PAYLOAD = "";

    /**
     * The constant EMPTY_PAYLOAD.
     */
    public static String EMPTY_PAYLOAD = "{}";

    /**
     * The constant VALID_PAYLOAD.
     */
    public static String VALID_PAYLOAD = "{\n" +
            "   \"code\": \"666\",\n" +
            "   \"name\": \"Coca Zero 350ml\",\n" +
            "   \"description\": \"Coca zero lata 350ml\",\n" +
            "   \"barcode\": \"7894900700046\",\n" +
            "   \"unit\": {\n" +
            "      \"code\": \"PC\",\n" +
            "      \"name\": \"PIECE\"\n" +
            "   },\n" +
            "   \"price\": 2.5,\n" +
            "   \"store\": {\n" +
            "      \"code\": \"132\",\n" +
            "      \"name\": \"Matrix\"\n" +
            "   },\n" +
            "   \"images\": [\n" +
            "      {\n" +
            "         \"description\": \"principal\",\n" +
            "         \"url\": \"https://cdn-cosmos.bluesoft.com.br/products/7894900700220/zhksxcua\"\n" +
            "      },\n" +
            "      {\n" +
            "         \"description\": \"media\",\n" +
            "         \"url\": \"https://imagens.gimba.com.br/objetosmidia/ExibirObjetoMidia?Id=86826\"\n" +
            "      }\n" +
            "   ]\n" +
            "}";

    /**
     * The constant VALID_RESPONSE.
     */
    public static String VALID_RESPONSE = "{\n" +
            "   \"code\": \"666\",\n" +
            "   \"name\": \"Coca Zero 350ml\",\n" +
            "   \"description\": \"Coca zero lata 350ml\",\n" +
            "   \"barcode\": \"7894900700046\",\n" +
            "   \"unit\": {\n" +
            "      \"code\": \"PC\",\n" +
            "      \"name\": \"PIECE\"\n" +
            "   },\n" +
            "   \"price\": 2.5,\n" +
            "   \"store\": {\n" +
            "      \"code\": \"132\",\n" +
            "      \"name\": \"Matrix\"\n" +
            "   },\n" +
            "   \"images\": [\n" +
            "      {\n" +
            "         \"description\": \"principal\",\n" +
            "         \"url\": \"https://cdn-cosmos.bluesoft.com.br/products/7894900700220/zhksxcua\"\n" +
            "      },\n" +
            "      {\n" +
            "         \"description\": \"media\",\n" +
            "         \"url\": \"https://imagens.gimba.com.br/objetosmidia/ExibirObjetoMidia?Id=86826\"\n" +
            "      }\n" +
            "   ]\n" +
            "}";

    public static String VALID_UPDATE_PRICE_PAYLOAD = "{\n" +
            "   \"barcode\": \"7894900700046\",\n" +
            "   \"storeCode\": \"132\",\n" +
            "   \"price\": 9.99\n" +
            "}";

    public static String VALID_UPDATE_PRICE_RESPONSE = "{\n" +
            "   \"code\": \"666\",\n" +
            "   \"name\": \"Coca Zero 350ml\",\n" +
            "   \"description\": \"Coca zero lata 350ml\",\n" +
            "   \"barcode\": \"7894900700046\",\n" +
            "   \"unit\": {\n" +
            "      \"code\": \"PC\",\n" +
            "      \"name\": \"PIECE\"\n" +
            "   },\n" +
            "   \"price\": 9.99,\n" +
            "   \"store\": {\n" +
            "      \"code\": \"132\",\n" +
            "      \"name\": \"Matrix\"\n" +
            "   },\n" +
            "   \"images\": [\n" +
            "      {\n" +
            "         \"description\": \"principal\",\n" +
            "         \"url\": \"https://cdn-cosmos.bluesoft.com.br/products/7894900700220/zhksxcua\"\n" +
            "      },\n" +
            "      {\n" +
            "         \"description\": \"media\",\n" +
            "         \"url\": \"https://imagens.gimba.com.br/objetosmidia/ExibirObjetoMidia?Id=86826\"\n" +
            "      }\n" +
            "   ]\n" +
            "}";
}
