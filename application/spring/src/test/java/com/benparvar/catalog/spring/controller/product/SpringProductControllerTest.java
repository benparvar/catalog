package com.benparvar.catalog.spring.controller.product;

import com.benparvar.catalog.spring.fixture.ProductJson;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * The type Spring product controller test.
 */
@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class SpringProductControllerTest {

    @Autowired
    private MockMvc mockMvc;

    /**
     * Save products will no payload will return unprocessable entity.
     *
     * @throws Exception the exception
     */
    @Test
    public void saveProductsWillNoPayloadWillReturnUnprocessableEntity() throws Exception {
        mockMvc.perform(post("/api/v1/products")
                .accept(MediaType.APPLICATION_JSON)
                .content(ProductJson.NO_PAYLOAD)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isUnprocessableEntity())
                .andReturn();
    }

    /**
     * Save products will empty payload will return bad request.
     *
     * @throws Exception the exception
     */
    @Test
    public void saveProductsWillEmptyPayloadWillReturnBadRequest() throws Exception {
        mockMvc.perform(post("/api/v1/products")
                .accept(MediaType.APPLICATION_JSON)
                .content(ProductJson.EMPTY_PAYLOAD)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn();
    }

    /**
     * Save products with valid payload will return created.
     *
     * @throws Exception the exception
     */
    @Test
    public void saveProductsWithValidPayloadWillReturnCreated() throws Exception {
        mockMvc.perform(post("/api/v1/products")
                .accept(MediaType.APPLICATION_JSON)
                .content(ProductJson.VALID_PAYLOAD)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(content().json(ProductJson.VALID_RESPONSE))
                .andReturn();
    }

    /**
     * Save products already saved with valid payload will return conflict.
     *
     * @throws Exception the exception
     */
    @Test
    public void saveProductsAlreadySavedWithValidPayloadWillReturnConflict() throws Exception {
        // Creating
        mockMvc.perform(post("/api/v1/products")
                .accept(MediaType.APPLICATION_JSON)
                .content(ProductJson.VALID_PAYLOAD)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(content().json(ProductJson.VALID_RESPONSE))
                .andReturn();

        // Trying to create the same store again
        mockMvc.perform(post("/api/v1/products")
                .accept(MediaType.APPLICATION_JSON)
                .content(ProductJson.VALID_PAYLOAD)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isConflict())
                .andReturn();
    }

    /**
     * Update products with entities in repository will return ok.
     *
     * @throws Exception the exception
     */
    @Test
    public void updateProductsWithEntitiesInRepositoryWillReturnOk() throws Exception {
        // Creating
        mockMvc.perform(post("/api/v1/products")
                .accept(MediaType.APPLICATION_JSON)
                .content(ProductJson.VALID_PAYLOAD)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(content().json(ProductJson.VALID_RESPONSE))
                .andReturn();

        // Updating
        mockMvc.perform(put("/api/v1/products")
                .accept(MediaType.APPLICATION_JSON)
                .content(ProductJson.VALID_UPDATE_PRICE_PAYLOAD)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(ProductJson.VALID_UPDATE_PRICE_RESPONSE))
                .andReturn();
    }

    /**
     * Gets products with no entities in repository will return not found.
     *
     * @throws Exception the exception
     */
    @Test
    public void getProductsWithNoEntitiesInRepositoryWillReturnNotFound() throws Exception {
        mockMvc.perform(get("/api/v1/products/7894900700046/barcode/132/store")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound())
                .andReturn();
    }

    /**
     * Gets products with entities in repository will return ok.
     *
     * @throws Exception the exception
     */
    @Test
    public void getProductsWithEntitiesInRepositoryWillReturnOk() throws Exception {
        // Creating
        mockMvc.perform(post("/api/v1/products")
                .accept(MediaType.APPLICATION_JSON)
                .content(ProductJson.VALID_PAYLOAD)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(content().json(ProductJson.VALID_RESPONSE))
                .andReturn();

        // Reading
        mockMvc.perform(get("/api/v1/products/7894900700046/barcode/132/store")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(ProductJson.VALID_RESPONSE))
                .andReturn();
    }

    /**
     * Delete products with entities in repository will return no content.
     *
     * @throws Exception the exception
     */
    @Test
    public void deleteProductsWithEntitiesInRepositoryWillReturnNoContent() throws Exception {
        // Creating
        mockMvc.perform(post("/api/v1/products")
                .accept(MediaType.APPLICATION_JSON)
                .content(ProductJson.VALID_PAYLOAD)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(content().json(ProductJson.VALID_RESPONSE))
                .andReturn();

        // Deleting
        mockMvc.perform(delete("/api/v1/products/7894900700046/barcode/132/store")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent())
                .andReturn();
    }
}