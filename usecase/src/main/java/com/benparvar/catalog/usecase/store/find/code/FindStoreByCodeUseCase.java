package com.benparvar.catalog.usecase.store.find.code;

import com.benparvar.catalog.domain.entity.store.Store;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The type Find store by code use case.
 */
public class FindStoreByCodeUseCase {
    private final Logger log = LoggerFactory.getLogger(FindStoreByCodeUseCase.class);
    private final FindStoreByCode findStoreByCodeGateway;

    /**
     * Instantiates a new Find store by code use case.
     *
     * @param findStoreByCodeGateway the find store by code gateway
     */
    public FindStoreByCodeUseCase(FindStoreByCode findStoreByCodeGateway) {
        this.findStoreByCodeGateway = findStoreByCodeGateway;
    }

    /**
     * Execute store.
     *
     * @param code the code
     * @return the store
     */
    public Store execute(String code) {
        log.info("execute: {}", code);

        return findStoreByCodeGateway.findByCode(code);
    }
}
