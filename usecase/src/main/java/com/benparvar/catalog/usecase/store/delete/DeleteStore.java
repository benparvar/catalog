package com.benparvar.catalog.usecase.store.delete;

import com.benparvar.catalog.domain.entity.store.Store;

/**
 * The interface Delete store.
 */
public interface DeleteStore {

    /**
     * Delete.
     *
     * @param entity the entity
     */
    void delete(Store entity);
}
