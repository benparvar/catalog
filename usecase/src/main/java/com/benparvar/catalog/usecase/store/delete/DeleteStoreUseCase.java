package com.benparvar.catalog.usecase.store.delete;

import com.benparvar.catalog.domain.entity.store.Store;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The type Delete store use case.
 */
public class DeleteStoreUseCase {
    private final Logger log = LoggerFactory.getLogger(DeleteStoreUseCase.class);
    private final ValidateStoreBeforeDeleteUseCase validateStoreBeforeDeleteUseCase;
    private final DeleteStore deleteStoreGateway;

    /**
     * Instantiates a new Delete store use case.
     *
     * @param validateStoreBeforeDeleteUseCase the validate store before delete use case
     * @param deleteStoreGateway               the delete store gateway
     */
    public DeleteStoreUseCase(ValidateStoreBeforeDeleteUseCase validateStoreBeforeDeleteUseCase,
                              DeleteStore deleteStoreGateway) {
        this.validateStoreBeforeDeleteUseCase = validateStoreBeforeDeleteUseCase;
        this.deleteStoreGateway = deleteStoreGateway;
    }

    /**
     * Execute.
     *
     * @param entity the entity
     */
    public void execute(Store entity) {
        log.info("execute: {}", entity);

        validateStoreBeforeDeleteUseCase.execute(entity);
        deleteStoreGateway.delete(entity);
    }
}
