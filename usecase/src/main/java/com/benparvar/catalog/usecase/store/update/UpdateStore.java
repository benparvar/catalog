package com.benparvar.catalog.usecase.store.update;

import com.benparvar.catalog.domain.entity.store.Store;

/**
 * The interface Update unit.
 */
public interface UpdateStore {
    /**
     * Update unit.
     *
     * @param entity the entity
     * @return the unit
     */
    Store update(Store entity);
}
