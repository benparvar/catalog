package com.benparvar.catalog.usecase.store.update;

import com.benparvar.catalog.domain.entity.store.Store;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The type Update store use case.
 */
public class UpdateStoreUseCase {
    private final Logger log = LoggerFactory.getLogger(UpdateStoreUseCase.class);
    private final ValidateStoreBeforeUpdateUseCase validateStoreBeforeUpdateUseCase;
    private final UpdateStore updateStoreGateway;

    /**
     * Instantiates a new Update store use case.
     *
     * @param validateStoreBeforeUpdateUseCase the validate store before update use case
     * @param updateStoreGateway               the update store gateway
     */
    public UpdateStoreUseCase(ValidateStoreBeforeUpdateUseCase validateStoreBeforeUpdateUseCase,
                              UpdateStore updateStoreGateway) {
        this.validateStoreBeforeUpdateUseCase = validateStoreBeforeUpdateUseCase;
        this.updateStoreGateway = updateStoreGateway;
    }

    /**
     * Execute store.
     *
     * @param entity the entity
     * @return the store
     */
    public Store execute(Store entity) {
        log.info("execute: {}", entity);
        validateStoreBeforeUpdateUseCase.execute(entity);

        return updateStoreGateway.update(entity);
    }
}
