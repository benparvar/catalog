package com.benparvar.catalog.usecase.store.create;

import com.benparvar.catalog.domain.entity.store.Store;
import com.benparvar.catalog.domain.entity.validator.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The type Validate store before create use case.
 */
public class ValidateStoreBeforeCreateUseCase {
    private final Logger log = LoggerFactory.getLogger(ValidateStoreBeforeCreateUseCase.class);

    /**
     * Execute boolean.
     *
     * @param entity the entity
     * @return the boolean
     */
    public boolean execute(Store entity) {
        log.info("execute: {}", entity);
        Assert.notNull(entity, "store cannot be null");
        Assert.hasText(entity.getCode(), "store code must have a text");
        Assert.hasText(entity.getName(), "store name must have a text");

        return true;
    }
}
