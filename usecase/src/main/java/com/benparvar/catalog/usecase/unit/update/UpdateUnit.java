package com.benparvar.catalog.usecase.unit.update;

import com.benparvar.catalog.domain.entity.unit.Unit;

/**
 * The interface Update unit.
 */
public interface UpdateUnit {
    /**
     * Update unit.
     *
     * @param entity the entity
     * @return the unit
     */
    Unit update(Unit entity);
}
