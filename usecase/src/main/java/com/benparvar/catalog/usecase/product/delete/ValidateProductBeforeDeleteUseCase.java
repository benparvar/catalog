package com.benparvar.catalog.usecase.product.delete;

import com.benparvar.catalog.domain.entity.product.Product;
import com.benparvar.catalog.domain.entity.validator.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The type Validate product before delete use case.
 */
public class ValidateProductBeforeDeleteUseCase {
    private final Logger log = LoggerFactory.getLogger(ValidateProductBeforeDeleteUseCase.class);

    /**
     * Execute boolean.
     *
     * @param entity the entity
     * @return the boolean
     */
    public boolean execute(Product entity) {
        log.info("execute: {}", entity);
        Assert.notNull(entity, "product cannot be null");
        Assert.notNull(entity.getBarcode(), "barcode cannot be null");
        Assert.hasText(entity.getBarcode().getCode(), "barcode code must have a text");
        Assert.notNull(entity.getStore(), "store cannot be null");
        Assert.hasText(entity.getStore().getCode(), "store code must have a text");

        return true;
    }

}
