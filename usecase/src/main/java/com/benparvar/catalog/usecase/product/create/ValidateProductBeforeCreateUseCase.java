package com.benparvar.catalog.usecase.product.create;

import com.benparvar.catalog.domain.entity.product.Product;
import com.benparvar.catalog.domain.entity.validator.Assert;
import com.benparvar.catalog.usecase.barcode.CheckBarcodeDigitUseCase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The type Validate product before create use case.
 */
public class ValidateProductBeforeCreateUseCase {
    private final Logger log = LoggerFactory.getLogger(ValidateProductBeforeCreateUseCase.class);
    private final CheckBarcodeDigitUseCase checkBarcodeDigitUseCase;

    public ValidateProductBeforeCreateUseCase(CheckBarcodeDigitUseCase checkBarcodeDigitUseCase) {
        this.checkBarcodeDigitUseCase = checkBarcodeDigitUseCase;
    }

    /**
     * Execute boolean.
     *
     * @param entity the entity
     * @return the boolean
     */
    public boolean execute(Product entity) {
        log.info("execute: {}", entity);
        Assert.notNull(entity, "product cannot be null");
        Assert.hasText(entity.getCode(), "product code must have a text");
        Assert.hasText(entity.getName(), "product name must have a text");
        Assert.hasText(entity.getDescription(), "product description must have a text");
        Assert.notNull(entity.getBarcode(), "product barcode cannot be null");
        Assert.notNull(entity.getBarcode().getBarcodeType(), "product barcode type cannot be null");
        Assert.hasText(entity.getBarcode().getCode(), "product barcode code must have a text");
        Assert.notNull(entity.getUnit(), "product unit cannot be null");
        Assert.hasText(entity.getUnit().getCode(), "product unit code must have a text");
        Assert.hasText(entity.getUnit().getName(), "product unit name must have a text");
        Assert.notNull(entity.getPrice(), "product price cannot be null");
        Assert.notNull(entity.getStore(), "product store cannot be null");
        Assert.hasText(entity.getStore().getCode(), "product store code must have a text");
        Assert.hasText(entity.getStore().getName(), "product store name must have a text");
        Assert.notNull(entity.getImages(), "product images cannot be null");
        Assert.notEmpty(entity.getImages(), "product images cannot be empty");

        Assert.isTrue(checkBarcodeDigitUseCase.execute(entity.getBarcode().getCode()), "product barcode digit must be valid");

        return true;
    }
}
