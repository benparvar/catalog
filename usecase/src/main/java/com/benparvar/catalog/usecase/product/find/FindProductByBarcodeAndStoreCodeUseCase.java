package com.benparvar.catalog.usecase.product.find;

import com.benparvar.catalog.domain.entity.product.Product;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The type Find product by barcode and store code use case.
 */
public class FindProductByBarcodeAndStoreCodeUseCase {
    private final Logger log = LoggerFactory.getLogger(FindProductByBarcodeAndStoreCodeUseCase.class);
    private final FindProductByBarcodeAndStoreCode findProductByBarcodeAndStoreCodeGateway;

    /**
     * Instantiates a new Find product by barcode and store code use case.
     *
     * @param findProductByBarcodeAndStoreCodeGateway the find product by barcode and store code gateway
     */
    public FindProductByBarcodeAndStoreCodeUseCase(FindProductByBarcodeAndStoreCode findProductByBarcodeAndStoreCodeGateway) {
        this.findProductByBarcodeAndStoreCodeGateway = findProductByBarcodeAndStoreCodeGateway;
    }

    /**
     * Execute product.
     *
     * @param barcodeCode the barcode code
     * @param storeCode   the store code
     * @return the product
     */
    public Product execute(String barcodeCode, String storeCode) {
        log.info("execute barcode: {} storeCode: {}", barcodeCode, storeCode);

        return findProductByBarcodeAndStoreCodeGateway.findByBarcodeCodeAndStoreCode(barcodeCode, storeCode);
    }
}
