package com.benparvar.catalog.usecase.store.create;

import com.benparvar.catalog.domain.entity.store.Store;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The type Create store use case.
 */
public class CreateStoreUseCase {
    private final Logger log = LoggerFactory.getLogger(CreateStoreUseCase.class);
    private final ValidateStoreBeforeCreateUseCase validateStoreBeforeCreateUseCase;
    private final SaveStore saveStoreGateway;

    /**
     * Instantiates a new Create store use case.
     *
     * @param validateStoreBeforeCreateUseCase the validate store before create use case
     * @param saveStoreGateway                 the save store gateway
     */
    public CreateStoreUseCase(ValidateStoreBeforeCreateUseCase validateStoreBeforeCreateUseCase,
                              SaveStore saveStoreGateway) {
        this.validateStoreBeforeCreateUseCase = validateStoreBeforeCreateUseCase;
        this.saveStoreGateway = saveStoreGateway;
    }

    /**
     * Execute store.
     *
     * @param entity the entity
     * @return the store
     */
    public Store execute(Store entity) {
        log.info("execute: {}", entity);
        validateStoreBeforeCreateUseCase.execute(entity);

        return saveStoreGateway.save(entity);
    }
}
