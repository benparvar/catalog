package com.benparvar.catalog.usecase.barcode;

import com.benparvar.catalog.domain.entity.validator.Assert;
import com.benparvar.catalog.domain.entity.validator.checkdigit.BarcodeCheckDigit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The type Check barcode digit use case.
 */
public class CheckBarcodeDigitUseCase {
    private final Logger log = LoggerFactory.getLogger(CheckBarcodeDigitUseCase.class);
    private final BarcodeCheckDigit ean13CheckDigit;

    /**
     * Instantiates a new Check barcode digit use case.
     *
     * @param ean13CheckDigit the ean 13 check digit
     */
    public CheckBarcodeDigitUseCase(BarcodeCheckDigit ean13CheckDigit) {
        this.ean13CheckDigit = ean13CheckDigit;
    }

    /**
     * Execute boolean.
     *
     * @param barcode the barcode
     * @return the boolean
     */
    public boolean execute(String barcode) {
        log.info("execute: {}", barcode);
        Assert.notNull(barcode, "barcode cannot be null");

        return ean13CheckDigit.isValid(barcode);
    }
}
