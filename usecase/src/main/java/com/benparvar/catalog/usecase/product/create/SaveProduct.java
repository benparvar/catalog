package com.benparvar.catalog.usecase.product.create;

import com.benparvar.catalog.domain.entity.product.Product;

/**
 * The interface Save product.
 */
public interface SaveProduct {

    /**
     * Save product.
     *
     * @param entity the entity
     * @return the product
     */
    Product save(Product entity);
}
