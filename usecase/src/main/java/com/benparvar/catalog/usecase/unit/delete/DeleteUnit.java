package com.benparvar.catalog.usecase.unit.delete;

import com.benparvar.catalog.domain.entity.unit.Unit;

/**
 * The interface Delete unit.
 */
public interface DeleteUnit {

    /**
     * Delete.
     *
     * @param entity the entity
     */
    void delete(Unit entity);
}
