package com.benparvar.catalog.usecase.store.create;

import com.benparvar.catalog.domain.entity.store.Store;

/**
 * The interface Save store.
 */
public interface SaveStore {

    /**
     * Save store.
     *
     * @param entity the entity
     * @return the store
     */
    Store save(Store entity);
}
