package com.benparvar.catalog.usecase.store.find.all;

import com.benparvar.catalog.domain.entity.store.Store;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * The type Find all stores use case.
 */
public class FindAllStoresUseCase {
    private final Logger log = LoggerFactory.getLogger(FindAllStoresUseCase.class);
    private final FindAllStores findAllStoresGateway;

    /**
     * Instantiates a new Find all stores use case.
     *
     * @param findAllStoresGateway the find all stores gateway
     */
    public FindAllStoresUseCase(FindAllStores findAllStoresGateway) {
        this.findAllStoresGateway = findAllStoresGateway;
    }

    /**
     * Execute list.
     *
     * @return the list
     */
    public List<Store> execute() {
        log.info("execute:");
        return findAllStoresGateway.findAll();
    }
}
