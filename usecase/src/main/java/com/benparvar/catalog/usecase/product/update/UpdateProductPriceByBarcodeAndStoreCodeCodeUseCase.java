package com.benparvar.catalog.usecase.product.update;

import com.benparvar.catalog.domain.entity.product.Product;
import com.benparvar.catalog.domain.entity.product.UpdateProductPrice;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The type Update product price by barcode and store code code use case.
 */
public class UpdateProductPriceByBarcodeAndStoreCodeCodeUseCase {
    private final Logger log = LoggerFactory.getLogger(UpdateProductPriceByBarcodeAndStoreCodeCodeUseCase.class);
    private final UpdateProductPriceByBarcodeAndStoreCodeCode updateProductPriceByBarcodeAndStoreCodeCodeGateway;
    private final ValidateBeforeUpdatePriceUseCase validateBeforeUpdatePriceUseCase;

    /**
     * Instantiates a new Update product price by barcode and store code code use case.
     *
     * @param validatePriceBeforeUpdateUseCase                   the validate price before update use case
     * @param updateProductPriceByBarcodeAndStoreCodeCodeGateway the update product price by barcode and store code code gateway
     */
    public UpdateProductPriceByBarcodeAndStoreCodeCodeUseCase(
            ValidateBeforeUpdatePriceUseCase validatePriceBeforeUpdateUseCase,
            UpdateProductPriceByBarcodeAndStoreCodeCode updateProductPriceByBarcodeAndStoreCodeCodeGateway) {
        this.validateBeforeUpdatePriceUseCase = validatePriceBeforeUpdateUseCase;
        this.updateProductPriceByBarcodeAndStoreCodeCodeGateway = updateProductPriceByBarcodeAndStoreCodeCodeGateway;
    }

    /**
     * Execute product.
     *
     * @param updateProductPrice the update price product
     * @return the product
     */
    public Product execute(UpdateProductPrice updateProductPrice) {
        log.info("execute updateProductPrice: {}", updateProductPrice);

        validateBeforeUpdatePriceUseCase.execute(updateProductPrice);

        return updateProductPriceByBarcodeAndStoreCodeCodeGateway
                .updateProductPriceByBarcodeAndStoreCodeCode(updateProductPrice);
    }
}
