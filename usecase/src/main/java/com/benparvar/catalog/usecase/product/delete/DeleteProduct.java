package com.benparvar.catalog.usecase.product.delete;

import com.benparvar.catalog.domain.entity.product.Product;

/**
 * The interface Delete product.
 */
public interface DeleteProduct {

    /**
     * Delete.
     *
     * @param entity the entity
     */
    void delete(Product entity);
}
