package com.benparvar.catalog.usecase.product.create;

import com.benparvar.catalog.domain.entity.product.Product;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CreateProductUseCase {
    private final Logger log = LoggerFactory.getLogger(CreateProductUseCase.class);
    private final ValidateProductBeforeCreateUseCase validateProductBeforeCreateUseCase;
    private final SaveProduct saveProductGateway;

    public CreateProductUseCase(ValidateProductBeforeCreateUseCase validateProductBeforeCreateUseCase,
                                SaveProduct saveProductGateway) {
        this.validateProductBeforeCreateUseCase = validateProductBeforeCreateUseCase;
        this.saveProductGateway = saveProductGateway;
    }

    public Product execute(Product entity) {
        log.info("execute: {}", entity);
        validateProductBeforeCreateUseCase.execute(entity);

        return saveProductGateway.save(entity);
    }
}
