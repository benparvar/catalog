package com.benparvar.catalog.usecase.unit.create;

import com.benparvar.catalog.domain.entity.unit.Unit;

/**
 * The interface Save unit.
 */
public interface SaveUnit {

    /**
     * Save unit.
     *
     * @param entity the entity
     * @return the unit
     */
    Unit save(Unit entity);
}
