package com.benparvar.catalog.usecase.unit.create;

import com.benparvar.catalog.domain.entity.unit.Unit;
import com.benparvar.catalog.domain.entity.validator.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The type Validate unit before create use case.
 */
public class ValidateUnitBeforeCreateUseCase {
    private final Logger log = LoggerFactory.getLogger(ValidateUnitBeforeCreateUseCase.class);

    /**
     * Execute boolean.
     *
     * @param entity the entity
     * @return the boolean
     */
    public boolean execute(Unit entity) {
        log.info("execute: {}", entity);
        Assert.notNull(entity, "unit cannot be null");
        Assert.hasText(entity.getCode(), "unit code must have a text");
        Assert.hasText(entity.getName(), "unit name must have a text");

        return true;
    }
}
