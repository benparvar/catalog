package com.benparvar.catalog.usecase.unit.find.all;

import com.benparvar.catalog.domain.entity.unit.Unit;

import java.util.List;

/**
 * The interface Find all units.
 */
public interface FindAllUnits {
    /**
     * Find all list.
     *
     * @return the list
     */
    List<Unit> findAll();
}
