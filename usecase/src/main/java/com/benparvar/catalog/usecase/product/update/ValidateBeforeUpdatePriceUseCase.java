package com.benparvar.catalog.usecase.product.update;

import com.benparvar.catalog.domain.entity.product.UpdateProductPrice;
import com.benparvar.catalog.domain.entity.validator.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The type Validate price before update use case.
 */
public class ValidateBeforeUpdatePriceUseCase {
    private final Logger log = LoggerFactory.getLogger(ValidateBeforeUpdatePriceUseCase.class);

    /**
     * Execute boolean.
     *
     * @param updateProductPrice the update price product
     * @return the boolean
     */
    public boolean execute(UpdateProductPrice updateProductPrice) {
        log.info("execute updateProductPrice: {}", updateProductPrice);

        Assert.notNull(updateProductPrice.getPrice(), "product price cannot be null");
        Assert.isTrue(updateProductPrice.getPrice().signum() == 0 ||
                updateProductPrice.getPrice().signum() == 1, "product price cannot be negative");

        return Boolean.TRUE;
    }
}
