package com.benparvar.catalog.usecase.product.delete;

import com.benparvar.catalog.domain.entity.product.Product;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The type Delete product use case.
 */
public class DeleteProductUseCase {
    private final Logger log = LoggerFactory.getLogger(DeleteProductUseCase.class);
    private final ValidateProductBeforeDeleteUseCase validateProductBeforeDeleteUseCase;
    private final DeleteProduct deleteProductGateway;

    /**
     * Instantiates a new Delete product use case.
     *
     * @param validateProductBeforeDeleteUseCase the validate product before delete use case
     * @param deleteProductGateway               the delete product gateway
     */
    public DeleteProductUseCase(ValidateProductBeforeDeleteUseCase validateProductBeforeDeleteUseCase,
                                DeleteProduct deleteProductGateway) {
        this.validateProductBeforeDeleteUseCase = validateProductBeforeDeleteUseCase;
        this.deleteProductGateway = deleteProductGateway;
    }

    /**
     * Execute.
     *
     * @param entity the entity
     */
    public void execute(Product entity) {
        log.info("execute: {}", entity);

        validateProductBeforeDeleteUseCase.execute(entity);
        deleteProductGateway.delete(entity);
    }
}
