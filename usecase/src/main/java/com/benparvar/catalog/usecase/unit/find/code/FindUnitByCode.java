package com.benparvar.catalog.usecase.unit.find.code;

import com.benparvar.catalog.domain.entity.unit.Unit;

/**
 * The interface Find unit by code.
 */
public interface FindUnitByCode {

    /**
     * Find by code unit.
     *
     * @param code the code
     * @return the unit
     */
    Unit findByCode(String code);
}
