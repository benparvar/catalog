package com.benparvar.catalog.usecase.store.find.code;

import com.benparvar.catalog.domain.entity.store.Store;

/**
 * The interface Find store by code.
 */
public interface FindStoreByCode {

    /**
     * Find by code store.
     *
     * @param code the code
     * @return the store
     */
    Store findByCode(String code);
}
