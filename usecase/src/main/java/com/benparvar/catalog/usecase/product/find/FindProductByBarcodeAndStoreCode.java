package com.benparvar.catalog.usecase.product.find;

import com.benparvar.catalog.domain.entity.product.Product;

/**
 * The interface Find product by barcode and store code.
 */
public interface FindProductByBarcodeAndStoreCode {

    /**
     * Find by barcode code and store code product.
     *
     * @param barcodeCode the barcode code
     * @param storeCode   the store code
     * @return the product
     */
    Product findByBarcodeCodeAndStoreCode(String barcodeCode, String storeCode);
}
