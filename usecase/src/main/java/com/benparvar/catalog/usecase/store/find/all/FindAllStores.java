package com.benparvar.catalog.usecase.store.find.all;

import com.benparvar.catalog.domain.entity.store.Store;

import java.util.List;

/**
 * The interface Find all stores.
 */
public interface FindAllStores {
    /**
     * Find all list.
     *
     * @return the list
     */
    List<Store> findAll();
}
