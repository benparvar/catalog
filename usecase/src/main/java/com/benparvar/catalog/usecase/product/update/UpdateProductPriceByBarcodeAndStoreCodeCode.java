package com.benparvar.catalog.usecase.product.update;

import com.benparvar.catalog.domain.entity.product.Product;
import com.benparvar.catalog.domain.entity.product.UpdateProductPrice;

/**
 * The interface Update product price by barcode and store code code.
 */
public interface UpdateProductPriceByBarcodeAndStoreCodeCode {
    /**
     * Update product price by barcode and store code code product.
     *
     * @param updateProductPrice the update price product
     * @return the product
     */
    Product updateProductPriceByBarcodeAndStoreCodeCode(UpdateProductPrice updateProductPrice);

}
