module com.benparvar.catalog.usecase {
    exports com.benparvar.catalog.usecase.unit.create;
    exports com.benparvar.catalog.usecase.unit.find.all;
    exports com.benparvar.catalog.usecase.unit.find.code;
    exports com.benparvar.catalog.usecase.unit.delete;
    exports com.benparvar.catalog.usecase.unit.update;
    exports com.benparvar.catalog.usecase.store.create;
    exports com.benparvar.catalog.usecase.store.find.all;
    exports com.benparvar.catalog.usecase.store.find.code;
    exports com.benparvar.catalog.usecase.store.update;
    exports com.benparvar.catalog.usecase.store.delete;
    exports com.benparvar.catalog.usecase.product.create;
    exports com.benparvar.catalog.usecase.barcode;
    exports com.benparvar.catalog.usecase.product.find;
    exports com.benparvar.catalog.usecase.product.update;
    exports com.benparvar.catalog.usecase.product.delete;

    requires com.benparvar.catalog.domain;
    requires org.slf4j;
}