package com.benparvar.catalog.usecase.product.delete;

import com.benparvar.catalog.domain.entity.barcode.Barcode;
import com.benparvar.catalog.domain.entity.product.Product;
import com.benparvar.catalog.domain.entity.store.Store;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

/**
 * The type Delete product use case test.
 */
class DeleteProductUseCaseTest {
    private DeleteProductUseCase uc;
    private final ValidateProductBeforeDeleteUseCase validateProductBeforeDeleteUseCase
            = mock(ValidateProductBeforeDeleteUseCase.class);
    private final DeleteProduct deleteProductGateway = mock(DeleteProduct.class);

    /**
     * Init.
     */
    @BeforeEach
    public void init() {
        uc = new DeleteProductUseCase(validateProductBeforeDeleteUseCase, deleteProductGateway);
    }

    /**
     * Execute with a null entity will fail.
     */
    @Test
    public void executeWithANullEntityWillFail() {
        assertThrows(IllegalArgumentException.class, () -> {
            Product entity = null;

            when(validateProductBeforeDeleteUseCase.execute(entity))
                    .thenThrow(new IllegalArgumentException("product cannot be null"));

            try {
                uc.execute(entity);
            } catch (IllegalArgumentException e) {
                assertEquals("product cannot be null", e.getMessage());
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }


    /**
     * Execute with a entity already existent will success.
     */
    @Test
    public void executeWithAEntityAlreadyExistentWillSuccess() {
        final String barcodeCode = "3174780006235";
        final String storeCode = "123";
        Product entity = Product.newBuilder()
                .barcode(Barcode.newBuilder().code(barcodeCode).build())
                .store(Store.newBuilder().code(storeCode).build())
                .build();

        when(validateProductBeforeDeleteUseCase.execute(entity))
                .thenReturn(true);

        assertDoesNotThrow(() -> {
            uc.execute(entity);

        });

        verify(validateProductBeforeDeleteUseCase, times(1)).execute(entity);
        verify(deleteProductGateway, times(1)).delete(entity);
    }


    /**
     * Execute with a entity non existent will fail.
     */
    @Test
    public void executeWithAEntityNonExistentWillFail() {
        // TODO -- Implement and catch ProductNotFoundException
    }
}