package com.benparvar.catalog.usecase.product.update;

import com.benparvar.catalog.domain.entity.product.Product;
import com.benparvar.catalog.domain.entity.product.UpdateProductPrice;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.times;

/**
 * The type Update product price by barcode and store code code use case test.
 */
class UpdateProductPriceByBarcodeAndStoreCodeCodeUseCaseTest {
    private UpdateProductPriceByBarcodeAndStoreCodeCodeUseCase uc;
    private UpdateProductPriceByBarcodeAndStoreCodeCode updateProductPriceByBarcodeAndStoreCodeCodeGateway
            = mock(UpdateProductPriceByBarcodeAndStoreCodeCode.class);
    private ValidateBeforeUpdatePriceUseCase validatePriceBeforeUpdateUseCase = mock(ValidateBeforeUpdatePriceUseCase.class);

    /**
     * Init.
     */
    @BeforeEach
    public void init() {
        uc = new UpdateProductPriceByBarcodeAndStoreCodeCodeUseCase(validatePriceBeforeUpdateUseCase,
                updateProductPriceByBarcodeAndStoreCodeCodeGateway);
    }

    /**
     * Execute with null price will fail.
     */
    @Test
    public void executeWithNullPriceWillFail() {
        String barcodeCode = "3174780006235";
        String storeCode = "123";
        BigDecimal price = null;

        UpdateProductPrice updateProductPrice = UpdateProductPrice.newBuilder()
                .barcode(barcodeCode)
                .storeCode(storeCode)
                .price(price)
                .build();

        when(validatePriceBeforeUpdateUseCase.execute(updateProductPrice)).thenThrow(new IllegalArgumentException("product price cannot be null"));
        assertThrows(IllegalArgumentException.class, () -> {
            try {
                Product result = uc.execute(updateProductPrice);
            } catch (IllegalArgumentException e) {
                verify(validatePriceBeforeUpdateUseCase, times(1)).execute(updateProductPrice);
                verify(updateProductPriceByBarcodeAndStoreCodeCodeGateway, times(0)).updateProductPriceByBarcodeAndStoreCodeCode(updateProductPrice);
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }

    /**
     * Execute with valid price will success.
     */
    @Test
    public void executeWithValidPriceWillSuccess() {
        String barcodeCode = "3174780006235";
        String storeCode = "123";
        BigDecimal price = BigDecimal.valueOf(3.19);
        Product product = Product.newBuilder().build();

        UpdateProductPrice updateProductPrice = UpdateProductPrice.newBuilder()
                .barcode(barcodeCode)
                .storeCode(storeCode)
                .price(price)
                .build();

        when(validatePriceBeforeUpdateUseCase.execute(updateProductPrice)).thenReturn(Boolean.TRUE);
        when(updateProductPriceByBarcodeAndStoreCodeCodeGateway.updateProductPriceByBarcodeAndStoreCodeCode(updateProductPrice)).thenReturn(product);

        Product result = uc.execute(updateProductPrice);

        assertNotNull(result);
        verify(validatePriceBeforeUpdateUseCase, times(1)).execute(updateProductPrice);
        verify(updateProductPriceByBarcodeAndStoreCodeCodeGateway, times(1)).updateProductPriceByBarcodeAndStoreCodeCode(updateProductPrice);
    }
}