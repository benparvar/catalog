package com.benparvar.catalog.usecase.store.find.all;

import com.benparvar.catalog.domain.entity.store.Store;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * The type Find all stores use case test.
 */
class FindAllStoresUseCaseTest {
    private final FindAllStores findAllStoresGateway = mock(FindAllStores.class);
    private FindAllStoresUseCase uc;

    /**
     * Init.
     */
    @BeforeEach
    public void init() {
        uc = new FindAllStoresUseCase(findAllStoresGateway);
    }

    /**
     * Execute with no stores in repository will return an empty list.
     */
    @Test
    public void executeWithNoStoresInRepositoryWillReturnAnEmptyList() {
        when(findAllStoresGateway.findAll()).thenReturn(Collections.EMPTY_LIST);

        List<Store> result = uc.execute();

        assertNotNull(result);
        assertTrue(result.isEmpty());
    }

    /**
     * Execute with stores in repository will return an empty list.
     */
    @Test
    public void executeWithStoresInRepositoryWillReturnAnEmptyList() {
        when(findAllStoresGateway.findAll()).thenReturn(Arrays.asList(Store.newBuilder().code("PC").name("PIECES").build(),
                Store.newBuilder().code("MP").name("MILIPIECES").build()));

        List<Store> result = uc.execute();

        assertNotNull(result);
        assertFalse(result.isEmpty());
        assertEquals(2, result.size());
    }
}