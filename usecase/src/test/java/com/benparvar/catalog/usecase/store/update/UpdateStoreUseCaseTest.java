package com.benparvar.catalog.usecase.store.update;

import com.benparvar.catalog.domain.entity.store.Store;
import com.benparvar.catalog.domain.entity.store.exception.StoreNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

/**
 * The type Update store use case test.
 */
class UpdateStoreUseCaseTest {
    private  UpdateStoreUseCase uc;
    private final ValidateStoreBeforeUpdateUseCase validateStoreBeforeUpdateUseCase = mock(ValidateStoreBeforeUpdateUseCase.class);
    private final UpdateStore updateStoreGateway = mock(UpdateStore.class);

    /**
     * Init.
     */
    @BeforeEach
    public void init() {
        uc = new UpdateStoreUseCase(validateStoreBeforeUpdateUseCase, updateStoreGateway);
    }

    /**
     * Execute with a null entity will fail.
     */
    @Test
    public void executeWithANullEntityWillFail() {
        assertThrows(IllegalArgumentException.class, () -> {
            Store entity = null;

            when(validateStoreBeforeUpdateUseCase.execute(entity))
                    .thenThrow(new IllegalArgumentException("store cannot be null"));

            try {
                uc.execute(entity);
            } catch (IllegalArgumentException e) {
                assertEquals("store cannot be null", e.getMessage());
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }

    /**
     * Execute with a entity already existent will sucess.
     */
    @Test
    public void executeWithAEntityAlreadyExistentWillSucess() {
        Store entity = Store.newBuilder().code("PC").name("PIECE").build();
        when(validateStoreBeforeUpdateUseCase.execute(entity))
                .thenReturn(true);
        when(updateStoreGateway.update(entity)).thenReturn(entity);

        Store result = uc.execute(entity);

        assertNotNull(result);
        assertEquals(entity, result);

        verify(validateStoreBeforeUpdateUseCase, times(1)).execute(entity);
        verify(updateStoreGateway, times(1)).update(entity);
    }

    /**
     * Execute with a entity non existent will fail.
     */
    @Test
    public void executeWithAEntityNonExistentWillFail() {
        assertThrows(StoreNotFoundException.class, () -> {
            Store entity = Store.newBuilder().code("PC").name("PIECE").build();
            when(validateStoreBeforeUpdateUseCase.execute(entity))
                    .thenReturn(true);
            when(updateStoreGateway.update(entity)).thenThrow(new StoreNotFoundException());

            try {
                uc.execute(entity);
            } catch (StoreNotFoundException e) {
                verify(validateStoreBeforeUpdateUseCase, times(1)).execute(entity);
                verify(updateStoreGateway, times(1)).update(entity);

                throw e;
            }

            fail("should throw an StoreNotFoundException");
        });

    }
}