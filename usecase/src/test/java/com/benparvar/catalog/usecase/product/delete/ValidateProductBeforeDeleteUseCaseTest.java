package com.benparvar.catalog.usecase.product.delete;

import com.benparvar.catalog.domain.entity.barcode.Barcode;
import com.benparvar.catalog.domain.entity.product.Product;
import com.benparvar.catalog.domain.entity.store.Store;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * The type Validate product before delete use case test.
 */
class ValidateProductBeforeDeleteUseCaseTest {
    private ValidateProductBeforeDeleteUseCase uc;

    /**
     * Init.
     */
    @BeforeEach
    public void init() {
        uc = new ValidateProductBeforeDeleteUseCase();
    }

    /**
     * Execute with a null product will fail.
     */
    @Test
    public void executeWithANullProductWillFail() {
        assertThrows(IllegalArgumentException.class, () -> {
            Product entity = null;

            try {
                uc.execute(entity);
            } catch (IllegalArgumentException e) {
                assertEquals("product cannot be null", e.getMessage());
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }

    /**
     * Execute with empty barcode code and store code will fail.
     */
    @Test
    public void executeWithEmptyBarcodeCodeAndStoreCodeWillFail() {
        assertThrows(IllegalArgumentException.class, () -> {
            Product entity = Product.newBuilder().build();

            try {
                uc.execute(entity);
            } catch (IllegalArgumentException e) {
                assertEquals("barcode cannot be null", e.getMessage());
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }

    /**
     * Execute with empty barcode code will fail.
     */
    @Test
    public void executeWithEmptyBarcodeCodeWillFail() {
        assertThrows(IllegalArgumentException.class, () -> {
            final String barcodeCode = "";
            final String storeCode = "123";
            Product entity = Product.newBuilder()
                    .barcode(Barcode.newBuilder().code(barcodeCode).build())
                    .store(Store.newBuilder().code(storeCode).build())
                    .build();

            try {
                uc.execute(entity);
            } catch (IllegalArgumentException e) {
                assertEquals("barcode code must have a text", e.getMessage());
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }

    /**
     * Execute with empty store code will fail.
     */
    @Test
    public void executeWithEmptyStoreCodeWillFail() {
        assertThrows(IllegalArgumentException.class, () -> {
            final String barcodeCode = "3174780006235";
            final String storeCode = "";
            Product entity = Product.newBuilder()
                    .barcode(Barcode.newBuilder().code(barcodeCode).build())
                    .store(Store.newBuilder().code(storeCode).build())
                    .build();

            try {
                uc.execute(entity);
            } catch (IllegalArgumentException e) {
                assertEquals("store code must have a text", e.getMessage());
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }

    /**
     * Execute with a valid product will success.
     */
    @Test
    public void executeWithAValidProductWillSuccess() {
        final String barcodeCode = "3174780006235";
        final String storeCode = "123";
        Product entity = Product.newBuilder()
                .barcode(Barcode.newBuilder().code(barcodeCode).build())
                .store(Store.newBuilder().code(storeCode).build())
                .build();

        boolean result = uc.execute(entity);

        assertTrue(result, "must be a valid product");
    }
}