package com.benparvar.catalog.usecase.unit.find.code;

import com.benparvar.catalog.domain.entity.unit.Unit;
import com.benparvar.catalog.domain.entity.unit.exception.UnitNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

/**
 * The type Find unit by code use case test.
 */
class FindUnitByCodeUseCaseTest {
    private final FindUnitByCode findUnitByCodeGateway = mock(FindUnitByCode.class);
    private FindUnitByCodeUseCase uc;

    /**
     * Init.
     */
    @BeforeEach
    public void init() {
        uc = new FindUnitByCodeUseCase(findUnitByCodeGateway);
    }

    /**
     * Execute with null code will fail.
     */
    @Test
    public void executeWithNullCodeWillFail() {
        String code = null;
        when(findUnitByCodeGateway.findByCode(code)).thenThrow(new UnitNotFoundException());
        assertThrows(UnitNotFoundException.class, () -> {
            try {
                Unit result = uc.execute(code);
            } catch (UnitNotFoundException e) {
                verify(findUnitByCodeGateway, times(1)).findByCode(code);
                throw e;
            }

            fail("should throw an UnitNotFoundException");
        });
    }

    /**
     * Execute with empty code will fail.
     */
    @Test
    public void executeWithEmptyCodeWillFail() {
        String code = "";
        when(findUnitByCodeGateway.findByCode(code)).thenThrow(new UnitNotFoundException());
        assertThrows(UnitNotFoundException.class, () -> {
            try {
                Unit result = uc.execute(code);
            } catch (UnitNotFoundException e) {
                verify(findUnitByCodeGateway, times(1)).findByCode(code);
                throw e;
            }

            fail("should throw an UnitNotFoundException");
        });
    }

    /**
     * Execute with no units in repository will fail.
     */
    @Test
    public void executeWithNoUnitsInRepositoryWillFail() {
        String code = "PC";
        when(findUnitByCodeGateway.findByCode(code)).thenThrow(new UnitNotFoundException());
        assertThrows(UnitNotFoundException.class, () -> {
            try {
                Unit result = uc.execute(code);
            } catch (UnitNotFoundException e) {
                verify(findUnitByCodeGateway, times(1)).findByCode(code);
                throw e;
            }

            fail("should throw an UnitNotFoundException");
        });
    }

    /**
     * Execute with units in repository will return an entity.
     */
    @Test
    public void executeWithUnitsInRepositoryWillReturnAnEntity() {
        String code = "MP";
        when(findUnitByCodeGateway.findByCode(code)).thenReturn(Unit.newBuilder().code("MP").name("MILIPIECES").build());

        Unit result = uc.execute(code);

        assertNotNull(result);
        assertEquals("MP", result.getCode());
        assertEquals("MILIPIECES", result.getName());
    }
}