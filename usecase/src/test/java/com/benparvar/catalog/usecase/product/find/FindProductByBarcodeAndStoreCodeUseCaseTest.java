package com.benparvar.catalog.usecase.product.find;

import com.benparvar.catalog.domain.entity.product.Product;
import com.benparvar.catalog.domain.entity.product.exception.ProductNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

/**
 * The type Find product by barcode and store code use case test.
 */
class FindProductByBarcodeAndStoreCodeUseCaseTest {
    private final FindProductByBarcodeAndStoreCode findProductByBarcodeAndStoreCodeGateway = mock(FindProductByBarcodeAndStoreCode.class);
    private FindProductByBarcodeAndStoreCodeUseCase uc;

    /**
     * Init.
     */
    @BeforeEach
    public void init() {
        uc = new FindProductByBarcodeAndStoreCodeUseCase(findProductByBarcodeAndStoreCodeGateway);
    }

    /**
     * Execute with null barcode and null store code code will fail.
     */
    @Test
    public void executeWithNullBarcodeAndNullStoreCodeCodeWillFail() {
        String barcodeCode = null;
        String storeCode = null;
        when(findProductByBarcodeAndStoreCodeGateway.findByBarcodeCodeAndStoreCode(barcodeCode, storeCode)).thenThrow(new ProductNotFoundException());
        assertThrows(ProductNotFoundException.class, () -> {
            try {
                Product result = uc.execute(barcodeCode, storeCode);
            } catch (ProductNotFoundException e) {
                verify(findProductByBarcodeAndStoreCodeGateway, times(1)).findByBarcodeCodeAndStoreCode(barcodeCode, storeCode);
                throw e;
            }

            fail("should throw an ProductNotFoundException");
        });
    }

    /**
     * Execute with empty barcode and empty store code code will fail.
     */
    @Test
    public void executeWithEmptyBarcodeAndEmptyStoreCodeCodeWillFail() {
        String barcodeCode = "";
        String storeCode = "";
        when(findProductByBarcodeAndStoreCodeGateway.findByBarcodeCodeAndStoreCode(barcodeCode, storeCode)).thenThrow(new ProductNotFoundException());
        assertThrows(ProductNotFoundException.class, () -> {
            try {
                Product result = uc.execute(barcodeCode, storeCode);
            } catch (ProductNotFoundException e) {
                verify(findProductByBarcodeAndStoreCodeGateway, times(1)).findByBarcodeCodeAndStoreCode(barcodeCode, storeCode);
                throw e;
            }

            fail("should throw an ProductNotFoundException");
        });
    }

    /**
     * Execute with existent product and valid barcode and valid store code code will success.
     */
    @Test
    public void executeWithExistentProductAndValidBarcodeAndValidStoreCodeCodeWillSuccess() {
        String barcodeCode = "7894900011517";
        String storeCode = "741";
        when(findProductByBarcodeAndStoreCodeGateway.findByBarcodeCodeAndStoreCode(barcodeCode, storeCode)).thenReturn(Product.newBuilder().build());

        Product result = uc.execute(barcodeCode, storeCode);

        assertNotNull(result);
        verify(findProductByBarcodeAndStoreCodeGateway, times(1)).findByBarcodeCodeAndStoreCode(barcodeCode, storeCode);
    }
}