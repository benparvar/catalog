package com.benparvar.catalog.usecase.store.find.code;

import com.benparvar.catalog.domain.entity.store.Store;
import com.benparvar.catalog.domain.entity.store.exception.StoreNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

/**
 * The type Find store by code use case test.
 */
class FindStoreByCodeUseCaseTest {
    private final FindStoreByCode findStoreByCodeGateway = mock(FindStoreByCode.class);
    private FindStoreByCodeUseCase uc;

    /**
     * Init.
     */
    @BeforeEach
    public void init() {
        uc = new FindStoreByCodeUseCase(findStoreByCodeGateway);
    }

    /**
     * Execute with null code will fail.
     */
    @Test
    public void executeWithNullCodeWillFail() {
        String code = null;
        when(findStoreByCodeGateway.findByCode(code)).thenThrow(new StoreNotFoundException());
        assertThrows(StoreNotFoundException.class, () -> {
            try {
                Store result = uc.execute(code);
            } catch (StoreNotFoundException e) {
                verify(findStoreByCodeGateway, times(1)).findByCode(code);
                throw e;
            }

            fail("should throw an StoreNotFoundException");
        });
    }

    /**
     * Execute with empty code will fail.
     */
    @Test
    public void executeWithEmptyCodeWillFail() {
        String code = "";
        when(findStoreByCodeGateway.findByCode(code)).thenThrow(new StoreNotFoundException());
        assertThrows(StoreNotFoundException.class, () -> {
            try {
                Store result = uc.execute(code);
            } catch (StoreNotFoundException e) {
                verify(findStoreByCodeGateway, times(1)).findByCode(code);
                throw e;
            }

            fail("should throw an StoreNotFoundException");
        });
    }

    /**
     * Execute with no stores in repository will fail.
     */
    @Test
    public void executeWithNoStoresInRepositoryWillFail() {
        String code = "PC";
        when(findStoreByCodeGateway.findByCode(code)).thenThrow(new StoreNotFoundException());
        assertThrows(StoreNotFoundException.class, () -> {
            try {
                Store result = uc.execute(code);
            } catch (StoreNotFoundException e) {
                verify(findStoreByCodeGateway, times(1)).findByCode(code);
                throw e;
            }

            fail("should throw an StoreNotFoundException");
        });
    }

    /**
     * Execute with stores in repository will return an entity.
     */
    @Test
    public void executeWithStoresInRepositoryWillReturnAnEntity() {
        String code = "MP";
        when(findStoreByCodeGateway.findByCode(code)).thenReturn(Store.newBuilder().code("MP").name("MILIPIECES").build());

        Store result = uc.execute(code);

        assertNotNull(result);
        assertEquals("MP", result.getCode());
        assertEquals("MILIPIECES", result.getName());
    }

}