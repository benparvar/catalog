package com.benparvar.catalog.usecase.product.create;

import com.benparvar.catalog.domain.entity.product.Product;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.times;

/**
 * The type Create product use case test.
 */
class CreateProductUseCaseTest {
    private CreateProductUseCase uc;
    private final ValidateProductBeforeCreateUseCase validateProductBeforeCreateUseCase  = mock(ValidateProductBeforeCreateUseCase.class);
    private final SaveProduct saveProductGateway = mock(SaveProduct.class);

    /**
     * Init.
     */
    @BeforeEach
    public void init() {
        uc = new CreateProductUseCase(validateProductBeforeCreateUseCase, saveProductGateway);
    }


    /**
     * Execute with a null entity will fail.
     */
    @Test
    public void executeWithANullEntityWillFail() {
        assertThrows(IllegalArgumentException.class, () -> {
            Product entity = null;

            when(validateProductBeforeCreateUseCase.execute(entity))
                    .thenThrow(new IllegalArgumentException("product cannot be null"));

            try {
                uc.execute(entity);
            } catch (IllegalArgumentException e) {
                assertEquals("product cannot be null", e.getMessage());
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }


    /**
     * Execute with a entity non existent will success.
     */
    @Test
    public void executeWithAEntityNonExistentWillSuccess() {
        Product entity = Product.newBuilder().code("123").name("Bullet").build();
        when(validateProductBeforeCreateUseCase.execute(entity))
                .thenReturn(true);
        when(saveProductGateway.save(entity)).thenReturn(entity);

        Product result = uc.execute(entity);

        assertNotNull(result);
        assertEquals(entity, result);

        verify(validateProductBeforeCreateUseCase, times(1)).execute(entity);
        verify(saveProductGateway, times(1)).save(entity);
    }
}