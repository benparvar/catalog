package com.benparvar.catalog.usecase.product.update;

import com.benparvar.catalog.domain.entity.product.UpdateProductPrice;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.*;

/**
 * The type Validate price before update use case test.
 */
class ValidatePriceBeforeUpdateUseCaseTest {
    private ValidateBeforeUpdatePriceUseCase uc;

    /**
     * Init.
     */
    @BeforeEach
    public void init() {
        uc = new ValidateBeforeUpdatePriceUseCase();
    }

    /**
     * Execute with null price will fail.
     */
    @Test
    public void executeWithNullPriceWillFail() {
        String barcodeCode = "3174780006235";
        String storeCode = "123";
        BigDecimal price = null;

        UpdateProductPrice updateProductPrice = UpdateProductPrice.newBuilder()
                .barcode(barcodeCode)
                .storeCode(storeCode)
                .price(price)
                .build();

        assertThrows(IllegalArgumentException.class, () -> {
            try {
                boolean result = uc.execute(updateProductPrice);
            } catch (IllegalArgumentException e) {
                assertEquals("product price cannot be null", e.getMessage());
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }

    /**
     * Execute with negative price will fail.
     */
    @Test
    public void executeWithNegativePriceWillFail() {
        String barcodeCode = "3174780006235";
        String storeCode = "123";
        BigDecimal price = BigDecimal.valueOf(-0.001);

        UpdateProductPrice updateProductPrice = UpdateProductPrice.newBuilder()
                .barcode(barcodeCode)
                .storeCode(storeCode)
                .price(price)
                .build();

        assertThrows(IllegalArgumentException.class, () -> {
            try {
                boolean result = uc.execute(updateProductPrice);
            } catch (IllegalArgumentException e) {
                assertEquals("product price cannot be negative", e.getMessage());
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }

    /**
     * Execute with zero price will success.
     */
    @Test
    public void executeWithZeroPriceWillSuccess() {
        String barcodeCode = "3174780006235";
        String storeCode = "123";
        BigDecimal price = BigDecimal.valueOf(0.00000);

        UpdateProductPrice updateProductPrice = UpdateProductPrice.newBuilder()
                .barcode(barcodeCode)
                .storeCode(storeCode)
                .price(price)
                .build();


        boolean result = uc.execute(updateProductPrice);
        assertTrue(result);
    }

    /**
     * Execute with greater than zero price will success.
     */
    @Test
    public void executeWithGreaterThanZeroPriceWillSuccess() {
        String barcodeCode = "3174780006235";
        String storeCode = "123";
        BigDecimal price = BigDecimal.valueOf(0.000001);

        UpdateProductPrice updateProductPrice = UpdateProductPrice.newBuilder()
                .barcode(barcodeCode)
                .storeCode(storeCode)
                .price(price)
                .build();

        boolean result = uc.execute(updateProductPrice);
        assertTrue(result);
    }
}