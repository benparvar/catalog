package com.benparvar.catalog.usecase.store.create;

import com.benparvar.catalog.domain.entity.store.Store;
import com.benparvar.catalog.domain.entity.store.exception.StoreAlreadyExistException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

/**
 * The type Create store use case test.
 */
class CreateStoreUseCaseTest {
    private CreateStoreUseCase uc;
    private final ValidateStoreBeforeCreateUseCase validateStoreBeforeCreateUseCase = mock(ValidateStoreBeforeCreateUseCase.class);
    private final SaveStore saveStoreGateway = mock(SaveStore.class);

    /**
     * Init.
     */
    @BeforeEach
    public void init() {
        uc = new CreateStoreUseCase(validateStoreBeforeCreateUseCase, saveStoreGateway);
    }

    /**
     * Execute with a null entity will fail.
     */
    @Test
    public void executeWithANullEntityWillFail() {
        assertThrows(IllegalArgumentException.class, () -> {
            Store entity = null;

            when(validateStoreBeforeCreateUseCase.execute(entity))
                    .thenThrow(new IllegalArgumentException("unit cannot be null"));

            try {
                uc.execute(entity);
            } catch (IllegalArgumentException e) {
                assertEquals("unit cannot be null", e.getMessage());
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }

    /**
     * Execute with a entity already existent will fail.
     */
    @Test
    public void executeWithAEntityAlreadyExistentWillFail() {
        assertThrows(StoreAlreadyExistException.class, () -> {
            Store entity = Store.newBuilder().code("PC").name("PIECE").build();
            when(validateStoreBeforeCreateUseCase.execute(entity))
                    .thenReturn(true);
            when(saveStoreGateway.save(entity)).thenThrow(new StoreAlreadyExistException());

            try {
                uc.execute(entity);
            } catch (StoreAlreadyExistException e) {
                verify(validateStoreBeforeCreateUseCase, times(1)).execute(entity);
                verify(saveStoreGateway, times(1)).save(entity);

                throw e;
            }

            fail("should throw an StoreAlreadyExistException");
        });
    }

    /**
     * Execute with a entity non existent will success.
     */
    @Test
    public void executeWithAEntityNonExistentWillSuccess() {
        Store entity = Store.newBuilder().code("PC").name("PIECE").build();
        when(validateStoreBeforeCreateUseCase.execute(entity))
                .thenReturn(true);
        when(saveStoreGateway.save(entity)).thenReturn(entity);

        Store result = uc.execute(entity);

        assertNotNull(result);
        assertEquals(entity, result);

        verify(validateStoreBeforeCreateUseCase, times(1)).execute(entity);
        verify(saveStoreGateway, times(1)).save(entity);
    }
}