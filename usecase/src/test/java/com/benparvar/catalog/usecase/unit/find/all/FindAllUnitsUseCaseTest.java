package com.benparvar.catalog.usecase.unit.find.all;

import com.benparvar.catalog.domain.entity.unit.Unit;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * The type Find all units use case test.
 */
class FindAllUnitsUseCaseTest {
    private final FindAllUnits findAllUnitsGateway = mock(FindAllUnits.class);
    private FindAllUnitsUseCase uc;

    /**
     * Init.
     */
    @BeforeEach
    public void init() {
        uc = new FindAllUnitsUseCase(findAllUnitsGateway);
    }

    /**
     * Execute with no units in repository will return an empty list.
     */
    @Test
    public void executeWithNoUnitsInRepositoryWillReturnAnEmptyList() {
        when(findAllUnitsGateway.findAll()).thenReturn(Collections.EMPTY_LIST);

        List<Unit> result = uc.execute();

        assertNotNull(result);
        assertTrue(result.isEmpty());
    }

    /**
     * Execute with units in repository will return an empty list.
     */
    @Test
    public void executeWithUnitsInRepositoryWillReturnAnEmptyList() {
        when(findAllUnitsGateway.findAll()).thenReturn(Arrays.asList(Unit.newBuilder().code("PC").name("PIECES").build(),
                Unit.newBuilder().code("MP").name("MILIPIECES").build()));

        List<Unit> result = uc.execute();

        assertNotNull(result);
        assertFalse(result.isEmpty());
        assertEquals(2, result.size());
    }
}