package com.benparvar.catalog.usecase.barcode;

import com.benparvar.catalog.domain.entity.validator.checkdigit.BarcodeCheckDigit;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * The type Check barcode digit use case test.
 */
class CheckBarcodeDigitUseCaseTest {
    private CheckBarcodeDigitUseCase uc;
    private BarcodeCheckDigit barcodeCheckDigit = mock(BarcodeCheckDigit.class);

    /**
     * Init.
     */
    @BeforeEach
    public void init() {
        uc = new CheckBarcodeDigitUseCase(barcodeCheckDigit);
    }

    /**
     * Execute with a valid barcode will success.
     */
    @Test
    public void executeWithAValidBarcodeWillSuccess() {
        String barcode = "7896022204242";

        when(barcodeCheckDigit.isValid(barcode)).thenReturn(true);
        boolean result = uc.execute(barcode);

        assertNotNull(result);
        assertEquals(true, result);
    }
}