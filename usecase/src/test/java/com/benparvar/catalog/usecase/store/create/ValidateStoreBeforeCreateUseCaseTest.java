package com.benparvar.catalog.usecase.store.create;

import com.benparvar.catalog.domain.entity.store.Store;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * The type Validate store before create use case test.
 */
class ValidateStoreBeforeCreateUseCaseTest {
    private ValidateStoreBeforeCreateUseCase uc;

    /**
     * Init.
     */
    @BeforeEach
    public void init() {
        uc = new ValidateStoreBeforeCreateUseCase();
    }

    /**
     * Execute with a null store will fail.
     */
    @Test
    public void executeWithANullStoreWillFail() {
        assertThrows(IllegalArgumentException.class, () -> {
            Store entity = null;

            try {
                uc.execute(entity);
            } catch (IllegalArgumentException e) {
                assertEquals("store cannot be null", e.getMessage());
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }

    /**
     * Execute with empty code and name will fail.
     */
    @Test
    public void executeWithEmptyCodeAndNameWillFail() {
        assertThrows(IllegalArgumentException.class, () -> {
            Store entity = Store.newBuilder().build();

            try {
                uc.execute(entity);
            } catch (IllegalArgumentException e) {
                assertEquals("store code must have a text", e.getMessage());
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }

    /**
     * Execute with empty code will fail.
     */
    @Test
    public void executeWithEmptyCodeWillFail() {
        assertThrows(IllegalArgumentException.class, () -> {
            Store entity = Store.newBuilder().name("this is a name").build();

            try {
                uc.execute(entity);
            } catch (IllegalArgumentException e) {
                assertEquals("store code must have a text", e.getMessage());
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }

    /**
     * Execute with empty name will fail.
     */
    @Test
    public void executeWithEmptyNameWillFail() {
        assertThrows(IllegalArgumentException.class, () -> {
            Store entity = Store.newBuilder().code("this is a code").build();

            try {
                uc.execute(entity);
            } catch (IllegalArgumentException e) {
                assertEquals("store name must have a text", e.getMessage());
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }

    /**
     * Execute with a valid store will success.
     */
    @Test
    public void executeWithAValidStoreWillSuccess() {
        Store entity = Store.newBuilder().code("this is a code").name("this is a name").build();

        boolean result = uc.execute(entity);

        assertTrue(result, "must be a valid store");
    }
}