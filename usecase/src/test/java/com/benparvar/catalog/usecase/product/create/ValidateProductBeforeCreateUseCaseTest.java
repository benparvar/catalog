package com.benparvar.catalog.usecase.product.create;

import com.benparvar.catalog.domain.entity.barcode.Barcode;
import com.benparvar.catalog.domain.entity.barcode.BarcodeType;
import com.benparvar.catalog.domain.entity.image.Image;
import com.benparvar.catalog.domain.entity.product.Product;
import com.benparvar.catalog.domain.entity.store.Store;
import com.benparvar.catalog.domain.entity.unit.Unit;
import com.benparvar.catalog.usecase.barcode.CheckBarcodeDigitUseCase;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * The type Validate product before create use case test.
 */
class ValidateProductBeforeCreateUseCaseTest {
    private ValidateProductBeforeCreateUseCase uc;
    private CheckBarcodeDigitUseCase checkBarcodeDigitUseCase = mock(CheckBarcodeDigitUseCase.class);

    /**
     * Init.
     */
    @BeforeEach
    public void init() {
        uc = new ValidateProductBeforeCreateUseCase(checkBarcodeDigitUseCase);
    }

    /**
     * Execute with a null product will fail.
     */
    @Test
    public void executeWithANullProductWillFail() {
        assertThrows(IllegalArgumentException.class, () -> {
            Product entity = null;

            try {
                uc.execute(entity);
            } catch (IllegalArgumentException e) {
                assertEquals("product cannot be null", e.getMessage());
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }

    /**
     * Execute with a empty product code will fail.
     */
    @Test
    public void executeWithAEmptyProductCodeWillFail() {
        assertThrows(IllegalArgumentException.class, () -> {
            Product entity = Product.newBuilder().build();

            try {
                uc.execute(entity);
            } catch (IllegalArgumentException e) {
                assertEquals("product code must have a text", e.getMessage());
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }

    /**
     * Execute with a empty product name will fail.
     */
    @Test
    public void executeWithAEmptyProductNameWillFail() {
        assertThrows(IllegalArgumentException.class, () -> {
            String productCode = "123";

            Product entity = Product.newBuilder()
                    .code(productCode)
                    .build();

            try {
                uc.execute(entity);
            } catch (IllegalArgumentException e) {
                assertEquals("product name must have a text", e.getMessage());
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }

    /**
     * Execute with a empty product description will fail.
     */
    @Test
    public void executeWithAEmptyProductDescriptionWillFail() {
        assertThrows(IllegalArgumentException.class, () -> {
            String productCode = "123";
            String productName = "Royk sopp";

            Product entity = Product.newBuilder()
                    .code(productCode)
                    .name(productName)
                    .build();

            try {
                uc.execute(entity);
            } catch (IllegalArgumentException e) {
                assertEquals("product description must have a text", e.getMessage());
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }

    /**
     * Execute with a null product barcode will fail.
     */
    @Test
    public void executeWithANullProductBarcodeWillFail() {
        assertThrows(IllegalArgumentException.class, () -> {
            String productCode = "123";
            String productName = "Royk sopp";
            String productDescription = "The best soup";

            Product entity = Product.newBuilder()
                    .code(productCode)
                    .name(productName)
                    .description(productDescription)
                    .build();

            try {
                uc.execute(entity);
            } catch (IllegalArgumentException e) {
                assertEquals("product barcode cannot be null", e.getMessage());
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }

    /**
     * Execute with a empty product barcode type will fail.
     */
    @Test
    public void executeWithAEmptyProductBarcodeTypeWillFail() {
        assertThrows(IllegalArgumentException.class, () -> {
            String productCode = "123";
            String productName = "Royk sopp";
            String productDescription = "The best soup";
            Barcode barcode = Barcode.newBuilder()
                    .build();

            Product entity = Product.newBuilder()
                    .code(productCode)
                    .name(productName)
                    .description(productDescription)
                    .barcode(barcode)
                    .build();

            try {
                uc.execute(entity);
            } catch (IllegalArgumentException e) {
                assertEquals("product barcode type cannot be null", e.getMessage());
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }

    /**
     * Execute with a empty product barcode code will fail.
     */
    @Test
    public void executeWithAEmptyProductBarcodeCodeWillFail() {
        assertThrows(IllegalArgumentException.class, () -> {
            String productCode = "123";
            String productName = "Royk sopp";
            String productDescription = "The best soup";
            Barcode barcode = Barcode.newBuilder()
                    .barcodeType(BarcodeType.EAN_13)
                    .build();

            Product entity = Product.newBuilder()
                    .code(productCode)
                    .name(productName)
                    .description(productDescription)
                    .barcode(barcode)
                    .build();

            try {
                uc.execute(entity);
            } catch (IllegalArgumentException e) {
                assertEquals("product barcode code must have a text", e.getMessage());
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }

    /**
     * Execute with a null product unit will fail.
     */
    @Test
    public void executeWithANullProductUnitWillFail() {
        assertThrows(IllegalArgumentException.class, () -> {
            String productCode = "123";
            String productName = "Royk sopp";
            String productDescription = "The best soup";
            Barcode barcode = Barcode.newBuilder()
                    .barcodeType(BarcodeType.EAN_13)
                    .code("1")
                    .build();

            Product entity = Product.newBuilder()
                    .code(productCode)
                    .name(productName)
                    .description(productDescription)
                    .barcode(barcode)
                    .build();

            try {
                uc.execute(entity);
            } catch (IllegalArgumentException e) {
                assertEquals("product unit cannot be null", e.getMessage());
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }

    /**
     * Execute with a empty product unit code will fail.
     */
    @Test
    public void executeWithAEmptyProductUnitCodeWillFail() {
        assertThrows(IllegalArgumentException.class, () -> {
            String productCode = "123";
            String productName = "Royk sopp";
            String productDescription = "The best soup";
            Barcode barcode = Barcode.newBuilder()
                    .barcodeType(BarcodeType.EAN_13)
                    .code("1")
                    .build();
            Unit productUnit = Unit.newBuilder()
                    .build();

            Product entity = Product.newBuilder()
                    .code(productCode)
                    .name(productName)
                    .description(productDescription)
                    .barcode(barcode)
                    .unit(productUnit)
                    .build();

            try {
                uc.execute(entity);
            } catch (IllegalArgumentException e) {
                assertEquals("product unit code must have a text", e.getMessage());
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }

    /**
     * Execute with a empty product unit name will fail.
     */
    @Test
    public void executeWithAEmptyProductUnitNameWillFail() {
        assertThrows(IllegalArgumentException.class, () -> {
            String productCode = "123";
            String productName = "Royk sopp";
            String productDescription = "The best soup";
            Barcode barcode = Barcode.newBuilder()
                    .barcodeType(BarcodeType.EAN_13)
                    .code("1")
                    .build();
            Unit productUnit = Unit.newBuilder()
                    .code("PC")
                    .build();

            Product entity = Product.newBuilder()
                    .code(productCode)
                    .name(productName)
                    .description(productDescription)
                    .barcode(barcode)
                    .unit(productUnit)
                    .build();

            try {
                uc.execute(entity);
            } catch (IllegalArgumentException e) {
                assertEquals("product unit name must have a text", e.getMessage());
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }

    /**
     * Execute with a null product price will fail.
     */
    @Test
    public void executeWithANullProductPriceWillFail() {
        assertThrows(IllegalArgumentException.class, () -> {
            String productCode = "123";
            String productName = "Royk sopp";
            String productDescription = "The best soup";
            Barcode barcode = Barcode.newBuilder()
                    .barcodeType(BarcodeType.EAN_13)
                    .code("1")
                    .build();
            Unit productUnit = Unit.newBuilder()
                    .code("PC")
                    .name("PIECE")
                    .build();

            Product entity = Product.newBuilder()
                    .code(productCode)
                    .name(productName)
                    .description(productDescription)
                    .barcode(barcode)
                    .unit(productUnit)
                    .build();

            try {
                uc.execute(entity);
            } catch (IllegalArgumentException e) {
                assertEquals("product price cannot be null", e.getMessage());
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }

    /**
     * Execute with a null product store will fail.
     */
    @Test
    public void executeWithANullProductStoreWillFail() {
        assertThrows(IllegalArgumentException.class, () -> {
            String productCode = "123";
            String productName = "Royk sopp";
            String productDescription = "The best soup";
            Barcode barcode = Barcode.newBuilder()
                    .barcodeType(BarcodeType.EAN_13)
                    .code("1")
                    .build();
            Unit productUnit = Unit.newBuilder()
                    .code("PC")
                    .name("PIECE")
                    .build();
            BigDecimal productPrice = BigDecimal.ONE;

            Product entity = Product.newBuilder()
                    .code(productCode)
                    .name(productName)
                    .description(productDescription)
                    .barcode(barcode)
                    .unit(productUnit)
                    .price(productPrice)
                    .build();

            try {
                uc.execute(entity);
            } catch (IllegalArgumentException e) {
                assertEquals("product store cannot be null", e.getMessage());
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }

    /**
     * Execute with a empty product store code will fail.
     */
    @Test
    public void executeWithAEmptyProductStoreCodeWillFail() {
        assertThrows(IllegalArgumentException.class, () -> {
            String productCode = "123";
            String productName = "Royk sopp";
            String productDescription = "The best soup";
            Barcode barcode = Barcode.newBuilder()
                    .barcodeType(BarcodeType.EAN_13)
                    .code("1")
                    .build();
            Unit productUnit = Unit.newBuilder()
                    .code("PC")
                    .name("PIECE")
                    .build();
            BigDecimal productPrice = BigDecimal.ONE;
            Store productStore = Store.newBuilder()
                    .build();

            Product entity = Product.newBuilder()
                    .code(productCode)
                    .name(productName)
                    .description(productDescription)
                    .barcode(barcode)
                    .unit(productUnit)
                    .price(productPrice)
                    .store(productStore)
                    .build();

            try {
                uc.execute(entity);
            } catch (IllegalArgumentException e) {
                assertEquals("product store code must have a text", e.getMessage());
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }

    /**
     * Execute with a empty product store name will fail.
     */
    @Test
    public void executeWithAEmptyProductStoreNameWillFail() {
        assertThrows(IllegalArgumentException.class, () -> {
            String productCode = "123";
            String productName = "Royk sopp";
            String productDescription = "The best soup";
            Barcode barcode = Barcode.newBuilder()
                    .barcodeType(BarcodeType.EAN_13)
                    .code("1")
                    .build();
            Unit productUnit = Unit.newBuilder()
                    .code("PC")
                    .name("PIECE")
                    .build();
            BigDecimal productPrice = BigDecimal.ONE;
            Store productStore = Store.newBuilder()
                    .code("666")
                    .build();

            Product entity = Product.newBuilder()
                    .code(productCode)
                    .name(productName)
                    .description(productDescription)
                    .barcode(barcode)
                    .unit(productUnit)
                    .price(productPrice)
                    .store(productStore)
                    .build();

            try {
                uc.execute(entity);
            } catch (IllegalArgumentException e) {
                assertEquals("product store name must have a text", e.getMessage());
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }

    /**
     * Execute with a null product images will fail.
     */
    @Test
    public void executeWithANullProductImagesWillFail() {
        assertThrows(IllegalArgumentException.class, () -> {
            String productCode = "123";
            String productName = "Royk sopp";
            String productDescription = "The best soup";
            Barcode barcode = Barcode.newBuilder()
                    .barcodeType(BarcodeType.EAN_13)
                    .code("1")
                    .build();
            Unit productUnit = Unit.newBuilder()
                    .code("PC")
                    .name("PIECE")
                    .build();
            BigDecimal productPrice = BigDecimal.ONE;
            Store productStore = Store.newBuilder()
                    .code("666")
                    .name("Super Store")
                    .build();

            Product entity = Product.newBuilder()
                    .code(productCode)
                    .name(productName)
                    .description(productDescription)
                    .barcode(barcode)
                    .unit(productUnit)
                    .price(productPrice)
                    .store(productStore)
                    .build();

            try {
                uc.execute(entity);
            } catch (IllegalArgumentException e) {
                assertEquals("product images cannot be null", e.getMessage());
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }

    /**
     * Execute with a empty product images will fail.
     */
    @Test
    public void executeWithAEmptyProductImagesWillFail() {
        assertThrows(IllegalArgumentException.class, () -> {
            String productCode = "123";
            String productName = "Royk sopp";
            String productDescription = "The best soup";
            Barcode barcode = Barcode.newBuilder()
                    .barcodeType(BarcodeType.EAN_13)
                    .code("1")
                    .build();
            Unit productUnit = Unit.newBuilder()
                    .code("PC")
                    .name("PIECE")
                    .build();
            BigDecimal productPrice = BigDecimal.ONE;
            Store productStore = Store.newBuilder()
                    .code("666")
                    .name("Super Store")
                    .build();
            List<Image> productImages = Collections.EMPTY_LIST;

            Product entity = Product.newBuilder()
                    .code(productCode)
                    .name(productName)
                    .description(productDescription)
                    .barcode(barcode)
                    .unit(productUnit)
                    .price(productPrice)
                    .store(productStore)
                    .images(productImages)
                    .build();

            try {
                uc.execute(entity);
            } catch (IllegalArgumentException e) {
                assertEquals("product images cannot be empty", e.getMessage());
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }

    /**
     * Execute with a invalid product barcode digit will fail.
     */
    @Test
    public void executeWithAInvalidProductBarcodeDigitWillFail() {
        assertThrows(IllegalArgumentException.class, () -> {
            String productCode = "123";
            String productName = "Royk sopp";
            String productDescription = "The best soup";
            Barcode barcode = Barcode.newBuilder()
                    .barcodeType(BarcodeType.EAN_13)
                    .code("1")
                    .build();
            Unit productUnit = Unit.newBuilder()
                    .code("PC")
                    .name("PIECE")
                    .build();
            BigDecimal productPrice = BigDecimal.ONE;
            Store productStore = Store.newBuilder()
                    .code("666")
                    .name("Super Store")
                    .build();
            Image productImage = Image.newBuilder()
                    .build();
            List<Image> productImages = Arrays.asList(productImage);

            Product entity = Product.newBuilder()
                    .code(productCode)
                    .name(productName)
                    .description(productDescription)
                    .barcode(barcode)
                    .unit(productUnit)
                    .price(productPrice)
                    .store(productStore)
                    .images(productImages)
                    .build();

            when(checkBarcodeDigitUseCase.execute("1")).thenReturn(false);

            try {
                uc.execute(entity);
            } catch (IllegalArgumentException e) {
                assertEquals("product barcode digit must be valid", e.getMessage());
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }

    /**
     * Execute with a product will success.
     */
    @Test
    public void executeWithAProductWillSuccess() {
        String productCode = "123";
        String productName = "Royk sopp";
        String productDescription = "The best soup";
        Barcode barcode = Barcode.newBuilder()
                .barcodeType(BarcodeType.EAN_13)
                .code("5449000014535")
                .build();
        Unit productUnit = Unit.newBuilder()
                .code("PC")
                .name("PIECE")
                .build();
        BigDecimal productPrice = BigDecimal.ONE;
        Store productStore = Store.newBuilder()
                .code("666")
                .name("Super Store")
                .build();
        Image productImage = Image.newBuilder()
                .build();
        List<Image> productImages = Arrays.asList(productImage);

        Product entity = Product.newBuilder()
                .code(productCode)
                .name(productName)
                .description(productDescription)
                .barcode(barcode)
                .unit(productUnit)
                .price(productPrice)
                .store(productStore)
                .images(productImages)
                .build();

        when(checkBarcodeDigitUseCase.execute("5449000014535")).thenReturn(true);

        boolean result = uc.execute(entity);
        assertEquals(true, result);
    }
}