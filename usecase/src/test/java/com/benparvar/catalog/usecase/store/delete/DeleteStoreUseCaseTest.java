package com.benparvar.catalog.usecase.store.delete;

import com.benparvar.catalog.domain.entity.store.Store;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

/**
 * The type Delete store use case test.
 */
class DeleteStoreUseCaseTest {
    private DeleteStoreUseCase uc;
    private final ValidateStoreBeforeDeleteUseCase validateStoreBeforeDeleteUseCase = mock(ValidateStoreBeforeDeleteUseCase.class);
    private final DeleteStore deleteStoreGateway = mock(DeleteStore.class);

    /**
     * Init.
     */
    @BeforeEach
    public void init() {
        uc = new DeleteStoreUseCase(validateStoreBeforeDeleteUseCase, deleteStoreGateway);
    }

    /**
     * Execute with a null entity will fail.
     */
    @Test
    public void executeWithANullEntityWillFail() {
        assertThrows(IllegalArgumentException.class, () -> {
            Store entity = null;

            when(validateStoreBeforeDeleteUseCase.execute(entity))
                    .thenThrow(new IllegalArgumentException("store cannot be null"));

            try {
                uc.execute(entity);
            } catch (IllegalArgumentException e) {
                assertEquals("store cannot be null", e.getMessage());
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }


    /**
     * Execute with a entity already existent will success.
     */
    @Test
    public void executeWithAEntityAlreadyExistentWillSuccess() {
        Store entity = Store.newBuilder().code("PC").name("PIECE").build();
        when(validateStoreBeforeDeleteUseCase.execute(entity))
                .thenReturn(true);

        assertDoesNotThrow(() -> {
            uc.execute(entity);

        });

        verify(validateStoreBeforeDeleteUseCase, times(1)).execute(entity);
        verify(deleteStoreGateway, times(1)).delete(entity);
    }


    /**
     * Execute with a entity non existent will fail.
     */
    @Test
    public void executeWithAEntityNonExistentWillFail() {
        // TODO -- Implement and catch StoreNotFoundException
    }
}