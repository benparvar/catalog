package com.benparvar.catalog.e2e.unit;

import com.benparvar.catalog.e2e.config.E2eConfig;
import com.benparvar.catalog.e2e.config.E2eConfigParameterResolver;
import com.benparvar.catalog.e2e.fixture.UnitJson;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.http.HttpStatus;

import static io.restassured.RestAssured.given;
import static io.restassured.http.ContentType.JSON;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.core.IsEqual.equalTo;

/**
 * The type Unit api test.
 */
@ExtendWith(E2eConfigParameterResolver.class)
public class UnitApiTest {
    private final E2eConfig config;

    /**
     * Instantiates a new Unit api test.
     *
     * @param config the config
     */
    public UnitApiTest(E2eConfig config) {
        this.config = config;
    }

    /**
     * Clean.
     */
    @BeforeEach
    public void clean() {
        given()
                .baseUri(config.getHostname())
                .port(config.getPort())
                .when()
                .accept(JSON)
                .request("DELETE", "/api/v1/units/MP/code");

        given()
                .baseUri(config.getHostname())
                .port(config.getPort())
                .when()
                .accept(JSON)
                .request("DELETE", "/api/v1/units/PC/code");
    }

    /**
     * Save units will no payload will return unprocessable entity.
     */
    @Test
    public void saveUnitsWillNoPayloadWillReturnUnprocessableEntity() {
        given()
                .baseUri(config.getHostname())
                .port(config.getPort())
                .when()
                .contentType(JSON)
                .accept(JSON)
                .body(UnitJson.NO_PAYLOAD)
                .request("POST", "/api/v1/units")
                .then()
                .statusCode(HttpStatus.UNPROCESSABLE_ENTITY.value());
    }

    /**
     * Save units will empty payload will return bad request.
     */
    @Test
    public void saveUnitsWillEmptyPayloadWillReturnBadRequest() {
        given()
                .baseUri(config.getHostname())
                .port(config.getPort())
                .when()
                .contentType(JSON)
                .accept(JSON)
                .body(UnitJson.EMPTY_PAYLOAD)
                .request("POST", "/api/v1/units")
                .then()
                .statusCode(HttpStatus.BAD_REQUEST.value());
    }

    /**
     * Save units with valid payload will return created.
     */
    @Test
    public void saveUnitsWithValidPayloadWillReturnCreated() {
        given()
                .baseUri(config.getHostname())
                .port(config.getPort())
                .when()
                .contentType(JSON)
                .accept(JSON)
                .body(UnitJson.VALID_PAYLOAD)
                .request("POST", "/api/v1/units")
                .then()
                .statusCode(HttpStatus.CREATED.value())
                .body(is(equalTo(UnitJson.VALID_RESPONSE)));
    }

    /**
     * Save units already saved with valid payload will return conflict.
     */
    @Test
    public void saveUnitsAlreadySavedWithValidPayloadWillReturnConflict() {
        // Creating
        given()
                .baseUri(config.getHostname())
                .port(config.getPort())
                .when()
                .contentType(JSON)
                .accept(JSON)
                .body(UnitJson.VALID_PAYLOAD)
                .request("POST", "/api/v1/units")
                .then()
                .statusCode(HttpStatus.CREATED.value())
                .body(is(equalTo(UnitJson.VALID_RESPONSE)));

        // Trying to create the same unit again
        given()
                .baseUri(config.getHostname())
                .port(config.getPort())
                .when()
                .contentType(JSON)
                .accept(JSON)
                .body(UnitJson.VALID_PAYLOAD)
                .request("POST", "/api/v1/units")
                .then()
                .statusCode(HttpStatus.CONFLICT.value());
    }

    /**
     * Gets all units with no entities in database will return not found.
     */
    @Test
    public void getAllUnitsWithNoEntitiesInDatabaseWillReturnNotFound() {
        given()
                .baseUri(config.getHostname())
                .port(config.getPort())
                .when()
                .contentType(JSON)
                .accept(JSON)
                .request("GET", "/api/v1/units")
                .then()
                .statusCode(HttpStatus.NOT_FOUND.value());
    }

    /**
     * Gets all units with entities in database will return ok.
     */
    @Test
    public void getAllUnitsWithEntitiesInDatabaseWillReturnOk() {
        // Creating
        given()
                .baseUri(config.getHostname())
                .port(config.getPort())
                .when()
                .contentType(JSON)
                .accept(JSON)
                .body(UnitJson.VALID_PAYLOAD)
                .request("POST", "/api/v1/units")
                .then()
                .statusCode(HttpStatus.CREATED.value())
                .body(is(equalTo(UnitJson.VALID_RESPONSE)));

        // Reading
        given()
                .baseUri(config.getHostname())
                .port(config.getPort())
                .when()
                .contentType(JSON)
                .accept(JSON)
                .request("GET", "/api/v1/units")
                .then()
                .statusCode(HttpStatus.OK.value())
                .body(is(equalTo(UnitJson.VALID_ARRAY_RESPONSE)));
    }

    /**
     * Delete units with entities in database will return no content.
     */
    @Test
    public void deleteUnitsWithEntitiesInDatabaseWillReturnNoContent() {
        // Creating
        given()
                .baseUri(config.getHostname())
                .port(config.getPort())
                .when()
                .contentType(JSON)
                .accept(JSON)
                .body(UnitJson.VALID_PAYLOAD)
                .request("POST", "/api/v1/units")
                .then()
                .statusCode(HttpStatus.CREATED.value())
                .body(is(equalTo(UnitJson.VALID_RESPONSE)));

        // Deleting
        given()
                .baseUri(config.getHostname())
                .port(config.getPort())
                .when()
                .accept(JSON)
                .request("DELETE", "/api/v1/units/MP/code")
                .then()
                .statusCode(HttpStatus.NO_CONTENT.value());
    }

    /**
     * Gets by code units with entities in database will return ok.
     */
    @Test
    public void getByCodeUnitsWithEntitiesInDatabaseWillReturnOk() {
        // Creating
        given()
                .baseUri(config.getHostname())
                .port(config.getPort())
                .when()
                .contentType(JSON)
                .accept(JSON)
                .body(UnitJson.VALID_PAYLOAD)
                .request("POST", "/api/v1/units")
                .then()
                .statusCode(HttpStatus.CREATED.value())
                .body(is(equalTo(UnitJson.VALID_RESPONSE)));

        // Reading
        given()
                .baseUri(config.getHostname())
                .port(config.getPort())
                .when()
                .accept(JSON)
                .request("DELETE", "/api/v1/units/MP/code")
                .then()
                .statusCode(HttpStatus.NO_CONTENT.value());
    }

    /**
     * Update units with entities in database will return ok.
     */
    @Test
    public void updateUnitsWithEntitiesInDatabaseWillReturnOk() {
        // Creating
        given()
                .baseUri(config.getHostname())
                .port(config.getPort())
                .when()
                .contentType(JSON)
                .accept(JSON)
                .body(UnitJson.VALID_PAYLOAD)
                .request("POST", "/api/v1/units")
                .then()
                .statusCode(HttpStatus.CREATED.value())
                .body(is(equalTo(UnitJson.VALID_RESPONSE)));

        // Updating
        given()
                .baseUri(config.getHostname())
                .port(config.getPort())
                .when()
                .contentType(JSON)
                .accept(JSON)
                .body(UnitJson.VALID_UPDATE_PAYLOAD)
                .request("PUT", "/api/v1/units")
                .then()
                .statusCode(HttpStatus.OK.value())
                .body(is(equalTo(UnitJson.VALID_UPDATE_RESPONSE)));
    }
}
