package com.benparvar.catalog.e2e.product;

import com.benparvar.catalog.e2e.config.E2eConfig;
import com.benparvar.catalog.e2e.config.E2eConfigParameterResolver;
import com.benparvar.catalog.e2e.fixture.ProductJson;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.http.HttpStatus;

import static io.restassured.RestAssured.given;
import static io.restassured.http.ContentType.JSON;
import static org.hamcrest.Matchers.equalToCompressingWhiteSpace;
import static org.hamcrest.Matchers.is;

/**
 * The type Product api test.
 */
@ExtendWith(E2eConfigParameterResolver.class)
public class ProductApiTest {
    private final E2eConfig config;

    /**
     * Instantiates a new Product api test.
     *
     * @param config the config
     */
    public ProductApiTest(E2eConfig config) {
        this.config = config;
    }

    /**
     * Clean.
     */
    @BeforeEach
    public void clean() {
        given()
                .baseUri(config.getHostname())
                .port(config.getPort())
                .when()
                .accept(JSON)
                .request("DELETE", "/api/v1/products/7894900700046/barcode/132/store");
    }

    /**
     * Save products will no payload will return unprocessable entity.
     */
    @Test
    public void saveProductsWillNoPayloadWillReturnUnprocessableEntity() {
        given()
                .baseUri(config.getHostname())
                .port(config.getPort())
                .when()
                .contentType(JSON)
                .accept(JSON)
                .body(ProductJson.NO_PAYLOAD)
                .request("POST", "/api/v1/products")
                .then()
                .statusCode(HttpStatus.UNPROCESSABLE_ENTITY.value());
    }

    /**
     * Save products will empty payload will return bad request.
     */
    @Test
    public void saveProductsWillEmptyPayloadWillReturnBadRequest() {
        given()
                .baseUri(config.getHostname())
                .port(config.getPort())
                .when()
                .contentType(JSON)
                .accept(JSON)
                .body(ProductJson.EMPTY_PAYLOAD)
                .request("POST", "/api/v1/products")
                .then()
                .statusCode(HttpStatus.BAD_REQUEST.value());
    }

    /**
     * Save products with valid payload will return created.
     */
    @Test
    public void saveProductsWithValidPayloadWillReturnCreated() {
        given()
                .baseUri(config.getHostname())
                .port(config.getPort())
                .when()
                .contentType(JSON)
                .accept(JSON)
                .body(ProductJson.VALID_PAYLOAD)
                .request("POST", "/api/v1/products")
                .then()
                .statusCode(HttpStatus.CREATED.value())
                .body(is(equalToCompressingWhiteSpace(ProductJson.VALID_RESPONSE)));
    }

    /**
     * Save products already saved with valid payload will return conflict.
     */
    @Test
    public void saveProductsAlreadySavedWithValidPayloadWillReturnConflict() {
        // Creating
        given()
                .baseUri(config.getHostname())
                .port(config.getPort())
                .when()
                .contentType(JSON)
                .accept(JSON)
                .body(ProductJson.VALID_PAYLOAD)
                .request("POST", "/api/v1/products")
                .then()
                .statusCode(HttpStatus.CREATED.value())
                .body(is(equalToCompressingWhiteSpace(ProductJson.VALID_RESPONSE)));

        // Trying to create the same store again
        given()
                .baseUri(config.getHostname())
                .port(config.getPort())
                .when()
                .contentType(JSON)
                .accept(JSON)
                .body(ProductJson.VALID_PAYLOAD)
                .request("POST", "/api/v1/products")
                .then()
                .statusCode(HttpStatus.CONFLICT.value());
    }

    /**
     * Gets product with no entities in database will return not found.
     */
    @Test
    public void getProductWithNoEntitiesInDatabaseWillReturnNotFound() {
        given()
                .baseUri(config.getHostname())
                .port(config.getPort())
                .when()
                .contentType(JSON)
                .accept(JSON)
                .request("GET", "/api/v1/products/7894900700046/barcode/132/store")
                .then()
                .statusCode(HttpStatus.NOT_FOUND.value());
    }

    /**
     * Gets product with entities in database will return ok.
     */
    @Test
    public void getProductWithEntitiesInDatabaseWillReturnOk() {
        // Creating
        given()
                .baseUri(config.getHostname())
                .port(config.getPort())
                .when()
                .contentType(JSON)
                .accept(JSON)
                .body(ProductJson.VALID_PAYLOAD)
                .request("POST", "/api/v1/products")
                .then()
                .statusCode(HttpStatus.CREATED.value())
                .body(is(equalToCompressingWhiteSpace(ProductJson.VALID_RESPONSE)));

        // Reading
        given()
                .baseUri(config.getHostname())
                .port(config.getPort())
                .when()
                .contentType(JSON)
                .accept(JSON)
                .request("GET", "/api/v1/products/7894900700046/barcode/132/store")
                .then()
                .statusCode(HttpStatus.OK.value())
                .extract().body().equals(ProductJson.VALID_RESPONSE);
    }

    /**
     * Delete products with entities in database will return no content.
     */
    @Test
    public void deleteProductsWithEntitiesInDatabaseWillReturnNoContent() {
        // Creating
        given()
                .baseUri(config.getHostname())
                .port(config.getPort())
                .when()
                .contentType(JSON)
                .accept(JSON)
                .body(ProductJson.VALID_PAYLOAD)
                .request("POST", "/api/v1/products")
                .then()
                .statusCode(HttpStatus.CREATED.value())
                .body(is(equalToCompressingWhiteSpace(ProductJson.VALID_RESPONSE)));

        // Deleting
        given()
                .baseUri(config.getHostname())
                .port(config.getPort())
                .when()
                .accept(JSON)
                .request("DELETE", "/api/v1/products/7894900700046/barcode/132/store")
                .then()
                .statusCode(HttpStatus.NO_CONTENT.value());
    }

}
