package com.benparvar.catalog.e2e.store;

import com.benparvar.catalog.e2e.config.E2eConfig;
import com.benparvar.catalog.e2e.config.E2eConfigParameterResolver;
import com.benparvar.catalog.e2e.fixture.StoreJson;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.http.HttpStatus;

import static io.restassured.RestAssured.given;
import static io.restassured.http.ContentType.JSON;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.core.IsEqual.equalTo;

/**
 * The type Store api test.
 */
@ExtendWith(E2eConfigParameterResolver.class)
public class StoreApiTest {
    private final E2eConfig config;

    /**
     * Instantiates a new Store api test.
     *
     * @param config the config
     */
    public StoreApiTest(E2eConfig config) {
        this.config = config;
    }

    /**
     * Clean.
     */
    @BeforeEach
    public void clean() {
        given()
                .baseUri(config.getHostname())
                .port(config.getPort())
                .when()
                .accept(JSON)
                .request("DELETE", "/api/v1/stores/666/code");

        given()
                .baseUri(config.getHostname())
                .port(config.getPort())
                .when()
                .accept(JSON)
                .request("DELETE", "/api/v1/stores/132/code");
    }

    /**
     * Save stores will no payload will return unprocessable entity.
     */
    @Test
    public void saveStoresWillNoPayloadWillReturnUnprocessableEntity() {
        given()
                .baseUri(config.getHostname())
                .port(config.getPort())
                .when()
                .contentType(JSON)
                .accept(JSON)
                .body(StoreJson.NO_PAYLOAD)
                .request("POST", "/api/v1/stores")
                .then()
                .statusCode(HttpStatus.UNPROCESSABLE_ENTITY.value());
    }

    /**
     * Save stores will empty payload will return bad request.
     */
    @Test
    public void saveStoresWillEmptyPayloadWillReturnBadRequest() {
        given()
                .baseUri(config.getHostname())
                .port(config.getPort())
                .when()
                .contentType(JSON)
                .accept(JSON)
                .body(StoreJson.EMPTY_PAYLOAD)
                .request("POST", "/api/v1/stores")
                .then()
                .statusCode(HttpStatus.BAD_REQUEST.value());
    }

    /**
     * Save stores with valid payload will return created.
     */
    @Test
    public void saveStoresWithValidPayloadWillReturnCreated() {
        given()
                .baseUri(config.getHostname())
                .port(config.getPort())
                .when()
                .contentType(JSON)
                .accept(JSON)
                .body(StoreJson.VALID_PAYLOAD)
                .request("POST", "/api/v1/stores")
                .then()
                .statusCode(HttpStatus.CREATED.value())
                .body(is(equalTo(StoreJson.VALID_RESPONSE)));
    }

    /**
     * Save stores already saved with valid payload will return conflict.
     */
    @Test
    public void saveStoresAlreadySavedWithValidPayloadWillReturnConflict(){
        // Creating
        given()
                .baseUri(config.getHostname())
                .port(config.getPort())
                .when()
                .contentType(JSON)
                .accept(JSON)
                .body(StoreJson.VALID_PAYLOAD)
                .request("POST", "/api/v1/stores")
                .then()
                .statusCode(HttpStatus.CREATED.value())
                .body(is(equalTo(StoreJson.VALID_RESPONSE)));

        // Trying to create the same store again
        given()
                .baseUri(config.getHostname())
                .port(config.getPort())
                .when()
                .contentType(JSON)
                .accept(JSON)
                .body(StoreJson.VALID_PAYLOAD)
                .request("POST", "/api/v1/stores")
                .then()
                .statusCode(HttpStatus.CONFLICT.value());
    }

    /**
     * Gets all stores with no entities in database will return not found.
     */
    @Test
    public void getAllStoresWithNoEntitiesInDatabaseWillReturnNotFound() {
        given()
                .baseUri(config.getHostname())
                .port(config.getPort())
                .when()
                .contentType(JSON)
                .accept(JSON)
                .request("GET", "/api/v1/stores")
                .then()
                .statusCode(HttpStatus.NOT_FOUND.value());
    }

    /**
     * Gets all stores with entities in database will return ok.
     */
    @Test
    public void getAllStoresWithEntitiesInDatabaseWillReturnOk() {
        // Creating
        given()
                .baseUri(config.getHostname())
                .port(config.getPort())
                .when()
                .contentType(JSON)
                .accept(JSON)
                .body(StoreJson.VALID_PAYLOAD)
                .request("POST", "/api/v1/stores")
                .then()
                .statusCode(HttpStatus.CREATED.value())
                .body(is(equalTo(StoreJson.VALID_RESPONSE)));

        // Reading
        given()
                .baseUri(config.getHostname())
                .port(config.getPort())
                .when()
                .contentType(JSON)
                .accept(JSON)
                .request("GET", "/api/v1/stores")
                .then()
                .statusCode(HttpStatus.OK.value())
                .body(is(equalTo(StoreJson.VALID_ARRAY_RESPONSE)));
    }

    /**
     * Delete stores with entities in database will return no content.
     */
    @Test
    public void deleteStoresWithEntitiesInDatabaseWillReturnNoContent(){
        // Creating
        given()
                .baseUri(config.getHostname())
                .port(config.getPort())
                .when()
                .contentType(JSON)
                .accept(JSON)
                .body(StoreJson.VALID_PAYLOAD)
                .request("POST", "/api/v1/stores")
                .then()
                .statusCode(HttpStatus.CREATED.value())
                .body(is(equalTo(StoreJson.VALID_RESPONSE)));

        // Deleting
        given()
                .baseUri(config.getHostname())
                .port(config.getPort())
                .when()
                .accept(JSON)
                .request("DELETE", "/api/v1/stores/666/code")
                .then()
                .statusCode(HttpStatus.NO_CONTENT.value());
    }

    /**
     * Get by code stores with entities in database will return ok.
     */
    @Test
    public void getByCodeStoresWithEntitiesInDatabaseWillReturnOk(){
        // Creating
        given()
                .baseUri(config.getHostname())
                .port(config.getPort())
                .when()
                .contentType(JSON)
                .accept(JSON)
                .body(StoreJson.VALID_PAYLOAD)
                .request("POST", "/api/v1/stores")
                .then()
                .statusCode(HttpStatus.CREATED.value())
                .body(is(equalTo(StoreJson.VALID_RESPONSE)));

        // Reading
        given()
                .baseUri(config.getHostname())
                .port(config.getPort())
                .when()
                .accept(JSON)
                .request("DELETE", "/api/v1/stores/666/code")
                .then()
                .statusCode(HttpStatus.NO_CONTENT.value());
    }

    /**
     * Update stores with entities in database will return ok.
     */
    @Test
    public void updateStoresWithEntitiesInDatabaseWillReturnOk(){
        // Creating
        given()
                .baseUri(config.getHostname())
                .port(config.getPort())
                .when()
                .contentType(JSON)
                .accept(JSON)
                .body(StoreJson.VALID_PAYLOAD)
                .request("POST", "/api/v1/stores")
                .then()
                .statusCode(HttpStatus.CREATED.value())
                .body(is(equalTo(StoreJson.VALID_RESPONSE)));

        // Updating
        given()
                .baseUri(config.getHostname())
                .port(config.getPort())
                .when()
                .contentType(JSON)
                .accept(JSON)
                .body(StoreJson.VALID_UPDATE_PAYLOAD)
                .request("PUT", "/api/v1/stores")
                .then()
                .statusCode(HttpStatus.OK.value())
                .body(is(equalTo(StoreJson.VALID_UPDATE_RESPONSE)));
    }
}
