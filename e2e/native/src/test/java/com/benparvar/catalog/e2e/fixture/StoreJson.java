package com.benparvar.catalog.e2e.fixture;

/**
 * The type Store json.
 */
public class StoreJson {

    /**
     * The constant NO_PAYLOAD.
     */
    public static String NO_PAYLOAD = "";

    /**
     * The constant EMPTY_PAYLOAD.
     */
    public static String EMPTY_PAYLOAD = "{}";

    /**
     * The constant VALID_PAYLOAD.
     */
    public static String VALID_PAYLOAD = "{\"code\": \"666\", \"name\": \"Matrix Store\"}";

    /**
     * The constant VALID_UPDATE_PAYLOAD.
     */
    public static String VALID_UPDATE_PAYLOAD = "{\"code\": \"666\", \"name\": \"Matrix the Store\"}";

    /**
     * The constant VALID_RESPONSE.
     */
    public static String VALID_RESPONSE = "{\"code\":\"666\",\"name\":\"Matrix Store\"}";

    /**
     * The constant VALID_UPDATE_RESPONSE.
     */
    public static String VALID_UPDATE_RESPONSE = "{\"code\":\"666\",\"name\":\"Matrix the Store\"}";

    /**
     * The constant VALID_ARRAY_RESPONSE.
     */
    public static String VALID_ARRAY_RESPONSE = "[{\"code\":\"666\",\"name\":\"Matrix Store\"}]";
}
