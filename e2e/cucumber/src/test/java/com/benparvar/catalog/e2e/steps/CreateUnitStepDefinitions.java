package com.benparvar.catalog.e2e.steps;

import com.benparvar.catalog.domain.entity.unit.Unit;
import com.benparvar.catalog.e2e.config.E2eConfig;
import com.benparvar.catalog.e2e.config.E2eConfigParameterResolver;
import com.google.gson.Gson;
import io.cucumber.java8.En;
import io.restassured.response.Response;
import org.junit.jupiter.api.extension.ExtendWith;

import static io.restassured.RestAssured.given;
import static io.restassured.http.ContentType.JSON;
import static org.hamcrest.Matchers.equalToCompressingWhiteSpace;
import static org.hamcrest.Matchers.is;

/**
 * The type Create unit step definitions.
 */
@ExtendWith(E2eConfigParameterResolver.class)
public class CreateUnitStepDefinitions implements En {
    private final E2eConfig config;
    private String code;
    private String name;
    private Response response;

    /**
     * Instantiates a new Create unit step definitions.
     *
     * @param config the config
     */
    public CreateUnitStepDefinitions(E2eConfig config) {
        this.config = config;

        Before(() -> {
            given()
                    .baseUri(config.getHostname())
                    .port(config.getPort())
                    .when()
                    .accept(JSON)
                    .request("DELETE", "/api/v1/units/MP/code");

            given()
                    .baseUri(config.getHostname())
                    .port(config.getPort())
                    .when()
                    .accept(JSON)
                    .request("DELETE", "/api/v1/units/PC/code");
        });

        Given("a unit with code {string} and name {string}", (String code, String name) -> {
            this.code = code;
            this.name = name;
        });

        When("user call save unit API", () -> {
            String unit = new Gson().toJson(Unit.newBuilder().code(code).name(name).build());
            response = given()
                    .baseUri(this.config.getHostname())
                    .port(this.config.getPort())
                    .when()
                    .contentType(JSON)
                    .accept(JSON)
                    .body(unit)
                    .request("POST", "/api/v1/units");
        });

        Then("user should receive a status code {string} with a error message {string} from save API", (String statusCode, String errorMessage) -> {
            response.then().statusCode(Integer.parseInt(statusCode));
            if (!statusCode.equals("201"))
                response.then().body(is(equalToCompressingWhiteSpace(errorMessage)));
        });
    }
}
