package com.benparvar.catalog.e2e.steps;

import com.benparvar.catalog.domain.entity.unit.Unit;
import com.benparvar.catalog.e2e.config.E2eConfig;
import com.benparvar.catalog.e2e.config.E2eConfigParameterResolver;
import com.google.gson.Gson;
import io.cucumber.java8.En;
import io.restassured.response.Response;
import org.junit.jupiter.api.extension.ExtendWith;

import static io.restassured.RestAssured.given;
import static io.restassured.http.ContentType.JSON;
import static org.hamcrest.Matchers.equalToCompressingWhiteSpace;
import static org.hamcrest.Matchers.is;

@ExtendWith(E2eConfigParameterResolver.class)
public class UpdateUnitStepDefinitions implements En {
    private final E2eConfig config;
    private Response response;

    public UpdateUnitStepDefinitions(E2eConfig config) {
        this.config = config;

        Before(() -> {
            given()
                    .baseUri(config.getHostname())
                    .port(config.getPort())
                    .when()
                    .accept(JSON)
                    .request("DELETE", "/api/v1/units/PC/code");
        });

        When("user call update unit API with code {string} and give a new name {string}", (String code, String name) -> {
            String unit = new Gson().toJson(Unit.newBuilder().code(code).name(name).build());
            response = given()
                    .baseUri(this.config.getHostname())
                    .port(this.config.getPort())
                    .when()
                    .contentType(JSON)
                    .accept(JSON)
                    .body(unit)
                    .request("PUT", "/api/v1/units");
        });

        Then("user should receive a status code {string} with a error message {string} from update API", (String statusCode, String errorMessage) -> {
            response.then().statusCode(Integer.parseInt(statusCode));
            if (!statusCode.equals("200"))
                response.then().body(is(equalToCompressingWhiteSpace(errorMessage)));
        });
    }
}
