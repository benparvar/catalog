package com.benparvar.catalog.e2e;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

/**
 * The type Cucumber tests.
 */
@RunWith(Cucumber.class)
@CucumberOptions(plugin = {"pretty"})
public class CucumberTests {
}
