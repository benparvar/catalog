package com.benparvar.catalog.e2e.steps;

import com.benparvar.catalog.e2e.config.E2eConfig;
import com.benparvar.catalog.e2e.config.E2eConfigParameterResolver;
import io.cucumber.java8.En;
import io.restassured.response.Response;
import org.junit.jupiter.api.extension.ExtendWith;

import static io.restassured.RestAssured.given;
import static io.restassured.http.ContentType.JSON;
import static org.hamcrest.Matchers.equalToCompressingWhiteSpace;
import static org.hamcrest.Matchers.is;

/**
 * The type Get unit step definitions.
 */
@ExtendWith(E2eConfigParameterResolver.class)
public class GetUnitStepDefinitions implements En {
    private final E2eConfig config;
    private Response response;

    /**
     * Instantiates a new Get unit step definitions.
     *
     * @param config the config
     */
    public GetUnitStepDefinitions(E2eConfig config) {
        this.config = config;

        Before(() -> {
            given()
                    .baseUri(config.getHostname())
                    .port(config.getPort())
                    .when()
                    .accept(JSON)
                    .request("DELETE", "/api/v1/units/PC/code");
        });

        When("user call get unit API with code {string}", (String code) -> {
            response = given()
                    .baseUri(config.getHostname())
                    .port(config.getPort())
                    .when()
                    .accept(JSON)
                    .request("GET", "/api/v1/units/{code}/code", code);
        });

        When("user call get unit API without code", () -> {
            response = given()
                    .baseUri(config.getHostname())
                    .port(config.getPort())
                    .when()
                    .accept(JSON)
                    .request("GET", "/api/v1/units");
        });

        Then("user should receive a status code {string} with a error message {string} from get API", (String statusCode, String errorMessage) -> {
            response.then().statusCode(Integer.parseInt(statusCode));
            if (!statusCode.equals("200"))
                response.then().body(is(equalToCompressingWhiteSpace(errorMessage)));
        });
    }
}
