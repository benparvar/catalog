package com.benparvar.catalog.e2e.steps;

import com.benparvar.catalog.domain.entity.unit.Unit;
import com.benparvar.catalog.e2e.config.E2eConfig;
import com.benparvar.catalog.e2e.config.E2eConfigParameterResolver;
import com.google.gson.Gson;
import io.cucumber.java8.En;
import io.restassured.response.Response;
import org.junit.jupiter.api.extension.ExtendWith;

import static io.restassured.RestAssured.given;
import static io.restassured.http.ContentType.JSON;
import static org.hamcrest.Matchers.equalToCompressingWhiteSpace;
import static org.hamcrest.Matchers.is;

/**
 * The type Delete unit step definitions.
 */
@ExtendWith(E2eConfigParameterResolver.class)
public class DeleteUnitStepDefinitions implements En {
    private final E2eConfig config;
    private Response response;

    /**
     * Instantiates a new Delete unit step definitions.
     *
     * @param config the config
     */
    public DeleteUnitStepDefinitions(E2eConfig config) {
        this.config = config;

        Before(() -> {
            given()
                    .baseUri(config.getHostname())
                    .port(config.getPort())
                    .when()
                    .accept(JSON)
                    .request("DELETE", "/api/v1/units/PC/code");
        });

        Given("a created unit with code PC", () -> {
            String unit = new Gson().toJson(Unit.newBuilder().code("PC").name("PIECE").build());
            given()
                    .baseUri(this.config.getHostname())
                    .port(this.config.getPort())
                    .when()
                    .contentType(JSON)
                    .accept(JSON)
                    .body(unit)
                    .request("POST", "/api/v1/units");
        });

        When("user call delete unit API with code {string}", (String code) -> {
            response = given()
                    .baseUri(config.getHostname())
                    .port(config.getPort())
                    .when()
                    .accept(JSON)
                    .request("DELETE", "/api/v1/units/{code}/code", code);
        });

        Then("user should receive a status code {string} with a error message {string} from delete API", (String statusCode, String errorMessage) -> {
            response.then().statusCode(Integer.parseInt(statusCode));
            response.then().body(is(equalToCompressingWhiteSpace(errorMessage)));
        });
    }
}
