package com.benparvar.catalog.e2e.config;

import static java.lang.System.getProperty;

/**
 * The type E 2 e config.
 */
public class E2eConfig {
    private final static String DEFAULT_HOST = "http://localhost";
    private final static int DEFAULT_PORT = 9876;

    /**
     * Gets hostname.
     *
     * @return the hostname
     */
    public String getHostname() {
        String value = getProperty("hostname");
        return value != null ? value : DEFAULT_HOST;
    }

    /**
     * Gets port.
     *
     * @return the port
     */
    public int getPort() {
        String value = getProperty("port");
        return value != null ? Integer.parseInt(value) : DEFAULT_PORT;
    }
}
