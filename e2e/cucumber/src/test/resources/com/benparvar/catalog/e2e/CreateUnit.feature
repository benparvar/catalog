Feature: Create unit

  Scenario Outline: User calls web service to create a new unit
    Given a unit with code <code> and name <name>
    When user call save unit API
    Then user should receive a status code <status_code> with a error message <error_message> from save API
    Examples:
      | code | name    | status_code | error_message                       |
      | ""   | ""      | "400"       | "error: unit code must have a text" |
      | "PC" | ""      | "400"       | "error: unit name must have a text" |
      | ""   | "PIECE" | "400"       | "error: unit code must have a text" |
      | "PC" | "PIECE" | "201"       | ""                                  |