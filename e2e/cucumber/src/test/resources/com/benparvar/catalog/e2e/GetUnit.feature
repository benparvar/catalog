Feature: Get unit

  Scenario Outline: User calls web service to get a unit
    Given a created unit with code PC
    When user call get unit API with code <code>
    Then user should receive a status code <status_code> with a error message <error_message> from get API
    Examples:
      | code | status_code | error_message      |
      | "GZ" | "404"       | " error: Sorry..." |
      | "BC" | "404"       | " error: Sorry..." |
      | "PC" | "200"       | ""                 |

  Scenario Outline: User calls web service to get all units
    Given a created unit with code PC
    When user call get unit API without code
    Then user should receive a status code <status_code> with a error message <error_message> from get API
    Examples:
      | status_code | error_message      |
      | "200"       | ""                 |