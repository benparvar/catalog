Feature: Update unit

  Scenario Outline: User calls web service to update a unit
    Given a created unit with code PC
    When user call update unit API with code <code> and give a new name <name>
    Then user should receive a status code <status_code> with a error message <error_message> from update API
    Examples:
      | code | name   | status_code | error_message      |
      | "GZ" | "PICI" | "404"       | " error: Sorry..." |
      | "BC" | "PICI" | "404"       | " error: Sorry..." |
      | "PC" | "PICI" | "200"       | ""                 |