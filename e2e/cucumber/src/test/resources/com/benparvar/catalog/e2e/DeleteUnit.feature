Feature: Delete unit

  Scenario Outline: User calls web service to delete a unit
    Given a created unit with code PC
    When user call delete unit API with code <code>
    Then user should receive a status code <status_code> with a error message <error_message> from delete API
    Examples:
      | code | status_code | error_message      |
      | "GZ" | "404"       | " error: Sorry..." |
      | "BC" | "404"       | " error: Sorry..." |
      | "PC" | "204"       | ""                 |