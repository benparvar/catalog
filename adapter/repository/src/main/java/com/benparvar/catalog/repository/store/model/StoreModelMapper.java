package com.benparvar.catalog.repository.store.model;

import com.benparvar.catalog.domain.entity.store.Store;
import com.benparvar.catalog.domain.entity.store.Store.Builder;
import com.benparvar.catalog.domain.entity.validator.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * The type Store mapper.
 */
public class StoreModelMapper {
    /**
     * Model to entity store.
     *
     * @param model the model
     * @return the store
     */
    public Store modelToEntity(StoreModel model) {
        if (model == null)
            return null;

        Builder builder = Store.newBuilder();

        builder.code(model.getCode());
        builder.name(model.getName());

        return builder.build();
    }

    /**
     * Model to entity list.
     *
     * @param models the models
     * @return the list
     */
    public List<Store> modelToEntity(List<StoreModel> models) {
        List<Store> stores = new ArrayList();

        if (CollectionUtils.isNotEmpty(models))
            for (StoreModel model : models) {
                if (null != model)
                    stores.add(modelToEntity(model));
            }

        return stores;
    }

    /**
     * Entity to model store model.
     *
     * @param entity the entity
     * @return the store model
     */
    public StoreModel entityToModel(Store entity) {
        if (entity == null)
            return null;

        StoreModel storeModel = new StoreModel();

        storeModel.setCode(entity.getCode());
        storeModel.setName(entity.getName());

        return storeModel;
    }
}
