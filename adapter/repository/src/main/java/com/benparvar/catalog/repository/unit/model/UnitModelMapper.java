package com.benparvar.catalog.repository.unit.model;

import com.benparvar.catalog.domain.entity.unit.Unit;
import com.benparvar.catalog.domain.entity.unit.Unit.Builder;
import com.benparvar.catalog.domain.entity.validator.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * The type Unit mapper.
 */
public class UnitModelMapper {
    /**
     * Model to entity unit.
     *
     * @param model the model
     * @return the unit
     */
    public Unit modelToEntity(UnitModel model) {
        if (model == null)
            return null;

        Builder builder = Unit.newBuilder();

        builder.code(model.getCode());
        builder.name(model.getName());

        return builder.build();
    }

    /**
     * Model to entity list.
     *
     * @param models the models
     * @return the list
     */
    public List<Unit> modelToEntity(List<UnitModel> models) {
        List<Unit> units = new ArrayList();

        if (CollectionUtils.isNotEmpty(models))
            for (UnitModel model : models) {
                if (null != model)
                    units.add(modelToEntity(model));
            }

        return units;
    }

    /**
     * Entity to model unit model.
     *
     * @param entity the entity
     * @return the unit model
     */
    public UnitModel entityToModel(Unit entity) {
        if (entity == null)
            return null;

        UnitModel unitModel = new UnitModel();

        unitModel.setCode(entity.getCode());
        unitModel.setName(entity.getName());

        return unitModel;
    }
}
