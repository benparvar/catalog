package com.benparvar.catalog.repository.barcode.model;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Objects;

/**
 * The type Barcode model.
 */
@Entity
@Table(name = "BARCODE")
public class BarcodeModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "code", unique = false)
    @NotEmpty(message = "code cannot be empty")
    @NotNull(message = "code cannot be null")
    private String code;

    @Column(name = "barcodeType", unique = false)
    @NotNull(message = "barcode type cannot be null")
    @Enumerated(EnumType.STRING)
    private BarcodeModelType barcodeType;

    /**
     * Gets id.
     *
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param id the id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Gets code.
     *
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * Sets code.
     *
     * @param code the code
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * Gets barcode type.
     *
     * @return the barcode type
     */
    public BarcodeModelType getBarcodeType() {
        return barcodeType;
    }

    /**
     * Sets barcode type.
     *
     * @param barcodeType the barcode type
     */
    public void setBarcodeType(BarcodeModelType barcodeType) {
        this.barcodeType = barcodeType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BarcodeModel that = (BarcodeModel) o;
        return Objects.equals(id, that.id) &&
                code.equals(that.code) &&
                barcodeType == that.barcodeType;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, code, barcodeType);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("BarcodeModel{");
        sb.append("id=").append(id);
        sb.append(", code='").append(code).append('\'');
        sb.append(", barcodeType=").append(barcodeType);
        sb.append('}');
        return sb.toString();
    }
}
