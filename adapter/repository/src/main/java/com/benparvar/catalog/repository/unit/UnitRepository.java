package com.benparvar.catalog.repository.unit;

import com.benparvar.catalog.repository.unit.model.UnitModel;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * The interface Unit repository.
 */
public interface UnitRepository extends JpaRepository<UnitModel, Long> {

    /**
     * Find by code unit model.
     *
     * @param code the code
     * @return the unit model
     */
    UnitModel findByCode(String code);
}
