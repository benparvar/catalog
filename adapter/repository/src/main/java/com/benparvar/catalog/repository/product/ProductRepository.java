package com.benparvar.catalog.repository.product;

import com.benparvar.catalog.repository.product.model.ProductModel;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * The interface Product repository.
 */
public interface ProductRepository extends JpaRepository<ProductModel, Long> {
    /**
     * Find by code and store code product model.
     *
     * @param code      the code
     * @param storeCode the store code
     * @return the product model
     */
    ProductModel findByCodeAndStoreCode(String code, String storeCode);

    /**
     * Find by barcode code and store code product model.
     *
     * @param barcodeCode the barcode code
     * @param storeCode   the store code
     * @return the product model
     */
    ProductModel findByBarcodeCodeAndStoreCode(String barcodeCode, String storeCode);
}
