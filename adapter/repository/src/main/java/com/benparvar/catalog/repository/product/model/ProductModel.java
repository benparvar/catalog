package com.benparvar.catalog.repository.product.model;

import com.benparvar.catalog.repository.barcode.model.BarcodeModel;
import com.benparvar.catalog.repository.image.model.ImageModel;
import com.benparvar.catalog.repository.store.model.StoreModel;
import com.benparvar.catalog.repository.unit.model.UnitModel;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;

/**
 * The type Product model.
 */
@Entity
@Table(name = "PRODUCT")
public class ProductModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "code", unique = false)
    @NotEmpty(message = "code cannot be empty")
    @NotNull(message = "code cannot be null")
    private String code;

    @Column(name = "name", unique = false)
    @NotEmpty(message = "name cannot be empty")
    @NotNull(message = "name cannot be null")
    private String name;

    @Column(name = "description", unique = false)
    @NotEmpty(message = "description cannot be empty")
    @NotNull(message = "description cannot be null")
    private String description;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinColumn(name = "barcode_id")
    private BarcodeModel barcode;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinColumn(name = "unit_id")
    private UnitModel unit;

    @Column(name = "price", unique = false)
    @NotNull(message = "price cannot be null")
    private BigDecimal price;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinColumn(name = "store_id")
    private StoreModel store;

    @Column(name = "images", unique = false)
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<ImageModel> images;

    /**
     * Gets id.
     *
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param id the id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Gets code.
     *
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * Sets code.
     *
     * @param code the code
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets name.
     *
     * @param name the name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets description.
     *
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets description.
     *
     * @param description the description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Gets barcode.
     *
     * @return the barcode
     */
    public BarcodeModel getBarcode() {
        return barcode;
    }

    /**
     * Sets barcode.
     *
     * @param barcode the barcode
     */
    public void setBarcode(BarcodeModel barcode) {
        this.barcode = barcode;
    }

    /**
     * Gets unit.
     *
     * @return the unit
     */
    public UnitModel getUnit() {
        return unit;
    }

    /**
     * Sets unit.
     *
     * @param unit the unit
     */
    public void setUnit(UnitModel unit) {
        this.unit = unit;
    }

    /**
     * Gets price.
     *
     * @return the price
     */
    public BigDecimal getPrice() {
        return price;
    }

    /**
     * Sets price.
     *
     * @param price the price
     */
    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    /**
     * Gets store.
     *
     * @return the store
     */
    public StoreModel getStore() {
        return store;
    }

    /**
     * Sets store.
     *
     * @param store the store
     */
    public void setStore(StoreModel store) {
        this.store = store;
    }

    /**
     * Gets images.
     *
     * @return the images
     */
    public List<ImageModel> getImages() {
        return images;
    }

    /**
     * Sets images.
     *
     * @param images the images
     */
    public void setImages(List<ImageModel> images) {
        this.images = images;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProductModel that = (ProductModel) o;
        return Objects.equals(id, that.id) &&
                code.equals(that.code) &&
                name.equals(that.name) &&
                description.equals(that.description) &&
                barcode.equals(that.barcode) &&
                unit.equals(that.unit) &&
                price.equals(that.price) &&
                store.equals(that.store) &&
                images.equals(that.images);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, code, name, description, barcode, unit, price, store, images);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ProductModel{");
        sb.append("id=").append(id);
        sb.append(", code='").append(code).append('\'');
        sb.append(", name='").append(name).append('\'');
        sb.append(", description='").append(description).append('\'');
        sb.append(", barcode=").append(barcode);
        sb.append(", unit=").append(unit);
        sb.append(", price=").append(price);
        sb.append(", store=").append(store);
        sb.append(", images=").append(images);
        sb.append('}');
        return sb.toString();
    }
}
