package com.benparvar.catalog.repository.barcode.model;

public enum BarcodeModelType {
    /**
     * Ean 8 barcode type  with 8 digits.
     */
    EAN_8,
    /**
     * Ean 13 barcode type with 13 digits.
     */
    EAN_13,
    /**
     * Upc a barcode type with 12 digits.
     */
    UPC_A,
}
