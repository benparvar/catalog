package com.benparvar.catalog.repository.barcode.model;

import com.benparvar.catalog.domain.entity.barcode.Barcode;
import com.benparvar.catalog.domain.entity.barcode.BarcodeType;

/**
 * The type Barcode model mapper.
 */
public class BarcodeModelMapper {
    private static final String ONLY_NUMBER_REGEX = "\\D";

    /**
     * Entity to model barcode model.
     *
     * @param entity the entity
     * @return the barcode model
     */
    public BarcodeModel entityToModel(Barcode entity) {
        if (entity == null)
            return null;

        BarcodeModel barcodeModel = new BarcodeModel();

        barcodeModel.setCode(entity.getCode());
        barcodeModel.setBarcodeType(getBarCodeModelType(entity.getCode()));

        return barcodeModel;
    }

    /**
     * Model to entity barcode.
     *
     * @param model the model
     * @return the barcode
     */
    public Barcode modelToEntity(BarcodeModel model) {
        if (model == null)
            return null;

        Barcode.Builder builder = Barcode.newBuilder();

        builder.code(model.getCode());
        builder.barcodeType(getBarCodeType(model.getCode()));

        return builder.build();
    }

    private BarcodeModelType getBarCodeModelType(String barcode) {
        switch (barcode.replaceAll(ONLY_NUMBER_REGEX, "").length()) {
            case 8:
                return BarcodeModelType.EAN_8;
            case 12:
                return BarcodeModelType.UPC_A;
            case 13:
                return BarcodeModelType.EAN_13;
            default:
                return null;
        }
    }

    private BarcodeType getBarCodeType(String barcode) {
        switch (barcode.replaceAll(ONLY_NUMBER_REGEX, "").length()) {
            case 8:
                return BarcodeType.EAN_8;
            case 12:
                return BarcodeType.UPC_A;
            case 13:
                return BarcodeType.EAN_13;
            default:
                return null;
        }
    }
}
