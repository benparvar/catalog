package com.benparvar.catalog.repository.image.model;

import com.benparvar.catalog.domain.entity.image.Image;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The type Image model mapper.
 */
public class ImageModelMapper {

    /**
     * Entity to model image model.
     *
     * @param entity the entity
     * @return the image model
     */
    public ImageModel entityToModel(Image entity) {
        if (entity == null)
            return null;

        ImageModel model = new ImageModel();

        model.setUrl(entity.getUrl());
        model.setDescription(entity.getDescription());

        return model;
    }

    /**
     * Entity to model list.
     *
     * @param entities the entities
     * @return the list
     */
    public List<ImageModel> entityToModel(List<Image> entities) {
        if (entities == null)
            return Collections.emptyList();

        List<ImageModel> imageModels = new ArrayList<>();

        for (Image entity : entities) {
            imageModels.add(entityToModel(entity));
        }

        return imageModels;
    }

    /**
     * Model to entity image.
     *
     * @param model the model
     * @return the image
     */
    public Image modelToEntity(ImageModel model) {
        if (model == null)
            return null;

        Image.Builder builder = Image.newBuilder();
        builder.url(model.getUrl());
        builder.description(model.getDescription());

        return builder.build();
    }

    /**
     * Model to entity list.
     *
     * @param models the models
     * @return the list
     */
    public List<Image> modelToEntity(List<ImageModel> models) {
        if (models == null)
            return Collections.emptyList();

        List<Image> images = new ArrayList<>();

        for (ImageModel model : models) {
            images.add(modelToEntity(model));
        }

        return images;
    }
}
