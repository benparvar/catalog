package com.benparvar.catalog.repository.product.model;

import com.benparvar.catalog.repository.barcode.model.BarcodeModelMapper;
import com.benparvar.catalog.repository.image.model.ImageModelMapper;
import com.benparvar.catalog.repository.store.model.StoreModelMapper;
import com.benparvar.catalog.repository.unit.model.UnitModelMapper;
import com.benparvar.catalog.domain.entity.product.Product;

/**
 * The type Product model mapper.
 */
public class ProductModelMapper {
    private final BarcodeModelMapper barcodeModelMapper;
    private final UnitModelMapper unitModelMapper;
    private final StoreModelMapper storeModelMapper;
    private final ImageModelMapper imageModelMapper;

    /**
     * Instantiates a new Product model mapper.
     *
     * @param barcodeModelMapper the barcode model mapper
     * @param unitModelMapper    the unit model mapper
     * @param storeModelMapper   the store model mapper
     * @param imageModelMapper   the image model mapper
     */
    public ProductModelMapper(BarcodeModelMapper barcodeModelMapper, UnitModelMapper unitModelMapper,
                              StoreModelMapper storeModelMapper, ImageModelMapper imageModelMapper) {
        this.barcodeModelMapper = barcodeModelMapper;
        this.unitModelMapper = unitModelMapper;
        this.storeModelMapper = storeModelMapper;
        this.imageModelMapper = imageModelMapper;
    }

    /**
     * Entity to model product model.
     *
     * @param entity the entity
     * @return the product model
     */
    public ProductModel entityToModel(Product entity) {
        if (entity == null)
            return null;

        ProductModel productModel = new ProductModel();

        productModel.setCode(entity.getCode());
        productModel.setName(entity.getName());
        productModel.setDescription(entity.getDescription());
        productModel.setBarcode(barcodeModelMapper.entityToModel(entity.getBarcode()));
        productModel.setUnit(unitModelMapper.entityToModel(entity.getUnit()));
        productModel.setPrice(entity.getPrice());
        productModel.setStore(storeModelMapper.entityToModel(entity.getStore()));
        productModel.setImages(imageModelMapper.entityToModel(entity.getImages()));

        return productModel;
    }

    /**
     * Model to entity product.
     *
     * @param model the model
     * @return the product
     */
    public Product modelToEntity(ProductModel model) {
        if (model == null)
            return null;

        Product.Builder builder = Product.newBuilder();

        builder.code(model.getCode());
        builder.name(model.getName());
        builder.description(model.getDescription());
        builder.barcode(barcodeModelMapper.modelToEntity(model.getBarcode()));
        builder.unit(unitModelMapper.modelToEntity(model.getUnit()));
        builder.price(model.getPrice());
        builder.store(storeModelMapper.modelToEntity(model.getStore()));
        builder.images(imageModelMapper.modelToEntity(model.getImages()));

        return builder.build();
    }
}
