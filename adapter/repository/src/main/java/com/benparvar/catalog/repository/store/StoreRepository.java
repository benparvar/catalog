package com.benparvar.catalog.repository.store;

import com.benparvar.catalog.repository.store.model.StoreModel;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * The interface Store repository.
 */
public interface StoreRepository extends JpaRepository<StoreModel, Long> {

    /**
     * Find by code store model.
     *
     * @param code the code
     * @return the store model
     */
    StoreModel findByCode(String code);
}