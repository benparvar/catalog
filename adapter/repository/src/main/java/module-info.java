module com.benparvar.catalog.repository {
    exports com.benparvar.catalog.repository.unit;
    exports com.benparvar.catalog.repository.unit.model;
    exports com.benparvar.catalog.repository.store.model;
    exports com.benparvar.catalog.repository.store;
    exports com.benparvar.catalog.repository.product;
    exports com.benparvar.catalog.repository.product.model;
    exports com.benparvar.catalog.repository.barcode.model;
    exports com.benparvar.catalog.repository.image.model;

    requires com.benparvar.catalog.domain;
    requires java.persistence;
    requires java.validation;
    requires spring.data.jpa;
    requires spring.data.commons;
    requires spring.tx;
    requires spring.context;
}
