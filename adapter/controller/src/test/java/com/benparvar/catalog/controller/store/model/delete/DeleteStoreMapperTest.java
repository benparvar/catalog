package com.benparvar.catalog.controller.store.model.delete;

import com.benparvar.catalog.domain.entity.store.Store;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * The type Delete store mapper test.
 */
class DeleteStoreMapperTest {
    private DeleteStoreMapper mapper;

    /**
     * Init.
     */
    @BeforeEach
    public void init() {
        mapper = new DeleteStoreMapper();
    }

    /**
     * Execute request to entity with a null request will return null.
     */
    @Test
    public void executeRequestToEntityWithANullRequestWillReturnNull() {
        DeleteStoreRequest request = null;
        Store store = mapper.requestToEntity(request);

        assertNull(store, "must be null");
    }

    /**
     * Execute request to entity with a non null request will return with success.
     */
    @Test
    public void executeRequestToEntityWithANonNullRequestWillReturnWithSuccess() {
        DeleteStoreRequest request = DeleteStoreRequest.newBuilder().code("MT").name("Meter").build();

        Store store = mapper.requestToEntity(request);

        assertNotNull(store, "do not must be null");
        assertEquals("MT", store.getCode());
        assertEquals("Meter", store.getName());
    }
}