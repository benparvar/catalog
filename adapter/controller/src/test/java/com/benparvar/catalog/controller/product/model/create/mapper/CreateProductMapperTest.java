package com.benparvar.catalog.controller.product.model.create.mapper;

import com.benparvar.catalog.controller.product.model.create.request.CreateProductRequest;
import com.benparvar.catalog.controller.product.model.create.request.CreateProductImageRequest;
import com.benparvar.catalog.controller.product.model.create.request.CreateProductStoreRequest;
import com.benparvar.catalog.controller.product.model.create.request.CreateProductUnitRequest;
import com.benparvar.catalog.controller.product.model.create.response.CreateProductResponse;
import com.benparvar.catalog.domain.entity.barcode.Barcode;
import com.benparvar.catalog.domain.entity.barcode.BarcodeType;
import com.benparvar.catalog.domain.entity.image.Image;
import com.benparvar.catalog.domain.entity.product.Product;
import com.benparvar.catalog.domain.entity.store.Store;
import com.benparvar.catalog.domain.entity.unit.Unit;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * The type Create product mapper test.
 */
class CreateProductMapperTest {
    private CreateProductMapper mapper;
    private CreateProductUnitMapper createProductUnitMapper;
    private CreateProductStoreMapper createProductStoreMapper;
    private CreateProductImageMapper createProductImageMapper;
    private CreateProductBarcodeMapper createProductBarcodeMapper;

    /**
     * Init.
     */
    @BeforeEach
    public void init() {
        createProductUnitMapper = new CreateProductUnitMapper();
        createProductStoreMapper = new CreateProductStoreMapper();
        createProductImageMapper = new CreateProductImageMapper();
        createProductBarcodeMapper = new CreateProductBarcodeMapper();
        mapper = new CreateProductMapper(createProductUnitMapper, createProductStoreMapper, createProductImageMapper, createProductBarcodeMapper);
    }

    /**
     * Execute entity to response with a null entity will return null.
     */
    @Test
    public void executeEntityToResponseWithANullEntityWillReturnNull() {
        Product entity = null;
        CreateProductResponse response = mapper.entityToResponse(entity);

        assertNull(response, "must be null");
    }

    /**
     * Execute entity to response with a non null entity will return with success.
     */
    @Test
    public void executeEntityToResponseWithANonNullEntityWillReturnWithSuccess() {
        final String code = "5445";
        final String name = "Blue Product";
        final String barcode = "049000037111";
        final String description = "Blue Product is the most blue product.";
        final String unitCode = "PC";
        final String unitName = "PIECE";
        final BigDecimal price = BigDecimal.valueOf(1.20);
        final String storeCode = "345";
        final String storeName = "Matrix";
        final String imageUrl = "http://www.sharepic.com/pic01.png";
        final String imageDescription = "thumb";

        Product entity = Product.newBuilder()
                .code(code)
                .name(name)
                .barcode(Barcode.newBuilder().code(barcode).build())
                .description(description)
                .unit(Unit.newBuilder().code(unitCode).name(unitName).build())
                .price(price)
                .store(Store.newBuilder().code(storeCode).name(storeName).build())
                .images(Arrays.asList(Image.newBuilder().url(imageUrl).description(imageDescription).build()))
                .build();

        CreateProductResponse response = mapper.entityToResponse(entity);

        assertNotNull(response, "create product response do not must be null");
        assertNotNull(response.getUnit(), "create product response unit do not must be null");
        assertNotNull(response.getStore(), "create product response store do not must be null");
        assertNotNull(response.getImages(), "create product response images do not must be null");

        assertEquals(code, response.getCode());
        assertEquals(name, response.getName());
        assertEquals(barcode, response.getBarcode());
        assertEquals(description, response.getDescription());
        assertEquals(unitCode, response.getUnit().getCode());
        assertEquals(unitName, response.getUnit().getName());
        assertEquals(price, response.getPrice());
        assertEquals(storeCode, response.getStore().getCode());
        assertEquals(storeName, response.getStore().getName());
        assertEquals(imageUrl, response.getImages().get(0).getUrl());
        assertEquals(imageDescription, response.getImages().get(0).getDescription());
    }

    /**
     * Execute request to entity with a null request will return null.
     */
    @Test
    public void executeRequestToEntityWithANullRequestWillReturnNull() {
        CreateProductRequest request = null;
        Product store = mapper.requestToEntity(request);

        assertNull(store, "must be null");
    }

    /**
     * Execute request to entity with a non null request will return with success.
     */
    @Test
    public void executeRequestToEntityWithANonNullRequestWillReturnWithSuccess() {
        final String code = "5445";
        final String name = "Blue Product";
        final String barcode = "049000037111";
        final String description = "Blue Product is the most blue product.";
        final String unitCode = "PC";
        final String unitName = "PIECE";
        final String storeCode = "345";
        final String storeName = "Matrix";
        final String imageUrl = "http://www.sharepic.com/pic01.png";
        final String imageDescription = "thumb";

        final CreateProductUnitRequest unit = new CreateProductUnitRequest();
        unit.setCode(unitCode);
        unit.setName(unitName);

        final BigDecimal price = BigDecimal.valueOf(1.20);

        CreateProductStoreRequest store = new CreateProductStoreRequest();
        store.setCode(storeCode);
        store.setName(storeName);

        CreateProductImageRequest image = new CreateProductImageRequest();
        image.setUrl(imageUrl);
        image.setDescription(imageDescription);
        List<CreateProductImageRequest> images = Arrays.asList(image);

        CreateProductRequest request = new CreateProductRequest();

        request.setCode(code);
        request.setName(name);
        request.setBarcode(barcode);
        request.setDescription(description);
        request.setUnit(unit);
        request.setPrice(price);
        request.setStore(store);
        request.setImages(images);

        Product product = mapper.requestToEntity(request);

        assertNotNull(product, "product do not must be null");
        assertNotNull(product.getBarcode(), "product barcode do not must be null");
        assertNotNull(product.getUnit(), "product unit do not must be null");
        assertNotNull(product.getStore(), "product store do not must be null");
        assertNotNull(product.getImages(), "product images do not must be null");
        assertFalse(product.getImages().isEmpty());
        assertEquals(1, product.getImages().size());

        Image productImage = product.getImages().get(0);

        assertEquals(code, product.getCode());
        assertEquals(name, product.getName());
        assertEquals(barcode, product.getBarcode().getCode());
        assertEquals(BarcodeType.UPC_A, product.getBarcode().getBarcodeType());
        assertEquals(description, product.getDescription());
        assertEquals(unit.getCode(), product.getUnit().getCode());
        assertEquals(unit.getName(), product.getUnit().getName());
        assertEquals(price, product.getPrice());
        assertEquals(store.getCode(), product.getStore().getCode());
        assertEquals(store.getName(), product.getStore().getName());
        assertEquals(imageUrl, productImage.getUrl());
        assertEquals(imageDescription, productImage.getDescription());
    }

}