package com.benparvar.catalog.controller.store;

import com.benparvar.catalog.controller.store.model.create.CreateStoreMapper;
import com.benparvar.catalog.controller.store.model.create.CreateStoreRequest;
import com.benparvar.catalog.controller.store.model.create.CreateStoreResponse;
import com.benparvar.catalog.controller.store.model.delete.DeleteStoreMapper;
import com.benparvar.catalog.controller.store.model.find.FindStoreMapper;
import com.benparvar.catalog.controller.store.model.find.FindStoreResponse;
import com.benparvar.catalog.controller.store.model.update.UpdateStoreMapper;
import com.benparvar.catalog.controller.store.model.update.UpdateStoreRequest;
import com.benparvar.catalog.controller.store.model.update.UpdateStoreResponse;
import com.benparvar.catalog.domain.entity.store.Store;
import com.benparvar.catalog.usecase.store.create.CreateStoreUseCase;
import com.benparvar.catalog.usecase.store.delete.DeleteStoreUseCase;
import com.benparvar.catalog.usecase.store.find.all.FindAllStoresUseCase;
import com.benparvar.catalog.usecase.store.find.code.FindStoreByCodeUseCase;
import com.benparvar.catalog.usecase.store.update.UpdateStoreUseCase;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

/**
 * The type Store controller test.
 */
class StoreControllerTest {
    private StoreController controller;
    private CreateStoreUseCase createStoreUseCase = mock(CreateStoreUseCase.class);
    private CreateStoreMapper createStoreMapper = new CreateStoreMapper();
    private FindAllStoresUseCase findAllStoresUseCase = mock(FindAllStoresUseCase.class);
    private FindStoreMapper findStoreMapper = new FindStoreMapper();
    private FindStoreByCodeUseCase findStoreByCodeUseCase = mock(FindStoreByCodeUseCase.class);
    private UpdateStoreUseCase updateStoreUseCase = mock(UpdateStoreUseCase.class);
    private UpdateStoreMapper updateStoreMapper = new UpdateStoreMapper();
    private DeleteStoreUseCase deleteStoreUseCase = mock(DeleteStoreUseCase.class);
    private DeleteStoreMapper deleteStoreMapper = new DeleteStoreMapper();

    /**
     * Init.
     */
    @BeforeEach
    public void init() {
        controller = new StoreController(createStoreUseCase, createStoreMapper, findAllStoresUseCase, findStoreMapper,
                findStoreByCodeUseCase, updateStoreUseCase, updateStoreMapper, deleteStoreUseCase, deleteStoreMapper);
    }

    /**
     * Execute create use case with a null request will fail.
     */
    @Test
    public void executeCreateUseCaseWithANullRequestWillFail() {
        assertThrows(IllegalArgumentException.class, () -> {
            Store entity = null;
            CreateStoreRequest request = null;
            when(createStoreUseCase.execute(entity))
                    .thenThrow(new IllegalArgumentException("store cannot be null"));

            try {
                controller.create(request);
            } catch (IllegalArgumentException e) {
                assertEquals("store cannot be null", e.getMessage());
                verify(createStoreUseCase, times(1)).execute(entity);
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }

    /**
     * Execute create use case with a valid request will success.
     */
    @Test
    public void executeCreateUseCaseWithAValidRequestWillSuccess() {
        Store entity = Store.newBuilder().code("MP").name("Mili Pieces").build();
        CreateStoreRequest request = new CreateStoreRequest();
        request.setCode("MP");
        request.setName("Mili Pieces");
        when(createStoreUseCase.execute(entity)).thenReturn(entity);

        CreateStoreResponse response = controller.create(request);

        assertNotNull(response);
        assertEquals("MP", response.getCode());
        assertEquals("Mili Pieces", request.getName());
        verify(createStoreUseCase, times(1)).execute(entity);
    }

    /**
     * Execute find all use case with empty repository will return a empty list.
     */
    @Test
    public void executeFindAllUseCaseWithEmptyDatabaseWillReturnAEmptyList() {
        when(findAllStoresUseCase.execute()).thenReturn(Collections.EMPTY_LIST);

        List<FindStoreResponse> responses = controller.findAll();
        assertNotNull(responses);
        assertTrue(responses.isEmpty());
    }

    /**
     * Execute find all use case with repository will return a list.
     */
    @Test
    public void executeFindAllUseCaseWithDatabaseWillReturnAList() {
        List<Store> entities = Arrays.asList(Store.newBuilder().code("MP").name("Mili Pieces").build());

        when(findAllStoresUseCase.execute()).thenReturn(entities);

        List<FindStoreResponse> responses = controller.findAll();
        assertNotNull(responses);
        assertFalse(responses.isEmpty());
        assertEquals(1, responses.size());
    }

    /**
     * Execute find by code with a valid code will success.
     */
    @Test
    public void executeFindByCodeWithAValidCodeWillSuccess() {
        String code = "MP";
        Store entity = Store.newBuilder().code(code).name("Mili Pieces").build();

        when(findStoreByCodeUseCase.execute(code)).thenReturn(entity);

        FindStoreResponse response = controller.findByCode(code);
        assertNotNull(response);
        assertEquals("MP", response.getCode());
        assertEquals("Mili Pieces", response.getName());
    }

    /**
     * Execute update with an valid entity will success.
     */
    @Test
    public void executeUpdateWithAnValidEntityWillSuccess() {
        Store entity = Store.newBuilder().code("MP").name("Milipieces").build();
        UpdateStoreRequest request = new UpdateStoreRequest();
        request.setCode("MP");
        request.setName("Milipieces");

        when(updateStoreUseCase.execute(entity)).thenReturn(entity);

        UpdateStoreResponse response = controller.update(request);
        assertEquals("MP", response.getCode());
        assertEquals("Milipieces", response.getName());
    }

    /**
     * Execute delete with an valid entity wil success.
     */
    @Test
    public void executeDeleteWithAnValidEntityWilSuccess() {
        assertDoesNotThrow(() -> {
            controller.delete("MT");
        });
    }
}