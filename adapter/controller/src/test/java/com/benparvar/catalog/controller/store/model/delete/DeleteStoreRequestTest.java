package com.benparvar.catalog.controller.store.model.delete;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * The type Delete store request test.
 */
class DeleteStoreRequestTest {
    /**
     * Check hash code integrity.
     */
    @Test
    public void checkHashCodeIntegrity() {
        DeleteStoreRequest store1 = DeleteStoreRequest.newBuilder().code("LT").name("LITRE").build();
        DeleteStoreRequest store2 = DeleteStoreRequest.newBuilder().code("LT").name("LITRE").build();

        assertEquals(store1.hashCode(), store2.hashCode());
    }

    /**
     * Check equals integrity.
     */
    @Test
    public void checkEqualsIntegrity() {
        DeleteStoreRequest store1 = DeleteStoreRequest.newBuilder().code("LT").name("LITRE").build();
        DeleteStoreRequest store2 = DeleteStoreRequest.newBuilder().code("LT").name("LITRE").build();

        assertEquals(store1, store2);
    }

    /**
     * Check builder integrity.
     */
    @Test
    public void checkBuilderIntegrity() {
        DeleteStoreRequest store = DeleteStoreRequest.newBuilder().code("LT").name("LITRE").build();

        assertNotNull(store);
        assertEquals("LT", store.getCode());
        assertEquals("LITRE", store.getName());
    }

    /**
     * Check to string integrity.
     */
    @Test
    public void checkToStringIntegrity() {
        DeleteStoreRequest store = DeleteStoreRequest.newBuilder().code("LT").name("LITRE").build();

        assertNotNull(store);
        assertEquals("DeleteStoreRequest{code='LT', name='LITRE'}", store.toString());
    }
}