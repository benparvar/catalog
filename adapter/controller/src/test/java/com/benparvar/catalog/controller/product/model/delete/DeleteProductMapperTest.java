package com.benparvar.catalog.controller.product.model.delete;

import com.benparvar.catalog.domain.entity.product.Product;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * The type Delete product mapper test.
 */
class DeleteProductMapperTest {
    private DeleteProductMapper mapper;

    /**
     * Init.
     */
    @BeforeEach
    public void init() {
        mapper = new DeleteProductMapper();
    }

    /**
     * Execute request to entity with a null request will return null.
     */
    @Test
    public void executeRequestToEntityWithANullRequestWillReturnNull() {
        DeleteProductRequest request = null;
        Product product = mapper.requestToEntity(request);

        assertNull(product, "must be null");
    }

    /**
     * Execute request to entity with a non null request will return with success.
     */
    @Test
    public void executeRequestToEntityWithANonNullRequestWillReturnWithSuccess() {
        DeleteProductRequest request = DeleteProductRequest.newBuilder()
                .barcodeCode("049000006841").storeCode("12334").build();

        Product product = mapper.requestToEntity(request);

        assertNotNull(product, "do not must be null");
        assertEquals("049000006841", product.getBarcode().getCode());
        assertEquals("12334", product.getStore().getCode());
    }}