package com.benparvar.catalog.controller.store.model.find;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class FindStoreResponseTest {
    /**
     * Check hash code integrity.
     */
    @Test
    public void checkHashCodeIntegrity() {
        FindStoreResponse store1 = new FindStoreResponse();
        store1.setCode("LT");
        store1.setName("LITRE");
        FindStoreResponse store2 = new FindStoreResponse();
        store2.setCode("LT");
        store2.setName("LITRE");

        assertEquals(store1.hashCode(), store2.hashCode());
    }

    /**
     * Check equals integrity.
     */
    @Test
    public void checkEqualsIntegrity() {
        FindStoreResponse store1 = new FindStoreResponse();
        store1.setCode("LT");
        store1.setName("LITRE");
        FindStoreResponse store2 = new FindStoreResponse();
        store2.setCode("LT");
        store2.setName("LITRE");
        assertEquals(store1, store2);
    }

    /**
     * Check builder integrity.
     */
    @Test
    public void checkBuilderIntegrity() {
        FindStoreResponse store = new FindStoreResponse();
        store.setCode("LT");
        store.setName("LITRE");

        assertNotNull(store);
        assertEquals("LT", store.getCode());
        assertEquals("LITRE", store.getName());
    }

    /**
     * Check to string integrity.
     */
    @Test
    public void checkToStringIntegrity() {
        FindStoreResponse store = new FindStoreResponse();
        store.setCode("LT");
        store.setName("LITRE");

        assertNotNull(store);
        assertEquals("FindStoreResponse{code='LT', name='LITRE'}", store.toString());
    }
}