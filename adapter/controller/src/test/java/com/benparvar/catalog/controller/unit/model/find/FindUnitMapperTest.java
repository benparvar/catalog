package com.benparvar.catalog.controller.unit.model.find;

import com.benparvar.catalog.controller.unit.model.find.FindUnitMapper;
import com.benparvar.catalog.controller.unit.model.find.FindUnitResponse;
import com.benparvar.catalog.domain.entity.unit.Unit;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * The type Find unit mapper test.
 */
class FindUnitMapperTest {
    private FindUnitMapper mapper;

    /**
     * Init.
     */
    @BeforeEach
    public void init() {
        mapper = new FindUnitMapper();
    }

    /**
     * Execute entity to response with a null entity will return null.
     */
    @Test
    public void executeEntityToResponseWithANullEntityWillReturnNull() {
        Unit entity = null;
        FindUnitResponse response = mapper.entityToResponse(entity);

        assertNull(response, "must be null");
    }

    /**
     * Execute entity to response with a non null entity will return with success.
     */
    @Test
    public void executeEntityToResponseWithANonNullEntityWillReturnWithSuccess() {
        Unit entity = Unit.newBuilder().code("MT").name("Meter").build();

        FindUnitResponse response = mapper.entityToResponse(entity);

        assertNotNull(response, "do not must be null");
        assertEquals("MT", response.getCode());
        assertEquals("Meter", response.getName());
    }

    /**
     * Execute entity to response with a null entity list will return a empty list.
     */
    @Test
    public void executeEntityToResponseWithANullEntityListWillReturnAEmptyList() {
        List<Unit> entities = null;
        List<FindUnitResponse> responses = mapper.entityToResponse(entities);

        assertNotNull(responses, "must not be null");
        assertTrue(responses.isEmpty());
    }

    /**
     * Execute entity to response with a non empty entity list will return with success.
     */
    @Test
    public void executeEntityToResponseWithANonEmptyEntityListWillReturnWithSuccess() {
        Unit entity = Unit.newBuilder().code("MT").name("Meter").build();

        List<FindUnitResponse> responses = mapper.entityToResponse(Arrays.asList(entity));

        assertNotNull(responses, "do not must be null");
        assertFalse(responses.isEmpty());
        assertEquals(1, responses.size());

        FindUnitResponse response = responses.get(0);

        assertEquals("MT", response.getCode());
        assertEquals("Meter", response.getName());
    }
}