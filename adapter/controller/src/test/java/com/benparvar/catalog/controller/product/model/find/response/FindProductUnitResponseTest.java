package com.benparvar.catalog.controller.product.model.find.response;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * The type Find product unit response test.
 */
class FindProductUnitResponseTest {
    /**
     * Check hash code integrity.
     */
    @Test
    public void checkHashCodeIntegrity() {
        FindProductUnitResponse unit1 = new FindProductUnitResponse();
        unit1.setCode("LT");
        unit1.setName("LITRE");
        FindProductUnitResponse unit2 = new FindProductUnitResponse();
        unit2.setCode("LT");
        unit2.setName("LITRE");

        assertEquals(unit1.hashCode(), unit2.hashCode());
    }

    /**
     * Check equals integrity.
     */
    @Test
    public void checkEqualsIntegrity() {
        FindProductUnitResponse unit1 = new FindProductUnitResponse();
        unit1.setCode("LT");
        unit1.setName("LITRE");
        FindProductUnitResponse unit2 = new FindProductUnitResponse();
        unit2.setCode("LT");
        unit2.setName("LITRE");

        assertEquals(unit1, unit2);
    }

    /**
     * Check builder integrity.
     */
    @Test
    public void checkBuilderIntegrity() {
        FindProductUnitResponse unit = new FindProductUnitResponse();
        unit.setCode("LT");
        unit.setName("LITRE");

        assertNotNull(unit);
        assertEquals("LT", unit.getCode());
        assertEquals("LITRE", unit.getName());
    }

    /**
     * Check to string integrity.
     */
    @Test
    public void checkToStringIntegrity() {
        FindProductUnitResponse unit = new FindProductUnitResponse();
        unit.setCode("LT");
        unit.setName("LITRE");

        assertNotNull(unit);
        assertEquals("FindProductUnitResponse{code='LT', name='LITRE'}", unit.toString());
    }
}