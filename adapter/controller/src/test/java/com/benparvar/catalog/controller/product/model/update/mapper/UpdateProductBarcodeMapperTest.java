package com.benparvar.catalog.controller.product.model.update.mapper;

import com.benparvar.catalog.domain.entity.barcode.Barcode;
import com.benparvar.catalog.domain.entity.barcode.BarcodeType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * The type Update product barcode mapper test.
 */
class UpdateProductBarcodeMapperTest {
    private UpdateProductBarcodeMapper mapper;

    /**
     * Init.
     */
    @BeforeEach
    public void init() {
        mapper = new UpdateProductBarcodeMapper();
    }

    /**
     * Execute entity to code with a null entity will return null.
     */
    @Test
    public void executeEntityToCodeWithANullEntityWillReturnNull() {
        Barcode entity = null;
        String code = mapper.entityToCode(entity);

        assertNull(code, "must be null");
    }

    /**
     * Execute entity to code with a non null entity and upca will return success.
     */
    @Test
    public void executeEntityToCodeWithANonNullEntityAndUPCAWillReturnSuccess() {
        final String barcode = "049000037111";
        final BarcodeType barcodeType = BarcodeType.UPC_A;

        Barcode entity = Barcode.newBuilder().code(barcode).barcodeType(barcodeType).build();
        String code = mapper.entityToCode(entity);

        assertNotNull(code, "must not be null");
        assertEquals(barcode, code);
        assertEquals(barcodeType, entity.getBarcodeType());
        assertEquals(barcode, entity.getCode());
    }

    /**
     * Execute entity to code with a non null entity and ean 8 will return success.
     */
    @Test
    public void executeEntityToCodeWithANonNullEntityAndEAN8WillReturnSuccess() {
        final String barcode = "96385074";
        final BarcodeType barcodeType = BarcodeType.EAN_8;

        Barcode entity = Barcode.newBuilder().code(barcode).barcodeType(barcodeType).build();
        String code = mapper.entityToCode(entity);

        assertNotNull(code, "must not be null");
        assertEquals(barcode, code);
        assertEquals(barcodeType, entity.getBarcodeType());
        assertEquals(barcode, entity.getCode());
    }

    /**
     * Execute entity to code with a non null entity and ean 13 will return success.
     */
    @Test
    public void executeEntityToCodeWithANonNullEntityAndEAN13WillReturnSuccess() {
        final String barcode = "978020113447";
        final BarcodeType barcodeType = BarcodeType.EAN_13;

        Barcode entity = Barcode.newBuilder().code(barcode).barcodeType(barcodeType).build();
        String code = mapper.entityToCode(entity);

        assertNotNull(code, "must not be null");
        assertEquals(barcode, code);
        assertEquals(barcodeType, entity.getBarcodeType());
        assertEquals(barcode, entity.getCode());
    }
}