package com.benparvar.catalog.controller.product;

import com.benparvar.catalog.controller.product.model.create.mapper.*;
import com.benparvar.catalog.controller.product.model.create.request.*;
import com.benparvar.catalog.controller.product.model.create.response.CreateProductResponse;
import com.benparvar.catalog.controller.product.model.delete.DeleteProductMapper;
import com.benparvar.catalog.controller.product.model.find.mapper.*;
import com.benparvar.catalog.controller.product.model.find.response.FindProductResponse;
import com.benparvar.catalog.controller.product.model.update.mapper.*;
import com.benparvar.catalog.controller.product.model.update.response.UpdateProductResponse;
import com.benparvar.catalog.domain.entity.barcode.Barcode;
import com.benparvar.catalog.domain.entity.barcode.BarcodeType;
import com.benparvar.catalog.domain.entity.image.Image;
import com.benparvar.catalog.domain.entity.product.Product;
import com.benparvar.catalog.domain.entity.product.UpdateProductPrice;
import com.benparvar.catalog.domain.entity.store.Store;
import com.benparvar.catalog.domain.entity.unit.Unit;
import com.benparvar.catalog.usecase.product.create.CreateProductUseCase;
import com.benparvar.catalog.usecase.product.delete.DeleteProductUseCase;
import com.benparvar.catalog.usecase.product.find.FindProductByBarcodeAndStoreCodeUseCase;
import com.benparvar.catalog.usecase.product.update.UpdateProductPriceByBarcodeAndStoreCodeCodeUseCase;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

/**
 * The type Product controller test.
 */
class ProductControllerTest {
    private ProductController controller;
    private CreateProductUnitMapper createProductUnitMapper = new CreateProductUnitMapper();
    private CreateProductStoreMapper createProductStoreMapper = new CreateProductStoreMapper();
    private CreateProductImageMapper createProductImageMapper = new CreateProductImageMapper();
    private CreateProductBarcodeMapper createProductBarcodeMapper = new CreateProductBarcodeMapper();
    private FindProductUnitMapper unitMapper = new FindProductUnitMapper();
    private FindProductStoreMapper storeMapper = new FindProductStoreMapper();
    private FindProductImageMapper imageMapper = new FindProductImageMapper();
    private FindProductBarcodeMapper barcodeMapper = new FindProductBarcodeMapper();
    private UpdateProductUnitMapper updateProductUnitMapper = new UpdateProductUnitMapper();
    private UpdateProductStoreMapper updateProductStoreMapper = new UpdateProductStoreMapper();
    private UpdateProductImageMapper updateProductImageMapper = new UpdateProductImageMapper();
    private UpdateProductBarcodeMapper updateProductBarcodeMapper = new UpdateProductBarcodeMapper();
    private CreateProductUseCase createProductUseCase = mock(CreateProductUseCase.class);
    private FindProductByBarcodeAndStoreCodeUseCase findProductByBarcodeAndStoreCodeUseCase =
            mock(FindProductByBarcodeAndStoreCodeUseCase.class);
    private CreateProductMapper createProductMapper = new CreateProductMapper(createProductUnitMapper,
            createProductStoreMapper, createProductImageMapper, createProductBarcodeMapper);
    private FindProductMapper findProductMapper = new FindProductMapper(unitMapper, storeMapper, imageMapper, barcodeMapper);
    private UpdateProductPriceByBarcodeAndStoreCodeCodeUseCase updateProductPriceByBarcodeAndStoreCodeCodeUseCase =
            mock(UpdateProductPriceByBarcodeAndStoreCodeCodeUseCase.class);
    private UpdateProductMapper updateProductMapper = new UpdateProductMapper(updateProductUnitMapper,
            updateProductStoreMapper, updateProductImageMapper, updateProductBarcodeMapper);
    private DeleteProductUseCase deleteProductUseCase = mock(DeleteProductUseCase.class);
    private DeleteProductMapper deleteProductMapper = new DeleteProductMapper();

    /**
     * Init.
     */
    @BeforeEach
    public void init() {
        controller = new ProductController(createProductUseCase, createProductMapper, findProductByBarcodeAndStoreCodeUseCase,
                findProductMapper, updateProductPriceByBarcodeAndStoreCodeCodeUseCase, updateProductMapper,
                deleteProductUseCase, deleteProductMapper);
    }

    /**
     * Execute create use case with a null request will fail.
     */
    @Test
    public void executeCreateUseCaseWithANullRequestWillFail() {
        assertThrows(IllegalArgumentException.class, () -> {
            Product entity = null;
            CreateProductRequest request = null;
            when(createProductUseCase.execute(entity))
                    .thenThrow(new IllegalArgumentException("product cannot be null"));

            try {
                controller.create(request);
            } catch (IllegalArgumentException e) {
                assertEquals("product cannot be null", e.getMessage());
                verify(createProductUseCase, times(1)).execute(entity);
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }

    /**
     * Execute create use case with a valid request will success.
     */
    @Test
    public void executeCreateUseCaseWithAValidRequestWillSuccess() {
        final String code = "5445";
        final String name = "Blue Product";
        final String barcode = "049000037111";
        final String description = "Blue Product is the most blue product.";
        final String unitCode = "PC";
        final String unitName = "PIECE";
        final String storeCode = "345";
        final String storeName = "Matrix";
        final String imageUrl = "http://www.sharepic.com/pic01.png";
        final String imageDescription = "thumb";

        final CreateProductUnitRequest unit = new CreateProductUnitRequest();
        unit.setCode(unitCode);
        unit.setName(unitName);

        final BigDecimal price = BigDecimal.valueOf(1.20);

        CreateProductStoreRequest store = new CreateProductStoreRequest();
        store.setCode(storeCode);
        store.setName(storeName);

        CreateProductImageRequest image = new CreateProductImageRequest();
        image.setUrl(imageUrl);
        image.setDescription(imageDescription);

        Product entity = Product.newBuilder()
                .code(code)
                .name(name)
                .barcode(Barcode.newBuilder().code(barcode).barcodeType(BarcodeType.UPC_A).build())
                .description(description)
                .unit(Unit.newBuilder().code(unitCode).name(unitName).build())
                .price(price)
                .store(Store.newBuilder().code(storeCode).name(storeName).build())
                .images(Arrays.asList(Image.newBuilder().url(imageUrl).description(imageDescription).build()))
                .build();

        CreateProductRequest request = new CreateProductRequest();

        request.setCode(code);
        request.setName(name);
        request.setBarcode(barcode);
        request.setDescription(description);
        request.setUnit(unit);
        request.setPrice(price);
        request.setStore(store);
        request.setImages(Arrays.asList(image));

        when(createProductUseCase.execute(entity)).thenReturn(entity);

        CreateProductResponse response = controller.create(request);

        assertNotNull(response, "create product response do not must be null");
        assertNotNull(response.getUnit(), "create product response unit do not must be null");
        assertNotNull(response.getStore(), "create product response store do not must be null");
        assertNotNull(response.getImages(), "create product response images do not must be null");

        assertEquals(code, response.getCode());
        assertEquals(name, response.getName());
        assertEquals(barcode, response.getBarcode());
        assertEquals(description, response.getDescription());
        assertEquals(unitCode, response.getUnit().getCode());
        assertEquals(unitName, response.getUnit().getName());
        assertEquals(price, response.getPrice());
        assertEquals(storeCode, response.getStore().getCode());
        assertEquals(storeName, response.getStore().getName());
        assertEquals(image.getDescription(), response.getImages().get(0).getDescription());
        assertEquals(image.getUrl(), response.getImages().get(0).getUrl());

        verify(createProductUseCase, times(1)).execute(entity);
    }

    /**
     * Execute find product by barcode and store with a existent product will success.
     */
    @Test
    public void executeFindProductByBarcodeAndStoreWithAExistentProductWillSuccess() {
        final String code = "5445";
        final String name = "Blue Product";
        final String barcodeCode = "049000037111";
        final String description = "Blue Product is the most blue product.";
        final String unitCode = "PC";
        final String unitName = "PIECE";
        final String storeCode = "345";
        final String storeName = "Matrix";
        final String imageUrl = "http://www.sharepic.com/pic01.png";
        final String imageDescription = "thumb";

        final CreateProductUnitRequest unit = new CreateProductUnitRequest();
        unit.setCode(unitCode);
        unit.setName(unitName);

        final BigDecimal price = BigDecimal.valueOf(1.20);

        CreateProductStoreRequest store = new CreateProductStoreRequest();
        store.setCode(storeCode);
        store.setName(storeName);

        CreateProductImageRequest image = new CreateProductImageRequest();
        image.setUrl(imageUrl);
        image.setDescription(imageDescription);

        Product entity = Product.newBuilder()
                .code(code)
                .name(name)
                .barcode(Barcode.newBuilder().code(barcodeCode).barcodeType(BarcodeType.UPC_A).build())
                .description(description)
                .unit(Unit.newBuilder().code(unitCode).name(unitName).build())
                .price(price)
                .store(Store.newBuilder().code(storeCode).name(storeName).build())
                .images(Arrays.asList(Image.newBuilder().url(imageUrl).description(imageDescription).build()))
                .build();

        when(findProductByBarcodeAndStoreCodeUseCase.execute(barcodeCode, storeCode)).thenReturn(entity);

        FindProductResponse response = controller.findProductByBarcodeAndStoreCode(barcodeCode, storeCode);

        assertNotNull(response, "find product response do not must be null");

        verify(findProductByBarcodeAndStoreCodeUseCase, times(1)).execute(barcodeCode, storeCode);
    }

    /**
     * Execute update product price by barcode and store code with a existent product will success.
     */
    @Test
    public void executeUpdateProductPriceByBarcodeAndStoreCodeWithAExistentProductWillSuccess() {
        final String code = "5445";
        final String name = "Blue Product";
        final String barcodeCode = "049000037111";
        final String description = "Blue Product is the most blue product.";
        final String unitCode = "PC";
        final String unitName = "PIECE";
        final String storeCode = "345";
        final String storeName = "Matrix";
        final String imageUrl = "http://www.sharepic.com/pic01.png";
        final String imageDescription = "thumb";
        final BigDecimal price = BigDecimal.valueOf(1.20);

        Product entity = Product.newBuilder()
                .code(code)
                .name(name)
                .barcode(Barcode.newBuilder().code(barcodeCode).barcodeType(BarcodeType.UPC_A).build())
                .description(description)
                .unit(Unit.newBuilder().code(unitCode).name(unitName).build())
                .price(price)
                .store(Store.newBuilder().code(storeCode).name(storeName).build())
                .images(Arrays.asList(Image.newBuilder().url(imageUrl).description(imageDescription).build()))
                .build();

        UpdateProductPrice updateProductPrice = UpdateProductPrice.newBuilder()
                .barcode(barcodeCode)
                .storeCode(storeCode)
                .price(price)
                .build();

        UpdateProductPriceRequest updateProductPriceRequest = new UpdateProductPriceRequest();
        updateProductPriceRequest.setBarcode(barcodeCode);
        updateProductPriceRequest.setStoreCode(storeCode);
        updateProductPriceRequest.setPrice(price);

        when(updateProductPriceByBarcodeAndStoreCodeCodeUseCase.execute(updateProductPrice)).thenReturn(entity);

        UpdateProductResponse response = controller.updateProductPriceByBarcodeAndStoreCode(updateProductPriceRequest);

        assertNotNull(response, "find product response do not must be null");

        verify(updateProductPriceByBarcodeAndStoreCodeCodeUseCase, times(1)).execute(updateProductPrice);
    }

    /**
     * Execute delete with an valid entity wil success.
     */
    @Test
    public void executeDeleteWithAnValidEntityWilSuccess() {
        assertDoesNotThrow(() -> {
            controller.delete("7894900700046", "123");
        });
    }
}