package com.benparvar.catalog.controller.product.model.find.mapper;

import com.benparvar.catalog.controller.product.model.find.response.FindProductImageResponse;
import com.benparvar.catalog.domain.entity.image.Image;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * The type Find product image mapper test.
 */
class FindProductCreateProductImageMapperTest {
    private FindProductImageMapper mapper;

    /**
     * Init.
     */
    @BeforeEach
    public void init() {
        mapper = new FindProductImageMapper();
    }

    /**
     * Execute entity to response with a null entity will return null.
     */
    @Test
    public void executeEntityToResponseWithANullEntityWillReturnNull() {
        Image entity = null;
        FindProductImageResponse response = mapper.entityToResponse(entity);

        assertNull(response, "must be null");
    }

    /**
     * Execute entity to response with a non null entity will return sucess.
     */
    @Test
    public void executeEntityToResponseWithANonNullEntityWillReturnSucess() {
        Image entity = Image.newBuilder().description("thumbs").url("www.goo.com/pic1.png").build();
        FindProductImageResponse response = mapper.entityToResponse(entity);

        assertNotNull(response, "must not be null");
        assertEquals("thumbs", response.getDescription());
        assertEquals("www.goo.com/pic1.png", response.getUrl());
    }
}