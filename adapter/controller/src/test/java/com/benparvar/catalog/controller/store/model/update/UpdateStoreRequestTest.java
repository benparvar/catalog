package com.benparvar.catalog.controller.store.model.update;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * The type Update store request test.
 */
class UpdateStoreRequestTest {
    /**
     * Check hash code integrity.
     */
    @Test
    public void checkHashCodeIntegrity() {
        UpdateStoreRequest store1 = new UpdateStoreRequest();
        store1.setCode("LT");
        store1.setName("LITRE");
        UpdateStoreRequest store2 = new UpdateStoreRequest();
        store2.setCode("LT");
        store2.setName("LITRE");

        assertEquals(store1.hashCode(), store2.hashCode());
    }

    /**
     * Check equals integrity.
     */
    @Test
    public void checkEqualsIntegrity() {
        UpdateStoreRequest store1 = new UpdateStoreRequest();
        store1.setCode("LT");
        store1.setName("LITRE");
        UpdateStoreRequest store2 = new UpdateStoreRequest();
        store2.setCode("LT");
        store2.setName("LITRE");

        assertEquals(store1, store2);
    }

    /**
     * Check builder integrity.
     */
    @Test
    public void checkBuilderIntegrity() {
        UpdateStoreRequest store = new UpdateStoreRequest();
        store.setCode("LT");
        store.setName("LITRE");

        assertNotNull(store);
        assertEquals("LT", store.getCode());
        assertEquals("LITRE", store.getName());
    }

    /**
     * Check to string integrity.
     */
    @Test
    public void checkToStringIntegrity() {
        UpdateStoreRequest store = new UpdateStoreRequest();
        store.setCode("LT");
        store.setName("LITRE");

        assertNotNull(store);
        assertEquals("UpdateStoreRequest{code='LT', name='LITRE'}", store.toString());
    }
}