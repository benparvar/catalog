package com.benparvar.catalog.controller.product.model.update.mapper;

import com.benparvar.catalog.controller.product.model.update.response.UpdateProductUnitResponse;
import com.benparvar.catalog.domain.entity.unit.Unit;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * The type Update product unit mapper test.
 */
class UpdateProductUnitMapperTest {
    private  UpdateProductUnitMapper mapper;

    /**
     * Init.
     */
    @BeforeEach
    public void init() {
        mapper = new  UpdateProductUnitMapper();
    }

    /**
     * Execute entity to response with a null entity will return null.
     */
    @Test
    public void executeEntityToResponseWithANullEntityWillReturnNull() {
        Unit entity = null;
        UpdateProductUnitResponse response = mapper.entityToResponse(entity);

        assertNull(response, "must be null");
    }

    /**
     * Execute entity to response with a non null entity will return with success.
     */
    @Test
    public void executeEntityToResponseWithANonNullEntityWillReturnWithSuccess() {
        Unit entity = Unit.newBuilder().code("MT").name("Meter").build();

        UpdateProductUnitResponse response = mapper.entityToResponse(entity);

        assertNotNull(response, "do not must be null");
        assertEquals("MT", response.getCode());
        assertEquals("Meter", response.getName());
    }
}