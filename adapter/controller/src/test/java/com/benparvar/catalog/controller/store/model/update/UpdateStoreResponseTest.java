package com.benparvar.catalog.controller.store.model.update;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * The type Update store response test.
 */
class UpdateStoreResponseTest {
    /**
     * Check hash code integrity.
     */
    @Test
    public void checkHashCodeIntegrity() {
        UpdateStoreResponse store1 = new UpdateStoreResponse();
        store1.setCode("LT");
        store1.setName("LITRE");
        UpdateStoreResponse store2 = new UpdateStoreResponse();
        store2.setCode("LT");
        store2.setName("LITRE");

        assertEquals(store1.hashCode(), store2.hashCode());
    }

    /**
     * Check equals integrity.
     */
    @Test
    public void checkEqualsIntegrity() {
        UpdateStoreResponse store1 = new UpdateStoreResponse();
        store1.setCode("LT");
        store1.setName("LITRE");
        UpdateStoreResponse store2 = new UpdateStoreResponse();
        store2.setCode("LT");
        store2.setName("LITRE");
        assertEquals(store1, store2);
    }

    /**
     * Check builder integrity.
     */
    @Test
    public void checkBuilderIntegrity() {
        UpdateStoreResponse store = new UpdateStoreResponse();
        store.setCode("LT");
        store.setName("LITRE");

        assertNotNull(store);
        assertEquals("LT", store.getCode());
        assertEquals("LITRE", store.getName());
    }

    /**
     * Check to string integrity.
     */
    @Test
    public void checkToStringIntegrity() {
        UpdateStoreResponse store = new UpdateStoreResponse();
        store.setCode("LT");
        store.setName("LITRE");

        assertNotNull(store);
        assertEquals("UpdateStoreResponse{code='LT', name='LITRE'}", store.toString());
    }
}