package com.benparvar.catalog.controller.product.model.find.response;

import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * The type Find product response test.
 */
class FindProductResponseTest {
    /**
     * Check hash code integrity.
     */
    @Test
    public void checkHashCodeIntegrity() {
        FindProductStoreResponse store = new FindProductStoreResponse();
        store.setCode("654");
        store.setName("Matrix store");

        FindProductUnitResponse unit = new FindProductUnitResponse();
        unit.setCode("UN");
        unit.setName("Unit");

        FindProductImageResponse image = new FindProductImageResponse();
        image.setUrl("www.matrix/123456/pic1.jpg");
        image.setDescription("thumb");

        FindProductResponse product1 = new FindProductResponse();
        product1.setCode("123456");
        product1.setName("Xablau Gibi");
        product1.setDescription("Xablau Gibi Zero edition");
        product1.setBarcode("5449000131843");
        product1.setImages(Arrays.asList(image));
        product1.setPrice(BigDecimal.valueOf(2.34));
        product1.setStore(store);
        product1.setUnit(unit);

        FindProductResponse product2 = new FindProductResponse();
        product2.setCode("123456");
        product2.setName("Xablau Gibi");
        product2.setDescription("Xablau Gibi Zero edition");
        product2.setBarcode("5449000131843");
        product2.setImages(Arrays.asList(image));
        product2.setPrice(BigDecimal.valueOf(2.34));
        product2.setStore(store);
        product2.setUnit(unit);

        assertEquals(product1.hashCode(), product2.hashCode());
    }

    /**
     * Check equals integrity.
     */
    @Test
    public void checkEqualsIntegrity() {
        FindProductStoreResponse store = new FindProductStoreResponse();
        store.setCode("654");
        store.setName("Matrix store");

        FindProductUnitResponse unit = new FindProductUnitResponse();
        unit.setCode("UN");
        unit.setName("Unit");

        FindProductImageResponse image = new FindProductImageResponse();
        image.setUrl("www.matrix/123456/pic1.jpg");
        image.setDescription("thumb");

        FindProductResponse product1 = new FindProductResponse();
        product1.setCode("123456");
        product1.setName("Xablau Gibi");
        product1.setDescription("Xablau Gibi Zero edition");
        product1.setBarcode("5449000131843");
        product1.setImages(Arrays.asList(image));
        product1.setPrice(BigDecimal.valueOf(2.34));
        product1.setStore(store);
        product1.setUnit(unit);

        FindProductResponse product2 = new FindProductResponse();
        product2.setCode("123456");
        product2.setName("Xablau Gibi");
        product2.setDescription("Xablau Gibi Zero edition");
        product2.setBarcode("5449000131843");
        product2.setImages(Arrays.asList(image));
        product2.setPrice(BigDecimal.valueOf(2.34));
        product2.setStore(store);
        product2.setUnit(unit);

        assertEquals(product1, product2);
    }

    /**
     * Check builder integrity.
     */
    @Test
    public void checkBuilderIntegrity() {
        FindProductStoreResponse store = new FindProductStoreResponse();
        store.setCode("654");
        store.setName("Matrix store");

        FindProductUnitResponse unit = new FindProductUnitResponse();
        unit.setCode("UN");
        unit.setName("Unit");

        FindProductImageResponse image = new FindProductImageResponse();
        image.setUrl("www.matrix/123456/pic1.jpg");
        image.setDescription("thumb");

        FindProductResponse product = new FindProductResponse();
        product.setCode("123456");
        product.setName("Xablau Gibi");
        product.setDescription("Xablau Gibi Zero edition");
        product.setBarcode("5449000131843");
        product.setImages(Arrays.asList(image));
        product.setPrice(BigDecimal.valueOf(2.34));
        product.setStore(store);
        product.setUnit(unit);

        assertNotNull(product);
        assertEquals("123456", product.getCode());
        assertEquals("Xablau Gibi", product.getName());
    }

    /**
     * Check to string integrity.
     */
    @Test
    public void checkToStringIntegrity() {
        FindProductStoreResponse store = new FindProductStoreResponse();
        store.setCode("654");
        store.setName("Matrix store");

        FindProductUnitResponse unit = new FindProductUnitResponse();
        unit.setCode("UN");
        unit.setName("Unit");

        FindProductImageResponse image = new FindProductImageResponse();
        image.setUrl("www.matrix/123456/pic1.jpg");
        image.setDescription("thumb");

        FindProductResponse product = new FindProductResponse();
        product.setCode("123456");
        product.setName("Xablau Gibi");
        product.setDescription("Xablau Gibi Zero edition");
        product.setBarcode("5449000131843");
        product.setImages(Arrays.asList(image));
        product.setPrice(BigDecimal.valueOf(2.34));
        product.setStore(store);
        product.setUnit(unit);

        assertNotNull(product);
        assertNotNull(product.getPrice());
        assertNotNull(product.getUnit());
        assertEquals(unit, product.getUnit());
        assertNotNull(product.getStore());
        assertEquals(store, product.getStore());

        assertEquals("FindProductResponse{code='123456', name='Xablau Gibi', description='Xablau Gibi Zero edition', barcode='5449000131843', price=2.34, unit=FindProductUnitResponse{code='UN', name='Unit'}, store=FindProductStoreResponse{code='654', name='Matrix store'}, images=[FindProductImageResponse{description='thumb', url='www.matrix/123456/pic1.jpg'}]}", product.toString());
    }
}