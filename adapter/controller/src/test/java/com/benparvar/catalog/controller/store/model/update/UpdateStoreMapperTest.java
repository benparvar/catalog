package com.benparvar.catalog.controller.store.model.update;

import com.benparvar.catalog.domain.entity.store.Store;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * The type Update store mapper test.
 */
class UpdateStoreMapperTest {
    private UpdateStoreMapper mapper;

    /**
     * Init.
     */
    @BeforeEach
    public void init() {
        mapper = new UpdateStoreMapper();
    }

    /**
     * Execute entity to response with a null entity will return null.
     */
    @Test
    public void executeEntityToResponseWithANullEntityWillReturnNull() {
        Store entity = null;
        UpdateStoreResponse response = mapper.entityToResponse(entity);

        assertNull(response, "must be null");
    }

    /**
     * Execute entity to response with a non null entity will return with success.
     */
    @Test
    public void executeEntityToResponseWithANonNullEntityWillReturnWithSuccess() {
        Store entity = Store.newBuilder().code("MT").name("Meter").build();

        UpdateStoreResponse response = mapper.entityToResponse(entity);

        assertNotNull(response, "do not must be null");
        assertEquals("MT", response.getCode());
        assertEquals("Meter", response.getName());
    }

    /**
     * Execute request to entity with a null request will return null.
     */
    @Test
    public void executeRequestToEntityWithANullRequestWillReturnNull() {
        UpdateStoreRequest request = null;
        Store store = mapper.requestToEntity(request);

        assertNull(store, "must be null");
    }

    /**
     * Execute request to entity with a non null request will return with success.
     */
    @Test
    public void executeRequestToEntityWithANonNullRequestWillReturnWithSuccess() {
        UpdateStoreRequest request = new UpdateStoreRequest();
        request.setCode("MT");
        request.setName("Meter");

        Store store = mapper.requestToEntity(request);

        assertNotNull(store, "do not must be null");
        assertEquals("MT", store.getCode());
        assertEquals("Meter", store.getName());
    }
}