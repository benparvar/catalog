package com.benparvar.catalog.controller.product.model.create.response;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * The type Unit response test.
 */
class CreateProductUnitResponseTest {
    /**
     * Check hash code integrity.
     */
    @Test
    public void checkHashCodeIntegrity() {
        CreateProductUnitResponse unit1 = new CreateProductUnitResponse();
        unit1.setCode("LT");
        unit1.setName("LITRE");
        CreateProductUnitResponse unit2 = new CreateProductUnitResponse();
        unit2.setCode("LT");
        unit2.setName("LITRE");

        assertEquals(unit1.hashCode(), unit2.hashCode());
    }

    /**
     * Check equals integrity.
     */
    @Test
    public void checkEqualsIntegrity() {
        CreateProductUnitResponse unit1 = new CreateProductUnitResponse();
        unit1.setCode("LT");
        unit1.setName("LITRE");
        CreateProductUnitResponse unit2 = new CreateProductUnitResponse();
        unit2.setCode("LT");
        unit2.setName("LITRE");

        assertEquals(unit1, unit2);
    }

    /**
     * Check builder integrity.
     */
    @Test
    public void checkBuilderIntegrity() {
        CreateProductUnitResponse unit = new CreateProductUnitResponse();
        unit.setCode("LT");
        unit.setName("LITRE");

        assertNotNull(unit);
        assertEquals("LT", unit.getCode());
        assertEquals("LITRE", unit.getName());
    }

    /**
     * Check to string integrity.
     */
    @Test
    public void checkToStringIntegrity() {
        CreateProductUnitResponse unit = new CreateProductUnitResponse();
        unit.setCode("LT");
        unit.setName("LITRE");

        assertNotNull(unit);
        assertEquals("UnitResponse{code='LT', name='LITRE'}", unit.toString());
    }
}