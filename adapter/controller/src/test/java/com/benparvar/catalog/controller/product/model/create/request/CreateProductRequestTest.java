package com.benparvar.catalog.controller.product.model.create.request;

import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * The type Create product request test.
 */
class CreateProductRequestTest {
    /**
     * Check hash code integrity.
     */
    @Test
    public void checkHashCodeIntegrity() {
        CreateProductStoreRequest store = new CreateProductStoreRequest();
        store.setCode("654");
        store.setName("Matrix store");

        CreateProductUnitRequest unit = new CreateProductUnitRequest();
        unit.setCode("UN");
        unit.setName("Unit");

        CreateProductImageRequest image = new CreateProductImageRequest();
        image.setUrl("http://www.sharepic.com/pic01.png");
        image.setDescription("thumb");

        CreateProductRequest product1 = new CreateProductRequest();
        product1.setCode("123456");
        product1.setName("Xablau Gibi");
        product1.setDescription("Xablau Gibi Zero edition");
        product1.setBarcode("5449000131843");
        product1.setImages(Arrays.asList(image));
        product1.setPrice(BigDecimal.valueOf(2.34));
        product1.setStore(store);
        product1.setUnit(unit);

        CreateProductRequest product2 = new CreateProductRequest();
        product2.setCode("123456");
        product2.setName("Xablau Gibi");
        product2.setDescription("Xablau Gibi Zero edition");
        product2.setBarcode("5449000131843");
        product2.setImages(Arrays.asList(image));
        product2.setPrice(BigDecimal.valueOf(2.34));
        product2.setStore(store);
        product2.setUnit(unit);

        assertEquals(product1.hashCode(), product2.hashCode());
    }

    /**
     * Check equals integrity.
     */
    @Test
    public void checkEqualsIntegrity() {
        CreateProductStoreRequest store = new CreateProductStoreRequest();
        store.setCode("654");
        store.setName("Matrix store");

        CreateProductUnitRequest unit = new CreateProductUnitRequest();
        unit.setCode("UN");
        unit.setName("Unit");

        CreateProductImageRequest image = new CreateProductImageRequest();
        image.setUrl("http://www.sharepic.com/pic01.png");
        image.setDescription("thumb");

        CreateProductRequest product1 = new CreateProductRequest();
        product1.setCode("123456");
        product1.setName("Xablau Gibi");
        product1.setDescription("Xablau Gibi Zero edition");
        product1.setBarcode("5449000131843");
        product1.setImages(Arrays.asList(image));
        product1.setPrice(BigDecimal.valueOf(2.34));
        product1.setStore(store);
        product1.setUnit(unit);

        CreateProductRequest product2 = new CreateProductRequest();
        product2.setCode("123456");
        product2.setName("Xablau Gibi");
        product2.setDescription("Xablau Gibi Zero edition");
        product2.setBarcode("5449000131843");
        product2.setImages(Arrays.asList(image));
        product2.setPrice(BigDecimal.valueOf(2.34));
        product2.setStore(store);
        product2.setUnit(unit);

        assertEquals(product1, product2);
    }

    /**
     * Check builder integrity.
     */
    @Test
    public void checkBuilderIntegrity() {
        CreateProductStoreRequest store = new CreateProductStoreRequest();
        store.setCode("654");
        store.setName("Matrix store");

        CreateProductUnitRequest unit = new CreateProductUnitRequest();
        unit.setCode("UN");
        unit.setName("Unit");

        CreateProductImageRequest image = new CreateProductImageRequest();
        image.setUrl("http://www.sharepic.com/pic01.png");
        image.setDescription("thumb");

        CreateProductRequest product = new CreateProductRequest();
        product.setCode("123456");
        product.setName("Xablau Gibi");
        product.setDescription("Xablau Gibi Zero edition");
        product.setBarcode("5449000131843");
        product.setImages(Arrays.asList(image));
        product.setPrice(BigDecimal.valueOf(2.34));
        product.setStore(store);
        product.setUnit(unit);

        assertNotNull(product);
        assertEquals("123456", product.getCode());
        assertEquals("Xablau Gibi", product.getName());
    }

    /**
     * Check to string integrity.
     */
    @Test
    public void checkToStringIntegrity() {
        CreateProductStoreRequest store = new CreateProductStoreRequest();
        store.setCode("654");
        store.setName("Matrix store");

        CreateProductUnitRequest unit = new CreateProductUnitRequest();
        unit.setCode("UN");
        unit.setName("Unit");

        CreateProductImageRequest image = new CreateProductImageRequest();
        image.setUrl("http://www.sharepic.com/pic01.png");
        image.setDescription("thumb");

        CreateProductRequest product = new CreateProductRequest();
        product.setCode("123456");
        product.setName("Xablau Gibi");
        product.setDescription("Xablau Gibi Zero edition");
        product.setBarcode("5449000131843");
        product.setImages(Arrays.asList(image));
        product.setPrice(BigDecimal.valueOf(2.34));
        product.setStore(store);
        product.setUnit(unit);

        assertNotNull(product);
        assertNotNull(product.getPrice());
        assertNotNull(product.getUnit());
        assertEquals(unit, product.getUnit());
        assertNotNull(product.getStore());
        assertEquals(store, product.getStore());

        assertEquals("CreateProductRequest{code='123456', name='Xablau Gibi', description='Xablau Gibi Zero edition', barcode='5449000131843', price=2.34, unit=UnitRequest{code='UN', name='Unit'}, store=StoreRequest{code='654', name='Matrix store'}, images=[ImageRequest{description='thumb', url='http://www.sharepic.com/pic01.png'}]}", product.toString());
    }
}