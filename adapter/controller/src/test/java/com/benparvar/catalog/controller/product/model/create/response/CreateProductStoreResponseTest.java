package com.benparvar.catalog.controller.product.model.create.response;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * The type Store response test.
 */
class CreateProductStoreResponseTest {
    /**
     * Check hash code integrity.
     */
    @Test
    public void checkHashCodeIntegrity() {
        CreateProductStoreResponse store1 = new CreateProductStoreResponse();
        store1.setCode("4323");
        store1.setName("Almerinda Store");
        CreateProductStoreResponse store2 = new CreateProductStoreResponse();
        store2.setCode("4323");
        store2.setName("Almerinda Store");

        assertEquals(store1.hashCode(), store2.hashCode());
    }

    /**
     * Check equals integrity.
     */
    @Test
    public void checkEqualsIntegrity() {
        CreateProductStoreResponse store1 = new CreateProductStoreResponse();
        store1.setCode("4323");
        store1.setName("Almerinda Store");
        CreateProductStoreResponse store2 = new CreateProductStoreResponse();
        store2.setCode("4323");
        store2.setName("Almerinda Store");

        assertEquals(store1, store2);
    }

    /**
     * Check builder integrity.
     */
    @Test
    public void checkBuilderIntegrity() {
        CreateProductStoreResponse store = new CreateProductStoreResponse();
        store.setCode("4323");
        store.setName("Almerinda Store");

        assertNotNull(store);
        assertEquals("4323", store.getCode());
        assertEquals("Almerinda Store", store.getName());
    }

    /**
     * Check to string integrity.
     */
    @Test
    public void checkToStringIntegrity() {
        CreateProductStoreResponse store = new CreateProductStoreResponse();
        store.setCode("4323");
        store.setName("Almerinda Store");

        assertNotNull(store);
        assertEquals("StoreResponse{code='4323', name='Almerinda Store'}", store.toString());
    }
}