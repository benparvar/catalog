package com.benparvar.catalog.controller.product.model.create.mapper;

import com.benparvar.catalog.controller.product.model.create.request.CreateProductImageRequest;
import com.benparvar.catalog.controller.product.model.create.response.CreateProductImageResponse;
import com.benparvar.catalog.domain.entity.image.Image;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * The type Image mapper test.
 */
class CreateProductImageMapperTest {
    private CreateProductImageMapper mapper;

    /**
     * Init.
     */
    @BeforeEach
    public void init() {
        mapper = new CreateProductImageMapper();
    }

    /**
     * Execute entity to response with a null entity will return null.
     */
    @Test
    public void executeEntityToResponseWithANullEntityWillReturnNull() {
        Image entity = null;
        CreateProductImageResponse response = mapper.entityToResponse(entity);

        assertNull(response, "must be null");
    }

    /**
     * Execute entity to response with a non null entity will return sucess.
     */
    @Test
    public void executeEntityToResponseWithANonNullEntityWillReturnSucess() {
        Image entity = Image.newBuilder().description("thumbs").url("www.goo.com/pic1.png").build();
        CreateProductImageResponse response = mapper.entityToResponse(entity);

        assertNotNull(response, "must not be null");
        assertEquals("thumbs", response.getDescription());
        assertEquals("www.goo.com/pic1.png", response.getUrl());
    }

    /**
     * Execute request to entity with a null request will return null.
     */
    @Test
    public void executeRequestToEntityWithANullRequestWillReturnNull() {
        CreateProductImageRequest request = null;
        Image entity = mapper.requestToEntity(request);

        assertNull(entity, "must be null");
    }

    /**
     * Execute request to entity with a non null request will return success.
     */
    @Test
    public void executeRequestToEntityWithANonNullRequestWillReturnSuccess() {
        CreateProductImageRequest request = new CreateProductImageRequest();
        request.setDescription("thumbs");
        request.setUrl("www.goo.com/pic1.png");

        Image entity = mapper.requestToEntity(request);

        assertNotNull(entity, "must not be null");
        assertEquals("thumbs", entity.getDescription());
        assertEquals("www.goo.com/pic1.png", entity.getUrl());
    }
}