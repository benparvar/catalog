package com.benparvar.catalog.controller.product.model.find.response;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * The type Find product image response test.
 */
class FindProductImageResponseTest {
    /**
     * Check hash code integrity.
     */
    @Test
    public void checkHashCodeIntegrity() {
        FindProductImageResponse image1 = new FindProductImageResponse();
        image1.setUrl("www.royksopp.com/roy01.png");
        image1.setDescription("Chicago Image");
        FindProductImageResponse image2 = new FindProductImageResponse();
        image2.setUrl("www.royksopp.com/roy01.png");
        image2.setDescription("Chicago Image");

        assertEquals(image1.hashCode(), image2.hashCode());
    }

    /**
     * Check equals integrity.
     */
    @Test
    public void checkEqualsIntegrity() {
        FindProductImageResponse image1 = new FindProductImageResponse();
        image1.setUrl("www.royksopp.com/roy01.png");
        image1.setDescription("Chicago Image");
        FindProductImageResponse image2 = new FindProductImageResponse();
        image2.setUrl("www.royksopp.com/roy01.png");
        image2.setDescription("Chicago Image");

        assertEquals(image1, image2);
    }

    /**
     * Check builder integrity.
     */
    @Test
    public void checkBuilderIntegrity() {
        FindProductImageResponse image = new FindProductImageResponse();
        image.setUrl("www.royksopp.com/roy01.png");
        image.setDescription("Chicago Image");

        assertNotNull(image);
        assertEquals("www.royksopp.com/roy01.png", image.getUrl());
        assertEquals("Chicago Image", image.getDescription());
    }

    /**
     * Check to string integrity.
     */
    @Test
    public void checkToStringIntegrity() {
        FindProductImageResponse image = new FindProductImageResponse();
        image.setUrl("www.royksopp.com/roy01.png");
        image.setDescription("Chicago Image");

        assertNotNull(image);
        assertEquals("FindProductImageResponse{description='Chicago Image', url='www.royksopp.com/roy01.png'}", image.toString());
    }
}