package com.benparvar.catalog.controller.store.model.create;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * The type Create store response test.
 */
class CreateStoreResponseTest {
    /**
     * Check hash code integrity.
     */
    @Test
    public void checkHashCodeIntegrity() {
        CreateStoreResponse store1 = new CreateStoreResponse();
        store1.setCode("LT");
        store1.setName("LITRE");
        CreateStoreResponse store2 = new CreateStoreResponse();
        store2.setCode("LT");
        store2.setName("LITRE");

        assertEquals(store1.hashCode(), store2.hashCode());
    }

    /**
     * Check equals integrity.
     */
    @Test
    public void checkEqualsIntegrity() {
        CreateStoreResponse store1 = new CreateStoreResponse();
        store1.setCode("LT");
        store1.setName("LITRE");
        CreateStoreResponse store2 = new CreateStoreResponse();
        store2.setCode("LT");
        store2.setName("LITRE");
        assertEquals(store1, store2);
    }

    /**
     * Check builder integrity.
     */
    @Test
    public void checkBuilderIntegrity() {
        CreateStoreResponse store = new CreateStoreResponse();
        store.setCode("LT");
        store.setName("LITRE");

        assertNotNull(store);
        assertEquals("LT", store.getCode());
        assertEquals("LITRE", store.getName());
    }

    /**
     * Check to string integrity.
     */
    @Test
    public void checkToStringIntegrity() {
        CreateStoreResponse store = new CreateStoreResponse();
        store.setCode("LT");
        store.setName("LITRE");

        assertNotNull(store);
        assertEquals("CreateStoreResponse{code='LT', name='LITRE'}", store.toString());
    }
}