package com.benparvar.catalog.controller.product.model.create.request;

import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.*;

/**
 * The type Update product price request test.
 */
class UpdateProductPriceRequestTest {
    /**
     * Check hash code integrity.
     */
    @Test
    public void checkHashCodeIntegrity() {
        UpdateProductPriceRequest request1 = new UpdateProductPriceRequest();
        request1.setBarcode("049000006841");
        request1.setStoreCode("12345");
        request1.setPrice(BigDecimal.TEN);
        UpdateProductPriceRequest request2 = new UpdateProductPriceRequest();
        request2.setBarcode("049000006841");
        request2.setStoreCode("12345");
        request2.setPrice(BigDecimal.TEN);

        assertEquals(request1.hashCode(), request2.hashCode());
    }

    /**
     * Check equals integrity.
     */
    @Test
    public void checkEqualsIntegrity() {
        UpdateProductPriceRequest request1 = new UpdateProductPriceRequest();
        request1.setBarcode("049000006841");
        request1.setStoreCode("12345");
        request1.setPrice(BigDecimal.TEN);
        UpdateProductPriceRequest request2 = new UpdateProductPriceRequest();
        request2.setBarcode("049000006841");
        request2.setStoreCode("12345");
        request2.setPrice(BigDecimal.TEN);

        assertEquals(request1, request2);
    }

    /**
     * Check builder integrity.
     */
    @Test
    public void checkBuilderIntegrity() {
        UpdateProductPriceRequest request = new UpdateProductPriceRequest();
        request.setBarcode("049000006841");
        request.setStoreCode("12345");
        request.setPrice(BigDecimal.TEN);

        assertNotNull(request);
        assertEquals("049000006841", request.getBarcode());
        assertEquals("12345", request.getStoreCode());
        assertEquals(BigDecimal.TEN, request.getPrice());
    }

    /**
     * Check to string integrity.
     */
    @Test
    public void checkToStringIntegrity() {
        UpdateProductPriceRequest request = new UpdateProductPriceRequest();
        request.setBarcode("049000006841");
        request.setStoreCode("12345");
        request.setPrice(BigDecimal.TEN);

        assertNotNull(request);
        assertEquals("UpdateProductPriceRequest{barcode='049000006841', storeCode='12345', price=10}", request.toString());
    }
}