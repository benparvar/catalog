package com.benparvar.catalog.controller.store.model.find;

import com.benparvar.catalog.domain.entity.store.Store;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * The type Find store mapper test.
 */
class FindStoreMapperTest {
    private FindStoreMapper mapper;

    /**
     * Init.
     */
    @BeforeEach
    public void init() {
        mapper = new FindStoreMapper();
    }

    /**
     * Execute entity to response with a null entity will return null.
     */
    @Test
    public void executeEntityToResponseWithANullEntityWillReturnNull() {
        Store entity = null;
        FindStoreResponse response = mapper.entityToResponse(entity);

        assertNull(response, "must be null");
    }

    /**
     * Execute entity to response with a non null entity will return with success.
     */
    @Test
    public void executeEntityToResponseWithANonNullEntityWillReturnWithSuccess() {
        Store entity = Store.newBuilder().code("MT").name("Meter").build();

        FindStoreResponse response = mapper.entityToResponse(entity);

        assertNotNull(response, "do not must be null");
        assertEquals("MT", response.getCode());
        assertEquals("Meter", response.getName());
    }

    /**
     * Execute entity to response with a null entity list will return a empty list.
     */
    @Test
    public void executeEntityToResponseWithANullEntityListWillReturnAEmptyList() {
        List<Store> entities = null;
        List<FindStoreResponse> responses = mapper.entityToResponse(entities);

        assertNotNull(responses, "must not be null");
        assertTrue(responses.isEmpty());
    }

    /**
     * Execute entity to response with a non empty entity list will return with success.
     */
    @Test
    public void executeEntityToResponseWithANonEmptyEntityListWillReturnWithSuccess() {
        Store entity = Store.newBuilder().code("MT").name("Meter").build();

        List<FindStoreResponse> responses = mapper.entityToResponse(Arrays.asList(entity));

        assertNotNull(responses, "do not must be null");
        assertFalse(responses.isEmpty());
        assertEquals(1, responses.size());

        FindStoreResponse response = responses.get(0);

        assertEquals("MT", response.getCode());
        assertEquals("Meter", response.getName());
    }
}