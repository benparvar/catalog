package com.benparvar.catalog.controller.product.model.update.response;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * The type Update product unit response test.
 */
class UpdateProductUnitResponseTest {
    /**
     * Check hash code integrity.
     */
    @Test
    public void checkHashCodeIntegrity() {
        UpdateProductUnitResponse unit1 = new UpdateProductUnitResponse();
        unit1.setCode("LT");
        unit1.setName("LITRE");
        UpdateProductUnitResponse unit2 = new UpdateProductUnitResponse();
        unit2.setCode("LT");
        unit2.setName("LITRE");

        assertEquals(unit1.hashCode(), unit2.hashCode());
    }

    /**
     * Check equals integrity.
     */
    @Test
    public void checkEqualsIntegrity() {
        UpdateProductUnitResponse unit1 = new UpdateProductUnitResponse();
        unit1.setCode("LT");
        unit1.setName("LITRE");
        UpdateProductUnitResponse unit2 = new UpdateProductUnitResponse();
        unit2.setCode("LT");
        unit2.setName("LITRE");

        assertEquals(unit1, unit2);
    }

    /**
     * Check builder integrity.
     */
    @Test
    public void checkBuilderIntegrity() {
        UpdateProductUnitResponse unit = new UpdateProductUnitResponse();
        unit.setCode("LT");
        unit.setName("LITRE");

        assertNotNull(unit);
        assertEquals("LT", unit.getCode());
        assertEquals("LITRE", unit.getName());
    }

    /**
     * Check to string integrity.
     */
    @Test
    public void checkToStringIntegrity() {
        UpdateProductUnitResponse unit = new UpdateProductUnitResponse();
        unit.setCode("LT");
        unit.setName("LITRE");

        assertNotNull(unit);
        assertEquals("UpdateProductUnitResponse{code='LT', name='LITRE'}", unit.toString());
    }
}