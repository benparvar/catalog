package com.benparvar.catalog.controller.product.model.find.mapper;

import com.benparvar.catalog.controller.product.model.find.response.FindProductStoreResponse;
import com.benparvar.catalog.domain.entity.store.Store;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * The type Find product store mapper test.
 */
class FindProductCreateProductStoreMapperTest {
    private FindProductStoreMapper mapper;

    /**
     * Init.
     */
    @BeforeEach
    public void init() {
        mapper = new FindProductStoreMapper();
    }

    /**
     * Execute entity to response with a null entity will return null.
     */
    @Test
    public void executeEntityToResponseWithANullEntityWillReturnNull() {
        Store entity = null;
        FindProductStoreResponse response = mapper.entityToResponse(entity);

        assertNull(response, "must be null");
    }

    /**
     * Execute entity to response with a non null entity will return with success.
     */
    @Test
    public void executeEntityToResponseWithANonNullEntityWillReturnWithSuccess() {
        Store entity = Store.newBuilder().code("5445").name("Blue Store").build();

        FindProductStoreResponse response = mapper.entityToResponse(entity);

        assertNotNull(response, "do not must be null");
        assertEquals("5445", response.getCode());
        assertEquals("Blue Store", response.getName());
    }

}