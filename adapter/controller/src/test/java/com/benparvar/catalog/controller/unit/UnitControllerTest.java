package com.benparvar.catalog.controller.unit;

import com.benparvar.catalog.controller.unit.model.create.CreateUnitMapper;
import com.benparvar.catalog.controller.unit.model.create.CreateUnitRequest;
import com.benparvar.catalog.controller.unit.model.create.CreateUnitResponse;
import com.benparvar.catalog.controller.unit.model.delete.DeleteUnitMapper;
import com.benparvar.catalog.controller.unit.model.find.FindUnitMapper;
import com.benparvar.catalog.controller.unit.model.find.FindUnitResponse;
import com.benparvar.catalog.controller.unit.model.update.UpdateUnitMapper;
import com.benparvar.catalog.controller.unit.model.update.UpdateUnitRequest;
import com.benparvar.catalog.controller.unit.model.update.UpdateUnitResponse;
import com.benparvar.catalog.domain.entity.unit.Unit;
import com.benparvar.catalog.usecase.unit.create.CreateUnitUseCase;
import com.benparvar.catalog.usecase.unit.delete.DeleteUnitUseCase;
import com.benparvar.catalog.usecase.unit.find.all.FindAllUnitsUseCase;
import com.benparvar.catalog.usecase.unit.find.code.FindUnitByCodeUseCase;
import com.benparvar.catalog.usecase.unit.update.UpdateUnitUseCase;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

/**
 * The type Unit controller test.
 */
class UnitControllerTest {
    private UnitController controller;
    private CreateUnitUseCase createUnitUseCase = mock(CreateUnitUseCase.class);
    private CreateUnitMapper createUnitMapper = new CreateUnitMapper();
    private FindAllUnitsUseCase findAllUnitsUseCase = mock(FindAllUnitsUseCase.class);
    private FindUnitMapper findUnitMapper = new FindUnitMapper();
    private FindUnitByCodeUseCase findUnitByCodeUseCase = mock(FindUnitByCodeUseCase.class);
    private UpdateUnitUseCase updateUnitUseCase = mock(UpdateUnitUseCase.class);
    private UpdateUnitMapper updateUnitMapper = new UpdateUnitMapper();
    private DeleteUnitUseCase deleteUnitUseCase = mock(DeleteUnitUseCase.class);
    private DeleteUnitMapper deleteUnitMapper = new DeleteUnitMapper();

    /**
     * Init.
     */
    @BeforeEach
    public void init() {
        controller = new UnitController(createUnitUseCase, createUnitMapper, findAllUnitsUseCase, findUnitMapper,
                findUnitByCodeUseCase, updateUnitUseCase, updateUnitMapper, deleteUnitUseCase, deleteUnitMapper);
    }

    /**
     * Execute create use case with a null request will fail.
     */
    @Test
    public void executeCreateUseCaseWithANullRequestWillFail() {
        assertThrows(IllegalArgumentException.class, () -> {
            Unit entity = null;
            CreateUnitRequest request = null;
            when(createUnitUseCase.execute(entity))
                    .thenThrow(new IllegalArgumentException("unit cannot be null"));

            try {
                controller.create(request);
            } catch (IllegalArgumentException e) {
                assertEquals("unit cannot be null", e.getMessage());
                verify(createUnitUseCase, times(1)).execute(entity);
                throw e;
            }

            fail("should throw an IllegalArgumentException");
        });
    }

    /**
     * Execute create use case with a valid request will success.
     */
    @Test
    public void executeCreateUseCaseWithAValidRequestWillSuccess() {
        Unit entity = Unit.newBuilder().code("MP").name("Mili Pieces").build();
        CreateUnitRequest request = new CreateUnitRequest();
        request.setCode("MP");
        request.setName("Mili Pieces");
        when(createUnitUseCase.execute(entity)).thenReturn(entity);

        CreateUnitResponse response = controller.create(request);

        assertNotNull(response);
        assertEquals("MP", response.getCode());
        assertEquals("Mili Pieces", request.getName());
        verify(createUnitUseCase, times(1)).execute(entity);
    }

    /**
     * Execute find all use case with empty repository will return a empty list.
     */
    @Test
    public void executeFindAllUseCaseWithEmptyDatabaseWillReturnAEmptyList() {
        when(findAllUnitsUseCase.execute()).thenReturn(Collections.EMPTY_LIST);

        List<FindUnitResponse> responses = controller.findAll();
        assertNotNull(responses);
        assertTrue(responses.isEmpty());
    }

    /**
     * Execute find all use case with repository will return a list.
     */
    @Test
    public void executeFindAllUseCaseWithDatabaseWillReturnAList() {
        List<Unit> entities = Arrays.asList(Unit.newBuilder().code("MP").name("Mili Pieces").build());

        when(findAllUnitsUseCase.execute()).thenReturn(entities);

        List<FindUnitResponse> responses = controller.findAll();
        assertNotNull(responses);
        assertFalse(responses.isEmpty());
        assertEquals(1, responses.size());
    }

    /**
     * Execute find by code with a valid code will success.
     */
    @Test
    public void executeFindByCodeWithAValidCodeWillSuccess() {
        String code = "MP";
        Unit entity = Unit.newBuilder().code(code).name("Mili Pieces").build();

        when(findUnitByCodeUseCase.execute(code)).thenReturn(entity);

        FindUnitResponse response = controller.findByCode(code);
        assertNotNull(response);
        assertEquals("MP", response.getCode());
        assertEquals("Mili Pieces", response.getName());
    }

    /**
     * Execute update with an valid entity will success.
     */
    @Test
    public void executeUpdateWithAnValidEntityWillSuccess() {
        Unit entity = Unit.newBuilder().code("MP").name("Milipieces").build();
        UpdateUnitRequest request = new UpdateUnitRequest();
        request.setCode("MP");
        request.setName("Milipieces");

        when(updateUnitUseCase.execute(entity)).thenReturn(entity);

        UpdateUnitResponse response = controller.update(request);
        assertEquals("MP", response.getCode());
        assertEquals("Milipieces", response.getName());
    }

    /**
     * Execute delete with an valid entity wil success.
     */
    @Test
    public void executeDeleteWithAnValidEntityWilSuccess() {
        assertDoesNotThrow(() -> {
            controller.delete("MT");
        });
    }
}