package com.benparvar.catalog.controller.product.model.create.response;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CreateProductImageResponseTest {
    /**
     * Check hash code integrity.
     */
    @Test
    public void checkHashCodeIntegrity() {
        CreateProductImageResponse image1 = new CreateProductImageResponse();
        image1.setUrl("www.royksopp.com/roy01.png");
        image1.setDescription("Chicago Image");
        CreateProductImageResponse image2 = new CreateProductImageResponse();
        image2.setUrl("www.royksopp.com/roy01.png");
        image2.setDescription("Chicago Image");

        assertEquals(image1.hashCode(), image2.hashCode());
    }

    /**
     * Check equals integrity.
     */
    @Test
    public void checkEqualsIntegrity() {
        CreateProductImageResponse image1 = new CreateProductImageResponse();
        image1.setUrl("www.royksopp.com/roy01.png");
        image1.setDescription("Chicago Image");
        CreateProductImageResponse image2 = new CreateProductImageResponse();
        image2.setUrl("www.royksopp.com/roy01.png");
        image2.setDescription("Chicago Image");

        assertEquals(image1, image2);
    }

    /**
     * Check builder integrity.
     */
    @Test
    public void checkBuilderIntegrity() {
        CreateProductImageResponse image = new CreateProductImageResponse();
        image.setUrl("www.royksopp.com/roy01.png");
        image.setDescription("Chicago Image");

        assertNotNull(image);
        assertEquals("www.royksopp.com/roy01.png", image.getUrl());
        assertEquals("Chicago Image", image.getDescription());
    }

    /**
     * Check to string integrity.
     */
    @Test
    public void checkToStringIntegrity() {
        CreateProductImageResponse image = new CreateProductImageResponse();
        image.setUrl("www.royksopp.com/roy01.png");
        image.setDescription("Chicago Image");

        assertNotNull(image);
        assertEquals("ImageResponse{description='Chicago Image', url='www.royksopp.com/roy01.png'}", image.toString());
    }
}