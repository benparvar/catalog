package com.benparvar.catalog.controller.product.model.create.request;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * The type Unit request test.
 */
class CreateProductUnitRequestTest {
    /**
     * Check hash code integrity.
     */
    @Test
    public void checkHashCodeIntegrity() {
        CreateProductUnitRequest unit1 = new CreateProductUnitRequest();
        unit1.setCode("LT");
        unit1.setName("LITRE");
        CreateProductUnitRequest unit2 = new CreateProductUnitRequest();
        unit2.setCode("LT");
        unit2.setName("LITRE");

        assertEquals(unit1.hashCode(), unit2.hashCode());
    }

    /**
     * Check equals integrity.
     */
    @Test
    public void checkEqualsIntegrity() {
        CreateProductUnitRequest unit1 = new CreateProductUnitRequest();
        unit1.setCode("LT");
        unit1.setName("LITRE");
        CreateProductUnitRequest unit2 = new CreateProductUnitRequest();
        unit2.setCode("LT");
        unit2.setName("LITRE");

        assertEquals(unit1, unit2);
    }

    /**
     * Check builder integrity.
     */
    @Test
    public void checkBuilderIntegrity() {
        CreateProductUnitRequest unit = new CreateProductUnitRequest();
        unit.setCode("LT");
        unit.setName("LITRE");

        assertNotNull(unit);
        assertEquals("LT", unit.getCode());
        assertEquals("LITRE", unit.getName());
    }

    /**
     * Check to string integrity.
     */
    @Test
    public void checkToStringIntegrity() {
        CreateProductUnitRequest unit = new CreateProductUnitRequest();
        unit.setCode("LT");
        unit.setName("LITRE");

        assertNotNull(unit);
        assertEquals("UnitRequest{code='LT', name='LITRE'}", unit.toString());
    }

}