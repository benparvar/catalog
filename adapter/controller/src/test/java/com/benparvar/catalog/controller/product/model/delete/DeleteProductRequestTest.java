package com.benparvar.catalog.controller.product.model.delete;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * The type Delete product request test.
 */
class DeleteProductRequestTest {
    /**
     * Check hash code integrity.
     */
    @Test
    public void checkHashCodeIntegrity() {
        DeleteProductRequest product1 = DeleteProductRequest.newBuilder().barcodeCode("049000006841").storeCode("1234").build();
        DeleteProductRequest product2 = DeleteProductRequest.newBuilder().barcodeCode("049000006841").storeCode("1234").build();

        assertEquals(product1.hashCode(), product2.hashCode());
    }

    /**
     * Check equals integrity.
     */
    @Test
    public void checkEqualsIntegrity() {
        DeleteProductRequest product1 = DeleteProductRequest.newBuilder().barcodeCode("049000006841").storeCode("1234").build();
        DeleteProductRequest product2 = DeleteProductRequest.newBuilder().barcodeCode("049000006841").storeCode("1234").build();

        assertEquals(product1, product2);
    }

    /**
     * Check builder integrity.
     */
    @Test
    public void checkBuilderIntegrity() {
        DeleteProductRequest product = DeleteProductRequest.newBuilder().barcodeCode("049000006841").storeCode("1234").build();

        assertNotNull(product);
        assertEquals("049000006841", product.getBarcodeCode());
        assertEquals("1234", product.getStoreCode());
    }

    /**
     * Check to string integrity.
     */
    @Test
    public void checkToStringIntegrity() {
        DeleteProductRequest product = DeleteProductRequest.newBuilder().barcodeCode("049000006841").storeCode("1234").build();

        assertNotNull(product);
        assertEquals("DeleteProductRequest{barcodeCode='049000006841', storeCode='1234'}", product.toString());
    }
}