package com.benparvar.catalog.controller.product.model.create.mapper;

import com.benparvar.catalog.controller.product.model.create.request.CreateProductStoreRequest;
import com.benparvar.catalog.controller.product.model.create.response.CreateProductStoreResponse;
import com.benparvar.catalog.domain.entity.store.Store;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * The type Store mapper test.
 */
class CreateProductStoreMapperTest {
    private CreateProductStoreMapper mapper;

    /**
     * Init.
     */
    @BeforeEach
    public void init() {
        mapper = new CreateProductStoreMapper();
    }

    /**
     * Execute entity to response with a null entity will return null.
     */
    @Test
    public void executeEntityToResponseWithANullEntityWillReturnNull() {
        Store entity = null;
        CreateProductStoreResponse response = mapper.entityToResponse(entity);

        assertNull(response, "must be null");
    }

    /**
     * Execute entity to response with a non null entity will return with success.
     */
    @Test
    public void executeEntityToResponseWithANonNullEntityWillReturnWithSuccess() {
        Store entity = Store.newBuilder().code("5445").name("Blue Store").build();

        CreateProductStoreResponse response = mapper.entityToResponse(entity);

        assertNotNull(response, "do not must be null");
        assertEquals("5445", response.getCode());
        assertEquals("Blue Store", response.getName());
    }

    /**
     * Execute request to entity with a null request will return null.
     */
    @Test
    public void executeRequestToEntityWithANullRequestWillReturnNull() {
        CreateProductStoreRequest request = null;
        Store store = mapper.requestToEntity(request);

        assertNull(store, "must be null");
    }

    /**
     * Execute request to entity with a non null request will return with success.
     */
    @Test
    public void executeRequestToEntityWithANonNullRequestWillReturnWithSuccess() {
        CreateProductStoreRequest request = new CreateProductStoreRequest();
        request.setCode("5445");
        request.setName("Blue Store");

        Store store = mapper.requestToEntity(request);

        assertNotNull(store, "do not must be null");
        assertEquals("5445", store.getCode());
        assertEquals("Blue Store", store.getName());
    }
}