package com.benparvar.catalog.controller.product.model.update.response;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * The type Update product store response test.
 */
class UpdateProductStoreResponseTest {
    /**
     * Check hash code integrity.
     */
    @Test
    public void checkHashCodeIntegrity() {
        UpdateProductStoreResponse store1 = new UpdateProductStoreResponse();
        store1.setCode("4323");
        store1.setName("Almerinda Store");
        UpdateProductStoreResponse store2 = new UpdateProductStoreResponse();
        store2.setCode("4323");
        store2.setName("Almerinda Store");

        assertEquals(store1.hashCode(), store2.hashCode());
    }

    /**
     * Check equals integrity.
     */
    @Test
    public void checkEqualsIntegrity() {
        UpdateProductStoreResponse store1 = new UpdateProductStoreResponse();
        store1.setCode("4323");
        store1.setName("Almerinda Store");
        UpdateProductStoreResponse store2 = new UpdateProductStoreResponse();
        store2.setCode("4323");
        store2.setName("Almerinda Store");

        assertEquals(store1, store2);
    }

    /**
     * Check builder integrity.
     */
    @Test
    public void checkBuilderIntegrity() {
        UpdateProductStoreResponse store = new UpdateProductStoreResponse();
        store.setCode("4323");
        store.setName("Almerinda Store");

        assertNotNull(store);
        assertEquals("4323", store.getCode());
        assertEquals("Almerinda Store", store.getName());
    }

    /**
     * Check to string integrity.
     */
    @Test
    public void checkToStringIntegrity() {
        UpdateProductStoreResponse store = new UpdateProductStoreResponse();
        store.setCode("4323");
        store.setName("Almerinda Store");

        assertNotNull(store);
        assertEquals("UpdateProductStoreResponse{code='4323', name='Almerinda Store'}", store.toString());
    }
}