package com.benparvar.catalog.controller.product.model.update.mapper;

import com.benparvar.catalog.controller.product.model.update.response.UpdateProductImageResponse;
import com.benparvar.catalog.domain.entity.image.Image;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * The type Update product image mapper test.
 */
class UpdateProductImageMapperTest {
    private UpdateProductImageMapper mapper;

    /**
     * Init.
     */
    @BeforeEach
    public void init() {
        mapper = new UpdateProductImageMapper();
    }

    /**
     * Execute entity to response with a null entity will return null.
     */
    @Test
    public void executeEntityToResponseWithANullEntityWillReturnNull() {
        Image entity = null;
        UpdateProductImageResponse response = mapper.entityToResponse(entity);

        assertNull(response, "must be null");
    }

    /**
     * Execute entity to response with a non null entity will return sucess.
     */
    @Test
    public void executeEntityToResponseWithANonNullEntityWillReturnSucess() {
        Image entity = Image.newBuilder().description("thumbs").url("www.goo.com/pic1.png").build();
        UpdateProductImageResponse response = mapper.entityToResponse(entity);

        assertNotNull(response, "must not be null");
        assertEquals("thumbs", response.getDescription());
        assertEquals("www.goo.com/pic1.png", response.getUrl());
    }
}