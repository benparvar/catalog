package com.benparvar.catalog.controller.store.model.create;

import com.benparvar.catalog.domain.entity.store.Store;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * The type Create store mapper test.
 */
class CreateStoreMapperTest {

    private CreateStoreMapper mapper;

    /**
     * Init.
     */
    @BeforeEach
    public void init() {
        mapper = new CreateStoreMapper();
    }

    /**
     * Execute entity to response with a null entity will return null.
     */
    @Test
    public void executeEntityToResponseWithANullEntityWillReturnNull() {
        Store entity = null;
        CreateStoreResponse response = mapper.entityToResponse(entity);

        assertNull(response, "must be null");
    }

    /**
     * Execute entity to response with a non null entity will return with success.
     */
    @Test
    public void executeEntityToResponseWithANonNullEntityWillReturnWithSuccess() {
        Store entity = Store.newBuilder().code("5445").name("Blue Store").build();

        CreateStoreResponse response = mapper.entityToResponse(entity);

        assertNotNull(response, "do not must be null");
        assertEquals("5445", response.getCode());
        assertEquals("Blue Store", response.getName());
    }

    /**
     * Execute request to entity with a null request will return null.
     */
    @Test
    public void executeRequestToEntityWithANullRequestWillReturnNull() {
        CreateStoreRequest request = null;
        Store store = mapper.requestToEntity(request);

        assertNull(store, "must be null");
    }

    /**
     * Execute request to entity with a non null request will return with success.
     */
    @Test
    public void executeRequestToEntityWithANonNullRequestWillReturnWithSuccess() {
        CreateStoreRequest request = new CreateStoreRequest();
        request.setCode("5445");
        request.setName("Blue Store");

        Store store = mapper.requestToEntity(request);

        assertNotNull(store, "do not must be null");
        assertEquals("5445", store.getCode());
        assertEquals("Blue Store", store.getName());
    }
}