package com.benparvar.catalog.controller.product.model.create.request;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CreateProductImageRequestTest {
    /**
     * Check hash code integrity.
     */
    @Test
    public void checkHashCodeIntegrity() {
        CreateProductImageRequest image1 = new CreateProductImageRequest();
        image1.setUrl("www.royksopp.com/roy01.png");
        image1.setDescription("Chicago Image");
        CreateProductImageRequest image2 = new CreateProductImageRequest();
        image2.setUrl("www.royksopp.com/roy01.png");
        image2.setDescription("Chicago Image");

        assertEquals(image1.hashCode(), image2.hashCode());
    }

    /**
     * Check equals integrity.
     */
    @Test
    public void checkEqualsIntegrity() {
        CreateProductImageRequest image1 = new CreateProductImageRequest();
        image1.setUrl("www.royksopp.com/roy01.png");
        image1.setDescription("Chicago Image");
        CreateProductImageRequest image2 = new CreateProductImageRequest();
        image2.setUrl("www.royksopp.com/roy01.png");
        image2.setDescription("Chicago Image");

        assertEquals(image1, image2);
    }

    /**
     * Check builder integrity.
     */
    @Test
    public void checkBuilderIntegrity() {
        CreateProductImageRequest image = new CreateProductImageRequest();
        image.setUrl("www.royksopp.com/roy01.png");
        image.setDescription("Chicago Image");

        assertNotNull(image);
        assertEquals("www.royksopp.com/roy01.png", image.getUrl());
        assertEquals("Chicago Image", image.getDescription());
    }

    /**
     * Check to string integrity.
     */
    @Test
    public void checkToStringIntegrity() {
        CreateProductImageRequest image = new CreateProductImageRequest();
        image.setUrl("www.royksopp.com/roy01.png");
        image.setDescription("Chicago Image");

        assertNotNull(image);
        assertEquals("ImageRequest{description='Chicago Image', url='www.royksopp.com/roy01.png'}", image.toString());
    }
}