package com.benparvar.catalog.controller.product.model.create.response;

import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * The type Create product response test.
 */
class CreateProductResponseTest {
    /**
     * Check hash code integrity.
     */
    @Test
    public void checkHashCodeIntegrity() {
        CreateProductStoreResponse store = new CreateProductStoreResponse();
        store.setCode("654");
        store.setName("Matrix store");

        CreateProductUnitResponse unit = new CreateProductUnitResponse();
        unit.setCode("UN");
        unit.setName("Unit");

        CreateProductImageResponse image = new CreateProductImageResponse();
        image.setUrl("www.matrix/123456/pic1.jpg");
        image.setDescription("thumb");

        CreateProductResponse product1 = new CreateProductResponse();
        product1.setCode("123456");
        product1.setName("Xablau Gibi");
        product1.setDescription("Xablau Gibi Zero edition");
        product1.setBarcode("5449000131843");
        product1.setImages(Arrays.asList(image));
        product1.setPrice(BigDecimal.valueOf(2.34));
        product1.setStore(store);
        product1.setUnit(unit);

        CreateProductResponse product2 = new CreateProductResponse();
        product2.setCode("123456");
        product2.setName("Xablau Gibi");
        product2.setDescription("Xablau Gibi Zero edition");
        product2.setBarcode("5449000131843");
        product2.setImages(Arrays.asList(image));
        product2.setPrice(BigDecimal.valueOf(2.34));
        product2.setStore(store);
        product2.setUnit(unit);

        assertEquals(product1.hashCode(), product2.hashCode());
    }

    /**
     * Check equals integrity.
     */
    @Test
    public void checkEqualsIntegrity() {
        CreateProductStoreResponse store = new CreateProductStoreResponse();
        store.setCode("654");
        store.setName("Matrix store");

        CreateProductUnitResponse unit = new CreateProductUnitResponse();
        unit.setCode("UN");
        unit.setName("Unit");

        CreateProductImageResponse image = new CreateProductImageResponse();
        image.setUrl("www.matrix/123456/pic1.jpg");
        image.setDescription("thumb");

        CreateProductResponse product1 = new CreateProductResponse();
        product1.setCode("123456");
        product1.setName("Xablau Gibi");
        product1.setDescription("Xablau Gibi Zero edition");
        product1.setBarcode("5449000131843");
        product1.setImages(Arrays.asList(image));
        product1.setPrice(BigDecimal.valueOf(2.34));
        product1.setStore(store);
        product1.setUnit(unit);

        CreateProductResponse product2 = new CreateProductResponse();
        product2.setCode("123456");
        product2.setName("Xablau Gibi");
        product2.setDescription("Xablau Gibi Zero edition");
        product2.setBarcode("5449000131843");
        product2.setImages(Arrays.asList(image));
        product2.setPrice(BigDecimal.valueOf(2.34));
        product2.setStore(store);
        product2.setUnit(unit);

        assertEquals(product1, product2);
    }

    /**
     * Check builder integrity.
     */
    @Test
    public void checkBuilderIntegrity() {
        CreateProductStoreResponse store = new CreateProductStoreResponse();
        store.setCode("654");
        store.setName("Matrix store");

        CreateProductUnitResponse unit = new CreateProductUnitResponse();
        unit.setCode("UN");
        unit.setName("Unit");

        CreateProductImageResponse image = new CreateProductImageResponse();
        image.setUrl("www.matrix/123456/pic1.jpg");
        image.setDescription("thumb");

        CreateProductResponse product = new CreateProductResponse();
        product.setCode("123456");
        product.setName("Xablau Gibi");
        product.setDescription("Xablau Gibi Zero edition");
        product.setBarcode("5449000131843");
        product.setImages(Arrays.asList(image));
        product.setPrice(BigDecimal.valueOf(2.34));
        product.setStore(store);
        product.setUnit(unit);

        assertNotNull(product);
        assertEquals("123456", product.getCode());
        assertEquals("Xablau Gibi", product.getName());
    }

    /**
     * Check to string integrity.
     */
    @Test
    public void checkToStringIntegrity() {
        CreateProductStoreResponse store = new CreateProductStoreResponse();
        store.setCode("654");
        store.setName("Matrix store");

        CreateProductUnitResponse unit = new CreateProductUnitResponse();
        unit.setCode("UN");
        unit.setName("Unit");

        CreateProductImageResponse image = new CreateProductImageResponse();
        image.setUrl("www.matrix/123456/pic1.jpg");
        image.setDescription("thumb");

        CreateProductResponse product = new CreateProductResponse();
        product.setCode("123456");
        product.setName("Xablau Gibi");
        product.setDescription("Xablau Gibi Zero edition");
        product.setBarcode("5449000131843");
        product.setImages(Arrays.asList(image));
        product.setPrice(BigDecimal.valueOf(2.34));
        product.setStore(store);
        product.setUnit(unit);

        assertNotNull(product);
        assertNotNull(product.getPrice());
        assertNotNull(product.getUnit());
        assertEquals(unit, product.getUnit());
        assertNotNull(product.getStore());
        assertEquals(store, product.getStore());

        assertEquals("CreateProductResponse{code='123456', name='Xablau Gibi', description='Xablau Gibi Zero edition', barcode='5449000131843', price=2.34, unit=UnitResponse{code='UN', name='Unit'}, store=StoreResponse{code='654', name='Matrix store'}, images=[ImageResponse{description='thumb', url='www.matrix/123456/pic1.jpg'}]}", product.toString());
    }

}