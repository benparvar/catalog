package com.benparvar.catalog.controller.product.model.create.mapper;

import com.benparvar.catalog.domain.entity.barcode.Barcode;
import com.benparvar.catalog.domain.entity.barcode.BarcodeType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * The type Barcode mapper test.
 */
class CreateProductBarcodeMapperTest {
    private CreateProductBarcodeMapper mapper;

    /**
     * Init.
     */
    @BeforeEach
    public void init() {
        mapper = new CreateProductBarcodeMapper();
    }

    /**
     * Execute code to entity with a null code will return null.
     */
    @Test
    public void executeCodeToEntityWithANullCodeWillReturnNull() {
        String code = null;
        Barcode entity = mapper.codeToEntity(code);

        assertNull(entity, "must be null");
    }

    /**
     * Execute code to entity with a non null ean 13 code will return with success.
     */
    @Test
    public void executeCodeToEntityWithANonNullEAN13CodeWillReturnWithSuccess() {
        String code = "3374650003399";
        Barcode entity = mapper.codeToEntity(code);

        assertNotNull(entity, "do not must be null");
        assertEquals("3374650003399", entity.getCode());
        assertEquals(BarcodeType.EAN_13, entity.getBarcodeType());
    }

    /**
     * Execute code to entity with a non null ean 8 code will return with success.
     */
    @Test
    public void executeCodeToEntityWithANonNullEAN8CodeWillReturnWithSuccess() {
        String code = "96385074";
        Barcode entity = mapper.codeToEntity(code);

        assertNotNull(entity, "do not must be null");
        assertEquals("96385074", entity.getCode());
        assertEquals(BarcodeType.EAN_8, entity.getBarcodeType());
    }

    /**
     * Execute code to entity with a non null upca code will return with success.
     */
    @Test
    public void executeCodeToEntityWithANonNullUPCACodeWillReturnWithSuccess() {
        String code = "049000037111";
        Barcode entity = mapper.codeToEntity(code);

        assertNotNull(entity, "do not must be null");
        assertEquals("049000037111", entity.getCode());
        assertEquals(BarcodeType.UPC_A, entity.getBarcodeType());
    }

    /**
     * Execute entity to code with a null entity will return null.
     */
    @Test
    public void executeEntityToCodeWithANullEntityWillReturnNull() {
        Barcode entity = null;
        String code = mapper.entityToCode(entity);

        assertNull(code, "must be null");
    }

    /**
     * Execute entity to code with a non null entity and upca will return success.
     */
    @Test
    public void executeEntityToCodeWithANonNullEntityAndUPCAWillReturnSuccess() {
        final String barcode = "049000037111";
        final BarcodeType barcodeType = BarcodeType.UPC_A;

        Barcode entity = Barcode.newBuilder().code(barcode).barcodeType(barcodeType).build();
        String code = mapper.entityToCode(entity);

        assertNotNull(code, "must not be null");
        assertEquals(barcode, code);
        assertEquals(barcodeType, entity.getBarcodeType());
        assertEquals(barcode, entity.getCode());
    }

    /**
     * Execute entity to code with a non null entity and ean 8 will return success.
     */
    @Test
    public void executeEntityToCodeWithANonNullEntityAndEAN8WillReturnSuccess() {
        final String barcode = "96385074";
        final BarcodeType barcodeType = BarcodeType.EAN_8;

        Barcode entity = Barcode.newBuilder().code(barcode).barcodeType(barcodeType).build();
        String code = mapper.entityToCode(entity);

        assertNotNull(code, "must not be null");
        assertEquals(barcode, code);
        assertEquals(barcodeType, entity.getBarcodeType());
        assertEquals(barcode, entity.getCode());
    }

    /**
     * Execute entity to code with a non null entity and ean 13 will return success.
     */
    @Test
    public void executeEntityToCodeWithANonNullEntityAndEAN13WillReturnSuccess() {
        final String barcode = "978020113447";
        final BarcodeType barcodeType = BarcodeType.EAN_13;

        Barcode entity = Barcode.newBuilder().code(barcode).barcodeType(barcodeType).build();
        String code = mapper.entityToCode(entity);

        assertNotNull(code, "must not be null");
        assertEquals(barcode, code);
        assertEquals(barcodeType, entity.getBarcodeType());
        assertEquals(barcode, entity.getCode());
    }
}