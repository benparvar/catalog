package com.benparvar.catalog.controller.product.model.create.request;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * The type Store request test.
 */
class CreateProductStoreRequestTest {
    /**
     * Check hash code integrity.
     */
    @Test
    public void checkHashCodeIntegrity() {
        CreateProductStoreRequest store1 = new CreateProductStoreRequest();
        store1.setCode("3432");
        store1.setName("Chicago Store");
        CreateProductStoreRequest store2 = new CreateProductStoreRequest();
        store2.setCode("3432");
        store2.setName("Chicago Store");

        assertEquals(store1.hashCode(), store2.hashCode());
    }

    /**
     * Check equals integrity.
     */
    @Test
    public void checkEqualsIntegrity() {
        CreateProductStoreRequest store1 = new CreateProductStoreRequest();
        store1.setCode("3432");
        store1.setName("Chicago Store");
        CreateProductStoreRequest store2 = new CreateProductStoreRequest();
        store2.setCode("3432");
        store2.setName("Chicago Store");

        assertEquals(store1, store2);
    }

    /**
     * Check builder integrity.
     */
    @Test
    public void checkBuilderIntegrity() {
        CreateProductStoreRequest store = new CreateProductStoreRequest();
        store.setCode("3432");
        store.setName("Chicago Store");

        assertNotNull(store);
        assertEquals("3432", store.getCode());
        assertEquals("Chicago Store", store.getName());
    }

    /**
     * Check to string integrity.
     */
    @Test
    public void checkToStringIntegrity() {
        CreateProductStoreRequest store = new CreateProductStoreRequest();
        store.setCode("3432");
        store.setName("Chicago Store");

        assertNotNull(store);
        assertEquals("StoreRequest{code='3432', name='Chicago Store'}", store.toString());
    }

}