package com.benparvar.catalog.controller.store.model.create;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * The type Create store request test.
 */
class CreateStoreRequestTest {
    /**
     * Check hash code integrity.
     */
    @Test
    public void checkHashCodeIntegrity() {
        CreateStoreRequest store1 = new CreateStoreRequest();
        store1.setCode("LT");
        store1.setName("LITRE");
        CreateStoreRequest store2 = new CreateStoreRequest();
        store2.setCode("LT");
        store2.setName("LITRE");

        assertEquals(store1.hashCode(), store2.hashCode());
    }

    /**
     * Check equals integrity.
     */
    @Test
    public void checkEqualsIntegrity() {
        CreateStoreRequest store1 = new CreateStoreRequest();
        store1.setCode("LT");
        store1.setName("LITRE");
        CreateStoreRequest store2 = new CreateStoreRequest();
        store2.setCode("LT");
        store2.setName("LITRE");

        assertEquals(store1, store2);
    }

    /**
     * Check builder integrity.
     */
    @Test
    public void checkBuilderIntegrity() {
        CreateStoreRequest store = new CreateStoreRequest();
        store.setCode("LT");
        store.setName("LITRE");

        assertNotNull(store);
        assertEquals("LT", store.getCode());
        assertEquals("LITRE", store.getName());
    }

    /**
     * Check to string integrity.
     */
    @Test
    public void checkToStringIntegrity() {
        CreateStoreRequest store = new CreateStoreRequest();
        store.setCode("LT");
        store.setName("LITRE");

        assertNotNull(store);
        assertEquals("CreateStoreRequest{code='LT', name='LITRE'}", store.toString());
    }
}