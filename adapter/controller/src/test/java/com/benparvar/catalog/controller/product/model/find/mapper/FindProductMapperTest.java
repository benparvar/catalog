package com.benparvar.catalog.controller.product.model.find.mapper;

import com.benparvar.catalog.controller.product.model.find.response.FindProductResponse;
import com.benparvar.catalog.domain.entity.barcode.Barcode;
import com.benparvar.catalog.domain.entity.image.Image;
import com.benparvar.catalog.domain.entity.product.Product;
import com.benparvar.catalog.domain.entity.store.Store;
import com.benparvar.catalog.domain.entity.unit.Unit;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

/**
 * The type Find product mapper test.
 */
class FindProductMapperTest {
    private FindProductMapper mapper;
    private FindProductUnitMapper unitMapper;
    private FindProductStoreMapper storeMapper;
    private FindProductImageMapper imageMapper;
    private FindProductBarcodeMapper barcodeMapper;

    /**
     * Init.
     */
    @BeforeEach
    public void init() {
        unitMapper = new FindProductUnitMapper();
        storeMapper = new FindProductStoreMapper();
        imageMapper = new FindProductImageMapper();
        barcodeMapper = new FindProductBarcodeMapper();
        mapper = new FindProductMapper(unitMapper, storeMapper, imageMapper, barcodeMapper);
    }

    /**
     * Execute entity to response with a null entity will return null.
     */
    @Test
    public void executeEntityToResponseWithANullEntityWillReturnNull() {
        Product entity = null;
        FindProductResponse response = mapper.entityToResponse(entity);

        assertNull(response, "must be null");
    }

    /**
     * Execute entity to response with a non null entity will return with success.
     */
    @Test
    public void executeEntityToResponseWithANonNullEntityWillReturnWithSuccess() {
        final String code = "5445";
        final String name = "Blue Product";
        final String barcode = "049000037111";
        final String description = "Blue Product is the most blue product.";
        final String unitCode = "PC";
        final String unitName = "PIECE";
        final BigDecimal price = BigDecimal.valueOf(1.20);
        final String storeCode = "345";
        final String storeName = "Matrix";
        final String imageUrl = "http://www.sharepic.com/pic01.png";
        final String imageDescription = "thumb";

        Product entity = Product.newBuilder()
                .code(code)
                .name(name)
                .barcode(Barcode.newBuilder().code(barcode).build())
                .description(description)
                .unit(Unit.newBuilder().code(unitCode).name(unitName).build())
                .price(price)
                .store(Store.newBuilder().code(storeCode).name(storeName).build())
                .images(Arrays.asList(Image.newBuilder().url(imageUrl).description(imageDescription).build()))
                .build();

        FindProductResponse response = mapper.entityToResponse(entity);

        assertNotNull(response, "create product response do not must be null");
        assertNotNull(response.getUnit(), "create product response unit do not must be null");
        assertNotNull(response.getStore(), "create product response store do not must be null");
        assertNotNull(response.getImages(), "create product response images do not must be null");

        assertEquals(code, response.getCode());
        assertEquals(name, response.getName());
        assertEquals(barcode, response.getBarcode());
        assertEquals(description, response.getDescription());
        assertEquals(unitCode, response.getUnit().getCode());
        assertEquals(unitName, response.getUnit().getName());
        assertEquals(price, response.getPrice());
        assertEquals(storeCode, response.getStore().getCode());
        assertEquals(storeName, response.getStore().getName());
        assertEquals(imageUrl, response.getImages().get(0).getUrl());
        assertEquals(imageDescription, response.getImages().get(0).getDescription());
    }}