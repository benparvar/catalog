package com.benparvar.catalog.controller.product.model.update.response;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * The type Update product image response test.
 */
class UpdateProductImageResponseTest {
    /**
     * Check hash code integrity.
     */
    @Test
    public void checkHashCodeIntegrity() {
        UpdateProductImageResponse image1 = new UpdateProductImageResponse();
        image1.setUrl("www.royksopp.com/roy01.png");
        image1.setDescription("Chicago Image");
        UpdateProductImageResponse image2 = new UpdateProductImageResponse();
        image2.setUrl("www.royksopp.com/roy01.png");
        image2.setDescription("Chicago Image");

        assertEquals(image1.hashCode(), image2.hashCode());
    }

    /**
     * Check equals integrity.
     */
    @Test
    public void checkEqualsIntegrity() {
        UpdateProductImageResponse image1 = new UpdateProductImageResponse();
        image1.setUrl("www.royksopp.com/roy01.png");
        image1.setDescription("Chicago Image");
        UpdateProductImageResponse image2 = new UpdateProductImageResponse();
        image2.setUrl("www.royksopp.com/roy01.png");
        image2.setDescription("Chicago Image");

        assertEquals(image1, image2);
    }

    /**
     * Check builder integrity.
     */
    @Test
    public void checkBuilderIntegrity() {
        UpdateProductImageResponse image = new UpdateProductImageResponse();
        image.setUrl("www.royksopp.com/roy01.png");
        image.setDescription("Chicago Image");

        assertNotNull(image);
        assertEquals("www.royksopp.com/roy01.png", image.getUrl());
        assertEquals("Chicago Image", image.getDescription());
    }

    /**
     * Check to string integrity.
     */
    @Test
    public void checkToStringIntegrity() {
        UpdateProductImageResponse image = new UpdateProductImageResponse();
        image.setUrl("www.royksopp.com/roy01.png");
        image.setDescription("Chicago Image");

        assertNotNull(image);
        assertEquals("UpdateProductImageResponse{description='Chicago Image', url='www.royksopp.com/roy01.png'}", image.toString());
    }
}