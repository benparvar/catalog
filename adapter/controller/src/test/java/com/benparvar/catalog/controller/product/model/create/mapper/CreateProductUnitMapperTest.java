package com.benparvar.catalog.controller.product.model.create.mapper;

import com.benparvar.catalog.controller.product.model.create.request.CreateProductUnitRequest;
import com.benparvar.catalog.controller.product.model.create.response.CreateProductUnitResponse;
import com.benparvar.catalog.domain.entity.unit.Unit;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * The type Unit mapper test.
 */
class CreateProductUnitMapperTest {
    private CreateProductUnitMapper mapper;

    /**
     * Init.
     */
    @BeforeEach
    public void init() {
        mapper = new CreateProductUnitMapper();
    }

    /**
     * Execute entity to response with a null entity will return null.
     */
    @Test
    public void executeEntityToResponseWithANullEntityWillReturnNull() {
        Unit entity = null;
        CreateProductUnitResponse response = mapper.entityToResponse(entity);

        assertNull(response, "must be null");
    }

    /**
     * Execute entity to response with a non null entity will return with success.
     */
    @Test
    public void executeEntityToResponseWithANonNullEntityWillReturnWithSuccess() {
        Unit entity = Unit.newBuilder().code("MT").name("Meter").build();

        CreateProductUnitResponse response = mapper.entityToResponse(entity);

        assertNotNull(response, "do not must be null");
        assertEquals("MT", response.getCode());
        assertEquals("Meter", response.getName());
    }

    /**
     * Execute request to entity with a null request will return null.
     */
    @Test
    public void executeRequestToEntityWithANullRequestWillReturnNull() {
        CreateProductUnitRequest request = null;
        Unit unit = mapper.requestToEntity(request);

        assertNull(unit, "must be null");
    }

    /**
     * Execute request to entity with a non null request will return with success.
     */
    @Test
    public void executeRequestToEntityWithANonNullRequestWillReturnWithSuccess() {
        CreateProductUnitRequest request = new CreateProductUnitRequest();
        request.setCode("MT");
        request.setName("Meter");

        Unit unit = mapper.requestToEntity(request);

        assertNotNull(unit, "do not must be null");
        assertEquals("MT", unit.getCode());
        assertEquals("Meter", unit.getName());
    }
}