package com.benparvar.catalog.controller.product.model.find.response;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * The type Find product store response test.
 */
class FindProductStoreResponseTest {
    /**
     * Check hash code integrity.
     */
    @Test
    public void checkHashCodeIntegrity() {
        FindProductStoreResponse store1 = new FindProductStoreResponse();
        store1.setCode("4323");
        store1.setName("Almerinda Store");
        FindProductStoreResponse store2 = new FindProductStoreResponse();
        store2.setCode("4323");
        store2.setName("Almerinda Store");

        assertEquals(store1.hashCode(), store2.hashCode());
    }

    /**
     * Check equals integrity.
     */
    @Test
    public void checkEqualsIntegrity() {
        FindProductStoreResponse store1 = new FindProductStoreResponse();
        store1.setCode("4323");
        store1.setName("Almerinda Store");
        FindProductStoreResponse store2 = new FindProductStoreResponse();
        store2.setCode("4323");
        store2.setName("Almerinda Store");

        assertEquals(store1, store2);
    }

    /**
     * Check builder integrity.
     */
    @Test
    public void checkBuilderIntegrity() {
        FindProductStoreResponse store = new FindProductStoreResponse();
        store.setCode("4323");
        store.setName("Almerinda Store");

        assertNotNull(store);
        assertEquals("4323", store.getCode());
        assertEquals("Almerinda Store", store.getName());
    }

    /**
     * Check to string integrity.
     */
    @Test
    public void checkToStringIntegrity() {
        FindProductStoreResponse store = new FindProductStoreResponse();
        store.setCode("4323");
        store.setName("Almerinda Store");

        assertNotNull(store);
        assertEquals("FindProductStoreResponse{code='4323', name='Almerinda Store'}", store.toString());
    }
}