package com.benparvar.catalog.controller.product;

import com.benparvar.catalog.controller.product.model.create.mapper.CreateProductMapper;
import com.benparvar.catalog.controller.product.model.create.request.CreateProductRequest;
import com.benparvar.catalog.controller.product.model.create.request.UpdateProductPriceRequest;
import com.benparvar.catalog.controller.product.model.create.response.CreateProductResponse;
import com.benparvar.catalog.controller.product.model.delete.DeleteProductMapper;
import com.benparvar.catalog.controller.product.model.delete.DeleteProductRequest;
import com.benparvar.catalog.controller.product.model.find.mapper.FindProductMapper;
import com.benparvar.catalog.controller.product.model.find.response.FindProductResponse;
import com.benparvar.catalog.controller.product.model.update.mapper.UpdateProductMapper;
import com.benparvar.catalog.controller.product.model.update.response.UpdateProductResponse;
import com.benparvar.catalog.usecase.product.create.CreateProductUseCase;
import com.benparvar.catalog.usecase.product.delete.DeleteProductUseCase;
import com.benparvar.catalog.usecase.product.find.FindProductByBarcodeAndStoreCodeUseCase;
import com.benparvar.catalog.usecase.product.update.UpdateProductPriceByBarcodeAndStoreCodeCodeUseCase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The type Product controller.
 */
public class ProductController {
    private final Logger log = LoggerFactory.getLogger(ProductController.class);
    private final CreateProductUseCase createProductUseCase;
    private final CreateProductMapper createProductMapper;
    private final FindProductByBarcodeAndStoreCodeUseCase findProductByBarcodeAndStoreCodeUseCase;
    private final FindProductMapper findProductMapper;
    private final UpdateProductPriceByBarcodeAndStoreCodeCodeUseCase updateProductPriceByBarcodeAndStoreCodeCodeUseCase;
    private final UpdateProductMapper updateProductMapper;
    private final DeleteProductUseCase deleteProductUseCase;
    private final DeleteProductMapper deleteProductMapper;

    /**
     * Instantiates a new Product controller.
     *
     * @param createProductUseCase                               the create store use case
     * @param createProductMapper                                the create store mapper
     * @param findProductByBarcodeAndStoreCodeUseCase            the find product by barcode and store code use case
     * @param findProductMapper                                  the find product mapper
     * @param updateProductPriceByBarcodeAndStoreCodeCodeUseCase the update product price by barcode and store code code use case
     * @param updateProductMapper                                the update product mapper
     * @param deleteProductUseCase                               the delete product use case
     * @param deleteProductMapper                                the delete product mapper
     */
    public ProductController(CreateProductUseCase createProductUseCase, CreateProductMapper createProductMapper,
                             FindProductByBarcodeAndStoreCodeUseCase findProductByBarcodeAndStoreCodeUseCase,
                             FindProductMapper findProductMapper,
                             UpdateProductPriceByBarcodeAndStoreCodeCodeUseCase updateProductPriceByBarcodeAndStoreCodeCodeUseCase,
                             UpdateProductMapper updateProductMapper, DeleteProductUseCase deleteProductUseCase,
                             DeleteProductMapper deleteProductMapper) {
        this.createProductUseCase = createProductUseCase;
        this.createProductMapper = createProductMapper;
        this.findProductByBarcodeAndStoreCodeUseCase = findProductByBarcodeAndStoreCodeUseCase;
        this.findProductMapper = findProductMapper;
        this.updateProductPriceByBarcodeAndStoreCodeCodeUseCase = updateProductPriceByBarcodeAndStoreCodeCodeUseCase;
        this.updateProductMapper = updateProductMapper;
        this.deleteProductUseCase = deleteProductUseCase;
        this.deleteProductMapper = deleteProductMapper;
    }

    /**
     * Create create product response.
     *
     * @param request the request
     * @return the create product response
     */
    public CreateProductResponse create(CreateProductRequest request) {
        log.info("create: {}", request);

        return createProductMapper.entityToResponse(createProductUseCase.execute(createProductMapper
                .requestToEntity(request)));
    }

    /**
     * Find product by barcode and store code find product response.
     *
     * @param barcodeCode the barcode code
     * @param storeCode   the store code
     * @return the find product response
     */
    public FindProductResponse findProductByBarcodeAndStoreCode(String barcodeCode, String storeCode) {
        log.info("findProductByBarcodeAndStoreCode barcodeCode: {} storeCode: {}", barcodeCode, storeCode);

        return findProductMapper.entityToResponse(findProductByBarcodeAndStoreCodeUseCase.execute(barcodeCode, storeCode));
    }

    /**
     * Update product price by barcode and store code update product response.
     *
     * @param request the request
     * @return the update product response
     */
    public UpdateProductResponse updateProductPriceByBarcodeAndStoreCode(UpdateProductPriceRequest request) {
        log.info("updateProductPriceByBarcodeAndStoreCode request: {}", request);

        return updateProductMapper.entityToResponse(updateProductPriceByBarcodeAndStoreCodeCodeUseCase
                .execute(updateProductMapper.requestToupdateProductPriceEntity(request)));
    }

    /**
     * Delete.
     *
     * @param barcodeCode the barcode code
     * @param storeCode   the store code
     */
    public void delete(String barcodeCode, String storeCode) {
        log.info("delete barcodeCode: {} storeCode: {}", barcodeCode, storeCode);

        deleteProductUseCase.execute(deleteProductMapper.requestToEntity(DeleteProductRequest.newBuilder()
                .barcodeCode(barcodeCode).storeCode(storeCode).build()));
    }
}
