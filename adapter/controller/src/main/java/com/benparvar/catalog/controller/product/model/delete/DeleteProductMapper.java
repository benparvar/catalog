package com.benparvar.catalog.controller.product.model.delete;

import com.benparvar.catalog.domain.entity.barcode.Barcode;
import com.benparvar.catalog.domain.entity.product.Product;
import com.benparvar.catalog.domain.entity.store.Store;

import java.io.Serializable;

/**
 * The type Delete product mapper.
 */
public class DeleteProductMapper implements Serializable {

    /**
     * Request to entity product.
     *
     * @param request the request
     * @return the product
     */
    public Product requestToEntity(DeleteProductRequest request) {
        if (request == null)
            return null;

        Product.Builder builder = Product.newBuilder();

        builder.barcode(Barcode.newBuilder().code(request.getBarcodeCode()).build());
        builder.store(Store.newBuilder().code(request.getStoreCode()).build());

        return builder.build();
    }
}
