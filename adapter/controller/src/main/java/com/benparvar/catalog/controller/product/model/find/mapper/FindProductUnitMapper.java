package com.benparvar.catalog.controller.product.model.find.mapper;

import com.benparvar.catalog.controller.product.model.find.response.FindProductUnitResponse;
import com.benparvar.catalog.domain.entity.unit.Unit;

/**
 * The type Find product unit mapper.
 */
public class FindProductUnitMapper {

    /**
     * Entity to response find product unit response.
     *
     * @param entity the entity
     * @return the find product unit response
     */
    public FindProductUnitResponse entityToResponse(Unit entity) {
        if (entity == null)
            return null;

        FindProductUnitResponse response = new FindProductUnitResponse();

        response.setCode(entity.getCode());
        response.setName(entity.getName());

        return response;
    }
}
