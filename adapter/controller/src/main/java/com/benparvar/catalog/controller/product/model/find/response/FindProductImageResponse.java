package com.benparvar.catalog.controller.product.model.find.response;

import java.util.Objects;

/**
 * The type Image response.
 */
public class FindProductImageResponse {
    private String description;
    private String url;

    /**
     * Gets description.
     *
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets description.
     *
     * @param description the description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Gets url.
     *
     * @return the url
     */
    public String getUrl() {
        return url;
    }

    /**
     * Sets url.
     *
     * @param url the url
     */
    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FindProductImageResponse that = (FindProductImageResponse) o;
        return description.equals(that.description) &&
                url.equals(that.url);
    }

    @Override
    public int hashCode() {
        return Objects.hash(description, url);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("FindProductImageResponse{");
        sb.append("description='").append(description).append('\'');
        sb.append(", url='").append(url).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
