package com.benparvar.catalog.controller.product.model.create.response;

import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;

/**
 * The type Create product response.
 */
public class CreateProductResponse {
    private String code;
    private String name;
    private String description;
    private String barcode;
    private BigDecimal price;
    private CreateProductUnitResponse unit;
    private CreateProductStoreResponse store;
    private List<CreateProductImageResponse> images;

    /**
     * Gets code.
     *
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * Sets code.
     *
     * @param code the code
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets name.
     *
     * @param name the name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets description.
     *
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets description.
     *
     * @param description the description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Gets barcode.
     *
     * @return the barcode
     */
    public String getBarcode() {
        return barcode;
    }

    /**
     * Sets barcode.
     *
     * @param barcode the barcode
     */
    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    /**
     * Gets price.
     *
     * @return the price
     */
    public BigDecimal getPrice() {
        return price;
    }

    /**
     * Sets price.
     *
     * @param price the price
     */
    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    /**
     * Gets unit.
     *
     * @return the unit
     */
    public CreateProductUnitResponse getUnit() {
        return unit;
    }

    /**
     * Sets unit.
     *
     * @param unit the unit
     */
    public void setUnit(CreateProductUnitResponse unit) {
        this.unit = unit;
    }

    /**
     * Gets store.
     *
     * @return the store
     */
    public CreateProductStoreResponse getStore() {
        return store;
    }

    /**
     * Sets store.
     *
     * @param store the store
     */
    public void setStore(CreateProductStoreResponse store) {
        this.store = store;
    }

    /**
     * Gets images.
     *
     * @return the images
     */
    public List<CreateProductImageResponse> getImages() {
        return images;
    }

    /**
     * Sets images.
     *
     * @param images the images
     */
    public void setImages(List<CreateProductImageResponse> images) {
        this.images = images;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CreateProductResponse that = (CreateProductResponse) o;
        return code.equals(that.code) &&
                name.equals(that.name) &&
                description.equals(that.description) &&
                barcode.equals(that.barcode) &&
                price.equals(that.price) &&
                unit.equals(that.unit) &&
                store.equals(that.store) &&
                images.equals(that.images);
    }

    @Override
    public int hashCode() {
        return Objects.hash(code, name, description, barcode, price, unit, store, images);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("CreateProductResponse{");
        sb.append("code='").append(code).append('\'');
        sb.append(", name='").append(name).append('\'');
        sb.append(", description='").append(description).append('\'');
        sb.append(", barcode='").append(barcode).append('\'');
        sb.append(", price=").append(price);
        sb.append(", unit=").append(unit);
        sb.append(", store=").append(store);
        sb.append(", images=").append(images);
        sb.append('}');
        return sb.toString();
    }
}
