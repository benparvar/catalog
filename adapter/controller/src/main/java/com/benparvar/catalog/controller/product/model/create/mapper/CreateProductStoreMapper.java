package com.benparvar.catalog.controller.product.model.create.mapper;

import com.benparvar.catalog.controller.product.model.create.request.CreateProductStoreRequest;
import com.benparvar.catalog.controller.product.model.create.response.CreateProductStoreResponse;
import com.benparvar.catalog.domain.entity.store.Store;

/**
 * The type Store mapper.
 */
public class CreateProductStoreMapper {

    /**
     * Request to entity store.
     *
     * @param request the request
     * @return the store
     */
    public Store requestToEntity(CreateProductStoreRequest request) {
        if (request == null)
            return null;

        Store.Builder builder = Store.newBuilder();

        builder.code(request.getCode());
        builder.name(request.getName());

        return builder.build();
    }

    /**
     * Entity to response store response.
     *
     * @param entity the entity
     * @return the store response
     */
    public CreateProductStoreResponse entityToResponse(Store entity) {
        if (entity == null)
            return null;

        CreateProductStoreResponse response = new CreateProductStoreResponse();

        response.setCode(entity.getCode());
        response.setName(entity.getName());

        return response;
    }
}
