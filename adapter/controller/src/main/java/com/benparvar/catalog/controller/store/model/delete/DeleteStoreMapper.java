package com.benparvar.catalog.controller.store.model.delete;

import com.benparvar.catalog.domain.entity.store.Store;

/**
 * The type Delete store mapper.
 */
public class DeleteStoreMapper {
    /**
     * Request to entity store.
     *
     * @param request the request
     * @return the store
     */
    public Store requestToEntity(DeleteStoreRequest request) {
        if (request == null)
            return null;

        Store.Builder builder = Store.newBuilder();

        builder.code(request.getCode());
        builder.name(request.getName());

        return builder.build();
    }
}
