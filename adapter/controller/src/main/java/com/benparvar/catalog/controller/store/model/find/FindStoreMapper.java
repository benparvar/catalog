package com.benparvar.catalog.controller.store.model.find;

import com.benparvar.catalog.domain.entity.store.Store;
import com.benparvar.catalog.domain.entity.validator.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * The type Find store mapper.
 */
public class FindStoreMapper {

    /**
     * Entity to response find store response.
     *
     * @param entity the entity
     * @return the find store response
     */
    public FindStoreResponse entityToResponse(Store entity) {
        if (entity == null)
            return null;

        FindStoreResponse response = new FindStoreResponse();

        response.setCode(entity.getCode());
        response.setName(entity.getName());

        return response;
    }

    /**
     * Entity to response list.
     *
     * @param entities the entities
     * @return the list
     */
    public List<FindStoreResponse> entityToResponse(List<Store> entities) {
        List<FindStoreResponse> responses = new ArrayList();

        if (CollectionUtils.isNotEmpty(entities))
            for (Store entity : entities) {
                if (null != entity)
                    responses.add(entityToResponse(entity));
            }

        return responses;
    }

}
