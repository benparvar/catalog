package com.benparvar.catalog.controller.product.model.create.request;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

/**
 * The type Update price product request.
 */
public class UpdateProductPriceRequest implements Serializable {
    private String barcode;
    private String storeCode;
    private BigDecimal price;

    /**
     * Gets price.
     *
     * @return the price
     */
    public BigDecimal getPrice() {
        return price;
    }

    /**
     * Sets price.
     *
     * @param price the price
     */
    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getStoreCode() {
        return storeCode;
    }

    public void setStoreCode(String storeCode) {
        this.storeCode = storeCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UpdateProductPriceRequest that = (UpdateProductPriceRequest) o;
        return barcode.equals(that.barcode) &&
                storeCode.equals(that.storeCode) &&
                price.equals(that.price);
    }

    @Override
    public int hashCode() {
        return Objects.hash(barcode, storeCode, price);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("UpdateProductPriceRequest{");
        sb.append("barcode='").append(barcode).append('\'');
        sb.append(", storeCode='").append(storeCode).append('\'');
        sb.append(", price=").append(price);
        sb.append('}');
        return sb.toString();
    }
}
