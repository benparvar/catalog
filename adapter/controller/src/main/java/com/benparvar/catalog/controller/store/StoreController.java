package com.benparvar.catalog.controller.store;

import com.benparvar.catalog.controller.store.model.create.CreateStoreMapper;
import com.benparvar.catalog.controller.store.model.create.CreateStoreRequest;
import com.benparvar.catalog.controller.store.model.create.CreateStoreResponse;
import com.benparvar.catalog.controller.store.model.delete.DeleteStoreMapper;
import com.benparvar.catalog.controller.store.model.delete.DeleteStoreRequest;
import com.benparvar.catalog.controller.store.model.find.FindStoreMapper;
import com.benparvar.catalog.controller.store.model.find.FindStoreResponse;
import com.benparvar.catalog.controller.store.model.update.UpdateStoreMapper;
import com.benparvar.catalog.controller.store.model.update.UpdateStoreRequest;
import com.benparvar.catalog.controller.store.model.update.UpdateStoreResponse;
import com.benparvar.catalog.usecase.store.create.CreateStoreUseCase;
import com.benparvar.catalog.usecase.store.delete.DeleteStoreUseCase;
import com.benparvar.catalog.usecase.store.find.all.FindAllStoresUseCase;
import com.benparvar.catalog.usecase.store.find.code.FindStoreByCodeUseCase;
import com.benparvar.catalog.usecase.store.update.UpdateStoreUseCase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * The type Store controller.
 */
public class StoreController {
    private final Logger log = LoggerFactory.getLogger(StoreController.class);
    private final CreateStoreUseCase createStoreUseCase;
    private final CreateStoreMapper createStoreMapper;
    private final FindAllStoresUseCase findAllStoresUseCase;
    private final FindStoreMapper findStoreMapper;
    private final FindStoreByCodeUseCase findStoreByCodeUseCase;
    private final UpdateStoreUseCase updateStoreUseCase;
    private final UpdateStoreMapper updateStoreMapper;
    private final DeleteStoreUseCase deleteStoreUseCase;
    private final DeleteStoreMapper deleteStoreMapper;


    /**
     * Instantiates a new Store controller.
     *
     * @param createStoreUseCase     the create store use case
     * @param createStoreMapper      the create store mapper
     * @param findAllStoresUseCase   the find all stores use case
     * @param findStoreMapper        the find store mapper
     * @param findStoreByCodeUseCase the find store by code use case
     * @param updateStoreUseCase     the update store use case
     * @param updateStoreMapper      the update store mapper
     * @param deleteStoreUseCase     the delete store use case
     * @param deleteStoreMapper      the delete store mapper
     */
    public StoreController(CreateStoreUseCase createStoreUseCase, CreateStoreMapper createStoreMapper,
                           FindAllStoresUseCase findAllStoresUseCase, FindStoreMapper findStoreMapper,
                           FindStoreByCodeUseCase findStoreByCodeUseCase, UpdateStoreUseCase updateStoreUseCase,
                           UpdateStoreMapper updateStoreMapper, DeleteStoreUseCase deleteStoreUseCase,
                           DeleteStoreMapper deleteStoreMapper) {
        this.createStoreUseCase = createStoreUseCase;
        this.createStoreMapper = createStoreMapper;
        this.findAllStoresUseCase = findAllStoresUseCase;
        this.findStoreMapper = findStoreMapper;
        this.findStoreByCodeUseCase = findStoreByCodeUseCase;
        this.updateStoreUseCase = updateStoreUseCase;
        this.updateStoreMapper = updateStoreMapper;
        this.deleteStoreUseCase = deleteStoreUseCase;
        this.deleteStoreMapper = deleteStoreMapper;
    }

    /**
     * Create create store response.
     *
     * @param request the request
     * @return the create store response
     */
    public CreateStoreResponse create(CreateStoreRequest request) {
        log.info("create: {}", request);

        return createStoreMapper.entityToResponse(createStoreUseCase.execute(createStoreMapper.requestToEntity(request)));
    }

    /**
     * Find all list.
     *
     * @return the list
     */
    public List<FindStoreResponse> findAll() {
        log.info("findAll: ");

        return findStoreMapper.entityToResponse(findAllStoresUseCase.execute());
    }

    /**
     * Find by code find store response.
     *
     * @param code the code
     * @return the find store response
     */
    public FindStoreResponse findByCode(String code) {
        log.info("findByCode: {}", code);

        return findStoreMapper.entityToResponse(findStoreByCodeUseCase.execute(code));
    }

    /**
     * Update.
     *
     * @param request the request
     * @return the update store response
     */
    public UpdateStoreResponse update(UpdateStoreRequest request) {
        log.info("update: {}", request);

        return updateStoreMapper.entityToResponse(updateStoreUseCase.execute(updateStoreMapper.requestToEntity(request)));
    }

    /**
     * Delete.
     *
     * @param code the code
     */
    public void delete(String code) {
        log.info("delete: {}", code);

        deleteStoreUseCase.execute(deleteStoreMapper.requestToEntity(DeleteStoreRequest.newBuilder().code(code).build()));
    }
}
