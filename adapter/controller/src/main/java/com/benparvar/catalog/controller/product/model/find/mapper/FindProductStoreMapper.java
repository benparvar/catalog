package com.benparvar.catalog.controller.product.model.find.mapper;

import com.benparvar.catalog.controller.product.model.find.response.FindProductStoreResponse;
import com.benparvar.catalog.domain.entity.store.Store;

/**
 * The type Find product store mapper.
 */
public class FindProductStoreMapper {

    /**
     * Entity to response find product store response.
     *
     * @param entity the entity
     * @return the find product store response
     */
    public FindProductStoreResponse entityToResponse(Store entity) {
        if (entity == null)
            return null;

        FindProductStoreResponse response = new FindProductStoreResponse();

        response.setCode(entity.getCode());
        response.setName(entity.getName());

        return response;
    }
}
