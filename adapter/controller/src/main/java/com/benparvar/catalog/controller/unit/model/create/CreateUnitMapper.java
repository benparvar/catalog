package com.benparvar.catalog.controller.unit.model.create;

import com.benparvar.catalog.domain.entity.unit.Unit;

/**
 * The type Create unit mapper.
 */
public class CreateUnitMapper {
    /**
     * Request to entity unit.
     *
     * @param request the request
     * @return the unit
     */
    public Unit requestToEntity(CreateUnitRequest request) {
        if (request == null)
            return null;

        Unit.Builder builder = Unit.newBuilder();

        builder.code(request.getCode());
        builder.name(request.getName());

        return builder.build();
    }

    /**
     * Entity to response create unit response.
     *
     * @param entity the entity
     * @return the create unit response
     */
    public CreateUnitResponse entityToResponse(Unit entity) {
        if (entity == null)
            return null;

        CreateUnitResponse response = new CreateUnitResponse();

        response.setCode(entity.getCode());
        response.setName(entity.getName());

        return response;
    }
}
