package com.benparvar.catalog.controller.store.model.create;

import com.benparvar.catalog.domain.entity.store.Store;

/**
 * The type Create store mapper.
 */
public class CreateStoreMapper {
    /**
     * Request to entity store.
     *
     * @param request the request
     * @return the store
     */
    public Store requestToEntity(CreateStoreRequest request) {
        if (request == null)
            return null;

        Store.Builder builder = Store.newBuilder();

        builder.code(request.getCode());
        builder.name(request.getName());

        return builder.build();
    }

    /**
     * Entity to response create store response.
     *
     * @param entity the entity
     * @return the create store response
     */
    public CreateStoreResponse entityToResponse(Store entity) {
        if (entity == null)
            return null;

        CreateStoreResponse response = new CreateStoreResponse();

        response.setCode(entity.getCode());
        response.setName(entity.getName());

        return response;
    }
}
