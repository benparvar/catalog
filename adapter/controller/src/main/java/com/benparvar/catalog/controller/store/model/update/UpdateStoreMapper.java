package com.benparvar.catalog.controller.store.model.update;

import com.benparvar.catalog.domain.entity.store.Store;

/**
 * The type Update store mapper.
 */
public class UpdateStoreMapper {
    /**
     * Request to entity store.
     *
     * @param request the request
     * @return the store
     */
    public Store requestToEntity(UpdateStoreRequest request) {
        if (request == null)
            return null;

        Store.Builder builder = Store.newBuilder();

        builder.code(request.getCode());
        builder.name(request.getName());

        return builder.build();
    }

    /**
     * Entity to response create store response.
     *
     * @param entity the entity
     * @return the create store response
     */
    public UpdateStoreResponse entityToResponse(Store entity) {
        if (entity == null)
            return null;

        UpdateStoreResponse response = new UpdateStoreResponse();

        response.setCode(entity.getCode());
        response.setName(entity.getName());

        return response;
    }
}
