package com.benparvar.catalog.controller.unit.model.find;

import com.benparvar.catalog.domain.entity.unit.Unit;
import com.benparvar.catalog.domain.entity.validator.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * The type Find unit mapper.
 */
public class FindUnitMapper {

    /**
     * Entity to response find unit response.
     *
     * @param entity the entity
     * @return the find unit response
     */
    public FindUnitResponse entityToResponse(Unit entity) {
        if (entity == null)
            return null;

        FindUnitResponse response = new FindUnitResponse();

        response.setCode(entity.getCode());
        response.setName(entity.getName());

        return response;
    }

    /**
     * Entity to response list.
     *
     * @param entities the entities
     * @return the list
     */
    public List<FindUnitResponse> entityToResponse(List<Unit> entities) {
        List<FindUnitResponse> responses = new ArrayList();

        if (CollectionUtils.isNotEmpty(entities))
            for (Unit entity : entities) {
                if (null != entity)
                    responses.add(entityToResponse(entity));
            }

        return responses;
    }

}
