package com.benparvar.catalog.controller.unit.model.update;

import com.benparvar.catalog.domain.entity.unit.Unit;

/**
 * The type Update unit mapper.
 */
public class UpdateUnitMapper {
    /**
     * Request to entity unit.
     *
     * @param request the request
     * @return the unit
     */
    public Unit requestToEntity(UpdateUnitRequest request) {
        if (request == null)
            return null;

        Unit.Builder builder = Unit.newBuilder();

        builder.code(request.getCode());
        builder.name(request.getName());

        return builder.build();
    }

    /**
     * Entity to response create unit response.
     *
     * @param entity the entity
     * @return the create unit response
     */
    public UpdateUnitResponse entityToResponse(Unit entity) {
        if (entity == null)
            return null;

        UpdateUnitResponse response = new UpdateUnitResponse();

        response.setCode(entity.getCode());
        response.setName(entity.getName());

        return response;
    }
}
