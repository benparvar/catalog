package com.benparvar.catalog.controller.product.model.create.mapper;

import com.benparvar.catalog.controller.product.model.create.request.CreateProductRequest;
import com.benparvar.catalog.controller.product.model.create.response.CreateProductResponse;
import com.benparvar.catalog.domain.entity.product.Product;

/**
 * The type Create product mapper.
 */
public class CreateProductMapper {
    private final CreateProductUnitMapper createProductUnitMapper;
    private final CreateProductStoreMapper createProductStoreMapper;
    private final CreateProductImageMapper createProductImageMapper;
    private final CreateProductBarcodeMapper createProductBarcodeMapper;

    /**
     * Instantiates a new Create product mapper.
     *
     * @param createProductUnitMapper    the unit mapper
     * @param createProductStoreMapper   the store mapper
     * @param createProductImageMapper   the image mapper
     * @param createProductBarcodeMapper the barcode mapper
     */
    public CreateProductMapper(CreateProductUnitMapper createProductUnitMapper, CreateProductStoreMapper createProductStoreMapper, CreateProductImageMapper createProductImageMapper, CreateProductBarcodeMapper createProductBarcodeMapper) {
        this.createProductUnitMapper = createProductUnitMapper;
        this.createProductStoreMapper = createProductStoreMapper;
        this.createProductImageMapper = createProductImageMapper;
        this.createProductBarcodeMapper = createProductBarcodeMapper;
    }

    /**
     * Request to entity product.
     *
     * @param request the request
     * @return the product
     */
    public Product requestToEntity(CreateProductRequest request) {
        if (request == null)
            return null;

        Product.Builder builder = Product.newBuilder();

        builder.unit(createProductUnitMapper.requestToEntity(request.getUnit()));
        builder.store(createProductStoreMapper.requestToEntity(request.getStore()));
        builder.images(createProductImageMapper.requestToEntity(request.getImages()));
        builder.name(request.getName());
        builder.description(request.getDescription());
        builder.barcode(createProductBarcodeMapper.codeToEntity(request.getBarcode()));
        builder.code(request.getCode());
        builder.price(request.getPrice());

        return builder.build();
    }

    /**
     * Entity to response create product response.
     *
     * @param entity the entity
     * @return the create product response
     */
    public CreateProductResponse entityToResponse(Product entity) {
        if (entity == null)
            return null;

        CreateProductResponse response = new CreateProductResponse();

        response.setCode(entity.getCode());
        response.setName(entity.getName());
        response.setBarcode(createProductBarcodeMapper.entityToCode(entity.getBarcode()));
        response.setDescription(entity.getDescription());
        response.setUnit(createProductUnitMapper.entityToResponse(entity.getUnit()));
        response.setPrice(entity.getPrice());
        response.setStore(createProductStoreMapper.entityToResponse(entity.getStore()));
        response.setImages(createProductImageMapper.entityToResponse(entity.getImages()));

        return response;
    }
}
