package com.benparvar.catalog.controller.product.model.create.mapper;

import com.benparvar.catalog.controller.product.model.create.request.CreateProductImageRequest;
import com.benparvar.catalog.controller.product.model.create.response.CreateProductImageResponse;
import com.benparvar.catalog.domain.entity.image.Image;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The type Image mapper.
 */
public class CreateProductImageMapper {

    /**
     * Request to entity image.
     *
     * @param request the request
     * @return the image
     */
    public Image requestToEntity(CreateProductImageRequest request) {
        if (request == null)
            return null;

        Image.Builder builder = Image.newBuilder();

        builder.url(request.getUrl());
        builder.description(request.getDescription());

        return builder.build();
    }

    /**
     * Request to entity list.
     *
     * @param requests the requests
     * @return the list
     */
    public List<Image> requestToEntity(List<CreateProductImageRequest> requests) {
        if (requests == null)
            return Collections.emptyList();

        List<Image> images = new ArrayList<>();

        for (CreateProductImageRequest request : requests) {
            images.add(requestToEntity(request));
        }

        return images;
    }

    /**
     * Entity to response image response.
     *
     * @param entity the entity
     * @return the image response
     */
    public CreateProductImageResponse entityToResponse(Image entity) {
        if (entity == null)
            return null;

        CreateProductImageResponse response = new CreateProductImageResponse();

        response.setDescription(entity.getDescription());
        response.setUrl(entity.getUrl());

        return response;
    }

    /**
     * Entity to response list.
     *
     * @param entities the entities
     * @return the list
     */
    public List<CreateProductImageResponse> entityToResponse(List<Image> entities) {
        if (entities == null)
            return Collections.emptyList();

        List<CreateProductImageResponse> responses = new ArrayList<>();

        for (Image entity : entities) {
            responses.add(entityToResponse(entity));
        }

        return responses;
    }
}
