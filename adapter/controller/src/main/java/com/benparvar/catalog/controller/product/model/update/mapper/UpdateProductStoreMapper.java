package com.benparvar.catalog.controller.product.model.update.mapper;

import com.benparvar.catalog.controller.product.model.update.response.UpdateProductStoreResponse;
import com.benparvar.catalog.domain.entity.store.Store;

/**
 * The type Update product store mapper.
 */
public class UpdateProductStoreMapper {

    /**
     * Entity to response find product store response.
     *
     * @param entity the entity
     * @return the find product store response
     */
    public UpdateProductStoreResponse entityToResponse(Store entity) {
        if (entity == null)
            return null;

        UpdateProductStoreResponse response = new UpdateProductStoreResponse();

        response.setCode(entity.getCode());
        response.setName(entity.getName());

        return response;
    }
}
