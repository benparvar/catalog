package com.benparvar.catalog.controller.product.model.delete;

import java.util.Objects;

/**
 * The type Delete product request.
 */
public class DeleteProductRequest {
    private String barcodeCode;
    private String storeCode;

    /**
     * Gets barcode code.
     *
     * @return the barcode code
     */
    public String getBarcodeCode() {
        return barcodeCode;
    }

    /**
     * Gets store code.
     *
     * @return the store code
     */
    public String getStoreCode() {
        return storeCode;
    }

    private DeleteProductRequest(Builder builder) {
        barcodeCode = builder.barcodeCode;
        storeCode = builder.storeCode;
    }

    /**
     * New builder builder.
     *
     * @return the builder
     */
    public static Builder newBuilder() {
        return new Builder();
    }

    /**
     * The type Builder.
     */
    public static final class Builder {
        private String barcodeCode;
        private String storeCode;

        private Builder() {
        }

        /**
         * Barcode code builder.
         *
         * @param val the val
         * @return the builder
         */
        public Builder barcodeCode(String val) {
            barcodeCode = val;
            return this;
        }

        /**
         * Store code builder.
         *
         * @param val the val
         * @return the builder
         */
        public Builder storeCode(String val) {
            storeCode = val;
            return this;
        }

        /**
         * Build delete product request.
         *
         * @return the delete product request
         */
        public DeleteProductRequest build() {
            return new DeleteProductRequest(this);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DeleteProductRequest that = (DeleteProductRequest) o;
        return barcodeCode.equals(that.barcodeCode) &&
                storeCode.equals(that.storeCode);
    }

    @Override
    public int hashCode() {
        return Objects.hash(barcodeCode, storeCode);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("DeleteProductRequest{");
        sb.append("barcodeCode='").append(barcodeCode).append('\'');
        sb.append(", storeCode='").append(storeCode).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
