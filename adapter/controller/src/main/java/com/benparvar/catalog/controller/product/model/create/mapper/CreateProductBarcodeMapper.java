package com.benparvar.catalog.controller.product.model.create.mapper;

import com.benparvar.catalog.domain.entity.barcode.Barcode;
import com.benparvar.catalog.domain.entity.barcode.BarcodeType;

import static com.benparvar.catalog.domain.entity.barcode.BarcodeType.*;

/**
 * The type Barcode mapper.
 */
public class CreateProductBarcodeMapper {
    private static final  String ONLY_NUMBER_REGEX = "\\D";

    /**
     * Code to entity barcode.
     *
     * @param code the code
     * @return the barcode
     */
    public Barcode codeToEntity(String code) {
        if (code == null)
            return null;

        Barcode.Builder builder = Barcode.newBuilder();

        builder.code(code);
        builder.barcodeType(getBarCodeType(code));

        return builder.build();
    }

    /**
     * Entity to code string.
     *
     * @param barcode the barcode
     * @return the string
     */
    public String entityToCode(Barcode barcode) {
        if (barcode == null) {
            return null;
        }

        return barcode.getCode();
    }

    private BarcodeType getBarCodeType(String barcode) {
        switch (barcode.replaceAll(ONLY_NUMBER_REGEX, "").length()) {
            case 8:
                return EAN_8;
            case 12:
                return UPC_A;
            case 13:
                return EAN_13;
            default:
                return null;
        }
    }
}
