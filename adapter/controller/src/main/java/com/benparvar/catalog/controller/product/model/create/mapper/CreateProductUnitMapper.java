package com.benparvar.catalog.controller.product.model.create.mapper;

import com.benparvar.catalog.controller.product.model.create.request.CreateProductUnitRequest;
import com.benparvar.catalog.controller.product.model.create.response.CreateProductUnitResponse;
import com.benparvar.catalog.domain.entity.unit.Unit;

/**
 * The type Unit mapper.
 */
public class CreateProductUnitMapper {
    /**
     * Request to entity unit.
     *
     * @param request the request
     * @return the unit
     */
    public Unit requestToEntity(CreateProductUnitRequest request) {
        if (request == null)
            return null;

        Unit.Builder builder = Unit.newBuilder();

        builder.code(request.getCode());
        builder.name(request.getName());

        return builder.build();
    }

    /**
     * Entity to response unit response.
     *
     * @param entity the entity
     * @return the unit response
     */
    public CreateProductUnitResponse entityToResponse(Unit entity) {
        if (entity == null)
            return null;

        CreateProductUnitResponse response = new CreateProductUnitResponse();

        response.setCode(entity.getCode());
        response.setName(entity.getName());

        return response;
    }
}
