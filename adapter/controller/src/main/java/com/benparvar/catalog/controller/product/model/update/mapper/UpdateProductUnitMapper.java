package com.benparvar.catalog.controller.product.model.update.mapper;

import com.benparvar.catalog.controller.product.model.update.response.UpdateProductUnitResponse;
import com.benparvar.catalog.domain.entity.unit.Unit;

/**
 * The type Update product unit mapper.
 */
public class UpdateProductUnitMapper {

    /**
     * Entity to response find product unit response.
     *
     * @param entity the entity
     * @return the find product unit response
     */
    public UpdateProductUnitResponse entityToResponse(Unit entity) {
        if (entity == null)
            return null;

        UpdateProductUnitResponse response = new UpdateProductUnitResponse();

        response.setCode(entity.getCode());
        response.setName(entity.getName());

        return response;
    }
}
