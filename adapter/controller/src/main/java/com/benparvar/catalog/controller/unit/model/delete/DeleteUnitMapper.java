package com.benparvar.catalog.controller.unit.model.delete;

import com.benparvar.catalog.domain.entity.unit.Unit;

/**
 * The type Delete unit mapper.
 */
public class DeleteUnitMapper {
    /**
     * Request to entity unit.
     *
     * @param request the request
     * @return the unit
     */
    public Unit requestToEntity(DeleteUnitRequest request) {
        if (request == null)
            return null;

        Unit.Builder builder = Unit.newBuilder();

        builder.code(request.getCode());
        builder.name(request.getName());

        return builder.build();
    }
}
