package com.benparvar.catalog.controller.product.model.find.mapper;

import com.benparvar.catalog.controller.product.model.find.response.FindProductImageResponse;
import com.benparvar.catalog.domain.entity.image.Image;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The type Find product image mapper.
 */
public class FindProductImageMapper {

    /**
     * Entity to response find product image response.
     *
     * @param entity the entity
     * @return the find product image response
     */
    public FindProductImageResponse entityToResponse(Image entity) {
        if (entity == null)
            return null;

        FindProductImageResponse response = new FindProductImageResponse();

        response.setDescription(entity.getDescription());
        response.setUrl(entity.getUrl());

        return response;
    }

    /**
     * Entity to response list.
     *
     * @param entities the entities
     * @return the list
     */
    public List<FindProductImageResponse> entityToResponse(List<Image> entities) {
        if (entities == null)
            return Collections.emptyList();

        List<FindProductImageResponse> responses = new ArrayList<>();

        for (Image entity : entities) {
            responses.add(entityToResponse(entity));
        }

        return responses;
    }
}
