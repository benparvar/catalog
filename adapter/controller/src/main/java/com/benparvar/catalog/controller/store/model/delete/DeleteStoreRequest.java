package com.benparvar.catalog.controller.store.model.delete;

import java.io.Serializable;
import java.util.Objects;

/**
 * The type Create store request.
 */
public class DeleteStoreRequest implements Serializable {
    private String code;
    private String name;

    private DeleteStoreRequest(Builder builder) {
        code = builder.code;
        name = builder.name;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    /**
     * Gets code.
     *
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DeleteStoreRequest that = (DeleteStoreRequest) o;
        return code.equals(that.code) &&
                name.equals(that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(code, name);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("DeleteStoreRequest{");
        sb.append("code='").append(code).append('\'');
        sb.append(", name='").append(name).append('\'');
        sb.append('}');
        return sb.toString();
    }

    public static final class Builder {
        private String code;
        private String name;

        private Builder() {
        }

        public Builder code(String val) {
            code = val;
            return this;
        }

        public Builder name(String val) {
            name = val;
            return this;
        }

        public DeleteStoreRequest build() {
            return new DeleteStoreRequest(this);
        }
    }
}
