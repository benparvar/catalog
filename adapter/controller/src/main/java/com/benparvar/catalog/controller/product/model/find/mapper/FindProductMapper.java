package com.benparvar.catalog.controller.product.model.find.mapper;

import com.benparvar.catalog.controller.product.model.find.response.FindProductResponse;
import com.benparvar.catalog.domain.entity.product.Product;

/**
 * The type Find product mapper.
 */
public class FindProductMapper {
    private final FindProductUnitMapper unitMapper;
    private final FindProductStoreMapper storeMapper;
    private final FindProductImageMapper imageMapper;
    private final FindProductBarcodeMapper barcodeMapper;

    /**
     * Instantiates a new Find product mapper.
     *
     * @param unitMapper    the unit mapper
     * @param storeMapper   the store mapper
     * @param imageMapper   the image mapper
     * @param barcodeMapper the barcode mapper
     */
    public FindProductMapper(FindProductUnitMapper unitMapper, FindProductStoreMapper storeMapper,
                             FindProductImageMapper imageMapper, FindProductBarcodeMapper barcodeMapper) {
        this.unitMapper = unitMapper;
        this.storeMapper = storeMapper;
        this.imageMapper = imageMapper;
        this.barcodeMapper = barcodeMapper;
    }


    /**
     * Entity to response find product response.
     *
     * @param entity the entity
     * @return the find product response
     */
    public FindProductResponse entityToResponse(Product entity) {
        if (entity == null)
            return null;

        FindProductResponse response = new FindProductResponse();

        response.setCode(entity.getCode());
        response.setName(entity.getName());
        response.setBarcode(barcodeMapper.entityToCode(entity.getBarcode()));
        response.setDescription(entity.getDescription());
        response.setUnit(unitMapper.entityToResponse(entity.getUnit()));
        response.setPrice(entity.getPrice());
        response.setStore(storeMapper.entityToResponse(entity.getStore()));
        response.setImages(imageMapper.entityToResponse(entity.getImages()));

        return response;
    }

}
