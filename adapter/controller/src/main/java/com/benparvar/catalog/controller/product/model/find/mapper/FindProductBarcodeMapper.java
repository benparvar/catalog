package com.benparvar.catalog.controller.product.model.find.mapper;

import com.benparvar.catalog.domain.entity.barcode.Barcode;

/**
 * The type Find product barcode mapper.
 */
public class FindProductBarcodeMapper {
    /**
     * Entity to code string.
     *
     * @param barcode the barcode
     * @return the string
     */
    public String entityToCode(Barcode barcode) {
        if (barcode == null) {
            return null;
        }

        return barcode.getCode();
    }
}
