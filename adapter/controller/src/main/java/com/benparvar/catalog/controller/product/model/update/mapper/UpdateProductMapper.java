package com.benparvar.catalog.controller.product.model.update.mapper;

import com.benparvar.catalog.controller.product.model.create.request.UpdateProductPriceRequest;
import com.benparvar.catalog.controller.product.model.update.response.UpdateProductResponse;
import com.benparvar.catalog.domain.entity.product.Product;
import com.benparvar.catalog.domain.entity.product.UpdateProductPrice;

/**
 * The type Update product mapper.
 */
public class UpdateProductMapper {
    private final UpdateProductUnitMapper unitMapper;
    private final UpdateProductStoreMapper storeMapper;
    private final UpdateProductImageMapper imageMapper;
    private final UpdateProductBarcodeMapper barcodeMapper;

    /**
     * Instantiates a new Update product mapper.
     *
     * @param unitMapper    the unit mapper
     * @param storeMapper   the store mapper
     * @param imageMapper   the image mapper
     * @param barcodeMapper the barcode mapper
     */
    public UpdateProductMapper(UpdateProductUnitMapper unitMapper, UpdateProductStoreMapper storeMapper,
                               UpdateProductImageMapper imageMapper, UpdateProductBarcodeMapper barcodeMapper) {
        this.unitMapper = unitMapper;
        this.storeMapper = storeMapper;
        this.imageMapper = imageMapper;
        this.barcodeMapper = barcodeMapper;
    }

    /**
     * Entity to response update product response.
     *
     * @param entity the entity
     * @return the update product response
     */
    public UpdateProductResponse entityToResponse(Product entity) {
        if (entity == null)
            return null;

        UpdateProductResponse response = new UpdateProductResponse();

        response.setCode(entity.getCode());
        response.setName(entity.getName());
        response.setBarcode(barcodeMapper.entityToCode(entity.getBarcode()));
        response.setDescription(entity.getDescription());
        response.setUnit(unitMapper.entityToResponse(entity.getUnit()));
        response.setPrice(entity.getPrice());
        response.setStore(storeMapper.entityToResponse(entity.getStore()));
        response.setImages(imageMapper.entityToResponse(entity.getImages()));

        return response;
    }

    /**
     * Request to entity product.
     *
     * @param request the request
     * @return the product
     */
    public UpdateProductPrice requestToupdateProductPriceEntity(UpdateProductPriceRequest request) {
        if (request == null)
            return null;

        UpdateProductPrice.Builder builder = UpdateProductPrice.newBuilder();

        builder.barcode(request.getBarcode());
        builder.storeCode(request.getStoreCode());
        builder.price(request.getPrice());

        return builder.build();
    }
}
