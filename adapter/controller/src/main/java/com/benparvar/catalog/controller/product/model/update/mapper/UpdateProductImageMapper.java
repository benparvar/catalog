package com.benparvar.catalog.controller.product.model.update.mapper;

import com.benparvar.catalog.controller.product.model.update.response.UpdateProductImageResponse;
import com.benparvar.catalog.domain.entity.image.Image;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The type Update product image mapper.
 */
public class UpdateProductImageMapper {

    /**
     * Entity to response find product image response.
     *
     * @param entity the entity
     * @return the find product image response
     */
    public UpdateProductImageResponse entityToResponse(Image entity) {
        if (entity == null)
            return null;

        UpdateProductImageResponse response = new UpdateProductImageResponse();

        response.setDescription(entity.getDescription());
        response.setUrl(entity.getUrl());

        return response;
    }

    /**
     * Entity to response list.
     *
     * @param entities the entities
     * @return the list
     */
    public List<UpdateProductImageResponse> entityToResponse(List<Image> entities) {
        if (entities == null)
            return Collections.emptyList();

        List<UpdateProductImageResponse> responses = new ArrayList<>();

        for (Image entity : entities) {
            responses.add(entityToResponse(entity));
        }

        return responses;
    }
}
