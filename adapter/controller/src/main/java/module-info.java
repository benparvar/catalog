module com.benparvar.catalog.controller {
    exports com.benparvar.catalog.controller.unit;
    exports com.benparvar.catalog.controller.unit.model.create;
    exports com.benparvar.catalog.controller.unit.model.find;
    exports com.benparvar.catalog.controller.unit.model.update;
    exports com.benparvar.catalog.controller.unit.model.delete;
    exports com.benparvar.catalog.controller.store;
    exports com.benparvar.catalog.controller.store.model.create;
    exports com.benparvar.catalog.controller.store.model.find;
    exports com.benparvar.catalog.controller.store.model.update;
    exports com.benparvar.catalog.controller.store.model.delete;
    exports com.benparvar.catalog.controller.product;
    exports com.benparvar.catalog.controller.product.model.create.request;
    exports com.benparvar.catalog.controller.product.model.create.response;
    exports com.benparvar.catalog.controller.product.model.create.mapper;
    exports com.benparvar.catalog.controller.product.model.find.mapper;
    exports com.benparvar.catalog.controller.product.model.find.response;
    exports com.benparvar.catalog.controller.product.model.update.mapper;
    exports com.benparvar.catalog.controller.product.model.update.response;
    exports com.benparvar.catalog.controller.product.model.delete;

    requires com.benparvar.catalog.usecase;
    requires com.benparvar.catalog.domain;
    requires org.slf4j;
}
