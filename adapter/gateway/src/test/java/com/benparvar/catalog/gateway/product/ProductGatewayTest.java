package com.benparvar.catalog.gateway.product;

import com.benparvar.catalog.repository.barcode.model.BarcodeModelMapper;
import com.benparvar.catalog.repository.image.model.ImageModelMapper;
import com.benparvar.catalog.repository.product.ProductRepository;
import com.benparvar.catalog.repository.product.model.ProductModel;
import com.benparvar.catalog.repository.product.model.ProductModelMapper;
import com.benparvar.catalog.repository.store.StoreRepository;
import com.benparvar.catalog.repository.store.model.StoreModel;
import com.benparvar.catalog.repository.store.model.StoreModelMapper;
import com.benparvar.catalog.repository.unit.UnitRepository;
import com.benparvar.catalog.repository.unit.model.UnitModel;
import com.benparvar.catalog.repository.unit.model.UnitModelMapper;
import com.benparvar.catalog.domain.entity.barcode.Barcode;
import com.benparvar.catalog.domain.entity.barcode.BarcodeType;
import com.benparvar.catalog.domain.entity.image.Image;
import com.benparvar.catalog.domain.entity.product.Product;
import com.benparvar.catalog.domain.entity.product.UpdateProductPrice;
import com.benparvar.catalog.domain.entity.product.exception.ProductAlreadyExistException;
import com.benparvar.catalog.domain.entity.product.exception.ProductNotFoundException;
import com.benparvar.catalog.domain.entity.store.Store;
import com.benparvar.catalog.domain.entity.unit.Unit;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

/**
 * The type Product gateway test.
 */
class ProductGatewayTest {
    private final ProductRepository productRepository = mock(ProductRepository.class);
    private final StoreRepository storeRepository = mock(StoreRepository.class);
    private final UnitRepository unitRepository = mock(UnitRepository.class);
    private final StoreModelMapper storeModelMapper = new StoreModelMapper();
    private final BarcodeModelMapper barcodeModelMapper = new BarcodeModelMapper();
    private final UnitModelMapper unitModelMapper = new UnitModelMapper();
    private final ImageModelMapper imageModelMapper = new ImageModelMapper();
    private final ProductModelMapper productModelMapper = new ProductModelMapper(barcodeModelMapper, unitModelMapper,
            storeModelMapper, imageModelMapper);

    private ProductGateway ugw;

    /**
     * Init.
     */
    @BeforeEach
    public void init() {
        ugw = new ProductGateway(productRepository, productModelMapper, storeRepository, unitRepository);
    }

    /**
     * Execute save with a entity already existent will fail.
     */
    @Test
    public void executeSaveWithAEntityAlreadyExistentWillFail() {
        assertThrows(ProductAlreadyExistException.class, () -> {
            Product entity = Product.newBuilder()
                    .code("666")
                    .name("Coca Zero 350ml")
                    .description("Coca zero lata 350ml")
                    .barcode(Barcode.newBuilder()
                            .code("7894900700046")
                            .barcodeType(BarcodeType.EAN_13)
                            .build())
                    .unit(Unit.newBuilder()
                            .name("PIECE")
                            .code("PC")
                            .build())
                    .price(BigDecimal.valueOf(2.50))
                    .store(Store.newBuilder()
                            .name("Matrix")
                            .code("123")
                            .build())
                    .images(Arrays.asList(Image.newBuilder()
                            .url("https://cdn-cosmos.bluesoft.com.br/products/7894900700220/zhksxcua")
                            .description("principal")
                            .build()))
                    .build();
            ProductModel model = productModelMapper.entityToModel(entity);

            when(productRepository.findByCodeAndStoreCode(model.getCode(), model.getStore().getCode())).thenThrow(new ProductAlreadyExistException());

            try {
                ugw.save(entity);
            } catch (ProductAlreadyExistException e) {
                verify(productRepository, times(1)).findByCodeAndStoreCode(model.getCode(), model.getStore().getCode());
                verify(productRepository, times(0)).saveAndFlush(model);
                throw e;
            }

            fail("should throw an ProductAlreadyExistException");
        });
    }

    /**
     * Execute save with a entity non existent will success.
     */
    @Test
    public void executeSaveWithAEntityNonExistentWillSuccess() {
        Product entity = Product.newBuilder()
                .code("666")
                .name("Coca Zero 350ml")
                .description("Coca zero lata 350ml")
                .barcode(Barcode.newBuilder()
                        .code("7894900700046")
                        .barcodeType(BarcodeType.EAN_13)
                        .build())
                .unit(Unit.newBuilder()
                        .name("PIECE")
                        .code("PC")
                        .build())
                .price(BigDecimal.valueOf(2.50))
                .store(Store.newBuilder()
                        .name("Matrix")
                        .code("123")
                        .build())
                .images(Arrays.asList(Image.newBuilder()
                        .url("https://cdn-cosmos.bluesoft.com.br/products/7894900700220/zhksxcua")
                        .description("principal")
                        .build()))
                .build();

        StoreModel storeModel = new StoreModel();
        storeModel.setId(1L);
        storeModel.setCode("123");
        storeModel.setName("Matrix");

        UnitModel unitModel = new UnitModel();
        unitModel.setId(2L);
        unitModel.setCode("PC");
        unitModel.setName("PIECE");

        ProductModel productModel = productModelMapper.entityToModel(entity);
        productModel.setStore(storeModel);
        productModel.setUnit(unitModel);

        when(productRepository.findByCodeAndStoreCode(productModel.getCode(), productModel.getStore().getCode())).thenReturn(null);
        when(storeRepository.findByCode("123")).thenReturn(storeModel);
        when(unitRepository.findByCode("PC")).thenReturn(unitModel);
        when(productRepository.saveAndFlush(productModel)).thenReturn(productModel);

        Product result = ugw.save(entity);

        assertNotNull(result);
        assertEquals(entity, result);

        verify(productRepository, times(1)).findByCodeAndStoreCode(productModel.getCode(), productModel.getStore().getCode());
        verify(productRepository, times(1)).saveAndFlush(productModel);
    }

    /**
     * Execute save with a entity non existent will success.
     */
    @Test
    public void executeSaveWithAEntityUnitAndStoreNonExistentWillSuccess() {
        Product entity = Product.newBuilder()
                .code("666")
                .name("Coca Zero 350ml")
                .description("Coca zero lata 350ml")
                .barcode(Barcode.newBuilder()
                        .code("7894900700046")
                        .barcodeType(BarcodeType.EAN_13)
                        .build())
                .unit(Unit.newBuilder()
                        .name("PIECE")
                        .code("PC")
                        .build())
                .price(BigDecimal.valueOf(2.50))
                .store(Store.newBuilder()
                        .name("Matrix")
                        .code("123")
                        .build())
                .images(Arrays.asList(Image.newBuilder()
                        .url("https://cdn-cosmos.bluesoft.com.br/products/7894900700220/zhksxcua")
                        .description("principal")
                        .build()))
                .build();

        StoreModel storeModel = new StoreModel();
        storeModel.setId(1L);
        storeModel.setCode("123");
        storeModel.setName("Matrix");

        UnitModel unitModel = new UnitModel();
        unitModel.setId(2L);
        unitModel.setCode("PC");
        unitModel.setName("PIECE");

        ProductModel productModel = productModelMapper.entityToModel(entity);
        productModel.setStore(storeModel);
        productModel.setUnit(unitModel);

        when(productRepository.findByCodeAndStoreCode(productModel.getCode(), productModel.getStore().getCode())).thenReturn(null);
        when(storeRepository.findByCode("123")).thenReturn(null);
        when(storeRepository.saveAndFlush(any())).thenReturn(storeModel);
        when(unitRepository.findByCode("PC")).thenReturn(null);
        when(unitRepository.saveAndFlush(any())).thenReturn(unitModel);

        when(productRepository.saveAndFlush(productModel)).thenReturn(productModel);

        Product result = ugw.save(entity);

        assertNotNull(result);
        assertEquals(entity, result);

        verify(productRepository, times(1)).findByCodeAndStoreCode(productModel.getCode(), productModel.getStore().getCode());
        verify(productRepository, times(1)).saveAndFlush(productModel);
    }

    /**
     * Execute save with a null entity will success.
     */
    @Test
    public void executeSaveWithANullEntityWillSuccess() {
        Product entity = null;

        Product result = ugw.save(entity);

        assertNull(result);

        verify(productRepository, times(0)).findByCodeAndStoreCode(any(), any());
        verify(productRepository, times(0)).saveAndFlush(any());
    }

    /**
     * Execute find by barcode code and store code with non existent product will fail.
     */
    @Test
    public void executeFindByBarcodeCodeAndStoreCodeWithNonExistentProductWillFail() {
        assertThrows(ProductNotFoundException.class, () -> {
            final String barcodeCode = "7896022204242";
            final String storeCode = "45678";

            when(productRepository.findByBarcodeCodeAndStoreCode(barcodeCode, storeCode)).thenReturn(null);

            try {
                ugw.findByBarcodeCodeAndStoreCode(barcodeCode, storeCode);
            } catch (ProductNotFoundException e) {
                verify(productRepository, times(1)).findByBarcodeCodeAndStoreCode(barcodeCode, storeCode);
                throw e;
            }

            fail("should throw an ProductNotFoundException");
        });
    }

    /**
     * Execute find by barcode code and store code with existent product will success.
     */
    @Test
    public void executeFindByBarcodeCodeAndStoreCodeWithExistentProductWillSuccess() {
        final String barcodeCode = "7894900700046";
        final String storeCode = "123";

        Product entity = Product.newBuilder()
                .code("666")
                .name("Coca Zero 350ml")
                .description("Coca zero lata 350ml")
                .barcode(Barcode.newBuilder()
                        .code(barcodeCode)
                        .barcodeType(BarcodeType.EAN_13)
                        .build())
                .unit(Unit.newBuilder()
                        .name("PIECE")
                        .code("PC")
                        .build())
                .price(BigDecimal.valueOf(2.50))
                .store(Store.newBuilder()
                        .name("Matrix")
                        .code(storeCode)
                        .build())
                .images(Arrays.asList(Image.newBuilder()
                        .url("https://cdn-cosmos.bluesoft.com.br/products/7894900700220/zhksxcua")
                        .description("principal")
                        .build()))
                .build();

        StoreModel storeModel = new StoreModel();
        storeModel.setId(1L);
        storeModel.setCode(storeCode);
        storeModel.setName("Matrix");

        UnitModel unitModel = new UnitModel();
        unitModel.setId(2L);
        unitModel.setCode("PC");
        unitModel.setName("PIECE");

        ProductModel productModel = productModelMapper.entityToModel(entity);
        productModel.setStore(storeModel);
        productModel.setUnit(unitModel);

        when(productRepository.findByBarcodeCodeAndStoreCode(barcodeCode, storeCode)).thenReturn(productModel);

        Product result = ugw.findByBarcodeCodeAndStoreCode(barcodeCode, storeCode);

        assertNotNull(result);
        assertEquals(entity, result);

        verify(productRepository, times(1)).findByBarcodeCodeAndStoreCode(productModel.getBarcode().getCode(), productModel.getStore().getCode());
    }

    /**
     * Execute update price with a existent entity will success.
     */
    @Test
    public void executeUpdatePriceWithAExistentEntityWillSuccess() {
        final String barcodeCode = "7894900700046";
        final String storeCode = "123";
        final BigDecimal price = BigDecimal.valueOf(9.78);

        Product entity = Product.newBuilder()
                .code("666")
                .name("Coca Zero 350ml")
                .description("Coca zero lata 350ml")
                .barcode(Barcode.newBuilder()
                        .code(barcodeCode)
                        .barcodeType(BarcodeType.EAN_13)
                        .build())
                .unit(Unit.newBuilder()
                        .name("PIECE")
                        .code("PC")
                        .build())
                .price(BigDecimal.valueOf(2.50))
                .store(Store.newBuilder()
                        .name("Matrix")
                        .code(storeCode)
                        .build())
                .images(Arrays.asList(Image.newBuilder()
                        .url("https://cdn-cosmos.bluesoft.com.br/products/7894900700220/zhksxcua")
                        .description("principal")
                        .build()))
                .build();

        StoreModel storeModel = new StoreModel();
        storeModel.setId(1L);
        storeModel.setCode(storeCode);
        storeModel.setName("Matrix");

        UnitModel unitModel = new UnitModel();
        unitModel.setId(2L);
        unitModel.setCode("PC");
        unitModel.setName("PIECE");

        ProductModel productModelOld = productModelMapper.entityToModel(entity);
        productModelOld.setStore(storeModel);
        productModelOld.setUnit(unitModel);

        ProductModel productModelNew = productModelMapper.entityToModel(entity);
        productModelNew.setPrice(price);
        productModelNew.setStore(storeModel);
        productModelNew.setUnit(unitModel);

        UpdateProductPrice updateProductPrice = UpdateProductPrice.newBuilder()
                .barcode(barcodeCode)
                .storeCode(storeCode)
                .price(price)
                .build();

        when(productRepository.findByBarcodeCodeAndStoreCode(barcodeCode, storeCode)).thenReturn(productModelOld);
        when(productRepository.saveAndFlush(productModelNew)).thenReturn(productModelNew);

        Product result = ugw.updateProductPriceByBarcodeAndStoreCodeCode(updateProductPrice);

        assertNotNull(result);
        assertEquals(price, result.getPrice());

        verify(productRepository, times(1)).findByBarcodeCodeAndStoreCode(barcodeCode, storeCode);
        verify(productRepository, times(1)).saveAndFlush(productModelNew);
    }

    /**
     * Execute delete with a non existent object will fail.
     */
    @Test
    public void executeDeleteWithANonExistentObjectWillFail() {
        final String barcodeCode = "7896022204242";
        final String storeCode = "45678";

        Product entity = Product.newBuilder()
                .barcode(Barcode.newBuilder().code(barcodeCode).build())
                .store(Store.newBuilder().code(storeCode).build())
                .build();

        assertThrows(ProductNotFoundException.class, () -> {
            when(productRepository.findByBarcodeCodeAndStoreCode(barcodeCode, storeCode)).thenThrow(new ProductNotFoundException());
            try {
                ugw.delete(entity);
            } catch (ProductNotFoundException e) {
                verify(productRepository, times(0)).saveAndFlush(any());
                verify(productRepository, times(1)).findByBarcodeCodeAndStoreCode(barcodeCode, storeCode);
                throw e;
            }

            fail("should throw an ProductNotFound");
        });
    }

    /**
     * Execute delete with a existent object will success.
     */
    @Test
    public void executeDeleteWithAExistentObjectWillSuccess() {
        final String barcodeCode = "7896022204242";
        final String storeCode = "45678";

        Product entity = Product.newBuilder()
                .barcode(Barcode.newBuilder().code(barcodeCode).build())
                .store(Store.newBuilder().code(storeCode).build())
                .build();

        when(productRepository.findByBarcodeCodeAndStoreCode(barcodeCode, storeCode))
                .thenReturn(productModelMapper.entityToModel(entity));

        assertDoesNotThrow(() -> {
            ugw.delete(entity);
            verify(productRepository, times(1)).findByBarcodeCodeAndStoreCode(barcodeCode, storeCode);
        });
    }
}