package com.benparvar.catalog.gateway.store;

import com.benparvar.catalog.repository.store.StoreRepository;
import com.benparvar.catalog.repository.store.model.StoreModel;
import com.benparvar.catalog.repository.store.model.StoreModelMapper;
import com.benparvar.catalog.domain.entity.store.Store;
import com.benparvar.catalog.domain.entity.store.exception.StoreAlreadyExistException;
import com.benparvar.catalog.domain.entity.store.exception.StoreNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

/**
 * The type Store gateway test.
 */
class StoreGatewayTest {
    private final StoreRepository repository = mock(StoreRepository.class);
    private final StoreModelMapper storeModelMapper = new StoreModelMapper();
    private StoreGateway ugw;

    /**
     * Init.
     */
    @BeforeEach
    public void init() {
        ugw = new StoreGateway(repository, storeModelMapper);
    }

    /**
     * Execute save with a entity already existent will fail.
     */
    @Test
    public void executeSaveWithAEntityAlreadyExistentWillFail() {
        assertThrows(StoreAlreadyExistException.class, () -> {
            Store entity = Store.newBuilder().code("PC").name("PIECE").build();
            StoreModel model = storeModelMapper.entityToModel(entity);

            when(repository.findByCode(model.getCode())).thenThrow(new StoreAlreadyExistException());

            try {
                ugw.save(entity);
            } catch (StoreAlreadyExistException e) {
                verify(repository, times(1)).findByCode(model.getCode());
                verify(repository, times(0)).saveAndFlush(model);
                throw e;
            }

            fail("should throw an StoreAlreadyExistException");
        });
    }

    /**
     * Execute save with a entity non existent will success.
     */
    @Test
    public void executeSaveWithAEntityNonExistentWillSuccess() {
        Store entity = Store.newBuilder().code("PC").name("PIECE").build();
        StoreModel model = storeModelMapper.entityToModel(entity);

        when(repository.findByCode(model.getCode())).thenReturn(null);
        when(repository.saveAndFlush(model)).thenReturn(model);

        Store result = ugw.save(entity);

        assertNotNull(result);
        assertEquals(entity, result);

        verify(repository, times(1)).findByCode(model.getCode());
        verify(repository, times(1)).saveAndFlush(model);
    }

    /**
     * Execute save with a null entity will success.
     */
    @Test
    public void executeSaveWithANullEntityWillSuccess() {
        Store entity = null;

        Store result = ugw.save(entity);

        assertNull(result);

        verify(repository, times(0)).findByCode(any());
        verify(repository, times(0)).saveAndFlush(any());
    }

    /**
     * Execute find all without objects in repository will fail.
     */
    @Test
    public void executeFindAllWithoutObjectsInRepositoryWillFail() {
        when(repository.findAll()).thenReturn(null);

        assertThrows(StoreNotFoundException.class, () -> {
            try {
                ugw.findAll();
            } catch (StoreNotFoundException e) {
                verify(repository, times(1)).findAll();
                throw e;
            }

            fail("should throw an StoreNotFoundException");
        });
    }

    /**
     * Execute find all with objects in repository will return a list.
     */
    @Test
    public void executeFindAllWithObjectsInRepositoryWillReturnAList() {
        StoreModel model = new StoreModel();
        model.setCode("PC");
        model.setName("PIECE");

        when(repository.findAll()).thenReturn(Arrays.asList(model));

        List<Store> result = ugw.findAll();
        assertNotNull(result);
        assertFalse(result.isEmpty());
        assertEquals(1, result.size());
        assertEquals(model.getCode(), result.get(0).getCode());
        assertEquals(model.getName(), result.get(0).getName());
    }

    /**
     * Execute find by code without objects in repository will fail.
     */
    @Test
    public void executeFindByCodeWithoutObjectsInRepositoryWillFail() {
        String code = "MT";
        when(repository.findByCode(code)).thenReturn(null);

        assertThrows(StoreNotFoundException.class, () -> {
            try {
                ugw.findByCode(code);
            } catch (StoreNotFoundException e) {
                verify(repository, times(1)).findByCode(code);
                throw e;
            }

            fail("should throw an StoreNotFoundException");
        });
    }

    /**
     * Execute find by code with objects in repository will return a list.
     */
    @Test
    public void executeFindByCodeWithObjectsInRepositoryWillReturnAList() {
        String code = "PC";
        StoreModel model = new StoreModel();
        model.setCode(code);
        model.setName("PIECE");

        when(repository.findByCode(code)).thenReturn(model);

        Store result = ugw.findByCode(code);
        assertNotNull(result);
        assertEquals(model.getCode(), result.getCode());
        assertEquals(model.getName(), result.getName());
    }

    /**
     * Execute update with a non existent object will fail.
     */
    @Test
    public void executeUpdateWithANonExistentObjectWillFail() {
        String code = "AV";
        Store entity = Store.newBuilder().code(code).name("Avoided").build();

        when(repository.findByCode(code)).thenReturn(null);

        assertThrows(StoreNotFoundException.class, () -> {
            try {
                ugw.update(entity);
            } catch (StoreNotFoundException e) {
                verify(repository, times(1)).findByCode(code);
                throw e;
            }

            fail("should throw an StoreNotFound");
        });
    }

    /**
     * Execute update with a existent object will success.
     */
    @Test
    public void executeUpdateWithAExistentObjectWillSuccess() {
        String code = "AV";
        Store entity = Store.newBuilder().code(code).name("Avoided").build();

        StoreModel oldModel = new StoreModel();
        oldModel.setCode(code);
        oldModel.setName("Soup");

        StoreModel newModel = new StoreModel();
        newModel.setCode(code);
        newModel.setName("Avoided");

        when(repository.findByCode(code)).thenReturn(oldModel);
        when(repository.saveAndFlush(newModel)).thenReturn(newModel);

        Store result = ugw.update(entity);
        assertNotNull(result);
        assertEquals(newModel.getCode(), result.getCode());
        assertEquals(newModel.getName(), result.getName());

        verify(repository, times(1)).findByCode(code);
        verify(repository, times(1)).saveAndFlush(newModel);
    }

    /**
     * Execute delete with a non existent object will fail.
     */
    @Test
    public void executeDeleteWithANonExistentObjectWillFail() {
        String code = "AV";
        Store entity = Store.newBuilder().code(code).name("Avoided").build();

        assertThrows(StoreNotFoundException.class, () -> {

            when(repository.findByCode(code)).thenThrow(new StoreNotFoundException());
            try {
                ugw.delete(entity);
            } catch (StoreNotFoundException e) {
                verify(repository, times(0)).saveAndFlush(any());
                verify(repository, times(1)).findByCode(code);
                throw e;
            }

            fail("should throw an StoreNotFound");
        });
    }

    /**
     * Execute delete with a existent object will success.
     */
    @Test
    public void executeDeleteWithAExistentObjectWillSuccess() {
        String code = "AV";
        Store entity = Store.newBuilder().code(code).name("Avoided").build();

        when(repository.findByCode(code)).thenReturn(storeModelMapper.entityToModel(entity));

        assertDoesNotThrow(() -> {
            ugw.delete(entity);
            verify(repository, times(1)).findByCode(code);
        });
    }
}