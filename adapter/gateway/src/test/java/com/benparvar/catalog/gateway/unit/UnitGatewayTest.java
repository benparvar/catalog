package com.benparvar.catalog.gateway.unit;

import com.benparvar.catalog.repository.unit.UnitRepository;
import com.benparvar.catalog.repository.unit.model.UnitModel;
import com.benparvar.catalog.repository.unit.model.UnitModelMapper;
import com.benparvar.catalog.domain.entity.unit.Unit;
import com.benparvar.catalog.domain.entity.unit.exception.UnitAlreadyExistException;
import com.benparvar.catalog.domain.entity.unit.exception.UnitNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

/**
 * The type Unit gateway test.
 */
class UnitGatewayTest {
    private final UnitRepository repository = mock(UnitRepository.class);
    private final UnitModelMapper unitModelMapper = new UnitModelMapper();
    private UnitGateway ugw;

    /**
     * Init.
     */
    @BeforeEach
    public void init() {
        ugw = new UnitGateway(repository, unitModelMapper);
    }

    /**
     * Execute save with a entity already existent will fail.
     */
    @Test
    public void executeSaveWithAEntityAlreadyExistentWillFail() {
        assertThrows(UnitAlreadyExistException.class, () -> {
            Unit entity = Unit.newBuilder().code("PC").name("PIECE").build();
            UnitModel model = unitModelMapper.entityToModel(entity);

            when(repository.findByCode(model.getCode())).thenThrow(new UnitAlreadyExistException());

            try {
                ugw.save(entity);
            } catch (UnitAlreadyExistException e) {
                verify(repository, times(1)).findByCode(model.getCode());
                verify(repository, times(0)).saveAndFlush(model);
                throw e;
            }

            fail("should throw an UnitAlreadyExistException");
        });
    }

    /**
     * Execute save with a entity non existent will success.
     */
    @Test
    public void executeSaveWithAEntityNonExistentWillSuccess() {
        Unit entity = Unit.newBuilder().code("PC").name("PIECE").build();
        UnitModel model = unitModelMapper.entityToModel(entity);

        when(repository.findByCode(model.getCode())).thenReturn(null);
        when(repository.saveAndFlush(model)).thenReturn(model);

        Unit result = ugw.save(entity);

        assertNotNull(result);
        assertEquals(entity, result);

        verify(repository, times(1)).findByCode(model.getCode());
        verify(repository, times(1)).saveAndFlush(model);
    }

    /**
     * Execute save with a null entity will success.
     */
    @Test
    public void executeSaveWithANullEntityWillSuccess() {
        Unit entity = null;

        Unit result = ugw.save(entity);

        assertNull(result);

        verify(repository, times(0)).findByCode(any());
        verify(repository, times(0)).saveAndFlush(any());
    }

    /**
     * Execute find all without objects in repository will fail.
     */
    @Test
    public void executeFindAllWithoutObjectsInRepositoryWillFail() {
        when(repository.findAll()).thenReturn(null);

        assertThrows(UnitNotFoundException.class, () -> {
            try {
                ugw.findAll();
            } catch (UnitNotFoundException e) {
                verify(repository, times(1)).findAll();
                throw e;
            }

            fail("should throw an UnitNotFoundException");
        });
    }

    /**
     * Execute find all with objects in repository will return a list.
     */
    @Test
    public void executeFindAllWithObjectsInRepositoryWillReturnAList() {
        UnitModel model = new UnitModel();
        model.setCode("PC");
        model.setName("PIECE");

        when(repository.findAll()).thenReturn(Arrays.asList(model));

        List<Unit> result = ugw.findAll();
        assertNotNull(result);
        assertFalse(result.isEmpty());
        assertEquals(1, result.size());
        assertEquals(model.getCode(), result.get(0).getCode());
        assertEquals(model.getName(), result.get(0).getName());
    }

    /**
     * Execute find by code without objects in repository will fail.
     */
    @Test
    public void executeFindByCodeWithoutObjectsInRepositoryWillFail() {
        String code = "MT";
        when(repository.findByCode(code)).thenReturn(null);

        assertThrows(UnitNotFoundException.class, () -> {
            try {
                ugw.findByCode(code);
            } catch (UnitNotFoundException e) {
                verify(repository, times(1)).findByCode(code);
                throw e;
            }

            fail("should throw an UnitNotFoundException");
        });
    }

    /**
     * Execute find by code with objects in repository will return a list.
     */
    @Test
    public void executeFindByCodeWithObjectsInRepositoryWillReturnAList() {
        String code = "PC";
        UnitModel model = new UnitModel();
        model.setCode(code);
        model.setName("PIECE");

        when(repository.findByCode(code)).thenReturn(model);

        Unit result = ugw.findByCode(code);
        assertNotNull(result);
        assertEquals(model.getCode(), result.getCode());
        assertEquals(model.getName(), result.getName());
    }

    /**
     * Execute update with a non existent object will fail.
     */
    @Test
    public void executeUpdateWithANonExistentObjectWillFail() {
        String code = "AV";
        Unit entity = Unit.newBuilder().code(code).name("Avoided").build();

        when(repository.findByCode(code)).thenReturn(null);

        assertThrows(UnitNotFoundException.class, () -> {
            try {
                ugw.update(entity);
            } catch (UnitNotFoundException e) {
                verify(repository, times(1)).findByCode(code);
                throw e;
            }

            fail("should throw an UnitNotFound");
        });
    }

    /**
     * Execute update with a existent object will success.
     */
    @Test
    public void executeUpdateWithAExistentObjectWillSuccess() {
        String code = "AV";
        Unit entity = Unit.newBuilder().code(code).name("Avoided").build();

        UnitModel oldModel = new UnitModel();
        oldModel.setCode(code);
        oldModel.setName("Soup");

        UnitModel newModel = new UnitModel();
        newModel.setCode(code);
        newModel.setName("Avoided");

        when(repository.findByCode(code)).thenReturn(oldModel);
        when(repository.saveAndFlush(newModel)).thenReturn(newModel);

        Unit result = ugw.update(entity);
        assertNotNull(result);
        assertEquals(newModel.getCode(), result.getCode());
        assertEquals(newModel.getName(), result.getName());

        verify(repository, times(1)).findByCode(code);
        verify(repository, times(1)).saveAndFlush(newModel);
    }

    /**
     * Execute delete with a non existent object will fail.
     */
    @Test
    public void executeDeleteWithANonExistentObjectWillFail() {
        String code = "AV";
        Unit entity = Unit.newBuilder().code(code).name("Avoided").build();

        assertThrows(UnitNotFoundException.class, () -> {

            when(repository.findByCode(code)).thenThrow(new UnitNotFoundException());
            try {
                ugw.delete(entity);
            } catch (UnitNotFoundException e) {
                verify(repository, times(0)).saveAndFlush(any());
                verify(repository, times(1)).findByCode(code);
                throw e;
            }

            fail("should throw an UnitNotFound");
        });
    }

    /**
     * Execute delete with a existent object will success.
     */
    @Test
    public void executeDeleteWithAExistentObjectWillSuccess() {
        String code = "AV";
        Unit entity = Unit.newBuilder().code(code).name("Avoided").build();

        when(repository.findByCode(code)).thenReturn(unitModelMapper.entityToModel(entity));

        assertDoesNotThrow(() -> {
            ugw.delete(entity);
            verify(repository, times(1)).findByCode(code);
        });
    }
}