package com.benparvar.catalog.gateway.unit;

import com.benparvar.catalog.repository.unit.UnitRepository;
import com.benparvar.catalog.repository.unit.model.UnitModel;
import com.benparvar.catalog.repository.unit.model.UnitModelMapper;
import com.benparvar.catalog.domain.entity.unit.Unit;
import com.benparvar.catalog.domain.entity.unit.exception.UnitAlreadyExistException;
import com.benparvar.catalog.domain.entity.unit.exception.UnitNotFoundException;
import com.benparvar.catalog.domain.entity.validator.CollectionUtils;
import com.benparvar.catalog.usecase.unit.create.SaveUnit;
import com.benparvar.catalog.usecase.unit.delete.DeleteUnit;
import com.benparvar.catalog.usecase.unit.find.all.FindAllUnits;
import com.benparvar.catalog.usecase.unit.find.code.FindUnitByCode;
import com.benparvar.catalog.usecase.unit.update.UpdateUnit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * The type Unit gateway.
 */
public class UnitGateway implements SaveUnit, FindAllUnits, FindUnitByCode, UpdateUnit, DeleteUnit {
    private final Logger log = LoggerFactory.getLogger(UnitGateway.class);
    private final UnitRepository repository;
    private final UnitModelMapper unitModelMapper;

    /**
     * Instantiates a new Unit gateway.
     *
     * @param repository        the repository
     * @param unitModelMapper the unit mapper
     */
    public UnitGateway(UnitRepository repository, UnitModelMapper unitModelMapper) {
        this.repository = repository;
        this.unitModelMapper = unitModelMapper;
    }

    @Override
    public Unit save(Unit entity) {
        log.info("saving: {}", entity);

        if (null == entity) {
            return null;
        }

        if (null != repository.findByCode(entity.getCode()))
            throw new UnitAlreadyExistException();

        UnitModel model = repository.saveAndFlush(unitModelMapper.entityToModel(entity));
        log.info("saved: {}", model);

        return unitModelMapper.modelToEntity(model);
    }

    @Override
    public List<Unit> findAll() {
        log.info("finding All:");

        List<UnitModel> models = repository.findAll();
        if (CollectionUtils.isEmpty(models))
            throw new UnitNotFoundException();

        return unitModelMapper.modelToEntity(models);
    }

    @Override
    public Unit findByCode(String code) {
        log.info("finding by code: {}", code);

        UnitModel model = repository.findByCode(code);
        if (null == model)
            throw new UnitNotFoundException();

        return unitModelMapper.modelToEntity(model);
    }

    @Override
    public void delete(Unit entity) {
        log.info("deleting: {}", entity);

        UnitModel model = repository.findByCode(entity.getCode());
        if (null == model)
            throw new UnitNotFoundException();

        repository.delete(model);
    }

    @Override
    public Unit update(Unit entity) {
        log.info("updating: {}", entity);

        if (null == entity)
            return null;

        UnitModel model = repository.findByCode(entity.getCode());
        if (null == model)
            throw new UnitNotFoundException();

        model.setName(entity.getName());

        UnitModel updatedModel = repository.saveAndFlush(model);
        log.info("updated: {}", updatedModel);

        return unitModelMapper.modelToEntity(updatedModel);
    }
}
