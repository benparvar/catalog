package com.benparvar.catalog.gateway.product;

import com.benparvar.catalog.repository.product.ProductRepository;
import com.benparvar.catalog.repository.product.model.ProductModel;
import com.benparvar.catalog.repository.product.model.ProductModelMapper;
import com.benparvar.catalog.repository.store.StoreRepository;
import com.benparvar.catalog.repository.store.model.StoreModel;
import com.benparvar.catalog.repository.unit.UnitRepository;
import com.benparvar.catalog.repository.unit.model.UnitModel;
import com.benparvar.catalog.domain.entity.product.Product;
import com.benparvar.catalog.domain.entity.product.UpdateProductPrice;
import com.benparvar.catalog.domain.entity.product.exception.ProductAlreadyExistException;
import com.benparvar.catalog.domain.entity.product.exception.ProductNotFoundException;
import com.benparvar.catalog.usecase.product.create.SaveProduct;
import com.benparvar.catalog.usecase.product.delete.DeleteProduct;
import com.benparvar.catalog.usecase.product.find.FindProductByBarcodeAndStoreCode;
import com.benparvar.catalog.usecase.product.update.UpdateProductPriceByBarcodeAndStoreCodeCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The type Product gateway.
 */
public class ProductGateway implements SaveProduct, FindProductByBarcodeAndStoreCode,
        UpdateProductPriceByBarcodeAndStoreCodeCode, DeleteProduct {
    private final Logger log = LoggerFactory.getLogger(ProductGateway.class);
    private final ProductRepository productRepository;
    private final ProductModelMapper productModelMapper;
    private final StoreRepository storeRepository;
    private final UnitRepository unitRepository;

    /**
     * Instantiates a new Product gateway.
     *
     * @param repository           the repository
     * @param productModelMapper the product model mapper
     * @param storeRepository      the store repository
     * @param unitRepository       the unit repository
     */
    public ProductGateway(ProductRepository repository, ProductModelMapper productModelMapper,
                          StoreRepository storeRepository, UnitRepository unitRepository) {
        this.productRepository = repository;
        this.productModelMapper = productModelMapper;
        this.storeRepository = storeRepository;
        this.unitRepository = unitRepository;
    }

    @Override
    public Product save(Product entity) {
        log.info("saving: {}", entity);

        if (null == entity) {
            return null;
        }

        if (null != productRepository.findByCodeAndStoreCode(entity.getCode(),
                entity.getStore().getCode()))
            throw new ProductAlreadyExistException();

        // Product
        ProductModel productModel = productModelMapper.entityToModel(entity);

        // Store
        StoreModel storeModel = storeRepository.findByCode(productModel.getStore().getCode());
        if (null != storeModel)
            productModel.setStore(storeModel);
        else
            productModel.setStore(storeRepository.saveAndFlush(productModel.getStore()));


        // Unit
        UnitModel unitModel = unitRepository.findByCode(productModel.getUnit().getCode());
        if (null != unitModel)
            productModel.setUnit(unitModel);
        else
            productModel.setUnit(unitRepository.saveAndFlush(productModel.getUnit()));

        productModel = productRepository.saveAndFlush(productModel);
        log.info("saved: {}", productModel);

        return productModelMapper.modelToEntity(productModel);
    }

    @Override
    public Product findByBarcodeCodeAndStoreCode(String barcodeCode, String storeCode) {
        log.info("findByBarcodeCodeAndStoreCode barcodeCode: {} storeCode: {}", barcodeCode, storeCode);

        ProductModel productModel = productRepository.findByBarcodeCodeAndStoreCode(barcodeCode, storeCode);
        log.info("returned: {}", productModel);

        if (null == productModel)
            throw new ProductNotFoundException();

        return productModelMapper.modelToEntity(productModel);
    }

    @Override
    public Product updateProductPriceByBarcodeAndStoreCodeCode(UpdateProductPrice updateProductPrice) {
        log.info("updateProductPriceByBarcodeAndStoreCodeCode updateProductPrice: {}", updateProductPrice);

        ProductModel productModel = productRepository.findByBarcodeCodeAndStoreCode(updateProductPrice.getBarcode(), updateProductPrice.getStoreCode());
        log.info("returned to update: {}", productModel);

        if (null != productModel) {
            productModel.setPrice(updateProductPrice.getPrice());

            productModel = productRepository.saveAndFlush(productModel);
            log.info("updated: {}", productModel);
        }

        return productModelMapper.modelToEntity(productModel);
    }

    @Override
    public void delete(Product entity) {
        log.info("deleting: {}", entity);

        ProductModel model = productRepository.findByBarcodeCodeAndStoreCode(entity.getBarcode().getCode(),
                entity.getStore().getCode());
        if (null == model)
            throw new ProductNotFoundException();

        productRepository.delete(model);
    }
}
