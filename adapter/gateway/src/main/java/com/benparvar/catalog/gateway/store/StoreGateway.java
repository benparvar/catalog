package com.benparvar.catalog.gateway.store;

import com.benparvar.catalog.repository.store.StoreRepository;
import com.benparvar.catalog.repository.store.model.StoreModel;
import com.benparvar.catalog.repository.store.model.StoreModelMapper;
import com.benparvar.catalog.domain.entity.store.Store;
import com.benparvar.catalog.domain.entity.store.exception.StoreAlreadyExistException;
import com.benparvar.catalog.domain.entity.store.exception.StoreNotFoundException;
import com.benparvar.catalog.domain.entity.validator.CollectionUtils;
import com.benparvar.catalog.usecase.store.create.SaveStore;
import com.benparvar.catalog.usecase.store.delete.DeleteStore;
import com.benparvar.catalog.usecase.store.find.all.FindAllStores;
import com.benparvar.catalog.usecase.store.find.code.FindStoreByCode;
import com.benparvar.catalog.usecase.store.update.UpdateStore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * The type Store gateway.
 */
public class StoreGateway implements SaveStore, FindAllStores, FindStoreByCode, UpdateStore, DeleteStore {
    private final Logger log = LoggerFactory.getLogger(StoreGateway.class);
    private final StoreRepository repository;
    private final StoreModelMapper storeModelMapper;

    /**
     * Instantiates a new Store gateway.
     *
     * @param repository        the repository
     * @param storeModelMapper the store mapper
     */
    public StoreGateway(StoreRepository repository, StoreModelMapper storeModelMapper) {
        this.repository = repository;
        this.storeModelMapper = storeModelMapper;
    }

    @Override
    public Store save(Store entity) {
        log.info("saving: {}", entity);

        if (null == entity) {
            return null;
        }

        if (null != repository.findByCode(entity.getCode()))
            throw new StoreAlreadyExistException();

        StoreModel model = repository.saveAndFlush(storeModelMapper.entityToModel(entity));
        log.info("saved: {}", model);

        return storeModelMapper.modelToEntity(model);
    }

    @Override
    public List<Store> findAll() {
        log.info("finding All:");

        List<StoreModel> models = repository.findAll();
        if (CollectionUtils.isEmpty(models))
            throw new StoreNotFoundException();

        return storeModelMapper.modelToEntity(models);
    }

    @Override
    public Store findByCode(String code) {
        log.info("finding by code: {}", code);

        StoreModel model = repository.findByCode(code);
        if (null == model)
            throw new StoreNotFoundException();

        return storeModelMapper.modelToEntity(model);
    }

    @Override
    public void delete(Store entity) {
        log.info("deleting: {}", entity);

        StoreModel model = repository.findByCode(entity.getCode());
        if (null == model)
            throw new StoreNotFoundException();

        repository.delete(model);
    }

    @Override
    public Store update(Store entity) {
        log.info("updating: {}", entity);

        if (null == entity)
            return null;

        StoreModel model = repository.findByCode(entity.getCode());
        if (null == model)
            throw new StoreNotFoundException();

        model.setName(entity.getName());

        StoreModel updatedModel = repository.saveAndFlush(model);
        log.info("updated: {}", updatedModel);

        return storeModelMapper.modelToEntity(updatedModel);
    }
}
