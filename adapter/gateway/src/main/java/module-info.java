module com.benparvar.catalog.gateway {
    exports com.benparvar.catalog.gateway.unit;
    exports com.benparvar.catalog.gateway.store;
    exports com.benparvar.catalog.gateway.product;

    requires org.slf4j;
    requires com.benparvar.catalog.domain;
    requires com.benparvar.catalog.repository;
    requires com.benparvar.catalog.usecase;
    requires spring.data.jpa;
}