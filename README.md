# Catalog

A service that provides a catalog API

## Requirements
Java SE 11

Gradle 5

## Frameworks
SpringBoot

Swagger

Mockito

Junit 5

Jacoco

Pitest


## Database
H2 "in memorian" database

In dev mode we can acess the h2 console using http://localhost:9876/h2
JDBC URL: jdbc:h2:./catalog_db
User Name: sa
Password: 

## Building Project
gradlew clean build

## Running Spring Project
`java -jar application/spring-app/build/libs/spring-app-1.0.0.jar`

## Running on IDE
gradlew bootRun

## Unit Test
gradlew test

## Mutation Test
gradlew pitest

## Javadoc
gradlew javadoc

## Performance tests
Put jmx files in "src/test/jmeter/" folder.

### Launching GUI
gradle jmGui

### Running tests
gradle jmRun

### Creating report
gradle jmReport

## Docker
Generating the image gradlew clean build buildDocker

Running the image docker run -p 9876:9876 benparvar/catalog:1.1.2

or 

cd /application/spring/src/main/resources
sudo docker-compose -f docker-compose.yml up


## Run Application
gradlew -Penv="[ENV]" bootRun

ENV: "dev", "qa", "staging" or "prod"

Default is "dev" (if "env" parameter is not defined)

## Performance tests
See http://jmeter.foragerr.net/
To generate random cpf use `“${__RandomString(11,0123456789)}”`
To generate random name use `“${__RandomString(12,aldkjfhalskdfalkdfhalkdjfhaldjfhaldsjfhaldjfh)}”`

## E2E tests native
Arguments: -Pe2eNativeTests -Dhostname=http://localhost -Dport=9876

## E2E tests cucumber
Arguments: -Pe2eCucumberTests -Dhostname=http://localhost -Dport=9876

## REST Documentation

#### Units
 - url:
{{protocol}}://{{server}}:{{port}}/api/v1/units

 - method:
GET

 - header:
Accept:application/json
Content-Type:application/json 

 - body:
{"code": "${code}", "name": "${name}"}
  
#### Stores
 - url:
{{protocol}}://{{server}}:{{port}}/api/v1/stores

 - method:
GET

 - header:
Accept:application/json
Content-Type:application/json 

 - body:
{"code": "${code}", "name": "${name}"}

#### Products
- url:
{{protocol}}://{{server}}:{{port}}/api/v1/products

 - method:
POST

 - header:
Accept:application/json
Content-Type:application/json 

 - body:
{
	"code": "${product_code}", 
	"name": "${product_name}",
	"description": "${product_description}",
	"barcode": "${product_barcode}",
	"unit": {
		"code": "${unit_code}",
		"name": "${unit_name}"
	},
	"price": ${product_price},
	"store": {
		"code": "${store_code}",
		"name": "${store_name}"
	},
	"images": [{
			"description": "${image_description}",
			"url": "${image_url}"
		}]
}


#### TODO
- rest documentation
- security
